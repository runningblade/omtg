#ifndef COMPOSITE_SHAPE_EXACT_H
#define COMPOSITE_SHAPE_EXACT_H

#include "ShapeExact.h"
#include "BBoxExact.h"

namespace PHYSICSMOTION {
struct CompositeShapeExact : public ShapeExact {
  CompositeShapeExact();
  CompositeShapeExact(const std::vector<std::shared_ptr<ShapeExact>>& geoms,
                      const std::vector<Mat3X4T,Eigen::aligned_allocator<Mat3X4T>>& trans);
  CompositeShapeExact(const std::vector<std::shared_ptr<ShapeExact>>& geoms);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual const BBoxExact& getBB() const override;
  virtual bool empty() const override;
  virtual void getMesh(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
                       std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss) const override;
  virtual bool closestInner(const Vec3T& pt,Vec3T& n,Vec3T& normal,Mat3T& hessian,
                            T& rad,Eigen::Matrix<int,2,1>& feat,bool cache=false,
                            std::vector<Vec3T,Eigen::aligned_allocator<Vec3T>>* history=NULL) const override;
  void scale(T coef) override;
  const std::vector<std::shared_ptr<ShapeExact>>& getGeoms() const;
  const std::vector<Mat3X4T,Eigen::aligned_allocator<Mat3X4T>>& getTrans() const;
 protected:
  std::vector<std::shared_ptr<ShapeExact>> _geoms;
  std::vector<Mat3X4T,Eigen::aligned_allocator<Mat3X4T>> _trans;
  BBoxExact _bb;
};
}

#endif
