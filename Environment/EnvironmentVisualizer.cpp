#include "EnvironmentVisualizer.h"
#include "MeshExact.h"
#include "ConvexHullExact.h"
#include "SphericalBBoxExact.h"
#include "CompositeShapeExact.h"
#include "TriangleExact.h"
#include "Environment.h"
#include "BBoxExact.h"

#include <TinyVisualizer/Bullet3DShape.h>
#include <TinyVisualizer/MakeMesh.h>
#include <Utils/Epsilon.h>

namespace PHYSICSMOTION {
std::shared_ptr<DRAWER::Shape> visualizeTriangleExact(std::shared_ptr<TriangleExact> et,bool wire) {
  using namespace DRAWER;
  std::shared_ptr<MeshShape> tri(new MeshShape);
  tri->addVertex(et->v(0).cast<GLfloat>());
  tri->addVertex(et->v(1).cast<GLfloat>());
  tri->addVertex(et->v(2).cast<GLfloat>());
  if(wire) {
    tri->addIndex(Eigen::Matrix<int,2,1>(0,1));
    tri->addIndex(Eigen::Matrix<int,2,1>(0,2));
    tri->addIndex(Eigen::Matrix<int,2,1>(1,2));
    tri->setMode(GL_LINES);
  } else {
    tri->addIndex(Eigen::Matrix<int,3,1>(0,1,2));
    tri->setMode(GL_TRIANGLES);
  }
  return tri;
}
std::shared_ptr<DRAWER::Shape> visualizeMeshExact(std::shared_ptr<MeshExact> m,bool wire) {
  using namespace DRAWER;
  std::shared_ptr<MeshShape> tri(new MeshShape);
  for(int i=0; i<(int)m->vss().size(); i++)
    tri->addVertex(m->vss()[i].cast<GLfloat>());
  if(wire) {
    for(int i=0; i<(int)m->iss().size(); i++) {
      tri->addIndex(Eigen::Matrix<int,2,1>(m->iss()[i][0],m->iss()[i][1]));
      tri->addIndex(Eigen::Matrix<int,2,1>(m->iss()[i][1],m->iss()[i][2]));
      tri->addIndex(Eigen::Matrix<int,2,1>(m->iss()[i][2],m->iss()[i][0]));
    }
    tri->setMode(GL_LINES);
  } else {
    for(int i=0; i<(int)m->iss().size(); i++)
      tri->addIndex(m->iss()[i]);
    tri->setMode(GL_TRIANGLES);
  }
  if(!wire)
    tri->computeNormals();
  return tri;
}
std::shared_ptr<DRAWER::Shape> visualizeBBoxExact(std::shared_ptr<BBoxExact> m,bool wire) {
  using namespace DRAWER;
  std::shared_ptr<Bullet3DShape> shape(new Bullet3DShape);
  std::shared_ptr<MeshShape> box=makeBox(1,!wire,(m->maxCorner()-m->minCorner()).template cast<GLfloat>()/2);
  shape->setLocalTranslate((m->maxCorner()+m->minCorner()).template cast<GLfloat>()/2);
  shape->addShape(box);
  return shape;
}
std::shared_ptr<DRAWER::Shape> visualizeSphericalBBoxExact(std::shared_ptr<SphericalBBoxExact> m,bool wire) {
  using namespace DRAWER;
  std::shared_ptr<Bullet3DShape> shape(new Bullet3DShape);
  std::shared_ptr<MeshShape> sbox=makeSphericalBox(8,!wire,(GLfloat)m->radius(),(m->maxCorner()-m->minCorner()).template cast<GLfloat>()/2);
  shape->setLocalTranslate((m->maxCorner()+m->minCorner()).template cast<GLfloat>()/2);
  shape->addShape(sbox);
  return shape;
}
std::shared_ptr<DRAWER::Shape> visualizeShapeExactGetMesh(std::shared_ptr<ShapeExact> m,bool wire) {
  using namespace DRAWER;
  std::shared_ptr<MeshShape> tri(new MeshShape);
  std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>> vss;
  std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>> iss;
  m->getMesh(vss,iss);
  for(int i=0; i<(int)vss.size(); i++)
    tri->addVertex(vss[i].cast<GLfloat>());
  if(wire) {
    for(int i=0; i<(int)iss.size(); i++) {
      tri->addIndex(Eigen::Matrix<int,2,1>(iss[i][0],iss[i][1]));
      tri->addIndex(Eigen::Matrix<int,2,1>(iss[i][1],iss[i][2]));
      tri->addIndex(Eigen::Matrix<int,2,1>(iss[i][2],iss[i][0]));
    }
    tri->setMode(GL_LINES);
  } else {
    for(int i=0; i<(int)iss.size(); i++)
      tri->addIndex(iss[i]);
    tri->setMode(GL_TRIANGLES);
  }
  return tri;
}
std::shared_ptr<DRAWER::Shape> visualizeShapeExact(std::shared_ptr<ShapeExact> m,bool wire) {
  using namespace DRAWER;
  if(std::dynamic_pointer_cast<MeshExact>(m))
    return visualizeMeshExact(std::dynamic_pointer_cast<MeshExact>(m),wire);
  else if(std::dynamic_pointer_cast<SphericalBBoxExact>(m))
    return visualizeSphericalBBoxExact(std::dynamic_pointer_cast<SphericalBBoxExact>(m),wire);
  else if(std::dynamic_pointer_cast<BBoxExact>(m))
    return visualizeBBoxExact(std::dynamic_pointer_cast<BBoxExact>(m),wire);
  else if(std::dynamic_pointer_cast<CompositeShapeExact>(m)) {
    Eigen::Matrix<GLfloat,4,4> localT;
    localT.setIdentity();
    std::shared_ptr<Bullet3DShape> shape(new Bullet3DShape);
    const std::vector<std::shared_ptr<ShapeExact>>& geoms=std::dynamic_pointer_cast<CompositeShapeExact>(m)->getGeoms();
    const std::vector<CompositeShapeExact::Mat3X4T,Eigen::aligned_allocator<CompositeShapeExact::Mat3X4T>>& trans=std::dynamic_pointer_cast<CompositeShapeExact>(m)->getTrans();
    for(int i=0; i<(int)geoms.size(); i++) {
      std::shared_ptr<Bullet3DShape> shapeI(new Bullet3DShape);
      shapeI->addShape(visualizeShapeExact(geoms[i],wire));
      localT.block<3,4>(0,0)=trans[i].template cast<GLfloat>();
      shapeI->setLocalTransform(localT);
      shape->addShape(shapeI);
    }
    return shape;
  } else return NULL;
}
}
