#include "BVHNode.h"
#include "BBoxExact.h"
#include <Utils/Utils.h>
#include <Utils/Heap.h>
#include <Utils/IO.h>

namespace PHYSICSMOTION {
template <int D>
struct SurfaceArea;
template <>
struct SurfaceArea<3> {
  template <typename BoxType>
  static typename BoxType::Vec3T::Scalar area(const BoxType& bb) {
    typedef typename BoxType::Vec3T Vec3T;
    Vec3T ext=Vec3T::Zero().cwiseMax(bb.maxCorner()-bb.minCorner());
    return (ext[0]*ext[1]+ext[0]*ext[2]+ext[1]*ext[2])*2.0f;
  }
};
template <>
struct SurfaceArea<2> {
  template <typename BoxType>
  static typename BoxType::Vec3T::Scalar area(const BoxType& bb) {
    typedef typename BoxType::Vec3T Vec3T;
    Vec3T ext=Vec3T::Zero().cwiseMax(bb.maxCorner()-bb.minCorner());
    return (ext[0]+ext[1])*2.0f;
  }
};
//Node
template <typename T,typename BBOX>
Node<T,BBOX>::Node():_l(-1),_r(-1),_parent(-1),_nrCell(-1) {}
template <typename T,typename BBOX>
bool Node<T,BBOX>::read(std::istream& is,IOData* dat) {
  readBinaryData(_bb,is);
  readBinaryData(_cell,is,dat);
  readBinaryData(_l,is);
  readBinaryData(_r,is);
  readBinaryData(_parent,is);
  readBinaryData(_nrCell,is);
  return is.good();
}
template <typename T,typename BBOX>
bool Node<T,BBOX>::write(std::ostream& os,IOData* dat) const {
  writeBinaryData(_bb,os);
  writeBinaryData(_cell,os,dat);
  writeBinaryData(_l,os);
  writeBinaryData(_r,os);
  writeBinaryData(_parent,os);
  writeBinaryData(_nrCell,os);
  return os.good();
}
template <typename T,typename BBOX>
std::shared_ptr<SerializableBase> Node<T,BBOX>::copy() const {
  return std::shared_ptr<SerializableBase>(new Node<T,BBOX>);
}
template <typename T,typename BBOX>
std::string Node<T,BBOX>::type() const {
  return typeid(Node<T,BBOX>).name();
}
template <typename T,typename BBOX>
Node<T,BBOX>& Node<T,BBOX>::operator=(const Node<T,BBOX>& other) {
  _bb=other._bb;
  _cell=other._cell;
  _l=other._l;
  _r=other._r;
  _parent=other._parent;
  _nrCell=other._nrCell;
  return *this;
}
template <typename T,typename BBOX>
void Node<T,BBOX>::buildBVHTriangleBottomUp
(std::vector<Node<T,BBOX>>& bvh,
 const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss,bool mergeComponent) {
  std::unordered_map<Eigen::Matrix<int,2,1>,std::pair<int,int>,EdgeHash> edgeMap;
  buildEdge(iss,edgeMap);
  buildBVHBottomUp(bvh,edgeMap,mergeComponent);
}
template <typename T,typename BBOX>
void Node<T,BBOX>::buildBVHVertexBottomUp
(std::vector<Node<T,BBOX>>& bvh,
 const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss,bool mergeComponent) {
  std::unordered_map<Eigen::Matrix<int,2,1>,std::pair<int,int>,EdgeHash> edgeMap;
  for(const Eigen::Matrix<int,3,1>& t:iss)
    for(int d=0; d<3; d++) {
      Eigen::Matrix<int,2,1> e(t[(d+1)%3],t[(d+2)%3]);
      if(e[0]>e[1])
        std::swap(e[0],e[1]);
      edgeMap[e]=std::make_pair(e[0],e[1]);
    }
  buildBVHBottomUp(bvh,edgeMap,mergeComponent);
}
template <typename T,typename BBOX>
void Node<T,BBOX>::buildBVHBottomUp
(std::vector<Node<T,BBOX>>& bvh,
 const std::unordered_map<Eigen::Matrix<int,2,1>,std::pair<int,int>,EdgeHash>& edgeMap,bool mergeComponent) {
  //count leaves
  int nrLeaf=0;
  for(int i=0; i<(int)bvh.size(); i++)
    if(bvh[i]._l==-1)
      nrLeaf++;
  //initialize hash
  std::vector<int> heap;
  std::vector<int> heapOffsets;
  std::vector<std::pair<int,int>> ess;
  std::vector<typename BBOX::Vec3T::Scalar> cost;
  for(auto beg=edgeMap.begin(),end=edgeMap.end(); beg!=end; beg++) {
    heapOffsets.push_back(-1);
    ess.push_back(beg->second);
    BBOX bb=bvh[beg->second.first]._bb;
    if(beg->second.second>=0)
      bb.setUnion(bvh[beg->second.second]._bb);
    typename BBOX::Vec3T::Scalar c=SurfaceArea<3>::area(bb);
    cost.push_back(c);
  }
  for(int i=0; i<(int)ess.size(); i++)
    pushHeapDef(cost,heapOffsets,heap,i);
  //merge BVH
  int err;
  while(!heap.empty()) {
    int i=popHeapDef(cost,heapOffsets,heap,err);
    int t0=ess[i].first,t1=ess[i].second;
    //boundary edge
    if(t1==-1)
      continue;
    //find parent
    while(bvh[t0]._parent>=0)
      t0=bvh[t0]._parent;
    while(bvh[t1]._parent>=0)
      t1=bvh[t1]._parent;
    //check already merged
    if(t0==t1)
      continue;
    //merge
    BBOX bb=bvh[t0]._bb;
    bb.setUnion(bvh[t1]._bb);
    typename BBOX::Vec3T::Scalar c=SurfaceArea<3>::area(bb);
    if(c>cost[i]) {
      cost[i]=c;
      pushHeapDef(cost,heapOffsets,heap,i);
    } else {
      Node<T,BBOX> n;
      n._l=t0;
      n._r=t1;
      n._parent=-1;
      n._cell=-1;
      n._bb=bb;
      n._nrCell=bvh[n._l]._nrCell+bvh[n._r]._nrCell;
      bvh[t0]._parent=(int)bvh.size();
      bvh[t1]._parent=(int)bvh.size();
      bvh.push_back(n);
    }
  }
  if(mergeComponent) {
    mergeForest(bvh);
  } else {
    ASSERT_MSG((int)bvh.size()==nrLeaf*2-1,"Multi-component mesh detected!")
  }
}
template <typename T,typename BBOX>
void Node<T,BBOX>::mergeForest(std::vector<Node<T,BBOX>>& bvh) {
  //collect root
  std::vector<int> roots;
  for(int i=0; i<(int)bvh.size(); i++)
    if(bvh[i]._parent==-1)
      roots.push_back(i);
  //all-pair merge
  if(roots.size()>1) {
    std::unordered_map<Eigen::Matrix<int,2,1>,std::pair<int,int>,EdgeHash> edgeMap;
    for(int i=0; i<(int)roots.size(); i++)
      for(int j=i+1; j<(int)roots.size(); j++) {
        Eigen::Matrix<int,2,1> eid(roots[i],roots[j]);
        sort2(eid[0],eid[1]);
        edgeMap[eid]=std::make_pair(eid[0],eid[1]);
      }
    buildBVHBottomUp(bvh,edgeMap,false);
  }
}
template struct Node<int,BBoxExact>;
}
