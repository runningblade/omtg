#ifndef MESH_EXACT_H
#define MESH_EXACT_H

#include "ShapeExact.h"
#include "TriangleExact.h"

struct aiNode;
struct aiScene;
namespace PHYSICSMOTION {
struct MeshExact : public ShapeExact {
  MeshExact();
  MeshExact(const std::string& path,bool buildBVH=true,bool mergeComponent=false);
  MeshExact(const aiScene* scene,bool buildBVH=true,bool mergeComponent=false);
  MeshExact(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
            std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss,bool buildBVH=true,bool mergeComponent=false);
  void init(const aiScene* scene,const aiNode* node,
            std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
            std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss,bool buildBVH=true,bool mergeComponent=false);
  template <typename T2>
  void init(const std::vector<Eigen::Matrix<T2,3,1>,Eigen::aligned_allocator<Eigen::Matrix<T2,3,1>>>& vss,
            const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss,bool buildBVH=true,bool mergeComponent=false);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual const BBoxExact& getBB() const override;
  virtual bool empty() const override;
#ifndef SWIG
  const std::vector<Node<int,BBoxExact>>& getBVH() const;
#endif
  const std::vector<Vec3T,Eigen::aligned_allocator<Vec3T>>& vss() const;
  const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss() const;
  void getMesh(std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>>& vss,
               std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss) const override;
  bool closestInner(const Vec3T& pt,Vec3T& n,Vec3T& normal,Mat3T& hessian,
                    T& rad,Eigen::Matrix<int,2,1>& feat,bool cache=false,
                    std::vector<Vec3T,Eigen::aligned_allocator<Vec3T>>* history=NULL) const override;
  void scale(T coef) override;
 protected:
  std::vector<TriangleExact> _tss;
  std::vector<Vec3T,Eigen::aligned_allocator<Vec3T>> _vss;
  std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>> _iss;
  std::vector<Node<int,BBoxExact>> _bvh;
};
}

#endif
