%module(directors="2") pyPhysicsMotion
%include "std_shared_ptr.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_pair.i"
%template(vectorInt) std::vector<int>;
%include "typemaps.i"
%include "eigen.i"
typedef float GLfloat;
typedef double FLOAT;
%define ALL_TYPES(F)
%eigen_typemaps(Eigen::Matrix<F,-1,-1>)
%eigen_typemaps(Eigen::Matrix<F,-1,1>)
%eigen_typemaps(Eigen::Matrix<F,2,1>)
%eigen_typemaps(Eigen::Matrix<F,3,1>)
%eigen_typemaps(Eigen::Matrix<F,3,3>)
%eigen_typemaps(Eigen::Matrix<F,3,4>)
%eigen_typemaps(Eigen::Matrix<F,3,-1>)
%eigen_typemaps(Eigen::Matrix<F,4,1>)
%eigen_typemaps(Eigen::Matrix<F,6,1>)
%enddef
ALL_TYPES(int)
ALL_TYPES(float)
ALL_TYPES(double)
%{
#include <Utils/Utils.h>
#include <Utils/Pragma.h>
#include <Utils/ParallelVector.h>
#include <Utils/SparseUtils.h>
#include <Utils/RotationUtils.h>
#include <Utils/CrossSpatialUtils.h>
#include <Utils/SpatialRotationUtils.h>
#include <Utils/SOSPolynomial.h>
#include <Utils/Serializable.h>

#include <Environment/BBoxExact.h>
#include <Environment/ShapeExact.h>
#include <Environment/CompositeShapeExact.h>
#include <Environment/SphericalBBoxExact.h>
#include <Environment/MeshExact.h>
#include <Environment/ConvexHullExact.h>
#include <Environment/Environment.h>
#include <Environment/EnvironmentUtils.h>
#include <Environment/EnvironmentVisualizer.h>

#include <Deformable/FEMMesh.h>
#include <Deformable/FEMEnergy.h>
#include <Deformable/FEMPDController.h>
#include <Deformable/FEMCollision.h>
#include <Deformable/FEMSystem.h>
#include <Deformable/FEMGradientInfo.h>
#include <Deformable/FEMReducedSystem.h>
#include <Deformable/FEMOctreeMesh.h>
#include <Deformable/DeformableVisualizer.h>
%}
%include <Utils/Utils.h>
%include <Utils/Pragma.h>
%include <Utils/ParallelVector.h>
%include <Utils/SparseUtils.h>
%include <Utils/RotationUtils.h>
%include <Utils/CrossSpatialUtils.h>
%include <Utils/SpatialRotationUtils.h>
%include <Utils/SOSPolynomial.h>
%shared_ptr(PHYSICSMOTION::SerializableBase)
%include <Utils/Serializable.h>

%shared_ptr(PHYSICSMOTION::BBoxExact)
%include <Environment/BBoxExact.h>
%shared_ptr(PHYSICSMOTION::ShapeExact)
%include <Environment/ShapeExact.h>
%shared_ptr(PHYSICSMOTION::CompositeShapeExact)
%include <Environment/CompositeShapeExact.h>
%shared_ptr(PHYSICSMOTION::SphericalBBoxExact)
%include <Environment/SphericalBBoxExact.h>
%shared_ptr(PHYSICSMOTION::MeshExact)
%include <Environment/MeshExact.h>
%shared_ptr(PHYSICSMOTION::ConvexHullExact)
%include <Environment/ConvexHullExact.h>
%shared_ptr(PHYSICSMOTION::Environment<FLOAT>)
%shared_ptr(PHYSICSMOTION::EnvironmentExact<FLOAT>)
%shared_ptr(PHYSICSMOTION::EnvironmentHeight<FLOAT>)
%feature("director") EnvironmentCallback;
%include <Environment/Environment.h>
%template(EnvironmentFLOAT) PHYSICSMOTION::Environment<FLOAT>;
%template(EnvironmentExactFLOAT) PHYSICSMOTION::EnvironmentExact<FLOAT>;
%template(EnvironmentHeightFLOAT) PHYSICSMOTION::EnvironmentHeight<FLOAT>;
%include <Environment/EnvironmentUtils.h>
%include <Environment/EnvironmentVisualizer.h>
%template(visualizeEnvironmentFLOAT) PHYSICSMOTION::visualizeEnvironment<FLOAT>;

%apply bool *OUTPUT { bool* succ };
%feature("director") FixDOFCallback;
%shared_ptr(PHYSICSMOTION::FEMVertex<FLOAT>)
%shared_ptr(PHYSICSMOTION::FEMCell<FLOAT>)
%shared_ptr(PHYSICSMOTION::FEMMesh<FLOAT>)
%include <Deformable/FEMMesh.h>
%template(FEMVertexFLOAT) PHYSICSMOTION::FEMVertex<FLOAT>;
%template(FEMCellFLOAT) PHYSICSMOTION::FEMCell<FLOAT>;
%template(FEMMeshFLOAT) PHYSICSMOTION::FEMMesh<FLOAT>;
%shared_ptr(PHYSICSMOTION::FEMEnergy<FLOAT>)
%include <Deformable/FEMEnergy.h>
%template(FEMEnergyFLOAT) PHYSICSMOTION::FEMEnergy<FLOAT>;
%shared_ptr(PHYSICSMOTION::FEMPDController<FLOAT>)
%shared_ptr(PHYSICSMOTION::FEMReducedPDController<FLOAT>)
%include <Deformable/FEMPDController.h>
%template(FEMPDControllerFLOAT) PHYSICSMOTION::FEMPDController<FLOAT>;
%template(vectorFEMPDControllerFLOAT) std::vector<std::shared_ptr<PHYSICSMOTION::FEMPDController<FLOAT>>>;
%template(FEMReducedPDControllerFLOAT) PHYSICSMOTION::FEMReducedPDController<FLOAT>;
%template(vectorFEMReducedPDControllerFLOAT) std::vector<std::shared_ptr<PHYSICSMOTION::FEMReducedPDController<FLOAT>>>;
%shared_ptr(PHYSICSMOTION::FEMCollision<FLOAT>)
%include <Deformable/FEMCollision.h>
%template(FEMCollisionFLOAT) PHYSICSMOTION::FEMCollision<FLOAT>;
%template(vectorFEMCollisionFLOAT) std::vector<PHYSICSMOTION::FEMCollision<FLOAT>>;
%shared_ptr(PHYSICSMOTION::FEMSystem<FLOAT>)
%include <Deformable/FEMSystem.h>
%template(FEMSystemFLOAT) PHYSICSMOTION::FEMSystem<FLOAT>;
%include <Deformable/FEMGradientInfo.h>
%template(FEMGradientInfoFLOAT) PHYSICSMOTION::FEMGradientInfo<FLOAT>;
%template(vectorFEMGradientInfoFLOAT) std::vector<PHYSICSMOTION::FEMGradientInfo<FLOAT>>;
%template(pairInfoSucc) std::pair<PHYSICSMOTION::FEMGradientInfo<FLOAT>,bool>;
%template(vectorPairInfoSucc) std::vector<std::pair<PHYSICSMOTION::FEMGradientInfo<FLOAT>,bool>>;
%shared_ptr(PHYSICSMOTION::FEMReducedSystem<FLOAT>)
%include <Deformable/FEMReducedSystem.h>
%template(FEMReducedSystemFLOAT) PHYSICSMOTION::FEMReducedSystem<FLOAT>;
%shared_ptr(PHYSICSMOTION::FEMOctreeMesh<FLOAT>)
%include <Deformable/FEMOctreeMesh.h>
%template(FEMOctreeMeshFLOAT) PHYSICSMOTION::FEMOctreeMesh<FLOAT>;
%include <Deformable/DeformableVisualizer.h>
%template(visualizeFEMMeshSurfaceFLOAT) PHYSICSMOTION::visualizeFEMMeshSurface<FLOAT>;
%template(visualizeFEMLowDimensionalMeshSurfaceFLOAT) PHYSICSMOTION::visualizeFEMLowDimensionalMeshSurface<FLOAT>;
%template(visualizeFEMMeshTetrahedronFLOAT) PHYSICSMOTION::visualizeFEMMeshTetrahedron<FLOAT>;
%template(visualizeFEMMeshHexahedronFLOAT) PHYSICSMOTION::visualizeFEMMeshHexahedron<FLOAT>;
%template(updateSurfaceFLOAT) PHYSICSMOTION::updateSurface<FLOAT>;
%template(updateTetrahedronFLOAT) PHYSICSMOTION::updateTetrahedron<FLOAT>;
%template(updateHexahedronFLOAT) PHYSICSMOTION::updateHexahedron<FLOAT>;
%template(visualizeFEMWaterLevelFLOAT) PHYSICSMOTION::visualizeFEMWaterLevel<FLOAT>;

