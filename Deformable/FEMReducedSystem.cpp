#include "FEMReducedSystem.h"
#include "FEMGradientInfo.h"
#include "FEMEnergy.h"
#include "eigenmvn.h"
#include "eigenminres.h"
#include <stack>
#include <Utils/IO.h>
#include <Utils/Timing.h>
#include <Utils/SparseUtils.h>
#include <Utils/CrossSpatialUtils.h>
//solvers: first-/second-order bases
#include "DACGStab.h"
#include <Spectra/SymGEigsSolver.h>
#include <Spectra/MatOp/SparseGenMatProd.h>
#include <Spectra/MatOp/SparseCholesky.h>
#include <unsupported/Eigen/IterativeSolvers>

namespace PHYSICSMOTION {
template <typename T>
FEMReducedSystem<T>::FEMReducedSystem():_buildCellVertex(false) {
  disableTiming();
  _minEigenvalue=1e-4;
}
template <typename T>
FEMReducedSystem<T>::FEMReducedSystem(std::shared_ptr<FEMMesh<T>> mesh):FEMSystem<T>(mesh),_buildCellVertex(false) {
  disableTiming();
  _pos0=_mesh->pos0();
  _minEigenvalue=1e-4;
}
template <typename T>
FEMReducedSystem<T>::~FEMReducedSystem() {}
template <typename T>
bool FEMReducedSystem<T>::read(std::istream& is,IOData* dat) {
  FEMSystem<T>::read(is,dat);
  readBinaryData(_minEigenvalue,is);
  readBinaryData(_pos0,is);
  readBinaryData(_pos0Interp,is);
  readBinaryData(_U,is);
  readBinaryData(_UInterp,is);
  readBinaryData(_UTMU,is);
  readBinaryData(_UTKU,is);
  readBinaryData(_cubatures,is);
  buildReducedEnergies();
  return is.good();
}
template <typename T>
bool FEMReducedSystem<T>::write(std::ostream& os,IOData* dat) const {
  FEMSystem<T>::write(os,dat);
  writeBinaryData(_minEigenvalue,os);
  writeBinaryData(_pos0,os);
  writeBinaryData(_pos0Interp,os);
  writeBinaryData(_U,os);
  writeBinaryData(_UInterp,os);
  writeBinaryData(_UTMU,os);
  writeBinaryData(_UTKU,os);
  writeBinaryData(_cubatures,os);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> FEMReducedSystem<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new FEMReducedSystem<T>);
}
template <typename T>
std::string FEMReducedSystem<T>::type() const {
  return typeid(FEMReducedSystem<T>).name();
}
template <typename T>
std::shared_ptr<FEMVertex<T>> FEMReducedSystem<T>::getSV(int i) const {
  return _reducedVertices[i];
}
template <typename T>
void FEMReducedSystem<T>::debugDStiffness(int iter,T deltaPos) const {
  SMatT H,DHDU,H2;
  DEFINE_NUMERIC_DELTA_T(T)
  std::cout << "DebugDStiffness: " << typeid(*this).name() << std::endl;
  for(int i=0; i<iter; i++) {
    Vec p=_pos0+Vec::Random(_pos0.size())*deltaPos;
    Vec dx=Vec::Random(_pos0.size());
    H=stiffness(p);
    DHDU=DStiffness(p,dx);
    H2=stiffness(p+dx*DELTA);
    DEBUG_GRADIENT("DStiffness",DHDU.norm(),((H2-H)/DELTA-DHDU).norm())
  }
}
template <typename T>
std::shared_ptr<FEMVertex<T>> FEMReducedSystem<T>::getReducedVertex(std::shared_ptr<FEMVertex<T>> v) const {
  auto it=_mappedVertex.find(v);
  ASSERT_MSG(it!=_mappedVertex.end(),"Reduced cell vertex not mapped!")
  return it->second;
}
template <typename T>
void FEMReducedSystem<T>::setFluidEnergy(T level,T rho,T G,T dragMult,int deg) {
  FEMSystem<T>::setFluidEnergy(level,rho,G,dragMult,deg);
  for(std::shared_ptr<FEMEnergy<T>> e:_reducedEnergies)
    if(std::dynamic_pointer_cast<FEMFluidEnergy<T>>(e)) {
      std::shared_ptr<FEMFluidEnergy<T>> fe=std::dynamic_pointer_cast<FEMFluidEnergy<T>>(e);
      fe->setParameters(level,rho,G,dragMult,deg);
    }
}
template <typename T>
std::vector<std::shared_ptr<FEMEnergy<T>>> FEMReducedSystem<T>::getReducedEnergy() const {
  return _reducedEnergies;
}
template <typename T>
int FEMReducedSystem<T>::getReducedVertexOff(std::shared_ptr<FEMVertex<T>> v) const {
  return getReducedVertex(v)->jacobian().begin()->first;
}
template <typename T>
bool FEMReducedSystem<T>::buildBasis(int maxBases) {
  Vec eval;
  if(isLinearStiffness()) {
    if(!findFirstOrderBasis(maxBases,eval,_U,!_mesh->isFixed()))
      return false;
  } else if(_mesh->isFixed()) {
    int n=0;
    while(n+(n+1)*n/2<maxBases)
      n++;
    if(!findFirstOrderBasis(n,eval,_U,false))
      return false;
    if(!findSecondOrderBasisFixed(n,_U))
      return false;
  } else {
    int n=0;
    while(n+n*n<maxBases)
      n++;
    if(!findFirstOrderBasis(n,eval,_U,true))
      return false;
    if(!findSecondOrderBasisFree(n,eval,_U))
      return false;
  }
  if(!_mesh->isFixed())
    factorOutTranslation(_U);
  makeOrthogonal(_U);
  _UTMU=_U.transpose()*_mass*_U;
  _UTKU=_U.transpose()*stiffness(_pos0)*_U;
  buildCubature();
  buildReducedEnergies();
  return true;
}
template <typename T>
int FEMReducedSystem<T>::nFirstOrderBasis() const {
  if(isLinearStiffness())
    return _U.cols();
  else if(_mesh->isFixed()) {
    int n=0;
    while(n+(n+1)*n/2<_U.cols())
      n++;
    return n;
  } else {
    int n=0;
    while(n+n*n<_U.cols())
      n++;
    return n;
  }
}
template <typename T>
void FEMReducedSystem<T>::setBasis(const MatT& U) {
  _U=U;
  _UTMU=_U.transpose()*_mass*_U;
  _UTKU=_U.transpose()*stiffness(_pos0)*_U;
  buildCubature();
  buildReducedEnergies();
}
template <typename T>
const typename FEMReducedSystem<T>::MatT& FEMReducedSystem<T>::getBasisInterp() const {
  return _UInterp;
}
template <typename T>
const typename FEMReducedSystem<T>::MatT& FEMReducedSystem<T>::getBasis() const {
  return _U;
}
template <typename T>
const typename FEMReducedSystem<T>::Vec& FEMReducedSystem<T>::getPos0Interp() const {
  return _pos0Interp;
}
template <typename T>
const typename FEMReducedSystem<T>::Vec& FEMReducedSystem<T>::getPos0() const {
  return _pos0;
}
template <typename T>
const typename FEMReducedSystem<T>::MatT& FEMReducedSystem<T>::getUTMU() const {
  return _UTMU;
}
template <typename T>
typename FEMReducedSystem<T>::MatT FEMReducedSystem<T>::getUTKU() const {
  return _UTKU;
}
template <typename T>
T FEMReducedSystem<T>::minEigenvalue() const {
  return _minEigenvalue;
}
template <typename T>
void FEMReducedSystem<T>::minEigenvalue(T e) {
  _minEigenvalue=e;
}
template <typename T>
void FEMReducedSystem<T>::addSelfCC(T coef) {
  _buildCellVertex=true;
  buildReducedEnergies();
  FEMSystem<T>::addSelfCC(coef);
}
//simulate
template <typename T>
void FEMReducedSystem<T>::updateFluid(const Vec& pN,std::vector<FEMEnergyContext<T>>& context) const {
  updateFluid(FEMGradientInfo<T>(*this,mapCV(pN)),context);
}
template <typename T>
void FEMReducedSystem<T>::updateFluid(const FEMGradientInfo<T>& IN,std::vector<FEMEnergyContext<T>>& context) const {
  context.resize(_reducedEnergies.size());
  Vec DQInterpDt=IN.getDQInterpDt(mapCM(_UInterp));
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_reducedEnergies.size(); i++)
    if(std::dynamic_pointer_cast<FEMFluidEnergy<T>>(_reducedEnergies[i]))
      context[i]=std::dynamic_pointer_cast<FEMFluidEnergy<T>>(_reducedEnergies[i])->updateFluid(IN.getQInterp(),DQInterpDt);
}
template <typename T>
void FEMReducedSystem<T>::detectCollision(const Vec& p,std::vector<FEMCollision<T>>& colls) const {
  detectCollision(FEMGradientInfo<T>(*this,mapCV(p)),colls);
}
template <typename T>
void FEMReducedSystem<T>::detectCollision(const FEMGradientInfo<T>& I,std::vector<FEMCollision<T>>& colls) const {
  if(!_env)
    return;
  colls.clear();
  std::vector<Node<int,BBoxExact>> bvhFEM;
  const std::vector<Node<int,BBoxExact>>& bvhEnv=_env->getBVH();
  _mesh->getSBVHSubspace(I,bvhFEM);
  //main loop
  std::stack<std::pair<int,int>> ss;
  ss.push(std::make_pair((int)bvhFEM.size()-1,(int)bvhEnv.size()-1));
  while(!ss.empty()) {
    int l=ss.top().first;
    int r=ss.top().second;
    ss.pop();
    if(!bvhFEM[l]._bb.intersect(bvhEnv[r]._bb))
      continue;
    else if(bvhFEM[l]._l==-1 && bvhEnv[r]._l==-1) {
      int off=l*3;
      Vec3T pos=I.getVPos(off);
      T phi=_env->phi(pos);
      if(phi<0)
        colls.push_back(FEMCollision<T>(l,I.getQInterpJac(mapCM(_UInterp),off),pos,_env->phiGrad(pos),phi));
    } else if(bvhFEM[l]._l==-1) {
      ss.push(std::make_pair(l,bvhEnv[r]._l));
      ss.push(std::make_pair(l,bvhEnv[r]._r));
    } else if(bvhEnv[r]._l==-1) {
      ss.push(std::make_pair(bvhFEM[l]._l,r));
      ss.push(std::make_pair(bvhFEM[l]._r,r));
    } else {
      ss.push(std::make_pair(bvhFEM[l]._l,bvhEnv[r]._l));
      ss.push(std::make_pair(bvhFEM[l]._r,bvhEnv[r]._l));
      ss.push(std::make_pair(bvhFEM[l]._l,bvhEnv[r]._r));
      ss.push(std::make_pair(bvhFEM[l]._r,bvhEnv[r]._r));
    }
  }
}
template <typename T>
void FEMReducedSystem<T>::debugDDxDtDu(const FEMGradientInfo<T>& IN,std::shared_ptr<FEMPDController<T>> ctrl,SOLVER solver) const {
  Vec dTarget=Vec::Random(ctrl->pTarget().size()+ctrl->dTarget().size());
  FEMGradientInfo<T> I,I2;
  step(IN,I,ctrl,solver,true);
  DEFINE_NUMERIC_DELTA_T(T)
  DELTA=Epsilon<double>::defaultEps();  //always use double to do low-precision finite-difference test
  ctrl->setPTarget(ctrl->pTarget()+dTarget.segment(0,ctrl->pTarget().size())*DELTA);
  ctrl->setDTarget(ctrl->dTarget()+dTarget.segment(ctrl->pTarget().size(),ctrl->dTarget().size())*DELTA);
  step(IN,I2,ctrl,solver,false);
  DEBUG_GRADIENT("DDxDtDu",(I.getDDxDtDu()*dTarget).norm(),(I.getDDxDtDu()*dTarget-(I2.getDDOFDt()-I.getDDOFDt())/DELTA).norm())
}
template <typename T>
bool FEMReducedSystem<T>::step(const Vec& pN,Vec& p,std::shared_ptr<FEMPDController<T>> ctrl,SOLVER solver,std::vector<FEMCollision<T>>* collsRet,MatT* DDxDtDu) const {
  FEMGradientInfo<T> IN(*this,mapCV(pN)),I;
  bool ret=step(IN,I,ctrl,solver,DDxDtDu!=NULL);
  if(collsRet)
    *collsRet=I.getCollisions();
  if(DDxDtDu)
    *DDxDtDu=I.getDDxDtDu();
  p=I.getX();
  return ret;
}
template <typename T>
bool FEMReducedSystem<T>::step(const FEMGradientInfo<T>& IN,FEMGradientInfo<T>& I,std::shared_ptr<FEMPDController<T>> ctrl,SOLVER solver,bool wantDDxDtDu) const {
  Vec GRHS,vNew;
  MatT GLHS;
  std::vector<std::shared_ptr<FEMEnergy<T>>> ess=_reducedEnergies;
  if(_selfCC)
    _selfCC->detectSelfCollision(IN,*this,_mesh,ess,_selfCCoef,_selfCCDebugOutputPath);
  if(!assemble(GRHS,GLHS,IN,ctrl,&ess))
    return false;
  TBEG("Solve");
  //account for collisions
  std::vector<FEMCollision<T>> colls;
  detectCollision(IN,colls);
  if((int)colls.size()>_maxContact)
    FEMSystem<T>::getContactSimplifier()->simplify(colls,_maxContact);
  for(FEMCollision<T>& c:colls)
    c.buildFrame();
  //solve
  bool succ=true;
  MatT DDxDtDu;
  FEMActiveSet<T> AS;
  SolverSPD<MatT> solH;
  solH.computeEigen(GLHS,_minEigenvalue);
  if(colls.empty()) {
    vNew=IN.getDDOFDt()-solH.solve(GRHS)*_dt;
    if(wantDDxDtDu) {
      ASSERT_MSG(ctrl,"Cannot compute DDxDtDu without PDController!")
      DDxDtDu=-solH.solve(ctrl->DGDu(GRHS.size(),_dt))*_dt;
    }
  } else if(solver==SP) {
    if(!FEMSystem<T>::staggeredProjectionD(IN.getDDOFDt(),vNew,GLHS,solH,GRHS*_dt,_mu,colls,wantDDxDtDu?&AS:NULL))
      succ=false;
    if(wantDDxDtDu) {
      ASSERT_MSG(ctrl,"Cannot compute DDxDtDu without PDController!")
      DDxDtDu=AS.computeDDxDuDt(-ctrl->DGDu(GRHS.size(),_dt)*_dt,solH);
    }
  } else if(solver==LCP) {
    if(!FEMSystem<T>::LCPProjectionD(IN.getDDOFDt(),vNew,GLHS,solH,GRHS*_dt,_mu,colls,wantDDxDtDu?&AS:NULL))
      succ=false;
    if(wantDDxDtDu) {
      ASSERT_MSG(ctrl,"Cannot compute DDxDtDu without PDController!")
      DDxDtDu=AS.computeDDxDuDt(-ctrl->DGDu(GRHS.size(),_dt)*_dt,solH);
    }
  } else {
    ASSERT_MSGV(false,"Unsupported solver type: %d!",(int)solver)
  }
  I=IN.integrate(*this,_dt,NULL,&vNew);
  I.getCollisions()=colls;
  if(wantDDxDtDu)
    I.setDDxDtDu(DDxDtDu);
  TEND();
  return succ;
}
template <typename T>
void FEMReducedSystem<T>::writeObj(const std::string& path, const FEMGradientInfo<T>& p) const {
  _mesh->writeObj(path,&p,NULL);
}
template <typename T>
void FEMReducedSystem<T>::writePov(const std::string& path, const FEMGradientInfo<T>& p) const {
  _mesh->writePov(path,p,NULL);
}
template <typename T>
typename FEMReducedSystem<T>::Vec FEMReducedSystem<T>::step(const Vec& pN,std::shared_ptr<FEMPDController<T>> ctrl,bool* succ,SOLVER solver,std::vector<FEMCollision<T>>* collsRet,MatT* DDxDtDu) const {
  FEMGradientInfo<T> IN(*this,mapCV(pN));
  FEMGradientInfo<T> I=step(IN,ctrl,succ,solver,DDxDtDu!=NULL);
  if(collsRet)
    *collsRet=I.getCollisions();
  if(DDxDtDu)
    *DDxDtDu=I.getDDxDtDu();
  return I.getX();
}
template <typename T>
FEMGradientInfo<T> FEMReducedSystem<T>::step(const FEMGradientInfo<T>& IN,std::shared_ptr<FEMPDController<T>> ctrl,bool* succ,SOLVER solver,bool wantDDxDtDu) const {
  FEMGradientInfo<T> I;
  bool retSucc=step(IN,I,ctrl,solver,wantDDxDtDu);
  if(succ)
    *succ=retSucc;
  return I;
}
template <typename T>
std::vector<std::pair<FEMGradientInfo<T>,bool>> FEMReducedSystem<T>::stepMultiple(const std::vector<FEMGradientInfo<T>>& IN,std::vector<std::shared_ptr<FEMPDController<T>>> ctrl,SOLVER solver,bool wantDDxDtDu) const {
  std::vector<std::pair<FEMGradientInfo<T>,bool>> rets(IN.size());
  //omp_set_num_threads(12);
  //omp_set_nested(1);
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)IN.size(); i++) {
    bool succI;
    rets[i].first=step(IN[i],ctrl[i],&succI,solver,wantDDxDtDu);
    rets[i].second=succI;
  }
  return rets;
}
//helper
template <typename T>
bool FEMReducedSystem<T>::assemble(Vec& GRHS,MatT& GLHS,const FEMGradientInfo<T>& IN,std::shared_ptr<FEMPDController<T>> ctrl,std::vector<std::shared_ptr<FEMEnergy<T>>>* ess) const {
  GRHS.setZero(IN.getDOF().size());
  GLHS.setZero(IN.getDOF().size(),IN.getDOF().size());
  Vec GKERHS=GRHS;
  MatT GKELHS=GLHS;
  TBEG("BuildInternal/External/FluidEnergy");
  //add linear FST
  if(IN.isNoninertial())
    GRHS+=_linearEnergy->assembleSystem(IN.getU(),IN.getR());
  else GRHS+=_linearEnergy->assembleSystem(IN.getU(),NULL);
  //add quadratic FST
  if(IN.isNoninertial())
    _quadraticEnergy->assembleSystem(IN.getT(),IN.getR(),IN.getU(),GLHS,GRHS,_dt);
  else _quadraticEnergy->assembleSystem(NULL,NULL,IN.getU(),GLHS,GRHS,_dt);
  //add linear stiffness
  if(isLinearStiffness()) {
    GRHS.segment(0,_U.cols())+=_UTKU*IN.getU();
    GLHS.block(0,0,_U.cols(),_U.cols())+=_UTKU*_dt;
  }
  //add SPD controller
  if(ctrl)
    ctrl->assemble(&GRHS,&GLHS,IN,_dt);
  //add cubature/fluid energy
  ParallelMatrix<Vec> GForceP(Vec::Zero(GRHS.size()));
  ParallelMatrix<Vec> GForceSP(Vec::Zero(_mesh->nrSV()*3));
  ParallelMatrix<MatT> GMassP(MatT::Zero(_U.cols(),_U.cols()));
  typename FEMEnergy<T>::GFunc GFunc=[&](int off,const Vec3T& blk) {
    GForceP.getMatrixI().segment(0,_UInterp.cols())+=_UInterp.block(off,0,3,_UInterp.cols()).transpose()*blk;
  };
  typename FEMEnergy<T>::HFunc HFunc=[&](int offr,int offc,const Mat3T& blk) {
    GMassP.getMatrixI()+=_UInterp.block(offr,0,3,_UInterp.cols()).transpose()*blk*_UInterp.block(offc,0,3,_UInterp.cols())*_dt;
  };
  typename FEMEnergy<T>::GFunc GFuncFluid=[&](int off,const Vec3T& blk) {
    GForceSP.getMatrixI().template segment<3>(off)+=blk;
  };
  const std::vector<std::shared_ptr<FEMEnergy<T>>>& essRef=ess?*ess:_energies;
  std::vector<FEMEnergyContext<T>> context;
  updateFluid(IN,context);
  bool valid=true;
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)essRef.size(); i++)
    if(!valid)
      continue;
    else if(std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(essRef[i]) || std::dynamic_pointer_cast<FEMPenetrationEnergy<T>>(essRef[i])) {
      if(!essRef[i]->eval(IN.getQInterpL(),NULL,&GFunc,&HFunc,&context[i]))
        valid=false;
    } else if(std::dynamic_pointer_cast<FEMFluidEnergy<T>>(essRef[i])) {
      if(!essRef[i]->eval(IN.getQInterp(),NULL,&GFuncFluid,NULL,&context[i]))
        valid=false;
    }
  if(!valid) {
    TEND();
    return false;
  }
  GForceSP.getMatrix();
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_mesh->nrSV(); i++) {
    int off=i*3;
    Vec3T blkL=IN.getR().transpose()*GForceSP.getMatrix().template segment<3>(off);
    GForceP.getMatrixI().segment(0,_UInterp.cols())+=_UInterp.block(off,0,3,_UInterp.cols()).transpose()*blkL;
    if(IN.isNoninertial()) {
      GForceP.getMatrixI().template segment<3>(_UInterp.cols())+=blkL;
      GForceP.getMatrixI().template segment<3>(_UInterp.cols()+3)+=IN.getQInterpL().template segment<3>(off).cross(blkL);
    }
  }
  GRHS+=GForceP.getMatrix();
  GLHS.block(0,0,_U.cols(),_U.cols())+=GMassP.getMatrix();
  //kinematic energy, account for coriolis and centrifugal force
  if(IN.isNoninertial())
    _noninertialFrame->assembleSystem(IN.getVLocal(),IN.getWLocal(),IN.getU(),IN.getDUDt(),GKELHS,GKERHS);
  else _noninertialFrame->assembleSystem(NULL,NULL,IN.getU(),IN.getDUDt(),GKELHS,GKERHS);
  //damping
  GLHS+=GKELHS*_dampingCoef;
  //assemble
  GRHS+=GLHS*IN.getDDOFDt()+GKERHS;
  GLHS=GLHS*_dt+GKELHS;
  TEND();
  return true;
}
template <typename T>
void FEMReducedSystem<T>::makeOrthogonal(MatT& U) {
  int n=U.cols();
  for(int i=0; i<n; i++) {
    for(int j=0; j<i; j++) {
      const T alpha=U.col(i).dot(U.col(j))/U.col(j).dot(U.col(j));
      U.col(i)-=alpha*U.col(j);
    }
    //INFOV("Orthogonal residual: %f!",U.col(i).norm())
    //ASSERT_MSGV(U.col(i).norm() > 1E-3f,"Error zero residual at: U.col(%d) in makeOrthogonal!",i)
    U.col(i)/=sqrt(U.col(i).dot(U.col(i)));
  }
  std::cout << "Orthogonal error: " << (U.transpose()*U-MatT::Identity(U.cols(),U.cols())).cwiseAbs().maxCoeff() << std::endl;
}
template <typename T>
void FEMReducedSystem<T>::factorOutTranslation(MatT& U) const {
  MatX3T I;
  I.resize(_pos0.size(),3);
  for(int i=0; i<_pos0.size(); i+=3)
    I.template block<3,3>(i,0).setIdentity();
  Mat3T ITMI=I.transpose()*(_mass*I);
  //solve
  SolverSPD<MatT> sol;
  sol.computeEigen(ITMI,0);
  //main loop
  for(int c=0; c<U.cols(); c++) {
    Vec3T sumP=I.transpose()*(_mass*U.col(c));
    std::cout << "Translation of " << c << "th basis is: " << sumP.transpose() << std::endl;
    //solve
    Vec3T lambda=sol.solve(sumP);
    U.col(c)-=I*lambda;
    //check
    sumP=I.transpose()*(_mass*U.col(c));
    std::cout << "Adjusted translation of " << c << "th basis is: " << sumP.transpose() << std::endl;
  }
}
template <typename T>
void FEMReducedSystem<T>::buildReducedEnergies() {
  _pos0Interp.setZero(0);
  _UInterp.setZero(0,_U.cols());
  _linearEnergy.reset(new FEMLinearFST<T>(_U,_pos0));
  _quadraticEnergy.reset(new FEMQuadraticFST<T>(_U,_pos0));
  _noninertialFrame.reset(new FEMNoninertialFrame<T>(_U,_pos0,_mass));
  _reducedEnergies.clear();
  _reducedVertices.clear();
  _mappedVertex.clear();
  std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper=[&](std::shared_ptr<FEMVertex<T>> v)->std::shared_ptr<FEMVertex<T>> {
    if(_mappedVertex.find(v)==_mappedVertex.end()) {
      Vec3T pos0=v->pos0();
      Mat3XT U=Mat3XT::Zero(3,_U.cols());
      for(const std::pair<int,T>& J:v->jacobian()) {
        pos0+=_pos0.template segment<3>(J.first,3)*J.second;
        U+=_U.block(J.first,0,3,_U.cols())*J.second;
      }
      _mappedVertex[v]=std::shared_ptr<FEMVertex<T>>(new FEMVertex<T>(_UInterp.rows()));
      _pos0Interp=concatRow(_pos0Interp,pos0);
      _UInterp=concatRow(_UInterp,U);
      _reducedVertices.push_back(_mappedVertex[v]);
    }
    return _mappedVertex[v];
  };
  //we make sure that surface vertices always appear first
  for(int i=0; i<_mesh->nrSV(); i++)
    vertexMapper(_mesh->getSV(i));
  if(_buildCellVertex)
    for(int i=0; i<_mesh->nrV(); i++)
      vertexMapper(_mesh->getV(i));
  //build energy
  std::unordered_map<std::shared_ptr<FEMCell<T>>,T> cubaturesCell;
  for(std::pair<int,T> c:_cubatures)
    cubaturesCell[_mesh->getC(c.first)]=c.second;
  for(std::shared_ptr<FEMEnergy<T>> e:_energies)
    if(std::dynamic_pointer_cast<FEMLinearElasticEnergy<T>>(e))
      continue; //handled by quadratic FST
    else if(std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(e)) {
      //energies selected by cubature should be included
      std::shared_ptr<FEMStVKElasticEnergy<T>> ce=std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(e);
      if(cubaturesCell.find(ce->getCell())!=cubaturesCell.end())
        _reducedEnergies.push_back(ce->toReducedEnergy(vertexMapper,cubaturesCell.find(ce->getCell())->second));
    } else if(std::dynamic_pointer_cast<FEMFluidEnergy<T>>(e))
      //all these energies should be included
      _reducedEnergies.push_back(e->toReducedEnergy(vertexMapper,1));
    else if(e->polyOrder()==1)
      //first-order fast sandwich transform can be used
      _linearEnergy->addEnergy(e);
    else if(e->polyOrder()==2) {
      //second-order fast sandwich transform can be used
      _linearEnergy->addEnergy(e);
      _quadraticEnergy->addEnergy(e);
    } else {
      ASSERT_MSGV(false,"Unsupported energy: %s in FEMReducedSystem!",e->type().c_str())
    }
}
template <typename T>
void FEMReducedSystem<T>::buildCubature(int batchSize,T acceptableReduction,bool debug) {
  _cubatures=CUBATURES();
  _reducedEnergies.clear();
  if(isLinearStiffness())
    return;
  CUBATURES cubaturesOneMore;
  std::pair<MatT,Vec> forcesVolume;
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double,-1,-1>> eig(getUTKU().template cast<double>(),Eigen::ComputeEigenvectors);
  ASSERT_MSG(eig.eigenvalues().minCoeff()>0,"UTKU not positive definite!")
  MatT invUTKU=(eig.eigenvectors()*(1/eig.eigenvalues().array()).matrix().asDiagonal()*eig.eigenvectors().transpose()).template cast<T>();
  T scaleCov=1,relativeErrBound=0,relativeErrBoundOneMore=0,lastRelativeErrBound=0;
  forcesVolume.first=MatT::Zero(0,_mesh->nrC());
  forcesVolume.second=Vec::Zero(0);
  int nrCubature=_U.cols();
  while(true) {
    //sample batch
    int nrTrial=0,rows=0;
    std::vector<Vec,Eigen::aligned_allocator<Vec>> poses;
    Eigen::EigenMultivariateNormal<double> dist(Eigen::Matrix<double,-1,1>::Zero(invUTKU.rows()),(invUTKU*scaleCov).template cast<double>(),true);
    while((int)poses.size()<batchSize && nrTrial<batchSize*10) {
      Vec u=_pos0+_U*dist.samples(1).template cast<T>();
      if(!hasFlippedElement(u))
        poses.push_back(u);
      nrTrial++;
    }
    if(nrTrial>=batchSize*10) {
      //too many samples are invalid, reduce covariance
      scaleCov*=0.8f;
      std::cout << "Too many samples (" << nrTrial << "/" << batchSize << "), decreasing covariance to: " << scaleCov << std::endl;
    } else if(nrTrial<batchSize*2) {
      //we can increase covariance to sampling more challenging pose
      scaleCov/=0.8f;
      std::cout << "Too easy samples (" << nrTrial << "/" << batchSize << "), increasing covariance to: " << scaleCov << std::endl;
    } else {
      rows=forcesVolume.first.rows();
      forcesVolume.first=concatRow(forcesVolume.first,MatT::Zero(_U.cols()*batchSize,_mesh->nrC()));
      forcesVolume.second=concatRow(forcesVolume.second,Vec::Zero(_U.cols()*batchSize));
      for(const Vec& u:poses) {
        Eigen::Block<MatT> blk=forcesVolume.first.block(rows,0,_U.cols(),_mesh->nrC());
        blk=cellwiseForceVolume(u);
        forcesVolume.second.segment(rows,_U.cols())=blk*Vec::Ones(_mesh->nrC());
        rows+=_U.cols();
      }
      //solve NN-HTP
      NNHTP<T> sol(forcesVolume.first,forcesVolume.second);
      _cubatures=sol.solve(nrCubature,1e-3f,debug);
      relativeErrBound=sol.error(_cubatures);
      //try solve using a larger cubature size
      while(nrCubature<forcesVolume.first.cols()) {
        cubaturesOneMore=sol.solve(nrCubature+1,1e-3f,debug);
        relativeErrBoundOneMore=sol.error(cubaturesOneMore);
        //take the larger cubature size if error reduction > acceptableReduction
        if(relativeErrBoundOneMore<relativeErrBound-acceptableReduction) {
          relativeErrBound=relativeErrBoundOneMore;
          _cubatures=cubaturesOneMore;
          nrCubature++;
        } else break;
      }
      std::cout << "Relative error bound=" << relativeErrBound << " with " << nrCubature << " cubatures on " << rows/_U.cols() << "!" << std::endl;
      if(relativeErrBound<=lastRelativeErrBound)  //same cubature set can handle [batchsize] additional samples, we exit!
        break;
      lastRelativeErrBound=relativeErrBound;
    }
  }
}
template <typename T>
bool FEMReducedSystem<T>::hasFlippedElement(const Vec& p) const {
  int flipped=0;
  OMP_PARALLEL_FOR_
  for(int i=0; i<_mesh->nrC(); i++) {
    if(flipped>0)
      continue;
    std::shared_ptr<FEMCell<T>> c=_mesh->getC(i);
    for(std::pair<Vec3T,T> bary:c->stencilIntegrateQuartic()) {
      Mat3T F=c->F(mapCV(p),bary.first);
      if(F.determinant()<0)
        OMP_ATOMIC_
        flipped++;
    }
  }
  return flipped>0;
}
template <typename T>
bool FEMReducedSystem<T>::findFirstOrderBasis(int n,Vec& eval,MatT& evec,bool removeRigidMode) {
#define USE_DACG
#ifdef USE_DACG
  //move to center of mass
  SMatT K=stiffness(_pos0);
  //invoke DACG
  MatT R0;
  //DACGSolver<T,InvDiagonalKrylovMatrix<T>> solDACG;
  DACGSolver<T,KKTKrylovMatrix<T,Eigen::SimplicialCholesky<SMatT>>> solDACG;
  if(removeRigidMode) {
    R0.resize(_pos0.size(),6);
    for(int i=0; i<_pos0.size(); i+=3) {
      R0.template block<3,3>(i,0).setIdentity();
      R0.template block<3,3>(i,3)=cross<T>(_pos0.template segment<3>(i));
    }
    solDACG.preconditioner().setUT(&R0);
  }
  if(!solDACG.setA(K)) {
    std::cout << "KKTKrylovMatrix failed!" << std::endl;
    return false;
  }
  solDACG.setB(_mass);
  if(removeRigidMode)
    solDACG.setU0(R0);
  eval.resize(n);
  evec.resize(_pos0.size(),n);
  if(!solDACG.solve(n,eval,evec))
    return false;
  solDACG.checkSolution(eval,evec);
  std::cout << "Eigenvalues: " << eval.transpose() << std::endl;
  return true;
#else
  using namespace std;
  //move to center of mass
  Eigen::SparseMatrix<double,0,int> stiff=stiffness(_mesh->pos0()).template cast<double>();
  Eigen::SparseMatrix<double,0,int> mass=_mass.template cast<double>();
  if(removeRigidMode)
    n+=6;
  //invoke spectra
  typedef Spectra::SparseSymMatProd<double> KOP;
  typedef Spectra::SparseCholesky<double> MOP;
  KOP Kop(stiff);
  MOP Mop(mass);
  if(Mop.info()!=Spectra::CompInfo::Successful) {
    std::cout << "Cholesky factorization failed!" << std::endl;
    return false;
  }
  std::cout << "Computing " << n << " first-order rigid modes!" << std::endl;
  Spectra::SymGEigsSolver<KOP,MOP,Spectra::GEigsMode::Cholesky> sol(Kop,Mop,n,min(n*10,(int)_mass.rows()));
  sol.init();
  sol.compute(Spectra::SortRule::SmallestMagn);
  //failure detection
  if(sol.info()!=Spectra::CompInfo::Successful)
    return false;
  eval=sol.eigenvalues().template cast<T>();
  evec=sol.eigenvectors().template cast<T>();
  //remove 3 rigid linear modes and 3 rigid rotational modes
  if(removeRigidMode) {
    std::cout << "Eigenvalues with rigid mode:" << eval.transpose() << std::endl;
    eval=eval.segment(0,eval.size()-6).eval();
    evec=evec.block(0,0,evec.rows(),evec.cols()-6).eval();
  }
  std::cout << "Eigenvalues: " << eval.transpose() << std::endl;
  return true;
#endif
}
template <typename T>
bool FEMReducedSystem<T>::findSecondOrderBasisFree(int n,const Vec& eval,MatT& evec) {
  MatT evecSecond;
  evecSecond.resize(evec.rows(),n*n);
  MatrixReplacement A(stiffness(_pos0).template cast<double>(),_mass.template cast<double>());
  for(int i=0,off=0; i<n; i++) {
    SMatT DS=DStiffness(_pos0,evec.col(i));
    A.attachEigenvector(double(eval[i]),evec.col(i).template cast<double>());
    for(int j=0; j<n; j++,off++) {
      Vec DSJ=DS*evec.col(j);
      Vec RHS=_mass*evec.col(i)*evec.col(i).dot(DSJ)-DSJ;
      Eigen::MINRES<MatrixReplacement,Eigen::Lower|Eigen::Upper,Eigen::IdentityPreconditioner> minres;
      minres.compute(A);
      if(minres.info()!=Eigen::Success)
        return false;
      Eigen::Matrix<double,-1,1> RHSD=RHS.template cast<double>();
      evecSecond.col(off)=minres.solve(RHSD).template cast<T>();
      std::cout << "MINRES error: " << minres.error() << " when solving for second order basis (" << i << "," << j << ")" << std::endl;
    }
  }
  evec=concatCol(evec,evecSecond);
  return true;
}
template <typename T>
bool FEMReducedSystem<T>::findSecondOrderBasisFixed(int n,MatT& evec) {
  MatT evecSecond;
  evecSecond.resize(evec.rows(),n*(n+1)/2);
  for(int i=0,off=0; i<n; i++) {
    SMatT DS=DStiffness(_pos0,evec.col(i));
    Eigen::SparseMatrix<double> stiff=stiffness(_pos0).template cast<double>();
    for(int j=0; j<=i; j++,off++) {
      Vec RHS=DS*evec.col(j);
      Eigen::MINRES<Eigen::SparseMatrix<double>,Eigen::Lower|Eigen::Upper,Eigen::IdentityPreconditioner> minres;
      minres.compute(stiff);
      if(minres.info()!=Eigen::Success)
        return false;
      Eigen::Matrix<double,-1,1> RHSD=RHS.template cast<double>();
      evecSecond.col(off)=minres.solve(RHSD).template cast<T>();
      std::cout << "MINRES error: " << minres.error() << " when solving for second order basis (" << i << "," << j << ")" << std::endl;
    }
  }
  evec=concatCol(evec,evecSecond);
  return true;
}
template <typename T>
typename FEMReducedSystem<T>::MatT FEMReducedSystem<T>::cellwiseForceVolume(const Vec& p) {
  if(_reducedEnergies.empty())
    for(std::shared_ptr<FEMEnergy<T>> e:_energies)
      if(std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(e))
        _reducedEnergies.push_back(std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(e));
  ASSERT_MSG((int)_reducedEnergies.size()==_mesh->nrC(),"Number of FEMStVKElasticEnergy<T> != number of cells!")
  MatT ret=MatT::Zero(_U.cols(),_mesh->nrC());
  OMP_PARALLEL_FOR_
  for(int i=0; i<_mesh->nrC(); i++) {
    typename FEMEnergy<T>::GFunc G=[&](int id,const Vec3T& GBlk) {
      ret.col(i)+=_U.block(id,0,3,_U.cols()).transpose()*GBlk;
    };
    _reducedEnergies[i]->eval(p,NULL,&G,NULL);
  }
  return ret;
}
template <typename T>
typename FEMReducedSystem<T>::SMatT FEMReducedSystem<T>::DStiffness(const Vec& p,const Vec& dir) const {
  SMatT ret;
  STrips trips;
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_energies.size(); i++)
    if(std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(_energies[i]))
      std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(_energies[i])->FEMStVKElasticEnergy<T>::DHDU(p,dir,trips);
  ret.resize(_mass.rows(),_mass.cols());
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
template <typename T>
typename FEMReducedSystem<T>::SMatT FEMReducedSystem<T>::stiffness(const Vec& p) const {
  SMatT ret;
  STrips trips;
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_energies.size(); i++)
    if(std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(_energies[i]))
      std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(_energies[i])->FEMStVKElasticEnergy<T>::eval(p,NULL,NULL,&trips);
    else if(std::dynamic_pointer_cast<FEMLinearElasticEnergy<T>>(_energies[i]))
      _energies[i]->eval(p,NULL,NULL,&trips);
  ret.resize(_mass.rows(),_mass.cols());
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
template <typename T>
bool FEMReducedSystem<T>::isLinearStiffness() const {
  bool nonlinear=false;
  for(int i=0; i<(int)_energies.size(); i++)
    if(std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(_energies[i]))
      nonlinear=true;
  return !nonlinear;
}
template class FEMReducedSystem<FLOAT>;
}
