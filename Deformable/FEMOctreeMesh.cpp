#include "FEMOctreeMesh.h"
#include "FEMMesh.h"
#include "Hexahedron.h"
#include <Utils/Interp.h>

namespace PHYSICSMOTION {
template <typename T>
FEMOctreeMesh<T>::FEMOctreeMesh(const FEMMesh<T>& body,int protectedLayers,int searchRange,int maxLv):_body(body) {
  std::cout << "Begin building octree #protectedLayers=" << protectedLayers << std::endl;
  _d.setConstant(std::dynamic_pointer_cast<Hexahedron<T>>(body.getC(0))->sideLen());

  //build vertex map
  for(int i=0; i<body.nrV(); i++) {
    Vec3T v=(body.getV(i)->operator()(body.pos0()).array()/_d.array()).matrix();
    Eigen::Matrix<int,3,1> c((int)round(v[0]),(int)round(v[1]),(int)round(v[2]));
    _vss[c]=body.getV(i);
  }

  //build base level
  _base=OctreeLevel();
  _base._cellSz=1;
  for(int i=0; i<body.nrC(); i++) {
    std::shared_ptr<Hexahedron<T>> hex=std::dynamic_pointer_cast<Hexahedron<T>>(body.getC(i));
    Vec3T v=(hex->V(0)->operator()(body.pos0()).array()/_d.array()).matrix();
    Eigen::Matrix<int,3,1> c((int)round(v[0]),(int)round(v[1]),(int)round(v[2]));
    _base._css.insert(c);
  }
  buildProtected(_base,protectedLayers);

  //coarsen
  int minSz=-1;
  for(int x=0; x<searchRange; x++)
    for(int y=0; y<searchRange; y++)
      for(int z=0; z<searchRange; z++) {
        int sz=build(protectedLayers,Eigen::Matrix<int,3,1>(x,y,z),maxLv);
        std::cout << "\tfound " << sz << " cells and " << _lss.size() << " levels using shift=(" << x << "," << y << "," << z << ") best=" << minSz << std::endl;
        if(minSz==-1||sz<minSz) {
          _shift=Eigen::Matrix<int,3,1>(x,y,z);
          minSz=sz;
        }
      }
  build(protectedLayers,_shift,maxLv);
  std::cout << "End building octree!" << std::endl;
}
template <typename T>
int FEMOctreeMesh<T>::build(int protectedLayers,const Eigen::Matrix<int,3,1>& shift,int maxLv) {
  //apply shift
  _lss.assign(1,OctreeLevel());
  for(const Eigen::Matrix<int,3,1>& ci:_base._css)
    _lss[0]._css.insert(ci+shift);
  for(const Eigen::Matrix<int,3,1>& c:_base._protected)
    _lss[0]._protected.insert(c+shift);
  _lss[0]._cellSz=_base._cellSz;

  //coarsen
  while((int)_lss.size()<maxLv) {
    OctreeLevel lv=coarsen(_lss.back());
    buildProtected(lv,protectedLayers);
    if(lv._css.empty())
      break;
    else _lss.push_back(lv);
  }

  //count cell
  int nrC=0;
  for(int i=0; i<(int)_lss.size(); i++)
    nrC+=(int)_lss[i]._css.size();
  return nrC;
}
template <typename T>
typename FEMOctreeMesh<T>::OctreeLevel FEMOctreeMesh<T>::coarsen(OctreeLevel& lv) const {
  OctreeLevel ret;
  ret._cellSz=lv._cellSz*2;
  for(const Eigen::Matrix<int,3,1>& ci:lv._css) {
    if(ci[0]%ret._cellSz!=0 || ci[1]%ret._cellSz!=0 || ci[2]%ret._cellSz!=0)
      continue;
    bool exists=true;
    for(int i=0; i<8; i++) {
      Eigen::Matrix<int,3,1> v=ci;
      if((i&1)==1)
        v[0]+=lv._cellSz;
      if((i&2)==2)
        v[1]+=lv._cellSz;
      if((i&4)==4)
        v[2]+=lv._cellSz;
      if(lv._css.find(v)==lv._css.end() || lv._protected.find(v)!=lv._protected.end()) {
        exists=false;
        break;
      }
    }
    if(exists)
      ret._css.insert(ci);
  }
  for(const Eigen::Matrix<int,3,1>& ci:ret._css) {
    for(int i=0; i<8; i++) {
      Eigen::Matrix<int,3,1> v=ci;
      if((i&1)==1)
        v[0]+=lv._cellSz;
      if((i&2)==2)
        v[1]+=lv._cellSz;
      if((i&4)==4)
        v[2]+=lv._cellSz;
      lv._css.erase(v);
    }
  }
  return ret;
}
template <typename T>
std::shared_ptr<FEMMesh<T>> FEMOctreeMesh<T>::getMesh() {
  std::shared_ptr<FEMMesh<T>> body(new FEMMesh<T>);
  //vertices
  _pos0Out.clear();
  _vssOut.clear();
  for(int l=(int)_lss.size()-1; l>=0; l--) {
    const OctreeLevel& lv=_lss[l];
    for(const Eigen::Matrix<int,3,1>& ci:lv._css) {
      const Eigen::Matrix<int,3,1> c=ci-_shift;
      for(int i=0; i<8; i++) {
        Eigen::Matrix<int,3,1> v=c;
        if((i&1)==1)
          v[0]+=lv._cellSz;
        if((i&2)==2)
          v[1]+=lv._cellSz;
        if((i&4)==4)
          v[2]+=lv._cellSz;
        if(_vssOut.find(v+_shift)!=_vssOut.end())
          continue;
        std::shared_ptr<FEMVertex<T>> V=getVert(v);
        body->_vertices.push_back(V);
        _vssOut[v+_shift]=V;
      }
    }
  }
  //indices
  for(int l=(int)_lss.size()-1; l>=0; l--) {
    OctreeLevel& lv=_lss[l];
    for(const Eigen::Matrix<int,3,1>& ci:lv._css) {
      const Eigen::Matrix<int,3,1> c=ci-_shift;
      std::shared_ptr<FEMVertex<T>> V[8];
      for(int i=0; i<8; i++) {
        Eigen::Matrix<int,3,1> v=c;
        if((i&1)==1)
          v[0]+=lv._cellSz;
        if((i&2)==2)
          v[1]+=lv._cellSz;
        if((i&4)==4)
          v[2]+=lv._cellSz;
        V[i]=_vssOut.find(v+_shift)->second;
      }
      std::shared_ptr<FEMCell<T>> cell(new Hexahedron<T>(_d[0]*lv._cellSz,V));
      body->_cells.push_back(cell);
    }
  }
  //assemble
  body->_pos0=Eigen::Map<Vec>(_pos0Out.data(),_pos0Out.size());
  for(std::shared_ptr<FEMVertex<T>> v:_body._surfaceVss)
    body->_surfaceVss.push_back(getVert(*v));
  body->_surfaceIss=_body._surfaceIss;
  body->_bvh=_body._bvh;
  return body;
}
template <typename T>
void FEMOctreeMesh<T>::buildProtected(OctreeLevel& lv,int protectedLayers) const {
  //build protected
  for(int i=0; i<protectedLayers; i++) {
    std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash> protectedV;
    for(const Eigen::Matrix<int,3,1>& ci:lv._css) {
      if(lv._protected.find(ci)!=lv._protected.end())
        continue;
      for(int x=-1; x<=1; x++)
        for(int y=-1; y<=1; y++)
          for(int z=-1; z<=1; z++)
            if(lv._css.find(ci+Eigen::Matrix<int,3,1>(x,y,z))==lv._css.end() ||
                lv._protected.find(ci+Eigen::Matrix<int,3,1>(x,y,z))!=lv._protected.end()) {
              protectedV.insert(ci);
              break;
            }
    }
    lv._protected.insert(protectedV.begin(),protectedV.end());
  }
}
template <typename T>
std::shared_ptr<FEMVertex<T>> FEMOctreeMesh<T>::getVert(const FEMVertex<T>& vert) const {
  Vec3T pos0=(vert(_body.pos0()).array()/_d.array()).matrix();
  Eigen::Matrix<int,3,1> c((int)floor(pos0[0]),(int)floor(pos0[1]),(int)floor(pos0[2]));
  for(int l=(int)_lss.size()-1; l>=0; l--) {
    const OctreeLevel& lv=_lss[l];
    Eigen::Matrix<int,3,1> cl=c+_shift;
    for(int d=0; d<3; d++)
      while((cl[d]%lv._cellSz)!=0)
        cl[d]--;
    //check exists
    if(lv._css.find(cl)!=lv._css.end()) {
      Vec3T posCorner=(cl-_shift).template cast<T>();
      Vec3T frac=(pos0-posCorner)/lv._cellSz;
      //coefficients
      T coef[8];
      stencil3D(coef,frac[0],frac[1],frac[2]);
      std::shared_ptr<FEMVertex<T>> ret(new FEMVertex<T>());
      for(int i=0; i<8; i++) {
        Eigen::Matrix<int,3,1> v=cl;
        if((i&1)==1)
          v[0]+=lv._cellSz;
        if((i&2)==2)
          v[1]+=lv._cellSz;
        if((i&4)==4)
          v[2]+=lv._cellSz;
        *ret+=*(_vssOut.find(v)->second)*coef[i];
      }
      return ret;
    }
  }
  ASSERT_MSG(false,"Failed when calling getVert()!")
  return std::shared_ptr<FEMVertex<T>>();
}
template <typename T>
std::shared_ptr<FEMVertex<T>> FEMOctreeMesh<T>::getVert(const Eigen::Matrix<int,3,1>& c) {
  for(int l=(int)_lss.size()-1; l>=0; l--) {
    const OctreeLevel& lv=_lss[l];
    std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>> neighbor,tmp;
    {
      //find index
      Eigen::Matrix<int,3,1> cl=c+_shift,cl0=cl;
      for(int d=0; d<3; d++)
        while((cl[d]%lv._cellSz)!=0)
          cl[d]--;
      neighbor.push_back(cl);
      for(int d=0; d<3; d++)
        if(cl[d]==cl0[d]) {
          tmp=neighbor;
          for(Eigen::Matrix<int,3,1>& n:tmp)
            neighbor.push_back(n-Eigen::Matrix<int,3,1>::Unit(d)*lv._cellSz);
        }
    }
    //check exists
    for(const Eigen::Matrix<int,3,1>& n:neighbor)
      if(lv._css.find(n)!=lv._css.end()) {
        Vec3T frac=(c+_shift-n).template cast<T>()/lv._cellSz;
        //check corner
        bool isCorner=true;
        for(int d=0; d<3; d++)
          if(frac[d]!=0&&frac[d]!=1)
            isCorner=false;
        if(isCorner) {
          std::shared_ptr<FEMVertex<T>> ret(new FEMVertex<T>((int)_pos0Out.size()));
          for(int d=0; d<3; d++)
            _pos0Out.push_back(c[d]*_d[d]);
          return ret;
        } else {
          //coefficients
          T coef[8];
          stencil3D(coef,frac[0],frac[1],frac[2]);
          std::shared_ptr<FEMVertex<T>> ret(new FEMVertex<T>());
          for(int i=0; i<8; i++) {
            Eigen::Matrix<int,3,1> v=n;
            if((i&1)==1)
              v[0]+=lv._cellSz;
            if((i&2)==2)
              v[1]+=lv._cellSz;
            if((i&4)==4)
              v[2]+=lv._cellSz;
            *ret+=*(_vssOut.find(v)->second)*coef[i];
          }
          return ret;
        }
      }
  }
  ASSERT_MSG(false,"Failed when calling getVert()!")
  return std::shared_ptr<FEMVertex<T>>();
}
template class FEMOctreeMesh<FLOAT>;
}
