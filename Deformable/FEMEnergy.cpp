#include "FEMEnergy.h"
#include "GaussLegendre.h"
#include <Utils/IO.h>
#include <Utils/RotationUtils.h>
#include <Utils/CrossSpatialUtils.h>
#include <unsupported/Eigen/KroneckerProduct>

namespace PHYSICSMOTION {
template <typename T>
bool FEMEnergy<T>::eval(const Vec& x,ParallelMatrix<T>* E,ParallelMatrix<Vec>* G,ParallelVector<Eigen::Triplet<T,int>>* H,const FEMEnergyContext<T>* ctx) const {
  EFunc EF=[&](T val)->void{
    E->getValueI()+=val;
  };
  GFunc GF=[&](int off,const Vec3T& val)->void{
    G->getMatrixI().template segment<3>(off)+=val;
  };
  HFunc HF=[&](int offr,int offc,const Mat3T& val)->void{
    addBlock(*H,offr,offc,val);
  };
  return eval(x,E?&EF:NULL,G?&GF:NULL,H?&HF:NULL,ctx);
}
template <typename T>
int FEMEnergy<T>::polyOrder() const {
  return std::numeric_limits<int>::max();
}
//FEMPointConstraintEnergy
template <typename T>
FEMPointConstraintEnergy<T>::FEMPointConstraintEnergy() {}
template <typename T>
FEMPointConstraintEnergy<T>::FEMPointConstraintEnergy(const Vec3T& pos,T coef):_pos(pos),_coef(coef) {}
template <typename T>
bool FEMPointConstraintEnergy<T>::read(std::istream& is,IOData* dat) {
  readBinaryData(_vert,is,dat);
  readBinaryData(_pos,is);
  readBinaryData(_coef,is);
  return is.good();
}
template <typename T>
bool FEMPointConstraintEnergy<T>::write(std::ostream& os,IOData* dat) const {
  writeBinaryData(_vert,os,dat);
  writeBinaryData(_pos,os);
  writeBinaryData(_coef,os);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> FEMPointConstraintEnergy<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new FEMPointConstraintEnergy<T>);
}
template <typename T>
std::string FEMPointConstraintEnergy<T>::type() const {
  return typeid(FEMPointConstraintEnergy<T>).name();
}
template <typename T>
std::shared_ptr<FEMEnergy<T>> FEMPointConstraintEnergy<T>::toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const {
  std::shared_ptr<FEMPointConstraintEnergy<T>> ret(new FEMPointConstraintEnergy<T>);
  for(std::pair<std::shared_ptr<FEMVertex<T>>,T> v:_vert)
    ret->_vert[vertexMapper(v.first)]=v.second;
  ret->_pos=_pos;
  ret->_coef=_coef*coef;
  return ret;
}
template <typename T>
bool FEMPointConstraintEnergy<T>::eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>*) const {
  Vec3T p=Vec3T::Zero();
  for(std::pair<std::shared_ptr<FEMVertex<T>>,T> v:_vert)
    p+=v.first->operator()(mapCV(x))*v.second;
  Vec3T g=(p-_pos)*_coef;
  if(E)
    (*E)(g.dot(p-_pos)/2);
  if(G)
    for(std::pair<std::shared_ptr<FEMVertex<T>>,T> v:_vert)
      for(const std::pair<int,T>& p:v.first->jacobian())
        (*G)(p.first,g*p.second*v.second);
  if(H)
    for(std::pair<std::shared_ptr<FEMVertex<T>>,T> v:_vert)
      for(const std::pair<int,T>& p:v.first->jacobian())
        for(std::pair<std::shared_ptr<FEMVertex<T>>,T> v2:_vert)
          for(const std::pair<int,T>& p2:v2.first->jacobian())
            (*H)(p.first,p2.first,Mat3T::Identity()*(p.second*v.second*p2.second*v2.second*_coef));
  return true;
}
template <typename T>
int FEMPointConstraintEnergy<T>::polyOrder() const {
  return 2;
}
template <typename T>
void FEMPointConstraintEnergy<T>::add(std::shared_ptr<FEMVertex<T>> vert,T coef) {
  if(_vert.find(vert)==_vert.end())
    _vert[vert]=coef;
  else _vert[vert]+=coef;
}
//FEMConstantForceEnergy
template <typename T>
FEMConstantForceEnergy<T>::FEMConstantForceEnergy() {}
template <typename T>
FEMConstantForceEnergy<T>::FEMConstantForceEnergy(std::shared_ptr<FEMCell<T>> cell,const Vec3T& f):_cell(cell) {
  MatT m=cell->massMatrix();
  _f.resize(m.rows()*3);
  for(int i=0,j=0; i<m.rows(); i++,j+=3)
    _f.template segment<3>(j)=f;
  _f=kronecker<T,0,int>(toSparse<T,0,int,MatT>(m),3)*_f;
}
template <typename T>
bool FEMConstantForceEnergy<T>::read(std::istream& is,IOData* dat) {
  readBinaryData(_cell,is,dat);
  readBinaryData(_f,is);
  return is.good();
}
template <typename T>
bool FEMConstantForceEnergy<T>::write(std::ostream& os,IOData* dat) const {
  writeBinaryData(_cell,os,dat);
  writeBinaryData(_f,os);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> FEMConstantForceEnergy<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new FEMConstantForceEnergy<T>());
}
template <typename T>
std::string FEMConstantForceEnergy<T>::type() const {
  return typeid(FEMConstantForceEnergy<T>()).name();
}
template <typename T>
std::shared_ptr<FEMEnergy<T>> FEMConstantForceEnergy<T>::toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const {
  std::shared_ptr<FEMConstantForceEnergy<T>> ret(new FEMConstantForceEnergy<T>);
  ret->_cell=_cell->toReducedCell(vertexMapper);
  ret->_f=_f*coef;
  return ret;
}
template <typename T>
bool FEMConstantForceEnergy<T>::eval(const Vec& x,EFunc* E,GFunc* G,HFunc*,const FEMEnergyContext<T>*) const {
  if(E)
    for(int i=0,j=0; i<_f.size(); i+=3,j++)
      (*E)(-_cell->V(j)->operator()(mapCV(x)).dot(_f.template segment<3>(i)));
  if(G)
    for(int i=0,j=0; i<_f.size(); i+=3,j++)
      for(const std::pair<int,T>& p:_cell->V(j)->jacobian())
        (*G)(p.first,-_f.template segment<3>(i)*p.second);
  return true;
}
template <typename T>
int FEMConstantForceEnergy<T>::polyOrder() const {
  return 1;
}
//FEMLinearElasticEnergy
template <typename T>
FEMLinearElasticEnergy<T>::FEMLinearElasticEnergy() {}
template <typename T>
FEMLinearElasticEnergy<T>::FEMLinearElasticEnergy(std::shared_ptr<FEMCell<T>> cell,VecCM pos0,T lambda,T mu):_cell(cell) {
  //H
  Mat9T H=computeH(lambda,mu);
  _H.resize(0,0);
  for(const std::pair<Vec3T,T>& coef:cell->stencilIntegrateQuadratic()) {
    MatT DFDV=cell->getDFDV(coef.first).toDense();
    if(_H.size()==0)
      _H.setZero(DFDV.cols(),DFDV.cols());
    _H+=DFDV.transpose()*(H*DFDV)*coef.second;
  }
  //_pos0
  _pos0=Vec::Zero(_H.rows());
  for(int i=0,j=0; i<_pos0.size(); i+=3,j++)
    _pos0.template segment<3>(i)=_cell->V(j)->operator()(pos0);
}
template <typename T>
bool FEMLinearElasticEnergy<T>::read(std::istream& is,IOData* dat) {
  readBinaryData(_cell,is,dat);
  readBinaryData(_pos0,is);
  readBinaryData(_H,is);
  return is.good();
}
template <typename T>
bool FEMLinearElasticEnergy<T>::write(std::ostream& os,IOData* dat) const {
  writeBinaryData(_cell,os,dat);
  writeBinaryData(_pos0,os);
  writeBinaryData(_H,os);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> FEMLinearElasticEnergy<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new FEMLinearElasticEnergy<T>);
}
template <typename T>
std::string FEMLinearElasticEnergy<T>::type() const {
  return typeid(FEMLinearElasticEnergy<T>).name();
}
template <typename T>
std::shared_ptr<FEMEnergy<T>> FEMLinearElasticEnergy<T>::toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const {
  std::shared_ptr<FEMLinearElasticEnergy<T>> ret(new FEMLinearElasticEnergy<T>);
  ret->_cell=_cell->toReducedCell(vertexMapper);
  ret->_pos0=_pos0;
  ret->_H=_H*coef;
  return ret;
}
template <typename T>
bool FEMLinearElasticEnergy<T>::eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>*) const {
  Vec v=-_pos0;
  for(int i=0,j=0; i<v.size(); i+=3,j++)
    v.template segment<3>(i)+=_cell->V(j)->operator()(mapCV(x));
  Vec g=_H*v;
  if(E)
    (*E)(g.dot(v)/2);
  if(G)
    for(int i=0,j=0; i<g.size(); i+=3,j++)
      for(const std::pair<int,T>& p:_cell->V(j)->jacobian())
        (*G)(p.first,g.template segment<3>(i)*p.second);
  if(H)
    for(int i=0,j=0; i<g.size(); i+=3,j++)
      for(const std::pair<int,T>& p:_cell->V(j)->jacobian())
        for(int i2=0,j2=0; i2<g.size(); i2+=3,j2++)
          for(const std::pair<int,T>& p2:_cell->V(j2)->jacobian())
            (*H)(p.first,p2.first,_H.template block<3,3>(i,i2)*p.second*p2.second);
  return true;
}
template <typename T>
int FEMLinearElasticEnergy<T>::polyOrder() const {
  return 2;
}
template <typename T>
typename FEMLinearElasticEnergy<T>::Mat9T FEMLinearElasticEnergy<T>::computeH(T lambda,T mu) {
  Mat9T H=Mat9T::Zero();
  for(int r=0; r<3; r++) {
    H(r*4,r*4)+=mu*2;
    for(int c=0; c<3; c++)
      H(r*4,c*4)+=lambda;
  }
  Mat3T mu1=Mat3T::Zero();
  mu1(0,0)=mu1(2,2)=mu1(0,2)=mu1(2,0)=mu1(1,1)=mu;
  H.template block<3,3>(1,1)=mu1;
  H.template block<3,3>(5,5)=mu1;
  Mat3T mu2=Mat3T::Zero();
  mu2(1,1)=mu;
  H.template block<3,3>(1,5)=mu2;
  H.template block<3,3>(5,1)=mu2;
  return H;
}
//FEMStVKElasticEnergy
template <typename T>
FEMStVKElasticEnergy<T>::FEMStVKElasticEnergy() {}
template <typename T>
FEMStVKElasticEnergy<T>::FEMStVKElasticEnergy(std::shared_ptr<FEMCell<T>> cell,T lambda,T mu):_cell(cell) {
  _coefs=cell->stencilIntegrateQuartic();
  _lambda=lambda;
  _mu=mu;
  _nR=cell->getDFDV(Vec3T::Zero()).cols();
}
template <typename T>
bool FEMStVKElasticEnergy<T>::read(std::istream& is,IOData* dat) {
  readBinaryData(_cell,is,dat);
  readBinaryData(_coefs,is);
  readBinaryData(_lambda,is);
  readBinaryData(_mu,is);
  readBinaryData(_nR,is);
  return is.good();
}
template <typename T>
bool FEMStVKElasticEnergy<T>::write(std::ostream& os,IOData* dat) const {
  writeBinaryData(_cell,os,dat);
  writeBinaryData(_coefs,os);
  writeBinaryData(_lambda,os);
  writeBinaryData(_mu,os);
  writeBinaryData(_nR,os);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> FEMStVKElasticEnergy<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new FEMStVKElasticEnergy<T>);
}
template <typename T>
std::string FEMStVKElasticEnergy<T>::type() const {
  return typeid(FEMStVKElasticEnergy<T>).name();
}
template <typename T>
std::shared_ptr<FEMEnergy<T>> FEMStVKElasticEnergy<T>::toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const {
  std::shared_ptr<FEMStVKElasticEnergy<T>> ret(new FEMStVKElasticEnergy<T>);
  ret->_cell=_cell->toReducedCell(vertexMapper);
  ret->_coefs=_coefs;
  ret->_lambda=_lambda*coef;
  ret->_mu=_mu*coef;
  ret->_nR=_nR;
  return ret;
}
template <typename T>
bool FEMStVKElasticEnergy<T>::eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>*) const {
  Vec v=Vec::Zero(_nR);
  for(int i=0,j=0; i<v.size(); i+=3,j++)
    v.template segment<3>(i)=_cell->V(j)->operator()(mapCV(x));
  //compute material
  Mat9T H0,fft,fft3x3T;
  Mat3T F,FTF,P,P0,e;
  SMatT DFDX;
  MatT HSum;
  Vec GSum;
  T ESum=0;
  if(G)
    GSum.setZero(_nR);
  if(H)
    HSum.setZero(_nR,_nR);
  for(const std::pair<Vec3T,T>& coef:_coefs) {
    DFDX=_cell->getDFDV(coef.first);
    Eigen::Map<Vec9T>(F.data())=DFDX*v;
    e=(F*F.transpose()-Mat3T::Identity())/2;
    T tre=e.trace();
    if(E)
      ESum+=(_mu*e.squaredNorm()+_lambda/2*tre*tre)*coef.second;
    if(G||H) {
      P0=2*_mu*e+_lambda*tre*Mat3T::Identity();
      P=P0*F;
      if(G)
        GSum+=DFDX.transpose()*Eigen::Map<Vec9T>(P.data())*coef.second;
    }
    if(H) {
      H0.setZero();
      fft=Eigen::Map<Vec9T>(F.data())*Eigen::Map<Vec9T>(F.data()).transpose();
      for(int r=0; r<3; r++)
        for(int c=0; c<3; c++)
          fft3x3T.template block<3,3>(r*3,c*3)=fft.template block<3,3>(c*3,r*3);
      H0.template block<3,3>(0,0)=H0.template block<3,3>(3,3)=H0.template block<3,3>(6,6)=P0;
      H0+=fft*_lambda+Eigen::kroneckerProduct(FTF=F.transpose()*F,Mat3T::Identity()*_mu).eval()+fft3x3T*_mu;
      HSum+=DFDX.transpose()*(H0*DFDX)*coef.second;
    }
  }
  //assemble
  if(E)
    (*E)(ESum);
  if(G)
    for(int i=0,j=0; i<GSum.size(); i+=3,j++)
      for(const std::pair<int,T>& p:_cell->V(j)->jacobian())
        (*G)(p.first,GSum.template segment<3>(i)*p.second);
  if(H)
    for(int i=0,j=0; i<HSum.rows(); i+=3,j++)
      for(const std::pair<int,T>& p:_cell->V(j)->jacobian())
        for(int i2=0,j2=0; i2<HSum.rows(); i2+=3,j2++)
          for(const std::pair<int,T>& p2:_cell->V(j2)->jacobian())
            (*H)(p.first,p2.first,HSum.template block<3,3>(i,i2)*p.second*p2.second);
  return true;
}
template <typename T>
int FEMStVKElasticEnergy<T>::polyOrder() const {
  return 4;
}
template <typename T>
std::shared_ptr<FEMCell<T>> FEMStVKElasticEnergy<T>::getCell() const {
  return _cell;
}
template <typename T>
void FEMStVKElasticEnergy<T>::DHDU(const Vec& x,const Vec& dir,ParallelVector<Eigen::Triplet<T,int>>& H) const {
  Mat9T H0;
  SMatT DFDX;
  Mat3T e,F,FTF,DeDu,DP0Du;
  Vec9T DFDu;
  Eigen::Matrix<T,9,9> fft,fft3x3T;
  MatT HSum=MatT::Zero(_nR,_nR);
  Vec cu=Vec::Zero(_nR),v=Vec::Zero(_nR);
  for(int i=0,j=0; i<_nR; i+=3,j++) {
    cu.template segment<3>(i)=_cell->V(j)->operator[](mapCV(dir));
    v.template segment<3>(i)=_cell->V(j)->operator()(mapCV(x));
  }
  for(const std::pair<Vec3T,T>& coef:_coefs) {
    DFDX=_cell->getDFDV(coef.first);
    Eigen::Map<Vec9T>(F.data())=DFDX*v;
    e=(F*F.transpose()-Mat3T::Identity())/2;
    DFDu=DFDX*cu;
    DeDu=(F*Eigen::Map<const Mat3T>(DFDu.data()).transpose()+Eigen::Map<const Mat3T>(DFDu.data())*F.transpose())/2;
    //part1: DP0
    H0.setZero();
    DP0Du=2*_mu*DeDu+_lambda*DeDu.trace()*Mat3T::Identity();
    H0.template block<3,3>(0,0)=H0.template block<3,3>(3,3)=H0.template block<3,3>(6,6)=DP0Du;
    //part2: DDP0
    fft=DFDu*Eigen::Map<Vec9T>(F.data()).transpose()+Eigen::Map<Vec9T>(F.data())*DFDu.transpose();
    for(int r=0; r<3; r++)
      for(int c=0; c<3; c++)
        fft3x3T.template block<3,3>(r*3,c*3)=fft.template block<3,3>(c*3,r*3);
    H0+=fft*_lambda+Eigen::kroneckerProduct(FTF=Eigen::Map<const Mat3T>(DFDu.data()).transpose()*F+F.transpose()*Eigen::Map<const Mat3T>(DFDu.data()),Mat3T::Identity()*_mu).eval()+fft3x3T*_mu;
    HSum+=DFDX.transpose()*(H0*DFDX)*coef.second;
  }
  //assemble
  for(int i=0,j=0; i<HSum.rows(); i+=3,j++)
    for(const std::pair<int,T>& p:_cell->V(j)->jacobian())
      for(int i2=0,j2=0; i2<HSum.cols(); i2+=3,j2++)
        for(const std::pair<int,T>& p2:_cell->V(j2)->jacobian())
          addBlock(H,p.first,p2.first,HSum.template block<3,3>(i,i2)*p.second*p2.second);
}
template <typename T>
const std::vector<std::pair<typename FEMStVKElasticEnergy<T>::Vec3T,T>>& FEMStVKElasticEnergy<T>::coefs() const {
  return _coefs;
}
template <typename T>
T FEMStVKElasticEnergy<T>::lambda() const {
  return _lambda;
}
template <typename T>
T FEMStVKElasticEnergy<T>::mu() const {
  return _mu;
}
//FEMCorotatedEnergy
template <typename T>
FEMCorotatedElasticEnergy<T>::FEMCorotatedElasticEnergy() {}
template <typename T>
FEMCorotatedElasticEnergy<T>::FEMCorotatedElasticEnergy(std::shared_ptr<FEMCell<T>> cell,T lambda,T mu):FEMStVKElasticEnergy<T>(cell,lambda,mu) {
  Mat9T H=FEMLinearElasticEnergy<T>::computeH(lambda,mu);
  for(const std::pair<Vec3T,T>& coef:cell->stencilIntegrateQuadratic()) {
    MatT DFDV=cell->getDFDV(coef.first).toDense();
    _H.push_back(DFDV.transpose()*(H*DFDV)*coef.second);
  }
}
template <typename T>
bool FEMCorotatedElasticEnergy<T>::read(std::istream& is,IOData* dat) {
  FEMStVKElasticEnergy<T>::read(is,dat);
  readBinaryData(_H,is);
  return is.good();
}
template <typename T>
bool FEMCorotatedElasticEnergy<T>::write(std::ostream& os,IOData* dat) const {
  FEMStVKElasticEnergy<T>::write(os,dat);
  writeBinaryData(_H,os);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> FEMCorotatedElasticEnergy<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new FEMCorotatedElasticEnergy<T>);
}
template <typename T>
std::string FEMCorotatedElasticEnergy<T>::type() const {
  return typeid(FEMCorotatedElasticEnergy).name();
}
template <typename T>
std::shared_ptr<FEMEnergy<T>> FEMCorotatedElasticEnergy<T>::toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const {
  std::shared_ptr<FEMCorotatedElasticEnergy<T>> ret(new FEMCorotatedElasticEnergy<T>);
  ret->_cell=_cell->toReducedCell(vertexMapper);
  ret->_coefs=_coefs;
  ret->_lambda=_lambda*coef;
  ret->_mu=_mu*coef;
  ret->_nR=_nR;
  ret->_H=_H;
  for(auto& H:ret->_H)
    H*=coef;
  return ret;
}
template <typename T>
bool FEMCorotatedElasticEnergy<T>::eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>*) const {
  Vec v=Vec::Zero(_nR),DFDXR;
  for(int i=0,j=0; i<v.size(); i+=3,j++)
    v.template segment<3>(i)=_cell->V(j)->operator()(mapCV(x));
  //compute material
  Mat3T F,R,e,P,S;
  SMatT DFDX;
  MatT HSum;
  Vec GSum,RDFDX;
  T ESum=0,tre;
  int HOff=0;
  if(G)
    GSum.setZero(_nR);
  if(H)
    HSum.setZero(_nR,_nR);
  for(const std::pair<Vec3T,T>& coef:_coefs) {
    DFDX=_cell->getDFDV(coef.first);
    Eigen::Map<Vec9T>(F.data())=DFDX*v;
    computeRFast(F,R);
    S=R.transpose()*F;
    e=(S+S.transpose())/2-Mat3T::Identity();
    tre=e.trace();
    if(E)
      ESum+=(_mu*e.squaredNorm()+_lambda/2*tre*tre)*coef.second;
    if(G) {
      P=_mu*R*(e+e.transpose())+_lambda*tre*R;
      GSum+=DFDX.transpose()*Eigen::Map<Vec9T>(P.data())*coef.second;
    }
    if(H) {
      for(int i=0,j=0; i<HSum.rows(); i+=3,j++)
        for(int i2=0,j2=0; i2<HSum.rows(); i2+=3,j2++)
          HSum.template block<3,3>(i,i2)+=R*_H[HOff].template block<3,3>(i,i2)*R.transpose();
      HOff++;
    }
  }
  //assemble
  if(E)
    (*E)(ESum);
  if(G)
    for(int i=0,j=0; i<GSum.size(); i+=3,j++)
      for(const std::pair<int,T>& p:_cell->V(j)->jacobian())
        (*G)(p.first,GSum.template segment<3>(i)*p.second);
  if(H)
    for(int i=0,j=0; i<HSum.rows(); i+=3,j++)
      for(const std::pair<int,T>& p:_cell->V(j)->jacobian())
        for(int i2=0,j2=0; i2<HSum.rows(); i2+=3,j2++)
          for(const std::pair<int,T>& p2:_cell->V(j2)->jacobian())
            (*H)(p.first,p2.first,HSum.template block<3,3>(i,i2)*p.second*p2.second);
  return true;
}
template <typename T>
int FEMCorotatedElasticEnergy<T>::polyOrder() const {
  return std::numeric_limits<int>::max();
}
template <typename T>
void FEMCorotatedElasticEnergy<T>::computeR(const Mat3T& A,Mat3T& R) const {
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double,3,3>> eig((A.transpose()*A).template cast<double>(),Eigen::ComputeEigenvectors);
  Mat3T V=eig.eigenvectors().template cast<T>();
  Vec3T S=eig.eigenvalues().template cast<T>();
  const T detV=V.determinant();
  if(detV<0.0) {
    T minLambda=DBL_MAX;
    unsigned char pos=0;
    for(unsigned char l=0; l<3; l++) {
      if(S[l]<minLambda) {
        minLambda=S[l];
        pos=l;
      }
    }
    V(0,pos)=-V(0,pos);
    V(1,pos)=-V(1,pos);
    V(2,pos)=-V(2,pos);
  }
  if(S[0]<0.0f)
    S[0]=0.0f;
  if(S[1]<0.0f)
    S[1]=0.0f;
  if(S[2]<0.0f)
    S[2]=0.0f;
  Vec3T sigma;
  sigma[0]=sqrt(S[0]);
  sigma[1]=sqrt(S[1]);
  sigma[2]=sqrt(S[2]);
  unsigned char chk=0;
  unsigned char pos=0;
  Mat3T U;
  for(unsigned char l=0; l<3; l++)
    if(fabs(sigma[l])<1.0e-4) { //we opt to prefer double precision
      pos=l;
      chk++;
    }
  if(chk>0) {
    if(chk>1) {
      U.setIdentity();
    } else {
      U=A*V;
      for(unsigned char l=0; l<3; l++)
        if(l!=pos)
          for(unsigned char m=0; m<3; m++)
            U(m,l)*=1.0f/sigma[l];
      Vec3T v[2];
      unsigned char index=0;
      for(unsigned char l=0; l<3; l++)
        if(l!=pos)
          v[index++]=Vec3T(U(0,l),U(1,l),U(2,l));
      Vec3T vec=v[0].cross(v[1]);
      vec.normalize();
      U(0,pos)=vec[0];
      U(1,pos)=vec[1];
      U(2,pos)=vec[2];
    }
  } else {
    Vec3T sigmaInv(1.0/sigma[0],1.0/sigma[1],1.0/sigma[2]);
    U=A*V;
    for(unsigned char l=0; l<3; l++)
      for(unsigned char m=0; m<3; m++)
        U(m,l)*=sigmaInv[l];
  }
  const T detU=U.determinant();
  if(detU<0.0) {
    T minLambda=DBL_MAX;
    unsigned char pos=0;
    for(unsigned char l=0; l<3; l++)
      if(sigma[l]<minLambda) {
        minLambda=sigma[l];
        pos=l;
      }
    sigma[pos]=-sigma[pos];
    U(0,pos)=-U(0,pos);
    U(1,pos)=-U(1,pos);
    U(2,pos)=-U(2,pos);
  }
  R=U*V.transpose();
}
template <typename T>
void FEMCorotatedElasticEnergy<T>::computeRFast(const Mat3T& A,Mat3T& R) const {
  static const unsigned int maxIter=100;
  QuatT q=QuatT::Identity();
  for(unsigned int iter=0; iter<maxIter; iter++) {
    R=q.matrix();
    Vec3T omega=(R.col(0).cross(A.col(0))+R.col(1).cross(A.col(1))+R.col(2).cross(A.col(2)))*
                (1.0/fabs(R.col(0).dot(A.col(0))+R.col(1).dot(A.col(1))+R.col(2).dot(A.col(2)))+Epsilon<T>::defaultEps());
    T w=omega.norm();
    if(w<Epsilon<T>::defaultEps())
      break;
    q=QuatT(Eigen::AngleAxis<T>(w,(1.0/w)*omega))*q;
    q.normalize();
  }
  R=q.matrix();
}
//FEMNonHookeanEnergy
template <typename T>
FEMNonHookeanElasticEnergy<T>::FEMNonHookeanElasticEnergy() {}
template <typename T>
FEMNonHookeanElasticEnergy<T>::FEMNonHookeanElasticEnergy(std::shared_ptr<FEMCell<T>> cell,T lambda,T mu):FEMStVKElasticEnergy<T>(cell,lambda,mu) {}
template <typename T>
std::shared_ptr<SerializableBase> FEMNonHookeanElasticEnergy<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new FEMNonHookeanElasticEnergy<T>);
}
template <typename T>
std::string FEMNonHookeanElasticEnergy<T>::type() const {
  return typeid(FEMNonHookeanElasticEnergy<T>).name();
}
template <typename T>
std::shared_ptr<FEMEnergy<T>> FEMNonHookeanElasticEnergy<T>::toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const {
  std::shared_ptr<FEMNonHookeanElasticEnergy<T>> ret(new FEMNonHookeanElasticEnergy<T>);
  ret->_cell=_cell->toReducedCell(vertexMapper);
  ret->_coefs=_coefs;
  ret->_lambda=_lambda*coef;
  ret->_mu=_mu*coef;
  ret->_nR=_nR;
  return ret;
}
template <typename T>
bool FEMNonHookeanElasticEnergy<T>::eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>*) const {
  Vec v=Vec::Zero(_nR);
  for(int i=0,j=0; i<v.size(); i+=3,j++)
    v.template segment<3>(i)=_cell->V(j)->operator()(mapCV(x));
  //compute material
  SMatT DFDX;
  Mat9T H0,fft;
  Mat3T F,invFT,P;
  T I1,sqrtI3,sqrtI3Log;
  MatT HSum;
  Vec GSum;
  T ESum=0;
  if(G)
    GSum.setZero(_nR);
  if(H)
    HSum.setZero(_nR,_nR);
  for(const std::pair<Vec3T,T>& coef:_coefs) {
    DFDX=_cell->getDFDV(coef.first);
    Eigen::Map<Vec9T>(F.data())=DFDX*v;
    I1=(F*F.transpose()).trace();
    sqrtI3=F.determinant();
    if(sqrtI3<=0.0)
      return false;
    sqrtI3Log=log(sqrtI3);
    if(E)
      ESum+=(_mu/2*(I1-2*sqrtI3Log-3)+_lambda/2*sqrtI3Log*sqrtI3Log)*coef.second;
    if(G || H) {
      invFT=F.inverse().transpose();
      P=_mu*(F-invFT)+_lambda*sqrtI3Log*invFT;
      GSum+=(DFDX.transpose()*Eigen::Map<Vec9T>(P.data()))*coef.second;
    }
    if(H) {
      H0.setZero();
      H0.diagonal()=Vec::Constant(9,_mu);
      fft=Eigen::Map<Vec9T>(invFT.data())*Eigen::Map<Vec9T>(invFT.data()).transpose();
      for(int r=0; r<3; r++)
        for(int c=0; c<3; c++)
          H0.template block<3,3>(r*3,c*3)+=fft.template block<3,3>(c*3,r*3)*(_mu-_lambda*sqrtI3Log);
      H0+=fft*_lambda;
      HSum+=(DFDX.transpose()*(H0*DFDX))*coef.second;
    }
  }
  //assemble
  if(E)
    (*E)(ESum);
  if(G)
    for(int i=0,j=0; i<GSum.size(); i+=3,j++)
      for(const std::pair<int,T>& p:_cell->V(j)->jacobian())
        (*G)(p.first,GSum.template segment<3>(i)*p.second);
  if(H)
    for(int i=0,j=0; i<HSum.rows(); i+=3,j++)
      for(const std::pair<int,T>& p:_cell->V(j)->jacobian())
        for(int i2=0,j2=0; i2<HSum.rows(); i2+=3,j2++)
          for(const std::pair<int,T>& p2:_cell->V(j2)->jacobian())
            (*H)(p.first,p2.first,HSum.template block<3,3>(i,i2)*p.second*p2.second);
  return true;
}
template <typename T>
int FEMNonHookeanElasticEnergy<T>::polyOrder() const {
  return std::numeric_limits<int>::max();
}
//FEMFluidEnergy
template <typename T>
FEMFluidEnergy<T>::FEMFluidEnergy() {}
template <typename T>
FEMFluidEnergy<T>::FEMFluidEnergy(std::shared_ptr<FEMVertex<T>> V[3],T level,T rho,T G,T dragMult,int deg) {
  setParameters(level,rho,G,dragMult,deg);
  _V[0]=V[0];
  _V[1]=V[1];
  _V[2]=V[2];
}
template <typename T>
bool FEMFluidEnergy<T>::read(std::istream& is,IOData* dat) {
  readBinaryData(_V[0],is,dat);
  readBinaryData(_V[1],is,dat);
  readBinaryData(_V[2],is,dat);
  readBinaryData(_coefs,is);
  readBinaryData(_level,is);
  readBinaryData(_rho,is);
  readBinaryData(_G,is);
  readBinaryData(_rhoG,is);
  readBinaryData(_dragMult,is);
  return is.good();
}
template <typename T>
bool FEMFluidEnergy<T>::write(std::ostream& os,IOData* dat) const {
  writeBinaryData(_V[0],os,dat);
  writeBinaryData(_V[1],os,dat);
  writeBinaryData(_V[2],os,dat);
  writeBinaryData(_coefs,os);
  writeBinaryData(_level,os);
  writeBinaryData(_rho,os);
  writeBinaryData(_G,os);
  writeBinaryData(_rhoG,os);
  writeBinaryData(_dragMult,os);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> FEMFluidEnergy<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new FEMFluidEnergy<T>);
}
template <typename T>
std::string FEMFluidEnergy<T>::type() const {
  return typeid(FEMFluidEnergy<T>).name();
}
template <typename T>
FEMEnergyContext<T> FEMFluidEnergy<T>::updateFluid(const Vec& pNN,const Vec& pN,T dt) const {
  FEMEnergyContext<T> ctx;
  ctx._dt=dt;
  for(int d=0; d<3; d++) {
    ctx._vN[d]=_V[d]->operator()(mapCV(pN));
    ctx._vNN[d]=_V[d]->operator()(mapCV(pNN));
    ctx._vRelN[d]=-(ctx._vN[d]-ctx._vNN[d])/ctx._dt;
  }
  ctx._n=(ctx._vN[1]-ctx._vN[0]).cross(ctx._vN[2]-ctx._vN[0])/2;
  ctx._A=sqrt(ctx._n.squaredNorm()+Epsilon<T>::defaultEps());
  ctx._nUnit=ctx._n/ctx._A;
  return ctx;
}
template <typename T>
FEMEnergyContext<T> FEMFluidEnergy<T>::updateFluid(const Vec& pN,const Vec& DpNDt) const {
  FEMEnergyContext<T> ctx;
  ctx._dt=0;    //this marks that we are in a reduced setting
  for(int d=0; d<3; d++) {
    ctx._vN[d]=_V[d]->operator()(mapCV(pN));
    ctx._vRelN[d]=-_V[d]->operator[](mapCV(DpNDt));
  }
  ctx._n=(ctx._vN[1]-ctx._vN[0]).cross(ctx._vN[2]-ctx._vN[0])/2;
  ctx._A=sqrt(ctx._n.squaredNorm()+Epsilon<T>::defaultEps());
  ctx._nUnit=ctx._n/ctx._A;
  return ctx;
}
template <typename T>
void FEMFluidEnergy<T>::setParameters(T level,T rho,T G,T dragMult,int deg) {
  _level=level;
  _rho=rho;
  _G=G;
  _rhoG=rho*G;
  _dragMult=dragMult;
  _coefs.clear();

  MatT ret=MatT::Zero(1,1);
  //put bary
  GaussLegendreIntegral<MatT,T>::integrateTri([&](Vec3T bary)->MatT {
    _coefs.push_back(std::make_pair(bary,0));
    return ret;
  },deg,ret);
  //put coef
  int off=0;
  ret.setZero(_coefs.size(),1);
  GaussLegendreIntegral<MatT,T>::integrateTri([&](Vec3T)->MatT {
    return Vec::Unit(ret.size(),off++);
  },deg,ret);
  //fill data
  for(int i=0; i<(int)_coefs.size(); i++)
    _coefs[i].second=ret(i,0);
}
template <typename T>
std::shared_ptr<FEMEnergy<T>> FEMFluidEnergy<T>::toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const {
  std::shared_ptr<FEMFluidEnergy<T>> ret(new FEMFluidEnergy<T>);
  for(int d=0; d<3; d++)
    ret->_V[d]=vertexMapper(_V[d]);
  ret->_coefs=_coefs;
  ret->_level=_level;
  ret->_rho=_rho*coef;
  ret->_G=_G;
  ret->_rhoG=_rhoG;
  ret->_dragMult=_dragMult;
  return ret;
}
template <typename T>
bool FEMFluidEnergy<T>::eval(const Vec& x,EFunc* E,GFunc* G,HFunc*,const FEMEnergyContext<T>* ctx) const {
  ASSERT_MSG(ctx!=NULL,"FEMFluidEnergy requires context!")
  Vec3T v[3];
  for(int j=0; j<3; j++)
    v[j]=_V[j]->operator()(mapCV(x));
  //we combine three terms (energies):
  //bouyancy energy: <n,p>*rhoG*(level-pN)*A
  //gradient of bouyancy energy: n*rhoG*(level-pN)*A
  //bouyancy force: -n*rhoG*(level-pN)*A
  //
  //drag energy: dt/2*rho*dragMult*A*|min(vRel,nUnit,0)|^3
  //gradient of drag energy: -3/2*rho*dragMult*A*|min(vRel,nUnit,0)|^2*nUnit
  //drag force: 3/2*rho*dragMult*A*|min(vRel,nUnit,0)|^2*nUnit
  T ESum=0;
  Vec GSum=Vec::Zero(9);
  for(const std::pair<Vec3T,T>& coef:_coefs) {
    Vec3T pN=ctx->_vN[0]*coef.first[0]+ctx->_vN[1]*coef.first[1]+ctx->_vN[2]*coef.first[2];
    if(pN[2]>=_level)
      continue;
    Vec3T p=v[0]*coef.first[0]+v[1]*coef.first[1]+v[2]*coef.first[2],vRel,vRelN,GBlk;
    if(ctx->_dt>0) {
      //this is for position-based simulation, formulate fluid force as an implicit energy
      vRel=-(p-pN)/ctx->_dt;
      vRelN=ctx->_vRelN[0]*coef.first[0]+ctx->_vRelN[1]*coef.first[1]+ctx->_vRelN[2]*coef.first[2];
    } else {
      //this is for semi-implicit simulation, fluid force is considered non-stiff
      vRel=vRelN=ctx->_vRelN[0]*coef.first[0]+ctx->_vRelN[1]*coef.first[1]+ctx->_vRelN[2]*coef.first[2];
    }
    T vRelNDotN=vRelN.dot(ctx->_nUnit),vRelNDotNSqr=vRelNDotN*vRelNDotN;
    T ndp=ctx->_n.dot(p),EBlk=0;
    if(E) {
      EBlk=-ctx->_dt*_rho*_dragMult*ctx->_A*vRelNDotNSqr*vRel.dot(ctx->_nUnit);
      ESum+=ndp*(_rhoG*(_level-pN[2])*coef.second);
      if(vRelNDotN<0)
        ESum+=EBlk*coef.second;
    }
    if(G) {
      GBlk=ctx->_nUnit*_rho*_dragMult*ctx->_A*vRelNDotNSqr;
      for(int i=0,j=0; i<GSum.size(); i+=3,j++) {
        GSum.template segment<3>(i)+=ctx->_n*(_rhoG*(_level-pN[2])*coef.first[j]*coef.second);
        if(vRelNDotN<0)
          GSum.template segment<3>(i)+=GBlk*(coef.first[j]*coef.second);
      }
    }
  }
  //assemble
  if(E)
    (*E)(ESum);
  if(G)
    for(int i=0,j=0; i<GSum.size(); i+=3,j++)
      for(const std::pair<int,T>& p:_V[j]->jacobian())
        (*G)(p.first,GSum.template segment<3>(i)*p.second);
  return true;
}
template <typename T>
std::shared_ptr<FEMVertex<T>> FEMFluidEnergy<T>::V(int d) const {
  return _V[d];
}
template <typename T>
const std::vector<std::pair<typename FEMFluidEnergy<T>::Vec3T,T>>& FEMFluidEnergy<T>::coefs() const {
  return _coefs;
}
template <typename T>
T FEMFluidEnergy<T>::level() const {
  return _level;
}
template <typename T>
T FEMFluidEnergy<T>::rho() const {
  return _rho;
}
template <typename T>
T FEMFluidEnergy<T>::G() const {
  return _G;
}
template <typename T>
T FEMFluidEnergy<T>::rhoG() const {
  return _rhoG;
}
template <typename T>
T FEMFluidEnergy<T>::dragMult() const {
  return _dragMult;
}
//FEMPenetrationEnergy
template <typename T>
FEMPenetrationEnergy<T>::FEMPenetrationEnergy() {}
template <typename T>
FEMPenetrationEnergy<T>::FEMPenetrationEnergy(std::shared_ptr<FEMVertex<T>> vert,const Vec3T& pos,const Vec3T& n,T phi,T coef):_vert(vert),_depth(n[0],n[1],n[2],phi-pos.dot(n)),_coef(coef) {}
template <typename T>
FEMPenetrationEnergy<T>::FEMPenetrationEnergy(std::shared_ptr<FEMVertex<T>> vert,std::shared_ptr<Tetrahedron<T>> tet,const Vec4T& depth,T coef):_vert(vert),_tet(tet),_depth(depth),_coef(coef) {}
template <typename T>
bool FEMPenetrationEnergy<T>::read(std::istream& is,IOData* dat) {
  readBinaryData(_vert,is,dat);
  readBinaryData(_tet,is,dat);
  readBinaryData(_depth,is);
  readBinaryData(_coef,is);
  return is.good();
}
template <typename T>
bool FEMPenetrationEnergy<T>::write(std::ostream& os,IOData* dat) const {
  writeBinaryData(_vert,os,dat);
  writeBinaryData(_tet,os,dat);
  writeBinaryData(_depth,os);
  writeBinaryData(_coef,os);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> FEMPenetrationEnergy<T>::copy() const {
  return std::shared_ptr<FEMPenetrationEnergy<T>>(new FEMPenetrationEnergy<T>(_vert,_tet,_depth,_coef));
}
template <typename T>
std::string FEMPenetrationEnergy<T>::type() const {
  return typeid(FEMPenetrationEnergy<T>).name();
}
template <typename T>
std::shared_ptr<FEMEnergy<T>> FEMPenetrationEnergy<T>::toReducedEnergy(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper,T coef) const {
  std::shared_ptr<FEMPenetrationEnergy<T>> ret(new FEMPenetrationEnergy<T>);
  ret->_vert=vertexMapper(_vert);
  ret->_tet=std::dynamic_pointer_cast<Tetrahedron<T>>(_tet->toReducedCell(vertexMapper));
  ret->_depth=_depth;
  ret->_coef=_coef*coef;
  return ret;
}
template <typename T>
bool FEMPenetrationEnergy<T>::eval(const Vec& x,EFunc* E,GFunc* G,HFunc* H,const FEMEnergyContext<T>*) const {
  if(_tet)
    return evalSelfCollision(x,E,G,H);
  else return evalEnvCollision(x,E,G,H);
}
template <typename T>
bool FEMPenetrationEnergy<T>::evalEnvCollision(const Vec& x,EFunc* E,GFunc* G,HFunc* H) const {
  Vec3T v=_vert->operator()(mapCV(x));
  T phi=_depth[3]+_depth.template segment<3>(0).dot(v);
  //if(phi>0)
  //  return true;
  Vec3T GSum=_depth.template segment<3>(0);
  Mat3T HSum=GSum*GSum.transpose();
  if(E)
    (*E)(phi*phi*_coef);
  if(G)
    for(const std::pair<int,T>& p:_vert->jacobian())
      (*G)(p.first,GSum*p.second*_coef*phi*2);
  if(H)
    for(const std::pair<int,T>& p:_vert->jacobian())
      for(const std::pair<int,T>& p2:_vert->jacobian())
        (*H)(p.first,p2.first,HSum*_coef*p.second*p2.second*2);
  return true;
}
template <typename T>
bool FEMPenetrationEnergy<T>::evalSelfCollision(const Vec& x,EFunc* E,GFunc* G,HFunc* H) const {
  Vec3T v[5];
  std::shared_ptr<FEMVertex<T>> V[5];
  v[0]=_vert->operator()(mapCV(x));
  V[0]=_vert;
  for(int j=0; j<4; j++) {
    v[j+1]=_tet->V(j)->operator()(mapCV(x));
    V[j+1]=_tet->V(j);
  }
  //compute bary
  Mat3T F,invF,HBlk;
  F.col(0)=v[2]-v[1];
  F.col(1)=v[3]-v[1];
  F.col(2)=v[4]-v[1];
  invF=F.inverse();
  Vec3T bary=invF*(v[0]-v[1]);
  Vec3T baryCoef(_depth[1]-_depth[0],_depth[2]-_depth[0],_depth[3]-_depth[0]);
  Vec4T baryAll(1-bary.sum(),bary[0],bary[1],bary[2]);
  T phi=-baryAll.dot(_depth);
  //if(phi>0)
  //  return true;
  Eigen::Matrix<T,15,1> GSum;
  Eigen::Matrix<T,15,15> HSum;
  Vec3T GBlk=invF.transpose()*baryCoef;
  if(G) {
    GSum.template segment<3>(0)=-GBlk;
    for(int d=0,j=3; d<4; d++,j+=3)
      GSum.template segment<3>(j)=GBlk*baryAll[d];
  }
  if(H) {
    HSum.setZero();
    //part 1: d GBlk/d v
    HSum.template block<3,3>(0,3)=-(invF.row(0)+invF.row(1)+invF.row(2)).transpose()*GBlk.transpose();
    HSum.template block<3,3>(0,6)=invF.row(0).transpose()*GBlk.transpose();
    HSum.template block<3,3>(0,9)=invF.row(1).transpose()*GBlk.transpose();
    HSum.template block<3,3>(0,12)=invF.row(2).transpose()*GBlk.transpose();
    for(int d=0,j=3; d<4; d++,j+=3) {
      HSum.template block<3,15>(j,0)=-HSum.template block<3,15>(0,0)*baryAll[d];
      //part 2: d baryAll/d v
      for(int d2=0,j2=0; d2<5; d2++,j2+=3) {
        if(d==0)
          HBlk=-GBlk*(invF.row(0)+invF.row(1)+invF.row(2));
        else HBlk=GBlk*invF.row(d-1);
        if(d2>0)
          HBlk*=-baryAll[d2-1];
        HSum.template block<3,3>(j,j2)+=HBlk;
      }
    }
  }
  if(E)
    (*E)(phi*phi*_coef);
  if(G)
    for(int i=0,j=0; i<GSum.size(); i+=3,j++)
      for(const std::pair<int,T>& p:V[j]->jacobian())
        (*G)(p.first,GSum.template segment<3>(i)*p.second*_coef*2*phi);
  if(H) {
    HSum=HSum*2*phi+GSum*GSum.transpose()*2;
    for(int i=0,j=0; i<HSum.rows(); i+=3,j++)
      for(const std::pair<int,T>& p:V[j]->jacobian())
        for(int i2=0,j2=0; i2<HSum.cols(); i2+=3,j2++)
          for(const std::pair<int,T>& p2:V[j2]->jacobian())
            (*H)(p.first,p2.first,HSum.template block<3,3>(i,i2)*p.second*p2.second*_coef);
  }
  return true;
}
template struct FEMPointConstraintEnergy<FLOAT>;
template struct FEMConstantForceEnergy<FLOAT>;
template struct FEMLinearElasticEnergy<FLOAT>;
template struct FEMStVKElasticEnergy<FLOAT>;
template struct FEMCorotatedElasticEnergy<FLOAT>;
template struct FEMNonHookeanElasticEnergy<FLOAT>;
template struct FEMFluidEnergy<FLOAT>;
template struct FEMPenetrationEnergy<FLOAT>;
}
