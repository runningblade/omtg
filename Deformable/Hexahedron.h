#ifndef HEXAHEDRON_H
#define HEXAHEDRON_H

#include "FEMMesh.h"

namespace PHYSICSMOTION {
template <typename T>
struct Hexahedron : public FEMCell<T> {
  DECL_MAT_VEC_MAP_TYPES_T
  using typename FEMCell<T>::SMatT;
  using typename FEMCell<T>::STrips;
  using typename FEMCell<T>::PolyXT;
 public:
  Hexahedron();
  Hexahedron(T sideLen,const std::shared_ptr<FEMVertex<T>> v[8],double eps=1e-5f,bool useIPOPT=false);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  std::shared_ptr<FEMCell<T>> copy(const std::unordered_map<std::shared_ptr<FEMVertex<T>>,std::shared_ptr<FEMVertex<T>>>& vmap) const override;
  std::shared_ptr<FEMCell<T>> toReducedCell(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper) const override;
  std::shared_ptr<FEMVertex<T>> V(int i) const override;
  void calcPointDist(VecCM vss,const Vec3T& pt,T& sqrDistance,Vec3T& cp,Vec3T& b) const override;
  Vec3T bary(VecCM vss,const Vec3T& pt,bool proj=false) const override;
  bool isInside(VecCM vss,const Vec3T& pt) const override;
  Vec3T operator()(VecCM vss,const Vec3T& b) const override;
  Mat3T F(VecCM vss,const Vec3T& b) const override;
  BBoxExact getBB(VecCM vss) const override;
  T volume0() const override;
  MatT massMatrix() const override;
  SMatT getDFDV(const Vec3T& bary) const override;
  void integrate(std::function<MatT(Vec3T,Vec)> func,int deg,MatT& ret) const override;
  std::vector<std::pair<Vec3T,T>> stencilIntegrateQuadratic() const override;
  std::vector<std::pair<Vec3T,T>> stencilIntegrateQuartic() const override;
  T sideLen() const;
  static std::shared_ptr<Hexahedron<T>> createRegular(T sideLen,Vec& vss);
 private:
  std::shared_ptr<FEMVertex<T>> _V[8];
  T _sideLen;
  bool _useIPOPT;
  double _eps;
};
}

#endif
