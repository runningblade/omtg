#ifndef FEM_SYSTEM_H
#define FEM_SYSTEM_H

#include "FEMMesh.h"
#include "FEMEnergy.h"
#include "FEMActiveSet.h"
#include "FEMSolverSPD.h"
#include "FEMCollision.h"
#include "FEMPDController.h"
#include "FEMSelfCollision.h"
#include "ContactSimplifier.h"
#include <Environment/Environment.h>

namespace PHYSICSMOTION {
template <typename T>
class FEMSystem : public SerializableBase {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  enum SOLVER {
    SP,
    LCP,
    OPT,
  };
  FEMSystem();
  FEMSystem(std::shared_ptr<FEMMesh<T>> mesh);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  //setup
  static T mu(T young,T poisson);
  static T lambda(T young,T poisson);
  std::shared_ptr<FEMMesh<T>> getBody() const;
  std::shared_ptr<Environment<T>> getEnv() const;
  std::shared_ptr<EnvironmentExact<T>> getEnvExact() const;
  std::vector<std::shared_ptr<FEMEnergy<T>>> getEnergy() const;
  void setEnv(std::shared_ptr<Environment<T>> env);
  void addPointConstraint(const Vec& p,Vec3T pos,T coef,bool constraintToPos=false);
  virtual void setFluidEnergy(T level,T rho,T G,T dragMult=1,int deg=1);
  void addFluidEnergy(T level,T rho,T G,T dragMult=1,int deg=1);
  void addRandomPenetrationEnergy(T coef);  //for debug only
  void addLinearElasticEnergy(T lambda,T mu);
  void addStVKElasticEnergy(T lambda,T mu);
  void addCorotatedElasticEnergy(T lambda,T mu);
  void addNonHookeanElasticEnergy(T lambda,T mu);
  void addGravitationalEnergy(T G);
  const SMatT& getMass() const;
  template <typename TYPE>
  void clearEnergyType() {
    int j=0;
    for(int i=0; i<(int)_energies.size(); i++)
      if(!std::dynamic_pointer_cast<TYPE>(_energies[i]))
        _energies[j++]=_energies[i];
    _energies.resize(j);
  }
  void clearEnergy();
  int maxContact() const;
  void maxContact(int n);
  virtual void setSelfCCDebugOutputPath(const std::string& path);
  virtual void addSelfCC(T coef);
  virtual T dampingCoef() const;
  virtual void dampingCoef(T d);
  virtual T dt() const;
  virtual void dt(T dt);
  T mu() const;
  void mu(T dt);
  //simulate
  virtual void updateFluid(const Vec& pNN,const Vec& pN,std::vector<FEMEnergyContext<T>>& context) const;
#ifndef SWIG
  virtual void detectCollision(const Vec& p,std::vector<FEMCollision<T>>& colls) const;
  virtual bool step(const Vec& pNN,const Vec& pN,Vec& p,std::shared_ptr<FEMPDController<T>> ctrl,const SOLVER solver,std::vector<FEMCollision<T>>* collsRet=NULL) const;
  virtual bool stepOptimize(const Vec& pNN,const Vec& pN,Vec& p,std::shared_ptr<FEMPDController<T>> ctrl,std::vector<FEMCollision<T>>* collsRet=NULL) const;
  virtual bool stepConstraint(const Vec& pNN,const Vec& pN,Vec& p,std::shared_ptr<FEMPDController<T>> ctrl,const SOLVER solver,std::vector<FEMCollision<T>>* collsRet=NULL) const;
#endif
  virtual Vec step(const Vec& pNN,const Vec& pN,std::shared_ptr<FEMPDController<T>> ctrl,bool* succ,SOLVER solver) const;  //for python
  virtual bool assemble(T* E,Vec* G,SMatT* H,const Vec& pNN,const Vec& pN,const Vec& p,std::shared_ptr<FEMPDController<T>> ctrl,std::vector<std::shared_ptr<FEMEnergy<T>>>* ess=NULL) const;
  virtual void debugAssemble(int iter=10,T deltaPos=1e-2f) const;
 protected:
  std::shared_ptr<ContactSimplifier> getContactSimplifier() const;
  static bool staggeredProjectionD(const Vec& pN,Vec& p,const MatT& H,const SolverSPD<MatT>& sol,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int maxIter=3,T eps=0.01f,bool cb=false);
  static bool staggeredProjectionS(const Vec& pN,Vec& p,const SMatT& H,const SolverSPD<SMatT>& sol,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int maxIter=3,T eps=0.01f,bool cb=false);
  static bool LCPProjectionD(const Vec& pN,Vec& p,const MatT& H,const SolverSPD<MatT>& sol,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int nDir=6,bool cb=false);
  static bool LCPProjectionS(const Vec& pN,Vec& p,const SMatT& H,const SolverSPD<SMatT>& sol,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int nDir=6,bool cb=false);
  template <typename MATH>
  static bool staggeredProjection(const Vec& pN,Vec& p,const MATH& H,const SolverSPD<MATH>& sol,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int maxIter=3,T eps=0.01f,bool cb=false);
  template <typename MATH>
  static bool LCPProjection(const Vec& pN,Vec& p,const MATH& H,const SolverSPD<MATH>& sol,const Vec& G,T mu,std::vector<FEMCollision<T>>& colls,FEMActiveSet<T>* AS,int nDir=6,bool cb=false);
  //argmin_x 0.5*x^T*Ht*x+Gt^T*x
  //    s.t. |x(i)|<=Bt(i)
  static bool QCPSolve(const MatT& Ht,const Vec& Gt,const Vec& Bt,Vec& x);
  //0 <= w \perp z >= 0
  //w = M*z + q
  static bool LCPSolve(const MatT& M,const Vec& q,Vec& z,bool useInterior=false);
  std::shared_ptr<SMatT> _proj;
  std::shared_ptr<FEMMesh<T>> _mesh;
  std::vector<std::shared_ptr<FEMEnergy<T>>> _energies;
  std::shared_ptr<Environment<T>> _env;
  T _dampingCoef,_dt,_mu;
  int _maxContact;
  SMatT _mass;
  //temporary
  std::shared_ptr<ContactSimplifier> _simplifier;
  std::shared_ptr<FEMSelfCollision<T>> _selfCC;
  std::string _selfCCDebugOutputPath;
  T _selfCCoef;
};
}

#endif
