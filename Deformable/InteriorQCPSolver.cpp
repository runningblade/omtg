#include "InteriorQCPSolver.h"

namespace PHYSICSMOTION {
template <typename T>
QCPSolver<T>::QCPSolver() {
  _muInit=1e-2f;
  _muDec=0.1f;
  _muFinal=Epsilon<T>::defaultEps()*1e4f;

  _alphaInc=1.1f;
  _alphaDec=0.5f;
  _tolAlpha=1e-20f;

  _tolG=1.0f;
  _tolGFinal=Epsilon<T>::defaultEps()*1e1f;
  _c1=0.0f;
  _c2=1.0f;

  _callback=false;
  _maxIter=10000;
}
template <typename T>
bool QCPSolver<T>::callback() const {
  return _callback;
}
template <typename T>
void QCPSolver<T>::callback(bool cb) {
  _callback=cb;
}
template <typename T>
T QCPSolver<T>::tolGFinal() const {
  return _tolGFinal;
}
template <typename T>
void QCPSolver<T>::tolGFinal(T tol) {
  _tolGFinal=tol;
}
template <typename T>
T QCPSolver<T>::muInit() const {
  return _muInit;
}
template <typename T>
void QCPSolver<T>::muInit(T mu) {
  _muInit=mu;
}
template <typename T>
T QCPSolver<T>::muFinal() const {
  return _muFinal;
}
template <typename T>
void QCPSolver<T>::muFinal(T mu) {
  _muFinal=mu;
}
template <typename T>
T QCPSolver<T>::tolAlpha() const {
  return _tolAlpha;
}
template <typename T>
void QCPSolver<T>::tolAlpha(T tol) {
  _tolAlpha=tol;
}
template <typename T>
int QCPSolver<T>::maxIter() const {
  return _maxIter;
}
template <typename T>
void QCPSolver<T>::maxIter(int iter) {
  _maxIter=iter;
}
template <typename T>
typename QCPSolver<T>::Vec QCPSolver<T>::solve(const Problem& prob,const Vec* xInit,bool& succ) const {
  using namespace std;
  succ=false;
  MatT h;
  Vec dx,g,g2,x0,x;
  T alphaInit,f,vio,dTg,dTg2,alpha=1,f2=0,mu=_muInit;
  //feasible initial guess
  x=sampleValid(false,prob,xInit);
  if(_callback) {
    std::cout << "QPProblem:" << std::endl;
    printConstraints(x,prob);
  }
  //main loop
  for(int it=0;; it++) {
    f=computeFGH(mu,x,&g,&h,prob);
    vio=g.norm();
    if(mu==_muFinal && vio<_tolGFinal) {
      if(_callback) {
        std::cout << "Solved with tolG(" << vio << ")<=" << _tolG << std::endl;
        printConstraints(x,prob);
      }
      succ=true;
      break;
    }
    //newton
    if(!solveDx(h,g,dx)) {
      std::cout << "Iteration " << it << ": solver failed" << std::endl;
      return x;
    }
    //limit alpha
    limitAlpha(alpha,x,dx,prob);
    //line-search
    alphaInit=alpha;
    {
      x0=x;
      dTg=dx.dot(g);
      while(alpha>_tolAlpha) {
        x=x0+dx*alpha;
        f2=computeFGH(mu,x,&g2,NULL,prob);
        dTg2=dx.dot(g2);
        if(f2!=std::numeric_limits<T>::quiet_NaN() && f2<f+_c1*alpha*dTg && -dTg2<=-_c2*dTg) {
          //smooth-change of alpha
          alpha=min(alpha*_alphaInc,T(1));
          break;
        } else {
          x=x0;
          alpha*=_alphaDec;
        }
      }
    }
    //termination condition
    if(_callback)
      std::cout << "Iteration " << it << ": mu(" << mu << ") f(" << f << ") f2(" << f2 << ") tolG(" << vio << ")" << " alphaInit(" << alphaInit << ") alphaFinal(" << alpha << ")" << std::endl;
    if(alpha<=_tolAlpha) {
      if(_callback)
        std::cout << "Stopped with alpha(" << alpha << ")<=" << _tolAlpha << " tolG(" << vio << ")" << std::endl;
      if(mu>_muFinal)
        mu=max(mu*_muDec,_muFinal);
      else {
        if(_callback)
          printConstraints(x,prob);
        break;
      }
    }
    if(it>=_maxIter) {
      if(_callback) {
        std::cout << "Stopped with it(" << it << ")>=" << _maxIter << " tolG(" << _tolG << ")" << std::endl;
        printConstraints(x,prob);
      }
      break;
    }
    if(vio<_tolG)
      mu=max(mu*_muDec,_muFinal);
  }
  return prob._scale.asDiagonal()*x;
}
template <typename T>
void QCPSolver<T>::debugGradient(const Problem& prob,const Vec* xInit) const {
  DEFINE_NUMERIC_DELTA_T(T)
  while(true) {
    MatT h;
    Vec x=sampleValid(true,prob,xInit),g,g2;
    Vec dx=Vec::Random(prob._G.size());
    T f=computeFGH(_muInit,x,&g,&h,prob);
    T f2=computeFGH(_muInit,x+dx*DELTA,&g2,NULL,prob);
    if(f==std::numeric_limits<T>::quiet_NaN())
      continue;
    std::cout << "-------------------------------------------------------------DebugQCPGradient" << std::endl;
    DEBUG_GRADIENT("QCPSolver-DF",g.dot(dx),g.dot(dx)-(f2-f)/DELTA)
    DEBUG_GRADIENT("QCPSolver-DG",(h*dx).norm(),(h*dx-(g2-g)/DELTA).norm())
    break;
  }
}
template <typename T>
T QCPSolver<T>::computeFGH(T mu,const Vec& x,Vec* g,MatT* h,const Problem& prob) {
  Vec Hx=prob._H*x;
  T obj=x.dot(prob._G+Hx/2);
  if(g)
    *g=prob._G+Hx;
  if(h)
    *h=prob._H;
  if(prob._QCP) {
    for(int i=0,j=0; i<prob._G.size()/2; i++,j+=2) {
      if(prob._scale[i]==0)
        continue;
      T c=1-x.template segment<2>(j).squaredNorm();
      if(c<0)
        return std::numeric_limits<T>::quiet_NaN();
      obj-=mu*log(c);
      if(g)
        g->template segment<2>(j)+=(mu*2/c)*x.template segment<2>(j);
      if(h)
        h->template block<2,2>(j,j)+=(mu*2/c)*Mat2T::Identity()+(mu*4/(c*c))*x.template segment<2>(j)*x.template segment<2>(j).transpose();
    }
  } else {
    for(int i=0; i<prob._G.size(); i++) {
      if(x[i]<0)
        return std::numeric_limits<T>::quiet_NaN();
      obj-=mu*log(x[i]);
      if(g)
        g->coeffRef(i)-=mu/x[i];
      if(h)
        h->coeffRef(i,i)+=mu/(x[i]*x[i]);
    }
  }
  return obj;
}
template <typename T>
typename QCPSolver<T>::Problem QCPSolver<T>::setupQCPProb(const MatT& H,const Vec& G,const Vec& B) {
  Problem ret;
  ret._scale=Vec::Zero(G.size());
  for(int i=0,j=0; i<G.size()/2; i++,j+=2)
    ret._scale.template segment<2>(j).setConstant(B[i]>Epsilon<T>::defaultEps()?B[i]:0);
  ret._H=ret._scale.asDiagonal()*(H*ret._scale.asDiagonal());
  ret._G=ret._scale.asDiagonal()*G;
  //make full rank
  for(int i=0,j=0; i<G.size()/2; i++,j+=2)
    if(ret._scale[j]==0)
      ret._H.template block<2,2>(j,j).setIdentity();
  ret._QCP=true;
  return ret;
}
template <typename T>
typename QCPSolver<T>::Problem QCPSolver<T>::setupQPProb(const MatT& H,const Vec& G) {
  Problem ret;
  ret._H=H;
  ret._G=G;
  ret._scale.setOnes(ret._G.size());
  ret._QCP=false;
  return ret;
}
template <typename T>
typename QCPSolver<T>::Vec QCPSolver<T>::sampleValid(bool random,const Problem& prob,const Vec* xInit,T offset) const {
  Vec x;
  if(prob._QCP) {
    if(random)
      x.setRandom(prob._G.size());
    else if(xInit) {
      x=*xInit;
      for(int i=0; i<prob._scale.size(); i++)
        if(prob._scale[i]>0)
          x[i]/=prob._scale[i];
        else x[i]=0;
    } else x=Vec::Zero(prob._G.size());
    //make feasible
    for(int i=0,j=0; i<prob._G.size()/2; i++,j+=2) {
      T lenSqr=x.template segment<2>(j).squaredNorm();
      if(lenSqr>=1-offset)
        x.template segment<2>(j)*=sqrt((1-offset)/lenSqr);
    }
  } else {
    if(random)
      x=(Vec::Random(prob._G.size())+Vec::Zero(prob._G.size()))/2;
    else if(xInit) {
      x=*xInit;
      for(int i=0; i<prob._scale.size(); i++)
        if(prob._scale[i]>0)
          x[i]/=prob._scale[i];
        else x[i]=0;
    } else x=Vec::Zero(prob._G.size());
    //make feasible
    x=x.cwiseMax(offset);
  }
  return x;
}
template <typename T>
void QCPSolver<T>::limitAlpha(T& alpha,const Vec& x,const Vec& dx,const Problem& prob,T scale) {
  alpha=1;
  if(prob._QCP) {
    for(int i=0,j=0; i<prob._G.size()/2; i++,j+=2)
      if(prob._scale[j]>0) {
        T a=dx.template segment<2>(j).squaredNorm();
        T b=dx.template segment<2>(j).dot(x.template segment<2>(j))*2;
        T c=(x.template segment<2>(j).squaredNorm()-1)*(1-scale);
        T alphaLmt=(-b+sqrt(b*b-4*a*c))/(2*a);
        if(alphaLmt<alpha)
          alpha=alphaLmt;
      }
  } else {
    for(int i=0; i<x.size(); i++)
      if(dx[i]<0 && x[i]*scale<-dx[i]*alpha)
        alpha=x[i]*scale/-dx[i];
  }
}
template <typename T>
void QCPSolver<T>::printConstraints(const Vec& x,const Problem& prob) {
  std::cout << "vio=";
  if(prob._QCP) {
    for(int i=0,j=0; i<prob._G.size()/2; i++,j+=2)
      if(prob._scale[j]>0)
        std::cout << " " << 1-x.template segment<2>(j).squaredNorm() << " ";
    std::cout << std::endl;
  } else {
    for(int i=0; i<x.size(); i++)
      std::cout << " " << x[i] << " ";
    std::cout << std::endl;
  }
}
template <typename T>
bool QCPSolver<T>::solveDx(const MatT& h,const Vec& g,Vec& dx) {
  Eigen::LLT<MatT> sol(h);
  if(sol.info()!=Eigen::Success)
    return false;
  dx=-sol.solve(g);
  return true;
}
template class QCPSolver<FLOAT>;
}
