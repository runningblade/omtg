#include "ContactSimplifier.h"
#include <Utils/Heap.h>

namespace PHYSICSMOTION {
ContactSimplifier::ContactSimplifier(int nV,const std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>>& iss) {
  std::unordered_map<int,std::unordered_set<int>> connectivity;
  for(const Eigen::Matrix<int,3,1>& I:iss)
    for(int d=0; d<3; d++) {
      connectivity[I[d]].insert(I[(d+1)%3]);
      connectivity[I[d]].insert(I[(d+2)%3]);
    }
  _distance.setConstant(nV,nV,std::numeric_limits<int>::max());
  OMP_PARALLEL_FOR_
  for(int i=0; i<nV; i++)
    _distance.col(i)=shortestDistance(i,nV,connectivity);
}
Eigen::Matrix<int,-1,1> ContactSimplifier::shortestDistance(int i,int nV,const std::unordered_map<int,std::unordered_set<int>>& connectivity) const {
  std::vector<int> heap;
  std::vector<int> heapOffsets(nV,-1);
  std::vector<int> cost(nV,std::numeric_limits<int>::max());
  //initialize
  int err;
  cost[i]=0;
  pushHeapDef(cost,heapOffsets,heap,i);
  while(!heap.empty()) {
    int minVid=popHeapDef(cost,heapOffsets,heap,err);
    for(int nid:connectivity.find(minVid)->second)
      if(cost[minVid]+1<cost[nid]) {
        cost[nid]=cost[minVid]+1;
        //insert or update
        if(heapOffsets[nid]==-1)
          pushHeapDef(cost,heapOffsets,heap,nid);
        else updateHeapDef(cost,heapOffsets,heap,nid);
      }
  }
  return Eigen::Map<Eigen::Matrix<int,-1,1>>(cost.data(),nV);
}
}
