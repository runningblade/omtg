#include "Hexahedron.h"
#include "GaussLegendre.h"
#include <Utils/IO.h>
#include <Utils/Interp.h>
#include <LBFGS.h>
#include <LBFGSB.h>

namespace PHYSICSMOTION {
template <typename T>
class HexahedronDistance {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  HexahedronDistance(const Vec3T v[8],const Vec3T& pt):_pt(pt) {
    for(int d=0; d<8; d++)
      _v[d]=v[d];
  }
  T operator()(const Vec& x,Vec& grad) const {
    T coefs[8];
    T coefsX[8];
    T coefsY[8];
    T coefsZ[8];
    stencil3D(coefs,x[0],x[1],x[2]);
    stencil3DGradXStencil(coefsX,x[0],x[1],x[2]);
    stencil3DGradYStencil(coefsY,x[0],x[1],x[2]);
    stencil3DGradZStencil(coefsZ,x[0],x[1],x[2]);

    Vec3T dx=-_pt;
    Vec3T dxX=Vec3T::Zero();
    Vec3T dxY=Vec3T::Zero();
    Vec3T dxZ=Vec3T::Zero();
    for(int i=0; i<8; i++) {
      dx+=coefs[i]*_v[i];
      dxX+=coefsX[i]*_v[i];
      dxY+=coefsY[i]*_v[i];
      dxZ+=coefsZ[i]*_v[i];
    }

    grad.resize(3);
    grad[0]=dxX.dot(dx);
    grad[1]=dxY.dot(dx);
    grad[2]=dxZ.dot(dx);
    return dx.squaredNorm()/2;
  }
  Vec3T interp(const Vec& x) const {
    T coefs[8];
    stencil3D(coefs,x[0],x[1],x[2]);

    Vec3T dx=Vec3T::Zero();
    for(int i=0; i<8; i++)
      dx+=coefs[i]*_v[i];
    return dx;
  }
 private:
  Vec3T _v[8],_pt;
};
template <typename T>
Hexahedron<T>::Hexahedron() {}
template <typename T>
Hexahedron<T>::Hexahedron(T sideLen,const std::shared_ptr<FEMVertex<T>> v[8],double eps,bool useIPOPT) {
  for(int d=0; d<8; d++)
    _V[d]=v[d];
  _sideLen=sideLen;
  _eps=eps;
  _useIPOPT=useIPOPT;
}
template <typename T>
bool Hexahedron<T>::read(std::istream& is,IOData* dat) {
  for(int d=0; d<8; d++)
    readBinaryData(_V[d],is,dat);
  readBinaryData(_sideLen,is,dat);
  return is.good();
}
template <typename T>
bool Hexahedron<T>::write(std::ostream& os,IOData* dat) const {
  for(int d=0; d<8; d++)
    writeBinaryData(_V[d],os,dat);
  writeBinaryData(_sideLen,os,dat);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> Hexahedron<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new Hexahedron<T>(_sideLen,_V,_eps,_useIPOPT));
}
template <typename T>
std::string Hexahedron<T>::type() const {
  return typeid(Hexahedron<T>).name();
}
template <typename T>
std::shared_ptr<FEMCell<T>> Hexahedron<T>::copy(const std::unordered_map<std::shared_ptr<FEMVertex<T>>,std::shared_ptr<FEMVertex<T>>>& vmap) const {
  std::shared_ptr<FEMVertex<T>> V[8];
  for(int d=0; d<8; d++)
    V[d]=vmap.find(_V[d])->second;
  return std::shared_ptr<FEMCell<T>>(new Hexahedron<T>(_sideLen,_V,_eps,_useIPOPT));
}
template <typename T>
std::shared_ptr<FEMCell<T>> Hexahedron<T>::toReducedCell(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper) const {
  std::shared_ptr<FEMVertex<T>> V[8]= {
    vertexMapper(_V[0]),vertexMapper(_V[1]),vertexMapper(_V[2]),vertexMapper(_V[3]),
    vertexMapper(_V[4]),vertexMapper(_V[5]),vertexMapper(_V[6]),vertexMapper(_V[7]),
  };
  return std::shared_ptr<Hexahedron<T>>(new Hexahedron<T>(_sideLen,V,_eps,_useIPOPT));
}
template <typename T>
std::shared_ptr<FEMVertex<T>> Hexahedron<T>::V(int i) const {
  return _V[i];
}
template <typename T>
void Hexahedron<T>::calcPointDist(VecCM vss,const Vec3T& pt,T& sqrDistance,Vec3T& cp,Vec3T& b) const {
  double fx=0;
  Eigen::Matrix<double,-1,1> x=Vec3T::Constant(0.5).template cast<double>();
  Eigen::Matrix<double,3,1> minX=Vec3T::Zero().template cast<double>();
  Eigen::Matrix<double,3,1> maxX=Vec3T::Ones().template cast<double>();
  LBFGSpp::LBFGSBParam<double> param;
  param.epsilon=1e-6;
  param.max_iterations=100;
  LBFGSpp::LBFGSBSolver<double> solver(param);
  const Eigen::Matrix<double,3,1> v[8]= {
    _V[0]->operator()(vss).template cast<double>(),
    _V[1]->operator()(vss).template cast<double>(),
    _V[2]->operator()(vss).template cast<double>(),
    _V[3]->operator()(vss).template cast<double>(),
    _V[4]->operator()(vss).template cast<double>(),
    _V[5]->operator()(vss).template cast<double>(),
    _V[6]->operator()(vss).template cast<double>(),
    _V[7]->operator()(vss).template cast<double>(),
  };
  HexahedronDistance<double> func(v,pt.template cast<double>());
  solver.minimize(func,x,fx,minX,maxX);
  cp=func.interp(x).template cast<T>();
  b=x.template cast<T>();
  sqrDistance=fx*2;
}
template <typename T>
typename Hexahedron<T>::Vec3T Hexahedron<T>::bary(VecCM vss,const Vec3T& pt,bool proj) const {
  if(proj) {
    Vec3T cp,b;
    T sqrDistance;
    calcPointDist(vss,pt,sqrDistance,cp,b);
    return b;
  } else {
    double fx=0;
    Eigen::Matrix<double,-1,1> x=Vec3T::Constant(0.5).template cast<double>();
    LBFGSpp::LBFGSParam<double> param;
    param.epsilon=1e-6;
    param.max_iterations=100;
    LBFGSpp::LBFGSSolver<double> solver(param);
    const Eigen::Matrix<double,3,1> v[8]= {
      _V[0]->operator()(vss).template cast<double>(),
      _V[1]->operator()(vss).template cast<double>(),
      _V[2]->operator()(vss).template cast<double>(),
      _V[3]->operator()(vss).template cast<double>(),
      _V[4]->operator()(vss).template cast<double>(),
      _V[5]->operator()(vss).template cast<double>(),
      _V[6]->operator()(vss).template cast<double>(),
      _V[7]->operator()(vss).template cast<double>(),
    };
    HexahedronDistance<double> func(v,pt.template cast<double>());
    solver.minimize(func,x,fx);
    return x.template cast<T>();
  }
}
template <typename T>
bool Hexahedron<T>::isInside(VecCM vss,const Vec3T& pt) const {
  double fx=0;
  Eigen::Matrix<double,-1,1> x=Vec3T::Constant(0.5).template cast<double>();
  Eigen::Matrix<double,-1,1> minX=Vec3T::Zero().template cast<double>();
  Eigen::Matrix<double,-1,1> maxX=Vec3T::Ones().template cast<double>();
  LBFGSpp::LBFGSBParam<double> param;
  param.epsilon=1e-6;
  param.max_iterations=100;
  LBFGSpp::LBFGSBSolver<double> solver(param);
  const Eigen::Matrix<double,3,1> v[8]= {
    _V[0]->operator()(vss).template cast<double>(),
    _V[1]->operator()(vss).template cast<double>(),
    _V[2]->operator()(vss).template cast<double>(),
    _V[3]->operator()(vss).template cast<double>(),
    _V[4]->operator()(vss).template cast<double>(),
    _V[5]->operator()(vss).template cast<double>(),
    _V[6]->operator()(vss).template cast<double>(),
    _V[7]->operator()(vss).template cast<double>(),
  };
  HexahedronDistance<double> func(v,pt.template cast<double>());
  solver.minimize(func,x,fx,minX,maxX);
  return fx<Epsilon<T>::defaultEps();
}
template <typename T>
typename Hexahedron<T>::Vec3T Hexahedron<T>::operator()(VecCM vss,const Vec3T& b) const {
  return interp3D<Vec3T,T>((*_V[0])(vss),(*_V[1])(vss),(*_V[2])(vss),(*_V[3])(vss),(*_V[4])(vss),(*_V[5])(vss),(*_V[6])(vss),(*_V[7])(vss),b[0],b[1],b[2]);
}
template <typename T>
typename Hexahedron<T>::Mat3T Hexahedron<T>::F(VecCM vss,const Vec3T& b) const {
  std::tuple<Vec3T,Vec3T,Vec3T> ret=interp3DGrad<Vec3T,T>
                                    ((*_V[0])(vss),(*_V[1])(vss),(*_V[2])(vss),(*_V[3])(vss),
                                     (*_V[4])(vss),(*_V[5])(vss),(*_V[6])(vss),(*_V[7])(vss),
                                     b[0],b[1],b[2]);
  Mat3T retM;
  retM.col(0)=std::get<0>(ret);
  retM.col(1)=std::get<1>(ret);
  retM.col(2)=std::get<2>(ret);
  return retM/_sideLen;
}
template <typename T>
BBoxExact Hexahedron<T>::getBB(VecCM vss) const {
  BBoxExact bb;
  for(int i=0; i<8; i++)
    bb.setUnion((*_V[i])(vss).template cast<BBoxExact::T>());
  return bb;
}
template <typename T>
T Hexahedron<T>::volume0() const {
  return pow(_sideLen,3);
}
template <typename T>
typename Hexahedron<T>::MatT Hexahedron<T>::massMatrix() const {
  MatT ret=MatT::Zero(8,8);
  integrate([&](Vec3T,Vec weight)->MatT {
    return weight*weight.transpose();
  },3,ret);
  return ret;
}
template <typename T>
typename Hexahedron<T>::SMatT Hexahedron<T>::getDFDV(const Vec3T& bary) const {
  SMatT singleton;
  STrips trips;
  T coefs[8];
  stencil3DGradXStencil(coefs,bary[0],bary[1],bary[2]);
  for(int d=0; d<8; d++)
    addBlockId<T>(trips,0,d*3,3,coefs[d]);
  stencil3DGradYStencil(coefs,bary[0],bary[1],bary[2]);
  for(int d=0; d<8; d++)
    addBlockId<T>(trips,3,d*3,3,coefs[d]);
  stencil3DGradZStencil(coefs,bary[0],bary[1],bary[2]);
  for(int d=0; d<8; d++)
    addBlockId<T>(trips,6,d*3,3,coefs[d]);
  singleton.resize(9,24);
  singleton.setFromTriplets(trips.begin(),trips.end());
  return singleton/_sideLen;
}
template <typename T>
void Hexahedron<T>::integrate(std::function<MatT(Vec3T,Vec)> func,int deg,MatT& ret) const {
  Eigen::Matrix<T,8,1> bary;
  std::function<MatT(Vec3T)> f=[&](Vec3T ret)->MatT {
    ret=(ret+Vec3T::Ones())*0.5f;
    stencil3D<T>(bary.data(),ret[0],ret[1],ret[2]);
    return func(ret,bary);
  };
  GaussLegendreIntegral<MatT,T>::integrate3D(f,deg,ret);
  ret*=T(1.0*volume0()/8.0);
}
template <typename T>
std::vector<std::pair<typename Hexahedron<T>::Vec3T,T>> Hexahedron<T>::stencilIntegrateQuadratic() const {
  return FEMCell<T>::getStencil(1);
}
template <typename T>
std::vector<std::pair<typename Hexahedron<T>::Vec3T,T>> Hexahedron<T>::stencilIntegrateQuartic() const {
  return FEMCell<T>::getStencil(2);
}
template <typename T>
T Hexahedron<T>::sideLen() const {
  return _sideLen;
}
template <typename T>
std::shared_ptr<Hexahedron<T>> Hexahedron<T>::createRegular(T sideLen,Vec& vss) {
  std::shared_ptr<FEMVertex<T>> V[8];
  for(int d=0; d<8; d++)
    V[d]=std::shared_ptr<FEMVertex<T>>(new FEMVertex<T>(d*3));

  vss.resize(24);
  vss.template segment<3>(0 )=Vec3T(0,0,0);
  vss.template segment<3>(3 )=Vec3T(1,0,0);
  vss.template segment<3>(6 )=Vec3T(0,1,0);
  vss.template segment<3>(9 )=Vec3T(1,1,0);
  vss.template segment<3>(12)=Vec3T(0,0,1);
  vss.template segment<3>(15)=Vec3T(1,0,1);
  vss.template segment<3>(18)=Vec3T(0,1,1);
  vss.template segment<3>(21)=Vec3T(1,1,1);
  vss*=sideLen;
  return std::shared_ptr<Hexahedron<T>>(new Hexahedron<T>(sideLen,V));
}
template struct Hexahedron<FLOAT>;
}
