#ifndef FEM_ACTIVE_SET_H
#define FEM_ACTIVE_SET_H

#include "FEMSolverSPD.h"
#include "FEMCollision.h"
#include <Utils/Utils.h>

namespace PHYSICSMOTION {
template <typename T>
struct FEMActiveSet {
  DECL_MAT_VEC_MAP_TYPES_T
  FEMActiveSet(bool callback=false):_callback(callback) {}
  void reset(const MatT& M,const Vec& q,const Vec& z,const MatT& N,const std::vector<FEMCollision<T>>& colls) {
    if(_callback) {
      std::cout << "M*z+q=" << (M*z+q).transpose() << std::endl;
      std::cout << "    z=" << z.transpose() << std::endl;
    }
    //fill active collisions
    _activeColls.clear();
    int nVarPerColl=q.size()/(int)colls.size();
    ASSERT_MSG(N.rows()==(int)colls.size(),"N.rows()!=colls.size() when filling active set!")
    for(int i=0,j=0; i<q.size(); i+=nVarPerColl,j++) {
      T Mzqi=M.row(i).dot(z)+q[i];
      if(Mzqi<z[i]) {
        if(_callback)
          std::cout << "Adding " << i << "th CC constraint to FEMActiveSet" <<std::endl;
        _activeColls.push_back(j);
      }
    }
    //fill in N
    int nA=(int)_activeColls.size();
    _N.resize(nA,N.cols());
    for(int i=0; i<nA; i++)
      _N.row(i)=N.row(_activeColls[i]);
  }
  template <typename MATH>
  MatT computeDDxDuDt(const MatT& DbDu,const SolverSPD<MATH>& invH) const {
    if(_N.size()==0)
      return invH.solve(DbDu);
    //We need to solve the following system:
    //H*(x+dx)=b+DbDu*du ==> H*dx=DbDu*du
    //N*(x+dx)=0         ==> N*dx=0
    //
    //A little re-organization leads to:
    //(H N^T)(dx)=(DbDu*du)
    //(N 0  )(dl)=0
    //
    //We can solve the first equation as:
    //Step 1:
    //H*dx+N^T*dl=DbDu*du
    //dx=H^{-1}*(DbDu*du-N^T*dl)
    //N*H^{-1}*(DbDu*du-N^T*dl)=0
    //
    //Step 2:
    //dl=(N*H^{-1}*N^T)^{-1}*(N*H^{-1}*DbDu)*du
    //
    //Step 3:
    //dx=H^{-1}*(DbDu-N^T*(N*H^{-1}*N^T)^{-1}*(N*H^{-1}*DbDu))*du
    //  =(H^{-1}-H^{-1}*N^T*(N*H^{-1}*N^T)^{-1}*N*H^{-1})*DbDu*du
    MatT invHNT=invH.solve(_N.transpose());
    MatT NInvHNT=_N*invHNT;

    SolverSPD<MatT> invNInvHNT;
    invNInvHNT.computeEigen(NInvHNT,1e-8);
    return invH.solve(DbDu)-invHNT*invNInvHNT.solve(invHNT.transpose())*DbDu;
  }
  std::vector<int> _activeColls;
  MatT _N;  //_N*v=0
  bool _callback;
};
}

#endif
