#include "Tetrahedron.h"
#include "GaussLegendre.h"
#include <Utils/IO.h>
#include <Utils/Interp.h>

namespace PHYSICSMOTION {
template <typename T>
T distToSqrTetrahedron(const Eigen::Matrix<T,3,1>& pt,const Eigen::Matrix<T,3,1> v[4],Eigen::Map<Eigen::Matrix<T,4,1>> bary,Eigen::Matrix<T,3,1>& cp,Eigen::Matrix<char,3,1>* feat) {
  DECL_MAT_VEC_MAP_TYPES_T
  Mat3T LHS;
  LHS.col(0)=v[1]-v[0];
  LHS.col(1)=v[2]-v[0];
  LHS.col(2)=v[3]-v[0];
  Vec3T RHS=pt-v[0];
  //bary
  bary.template segment<3>(1)=LHS.inverse()*RHS;
  bary[0]=1-bary.template segment<3>(1).sum();
  if(!(bary.array().isFinite().all()) || bary.minCoeff()<0) {
    T dist,minDist=std::numeric_limits<double>::max();
    //face
    bool needTestE[4][4]= {
      {true,true,true,true},
      {true,true,true,true},
      {true,true,true,true},
      {true,true,true,true},
    };
    bool needTestV[4]= {true,true,true,true};
    for(int d=0; d<3; d++)
      for(int d2=d+1; d2<4; d2++)
        for(int d3=d2+1; d3<4; d3++) {
          Mat3X2T LHSP;
          LHSP.col(0)=v[d2]-v[d];
          LHSP.col(1)=v[d3]-v[d];
          Vec3T RHSP=pt-v[d];
          Vec2T b=(LHSP.transpose()*LHSP).inverse()*(LHSP.transpose()*RHSP);
          if(b.array().isFinite().all() && b[0]>=0 && b[1]>=0 && b.sum()<=1) {
            needTestE[d][d2]=needTestE[d][d3]=needTestE[d2][d3]=false;
            needTestV[d]=needTestV[d2]=needTestV[d3]=false;
            dist=(LHSP*b-RHSP).squaredNorm();
            if(dist<minDist) {
              if(feat)
                *feat=Eigen::Matrix<char,3,1>(d,d2,d3);
              bary.setZero();
              bary[d2]=b[0];
              bary[d3]=b[1];
              bary[d]=1-b[0]-b[1];
              minDist=dist;
            }
          }
        }
    //edge
    for(int d=0; d<4; d++)
      for(int d2=d+1; d2<4; d2++)
        if(needTestE[d][d2]) {
          //|v[(d+1)%3+1]*alpha+v[d+1]*(1-alpha)-v[0]|^2
          Vec3T LHSE=v[d2]-v[d],RHSE=pt-v[d];
          T alpha=RHSE.dot(LHSE)/LHSE.dot(LHSE);
          if(isfinite(alpha) && alpha>=0 && alpha<=1) {
            needTestV[d]=needTestV[d2]=false;
            dist=(LHSE*alpha-RHSE).squaredNorm();
            if(dist<minDist) {
              if(feat)
                *feat=Eigen::Matrix<char,3,1>(d,d2,-1);
              bary.setZero();
              bary[d2]=alpha;
              bary[d]=1-alpha;
              minDist=dist;
            }
          }
        }
    //vertex
    for(int d=0; d<4; d++)
      if(needTestV[d]) {
        dist=(v[d]-pt).squaredNorm();
        if(dist<minDist) {
          if(feat)
            *feat=Eigen::Matrix<char,3,1>(d,-1,-1);
          bary.setUnit(d);
          minDist=dist;
        }
      }
  } else if(feat)
    *feat=Eigen::Matrix<char,3,1>(-1,-1,-1);
  cp=bary[0]*v[0]+bary[1]*v[1]+bary[2]*v[2]+bary[3]*v[3];
  return (pt-cp).squaredNorm();
}
template <typename T>
Tetrahedron<T>::Tetrahedron() {}
template <typename T>
Tetrahedron<T>::Tetrahedron(const Mat3T& d,const std::shared_ptr<FEMVertex<T>> V[4],double eps) {
  for(int d=0; d<4; d++)
    _V[d]=V[d];
  _d=d;
  _eps=eps;
}
template <typename T>
Tetrahedron<T>::Tetrahedron(const Mat3X4T& v,const std::shared_ptr<FEMVertex<T>> V[4],double eps) {
  for(int d=0; d<4; d++)
    _V[d]=V[d];
  for(int d=0; d<3; d++)
    _d.col(d)=v.col(d+1)-v.col(0);
  if(_d.determinant()<0) {
    _d.col(1).swap(_d.col(2));
    std::swap(_V[2],_V[3]);
  }
  _d=_d.inverse().eval();
  _eps=eps;
}
template <typename T>
bool Tetrahedron<T>::read(std::istream& is,IOData* dat) {
  for(int d=0; d<4; d++)
    readBinaryData(_V[d],is,dat);
  readBinaryData(_d,is,dat);
  return is.good();
}
template <typename T>
bool Tetrahedron<T>::write(std::ostream& os,IOData* dat) const {
  for(int d=0; d<4; d++)
    writeBinaryData(_V[d],os,dat);
  writeBinaryData(_d,os,dat);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> Tetrahedron<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new Tetrahedron<T>(_d,_V,_eps));
}
template <typename T>
std::string Tetrahedron<T>::type() const {
  return typeid(Tetrahedron<T>).name();
}
template <typename T>
std::shared_ptr<FEMCell<T>> Tetrahedron<T>::copy(const std::unordered_map<std::shared_ptr<FEMVertex<T>>,std::shared_ptr<FEMVertex<T>>>& vmap) const {
  std::shared_ptr<FEMVertex<T>> V[4];
  for(int d=0; d<4; d++)
    V[d]=vmap.find(_V[d])->second;
  return std::shared_ptr<FEMCell<T>>(new Tetrahedron<T>(_d,V,_eps));
}
template <typename T>
std::shared_ptr<FEMCell<T>> Tetrahedron<T>::toReducedCell(std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper) const {
  std::shared_ptr<FEMVertex<T>> V[4]= {
    vertexMapper(_V[0]),vertexMapper(_V[1]),vertexMapper(_V[2]),vertexMapper(_V[3]),
  };
  return std::shared_ptr<Tetrahedron<T>>(new Tetrahedron<T>(_d,V,_eps));
}
template <typename T>
std::shared_ptr<FEMVertex<T>> Tetrahedron<T>::V(int i) const {
  return _V[i];
}
template <typename T>
void Tetrahedron<T>::calcPointDist(VecCM vss,const Vec3T& pt,T& sqrDistance,Vec3T& cp,Vec3T& b) const {
  Vec4T bary;
  Vec3T v[4]= {_V[0]->operator()(vss),_V[1]->operator()(vss),_V[2]->operator()(vss),_V[3]->operator()(vss)};
  sqrDistance=distToSqrTetrahedron(pt,v,Eigen::Map<Vec4T>(bary.data()),cp,NULL);
  b=bary.template segment<3>(1);
}
template <typename T>
typename Tetrahedron<T>::Vec3T Tetrahedron<T>::bary(VecCM vss,const Vec3T& pt,bool proj) const {
  if(proj) {
    Vec3T cp,b;
    T sqrDistance;
    calcPointDist(vss,pt,sqrDistance,cp,b);
    return b;
  } else {
    Mat3T F;
    for(int d=0; d<3; d++)
      F.col(d)=(*_V[d+1])(vss)-(*_V[0])(vss);
    return F.inverse()*(pt-(*_V[0])(vss));
  }
}
template <typename T>
bool Tetrahedron<T>::isInside(VecCM vss,const Vec3T& pt) const {
  Vec3T b=bary(vss,pt);
  return (b.array()>=0).all() && b.sum()<=1;
}
template <typename T>
typename Tetrahedron<T>::Vec3T Tetrahedron<T>::operator()(VecCM vss,const Vec3T& b) const {
  return (*_V[0])(vss)*b[0]+(*_V[1])(vss)*b[1]+(*_V[2])(vss)*b[2]+(*_V[3])(vss)*(1-b.sum());
}
template <typename T>
typename Tetrahedron<T>::Mat3T Tetrahedron<T>::F(VecCM vss,const Vec3T&) const {
  Mat3T F;
  for(int d=0; d<3; d++)
    F.col(d)=(*_V[d+1])(vss)-(*_V[0])(vss);
  return F*_d;
}
template <typename T>
typename Tetrahedron<T>::PolyXT::VECP Tetrahedron<T>::operator()(VecCM vss,const typename PolyXT::VECP& b) const {
  typename Tetrahedron<T>::PolyXT::VECP ret;
  ret =(*_V[0])(vss).template cast<PolyXT>()*b[0];
  ret+=(*_V[1])(vss).template cast<PolyXT>()*b[1];
  ret+=(*_V[2])(vss).template cast<PolyXT>()*b[2];
  ret+=(*_V[3])(vss).template cast<PolyXT>()*(b.sum()*-1.+1.);
  return ret;
}
template <typename T>
T Tetrahedron<T>::volume0() const {
  return 1/_d.determinant()/6;
}
template <typename T>
BBoxExact Tetrahedron<T>::getBB(VecCM vss) const {
  BBoxExact bb;
  for(int i=0; i<4; i++)
    bb.setUnion((*_V[i])(vss).template cast<BBoxExact::T>());
  return bb;
}
template <typename T>
typename Tetrahedron<T>::MatT Tetrahedron<T>::massMatrix() const {
  MatT ret=MatT::Zero(4,4);
  integrate([&](Vec3T,Vec weight)->MatT {
    return weight*weight.transpose();
  },4,ret);
  return ret;
}
template <typename T>
typename Tetrahedron<T>::SMatT Tetrahedron<T>::getDFDV(const Vec3T&) const {
  static SMatT singleton;
  OMP_CRITICAL_
  if(singleton.size()==0) {
    STrips trips;
    addBlockId<T>(trips,0,0,3,-1);
    addBlockId<T>(trips,3,0,3,-1);
    addBlockId<T>(trips,6,0,3,-1);
    addBlockId<T>(trips,0,3,3, 1);
    addBlockId<T>(trips,3,6,3, 1);
    addBlockId<T>(trips,6,9,3, 1);
    singleton.resize(9,12);
    singleton.setFromTriplets(trips.begin(),trips.end());
  }
  return mat3x3MulRight(_d)*singleton;
}
template <typename T>
void Tetrahedron<T>::integrate(std::function<MatT(Vec3T,Vec)> func,int deg,MatT& ret) const {
  Vec4T bary;
  std::function<MatT(Vec3T)> f=[&](Vec3T ret)->MatT {
    ret=(ret+Vec3T::Ones())*0.5f;
    bary[0]=(1-ret[0])*ret[1];
    bary[1]=ret[0]*ret[1]*ret[2];
    bary[2]=ret[0]*ret[1]*(1-ret[2]);
    bary[3]=1-ret[1];
    return func(ret,bary)*(ret[0]*ret[1]*ret[1]);
  };
  GaussLegendreIntegral<MatT,T>::integrate3D(f,deg,ret);
  ret*=T(6.0*volume0()/8.0);
}
template <typename T>
std::vector<std::pair<typename Tetrahedron<T>::Vec3T,T>> Tetrahedron<T>::stencilIntegrateQuadratic() const {
  return FEMCell<T>::getStencil(0);
}
template <typename T>
std::vector<std::pair<typename Tetrahedron<T>::Vec3T,T>> Tetrahedron<T>::stencilIntegrateQuartic() const {
  return FEMCell<T>::getStencil(0);
}
template <typename T>
std::shared_ptr<Tetrahedron<T>> Tetrahedron<T>::createRegular(Vec& vss) {
  Mat3X4T d;
  d.col(0)=Vec3T(0,0,0);
  d.col(1)=Vec3T(1,0,0);
  d.col(2)=Vec3T(0,1,0);
  d.col(3)=Vec3T(0,0,2);
  std::shared_ptr<FEMVertex<T>> V[4];
  for(int d=0; d<4; d++)
    V[d]=std::shared_ptr<FEMVertex<T>>(new FEMVertex<T>(d*3));

  vss.resize(12);
  vss.template segment<3>(0)=Vec3T(0,0,0);
  vss.template segment<3>(3)=Vec3T(1,0,0);
  vss.template segment<3>(6)=Vec3T(0,1,0);
  vss.template segment<3>(9)=Vec3T(0,0,1);
  return std::shared_ptr<Tetrahedron<T>>(new Tetrahedron<T>(d,V));
}
template struct Tetrahedron<FLOAT>;
}
