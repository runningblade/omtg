#ifndef FEM_SOLVER_SPD_H
#define FEM_SOLVER_SPD_H

#include <Utils/SparseUtils.h>

namespace PHYSICSMOTION {
template <typename T>
struct SolverSPD;
template <typename T>
struct SolverSPD<Eigen::SparseMatrix<T,0,int>> {
  typedef Eigen::Matrix<T,-1,-1> MatT;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  typedef Eigen::SimplicialCholesky<SMatT> SolverType;
  SolverSPD() {}
  SolverSPD(const SMatT& H):_sol(H) {}
  void compute(const SMatT& H) {
    _sol.compute(H);
  }
  MatT solve(const MatT& b) const {
    return _sol.solve(b);
  }
  bool succ() const {
    return _sol.info()==Eigen::Success;
  }
 private:
  SolverType _sol;
};
template <typename T>
struct SolverSPD<Eigen::Matrix<T,-1,-1>> {
  typedef Eigen::Matrix<T,-1,1> Vec;
  typedef Eigen::Matrix<T,-1,-1> MatT;
  typedef Eigen::Matrix<double,-1,-1> Matd;
  typedef Eigen::LLT<MatT> SolverType;
  SolverSPD() {}
  SolverSPD(const MatT& H):_sol(H) {}
  void compute(const MatT& H) {
    _sol.compute(H);
    _solBackup=NULL;
  }
  MatT solve(const MatT& b) const {
    if(_solBackup)
      return _solBackup->eigenvectors().template cast<T>()*(_invEval.asDiagonal()*(_solBackup->eigenvectors().template cast<T>().transpose()*b));
    else return _sol.solve(b);
  }
  void computeEigen(const MatT& H,T minEig) {
    if(!_solBackup)
      _solBackup.reset(new Eigen::SelfAdjointEigenSolver<Matd>);
    _solBackup->compute(H.template cast<double>(),Eigen::ComputeEigenvectors);
    _invEval=_solBackup->eigenvalues().template cast<T>();
    for(int i=0; i<_invEval.size(); i++)
      if(_invEval[i]<minEig)
        _invEval[i]=1/minEig;
      else _invEval[i]=1/_invEval[i];
  }
  bool succ() const {
    return _sol.info()==Eigen::Success;
  }
 private:
  Vec _invEval;
  SolverType _sol;
  std::shared_ptr<Eigen::SelfAdjointEigenSolver<Matd>> _solBackup;
};
}

#endif
