#ifndef NNHTP_H
#define NNHTP_H

#include <Utils/SparseUtils.h>
#include <Utils/Pragma.h>
#include <unordered_map>
#include <unordered_set>
extern "C"
{
  /* Subroutine */
  int nnls_(double *a,int *mda,int *m,int *n,
            double *b,double *x,double *rnorm,double *w,double *zz,
            int *index,int *mode);
}

namespace PHYSICSMOTION {
template <typename T>
class NNHTP {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  typedef std::unordered_map<int,T> W;
  NNHTP(const MatT& A,const Vec& b):_A(A),_b(b) {}
  W solve(int s,T tol,bool debug,T tolAlpha=1e-3f) {
    W w,wNew,wOld;
    int it=0;
    Vec g=Vec::Zero(_A.cols());
    Vec gs=Vec::Zero(_A.cols());
    Vec Ags=Vec::Zero(_A.rows());
    std::unordered_set<int> S;
    T energy=std::numeric_limits<double>::max(),alpha=1;
    while(true) {
      g=-_b;
      for(const std::pair<int,T>& wi:w)
        g+=_A.col(wi.first)*wi.second;
      if(g.norm()>=energy) {
        alpha*=0.5f;
        w=wOld;
        if(alpha<tolAlpha)
          break;
      } else alpha=std::min<T>(alpha*1.5f,1);
      energy=g.norm();
      if(debug) {
        std::cout << "NN-HTP iter=" << it << " res=" << g.norm() << " alpha=" << alpha << std::endl;
      }
      g=_A.transpose()*g;
      //termination
      if(g.norm()<tol)
        break;
      determineS(w,g,S,s);
      //project g to S
      gs.setZero();
      Ags.setZero();
      for(int si:S) {
        gs[si]=g[si];
        Ags+=g[si]*_A.col(si);
      }
      //compute mu
      T mu=alpha*gs.squaredNorm()/Ags.squaredNorm();
      //update w
      wNew=w;
      for(int si:S) {
        if(wNew.find(si)==wNew.end())
          wNew[si]=-mu*g[si];
        else wNew[si]-=mu*g[si];
      }
      //project w
      wNew=HPositive(wNew,s);
      //termination
      bool same=true;
      for(const std::pair<int,T>& wi:w)
        if(wNew.find(wi.first)==wNew.end()) {
          same=false;
          break;
        }
      for(const std::pair<int,T>& wi:wNew)
        if(w.find(wi.first)==w.end()) {
          same=false;
          break;
        }
      if(same)
        break;
      //update w
      wNew=solveW(wNew);
      wOld=w;
      w=wNew;
      it++;
    }
    return w;
  }
  W solveW(const W& w) const {
    MatT Aw;
    int k=0;
    Aw.resize(_A.rows(),w.size());
    for(const std::pair<int,T>& wi:w)
      Aw.col(k++)=_A.col(wi.first);

    k=0;
    W ret;
    Vec x=Vec::Zero(w.size());
    ASSERT(NNLS(Aw,_b,x))
    for(const std::pair<int,T>& wi:w) {
      if(x[k]>0)
        ret[wi.first]=x[k];
      k++;
    }
    return ret;
  }
  W HPositive(const W& w,int s) const {
    W ret;
    std::vector<std::pair<int,T>> wss(w.begin(),w.end());
    std::make_heap(wss.begin(),wss.end(),[&](std::pair<int,T> a,std::pair<int,T> b) {
      return a.second<b.second;
    });
    for(int i=0; i<std::min<int>(wss.size(),s); i++) {
      std::pop_heap(wss.begin(),wss.end(),[&](std::pair<int,T> a,std::pair<int,T> b) {
        return a.second<b.second;
      });
      if(wss.back().second>0) {
        ret.insert(wss.back());
        wss.pop_back();
      } else break;
    }
    return ret;
  }
  bool NNLS(const MatT& A,const Vec& b,Vec& x) const {
    Eigen::Matrix<double,-1,-1> Atmp=A.template cast<double>();
    Eigen::Matrix<double,-1,1> btmp=b.template cast<double>();
    Eigen::Matrix<double,-1,1> xtmp=x.template cast<double>();
    int mda=(int)Atmp.rows();
    int m=(int)Atmp.rows();
    int n=(int)Atmp.cols();
    double rnorm;
    Eigen::Matrix<double,-1,1> w(n);
    Eigen::Matrix<double,-1,1> zz(m);
    Eigen::Matrix<int,-1,1> index(n);
    int mode;
    nnls_(Atmp.data(),&mda,&m,&n,
          btmp.data(),xtmp.data(),&rnorm,w.data(),zz.data(),
          index.data(),&mode);
    x=xtmp.cast<T>();
    return mode == 1;
  }
  void determineS(const W& w,Vec g,std::unordered_set<int>& S,int s) const {
    g=-g;
    S.clear();
    for(const std::pair<int,T>& wi:w) {
      S.insert(wi.first);
      g[wi.first]=0;
    }
    //HPositive
    std::vector<int> vss(g.size());
    for(int i=0; i<g.size(); i++)
      vss[i]=i;
    std::make_heap(vss.begin(),vss.end(),[&](int a,int b) {
      return g[a]<g[b];
    });
    for(int i=0; i<s; i++) {
      std::pop_heap(vss.begin(),vss.end(),[&](int a,int b) {
        return g[a]<g[b];
      });
      if(g[vss.back()]>0)
        S.insert(vss.back());
      else break;
      vss.pop_back();
    }
  }
  T error(const W& w) const {
    using namespace std;
    Vec err=_b;
    for(const std::pair<int,T>& wi:w)
      err-=_A.col(wi.first)*wi.second;
    return err.norm()/max(_b.norm(),Epsilon<T>::defaultEps());
  }
  const MatT& A() const {
    return _A;
  }
  const Vec& b() const {
    return _b;
  }
 protected:
  const MatT& _A;
  const Vec& _b;
};
}

#endif
