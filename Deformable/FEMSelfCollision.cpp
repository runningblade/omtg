#include "FEMSelfCollision.h"
#include "FEMReducedSystem.h"
#include <Environment/MeshExact.h>
#include <Environment/BVHNode.h>
#include <Utils/Utils.h>
#include <fstream>
#include <stack>

namespace PHYSICSMOTION {
template <typename T>
struct FEMFaceHash {
  size_t operator()(const std::array<std::shared_ptr<FEMVertex<T>>,3>& key) const {
    std::hash<std::shared_ptr<FEMVertex<T>>> h;
    return h(key[0])+h(key[1])+h(key[2]);
  }
};
template <typename T>
FEMSelfCollision<T>::FEMSelfCollision(std::shared_ptr<FEMMesh<T>> mesh) {
#define BUILD_TET(A,B,C,D)  \
{ \
  std::shared_ptr<FEMVertex<T>> vss[4]={H->V(A),H->V(B),H->V(C),H->V(D)}; \
  Mat3X4T pss;  \
  for(int d=0;d<4;d++)  \
    pss.col(d)=vss[d]->operator()(mesh->pos0());  \
  _tss.push_back(std::shared_ptr<Tetrahedron<T>>(new Tetrahedron<T>(pss,vss))); \
}
  for(int i=0; i<mesh->nrC(); i++) {
    std::shared_ptr<FEMCell<T>> C=mesh->getC(i);
    if(std::dynamic_pointer_cast<Tetrahedron<T>>(C))
      _tss.push_back(std::dynamic_pointer_cast<Tetrahedron<T>>(C));
    else if(std::dynamic_pointer_cast<Hexahedron<T>>(C)) {
      std::shared_ptr<Hexahedron<T>> H=std::dynamic_pointer_cast<Hexahedron<T>>(C);
      //   6--------7
      //  /|       /|
      // / |      / |
      //4--------5  |
      //|  |     |  |
      //|  2-----|--3
      //| /      | /
      //|/       |/
      //0--------1
      BUILD_TET(3,4,6,7)
      BUILD_TET(1,3,4,7)
      BUILD_TET(1,4,5,7)
      BUILD_TET(0,1,3,4)
      BUILD_TET(0,3,4,6)
      BUILD_TET(0,2,3,6)
    } else {
      ASSERT_MSG(false,"Unsupported cell type!")
    }
  }
  buildDistance(mesh);
  buildExcludeSet(mesh);
}
template <typename T>
std::vector<typename FEMSelfCollision<T>::Mat3X4T,Eigen::aligned_allocator<typename FEMSelfCollision<T>::Mat3X4T>> FEMSelfCollision<T>::getTBVH(const Vec& p,std::vector<Node<int,BBoxExact>>& bvh) const {
  bvh=_bvh;
  std::vector<Mat3X4T,Eigen::aligned_allocator<Mat3X4T>> vss(_tss.size());
  for(int i=0; i<(int)_tss.size(); i++) {
    bvh[i]._bb=BBoxExact();
    for(int d=0; d<4; d++) {
      vss[i].col(d)=_tss[i]->V(d)->operator()(mapCV(p));
      bvh[i]._bb.setUnion(vss[i].col(d).template cast<BBoxExact::T>());
    }
  }
  for(int i=(int)_tss.size(); i<(int)bvh.size(); i++) {
    bvh[i]._bb=bvh[bvh[i]._l]._bb;
    bvh[i]._bb.setUnion(bvh[bvh[i]._r]._bb);
  }
  return vss;
}
template <typename T>
std::vector<typename FEMSelfCollision<T>::Mat3X4T,Eigen::aligned_allocator<typename FEMSelfCollision<T>::Mat3X4T>> FEMSelfCollision<T>::getTBVHLocal(const FEMGradientInfo<T>& I,const FEMReducedSystem<T>& sys,std::vector<Node<int,BBoxExact>>& bvh) const {
  bvh=_bvh;
  std::vector<Mat3X4T,Eigen::aligned_allocator<Mat3X4T>> vss(_tss.size());
  for(int i=0; i<(int)_tss.size(); i++) {
    bvh[i]._bb=BBoxExact();
    for(int d=0; d<4; d++) {
      vss[i].col(d)=I.getVPosLocal(sys.getReducedVertexOff(_tss[i]->V(d)));
      bvh[i]._bb.setUnion(vss[i].col(d).template cast<BBoxExact::T>());
    }
  }
  for(int i=(int)_tss.size(); i<(int)bvh.size(); i++) {
    bvh[i]._bb=bvh[bvh[i]._l]._bb;
    bvh[i]._bb.setUnion(bvh[bvh[i]._r]._bb);
  }
  return vss;
}
template <typename T>
void FEMSelfCollision<T>::detectSelfCollision(const Vec& p,std::shared_ptr<FEMMesh<T>> mesh,std::vector<std::shared_ptr<FEMEnergy<T>>>& colls,T coef,const std::string& path) const {
  if(path.empty())
    detectSelfCollision(p,mesh,colls,coef);
  else {
    VTKWriter<double> os("SelfCC",path+"CC.vtk",true);
    detectSelfCollision(p,mesh,colls,coef,&os);
    writeDepthVTK(p,path+"Depth.vtk");
  }
}
template <typename T>
void FEMSelfCollision<T>::detectSelfCollision(const FEMGradientInfo<T>& I,const FEMReducedSystem<T>& sys,std::shared_ptr<FEMMesh<T>> mesh,std::vector<std::shared_ptr<FEMEnergy<T>>>& colls,T coef,const std::string& path) const {
  if(path.empty())
    detectSelfCollision(I,sys,mesh,colls,coef);
  else {
    VTKWriter<double> osCC("SelfCC",path+"CC.vtk",true);
    detectSelfCollision(I,sys,mesh,colls,coef,&osCC);
    writeDepthVTK(I,sys,path+"Depth.vtk");
  }
}
template <typename T>
void FEMSelfCollision<T>::detectSelfCollision(const Vec& p,std::shared_ptr<FEMMesh<T>> mesh,std::vector<std::shared_ptr<FEMEnergy<T>>>& colls,T coef,VTKWriter<double>* os) const {
  std::vector<Node<int,BBoxExact>> svBVH,tetBVH;
  std::vector<Vec3T,Eigen::aligned_allocator<Vec3T>> SVPos=mesh->getSBVH(p,svBVH);
  std::vector<Mat3X4T,Eigen::aligned_allocator<Mat3X4T>> TETPos=getTBVH(p,tetBVH);
  //main loop
  Mat3T F;
  Vec3T bary;
  Vec4T TETPhi;
  std::stack<std::pair<int,int>> ss;
  ss.push(std::make_pair((int)svBVH.size()-1,(int)tetBVH.size()-1));
  while(!ss.empty()) {
    int l=ss.top().first;
    int r=ss.top().second;
    ss.pop();
    if(!svBVH[l]._bb.intersect(tetBVH[r]._bb))
      continue;
    else if(svBVH[l]._l==-1 && tetBVH[r]._l==-1) {
      auto it=_exclude.find(r);
      if(it!=_exclude.end() && it->second.find(l)!=it->second.end())
        continue;
      //bary
      const Vec3T& SV=SVPos[l];
      const Mat3X4T& TET=TETPos[r];
      for(int d=0; d<3; d++)
        F.col(d)=TET.col(d+1)-TET.col(0);
      bary=F.inverse()*(SV-TET.col(0));
      if(bary[0]<0 || bary[1]<0 || bary[2]<0 || bary.sum()>1)
        continue;
      if(os)
        writeCCVTK(*os,SV,TET);
      //build energy
      for(int d=0; d<4; d++)
        TETPhi[d]=_depth.find(_tss[r]->V(d))->second;
      colls.push_back(std::shared_ptr<FEMEnergy<T>>(new FEMPenetrationEnergy<T>(mesh->getSV(l),_tss[r],TETPhi,coef)));
    } else if(svBVH[l]._l==-1) {
      ss.push(std::make_pair(l,tetBVH[r]._l));
      ss.push(std::make_pair(l,tetBVH[r]._r));
    } else if(tetBVH[r]._l==-1) {
      ss.push(std::make_pair(svBVH[l]._l,r));
      ss.push(std::make_pair(svBVH[l]._r,r));
    } else {
      ss.push(std::make_pair(svBVH[l]._l,tetBVH[r]._l));
      ss.push(std::make_pair(svBVH[l]._r,tetBVH[r]._l));
      ss.push(std::make_pair(svBVH[l]._l,tetBVH[r]._r));
      ss.push(std::make_pair(svBVH[l]._r,tetBVH[r]._r));
    }
  }
}
template <typename T>
void FEMSelfCollision<T>::detectSelfCollision(const FEMGradientInfo<T>& I,const FEMReducedSystem<T>& sys,std::shared_ptr<FEMMesh<T>> mesh,std::vector<std::shared_ptr<FEMEnergy<T>>>& colls,T coef,VTKWriter<double>* os) const {
  std::vector<Node<int,BBoxExact>> svBVH,tetBVH;
  mesh->getSBVHSubspaceLocal(I,svBVH);
  std::vector<Mat3X4T,Eigen::aligned_allocator<Mat3X4T>> TETPos=getTBVHLocal(I,sys,tetBVH);
  //vertexMapper
  std::function<std::shared_ptr<FEMVertex<T>>(std::shared_ptr<FEMVertex<T>>)> vertexMapper=[&](std::shared_ptr<FEMVertex<T>> v)->std::shared_ptr<FEMVertex<T>> {
    return sys.getReducedVertex(v);
  };
  //main loop
  Mat3T F;
  Vec3T bary;
  Vec4T TETPhi;
  std::stack<std::pair<int,int>> ss;
  ss.push(std::make_pair((int)svBVH.size()-1,(int)tetBVH.size()-1));
  while(!ss.empty()) {
    int l=ss.top().first;
    int r=ss.top().second;
    ss.pop();
    if(!svBVH[l]._bb.intersect(tetBVH[r]._bb))
      continue;
    else if(svBVH[l]._l==-1 && tetBVH[r]._l==-1) {
      auto it=_exclude.find(r);
      if(it!=_exclude.end() && it->second.find(l)!=it->second.end())
        continue;
      //bary
      const Vec3T& SV=I.getVPosLocal(l*3); //getVPos accepts offset and surface vertex appears first
      const Mat3X4T& TET=TETPos[r];
      for(int d=0; d<3; d++)
        F.col(d)=TET.col(d+1)-TET.col(0);
      bary=F.inverse()*(SV-TET.col(0));
      if(bary[0]<0 || bary[1]<0 || bary[2]<0 || bary.sum()>1)
        continue;
      if(os)
        writeCCVTK(*os,SV,TET,I.getR(),I.getT());
      //build energy
      for(int d=0; d<4; d++)
        TETPhi[d]=_depth.find(_tss[r]->V(d))->second;
      std::shared_ptr<FEMCell<T>> cell=_tss[r]->toReducedCell(vertexMapper);
      colls.push_back(std::shared_ptr<FEMEnergy<T>>(new FEMPenetrationEnergy<T>(sys.getSV(l),std::dynamic_pointer_cast<Tetrahedron<T>>(cell),TETPhi,coef)));
    } else if(svBVH[l]._l==-1) {
      ss.push(std::make_pair(l,tetBVH[r]._l));
      ss.push(std::make_pair(l,tetBVH[r]._r));
    } else if(tetBVH[r]._l==-1) {
      ss.push(std::make_pair(svBVH[l]._l,r));
      ss.push(std::make_pair(svBVH[l]._r,r));
    } else {
      ss.push(std::make_pair(svBVH[l]._l,tetBVH[r]._l));
      ss.push(std::make_pair(svBVH[l]._r,tetBVH[r]._l));
      ss.push(std::make_pair(svBVH[l]._l,tetBVH[r]._r));
      ss.push(std::make_pair(svBVH[l]._r,tetBVH[r]._r));
    }
  }
}
template <typename T>
void FEMSelfCollision<T>::writeDepthVTK(const Vec& p,const std::string& path) const {
  VTKWriter<double> os("MeshDistanceField",path,true);
  std::vector<Eigen::Matrix<double,3,1>> vss;
  std::vector<Eigen::Matrix<int,4,1>> tss;
  std::vector<double> dss;
  //vss
  std::unordered_map<std::shared_ptr<FEMVertex<T>>,int> vMap;
  for(auto d:_depth) {
    vMap[d.first]=(int)dss.size();
    vss.push_back(d.first->operator()(mapCV(p)).template cast<double>());
    dss.push_back((double)d.second);
  }
  //tss
  for(auto t:_tss)
    tss.push_back(Eigen::Matrix<int,4,1>(vMap[t->V(0)],vMap[t->V(1)],vMap[t->V(2)],vMap[t->V(3)]));
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(tss.begin(),tss.end(),VTKWriter<double>::TETRA);
  os.appendCustomPointData("depth",dss.begin(),dss.end());
}
template <typename T>
void FEMSelfCollision<T>::writeDepthVTK(const FEMGradientInfo<T>& I,const FEMReducedSystem<T>& sys,const std::string& path) const {
  VTKWriter<double> os("MeshDistanceField",path,true);
  std::vector<Eigen::Matrix<double,3,1>> vss;
  std::vector<Eigen::Matrix<int,4,1>> tss;
  std::vector<double> dss;
  //vss
  std::unordered_map<std::shared_ptr<FEMVertex<T>>,int> vMap;
  for(auto d:_depth) {
    vMap[d.first]=(int)dss.size();
    vss.push_back(I.getVPos(sys.getReducedVertexOff(d.first)).template cast<double>());
    dss.push_back((double)d.second);
  }
  //tss
  for(auto t:_tss)
    tss.push_back(Eigen::Matrix<int,4,1>(vMap[t->V(0)],vMap[t->V(1)],vMap[t->V(2)],vMap[t->V(3)]));
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(tss.begin(),tss.end(),VTKWriter<double>::TETRA);
  os.appendCustomPointData("depth",dss.begin(),dss.end());
}
template <typename T>
void FEMSelfCollision<T>::writeCCVTK(VTKWriter<double>& os,const Vec3T& SV,const Mat3X4T& TET) const {
  //vss
  std::vector<Eigen::Matrix<double,3,1>> vss;
  for(int d=0; d<4; d++)
    vss.push_back(TET.col(d).template cast<double>());
  vss.push_back(SV.template cast<double>());
  //iss
  std::vector<Eigen::Matrix<int,4,1>> iss;
  iss.push_back(Eigen::Matrix<int,4,1>(0,1,2,3));
  iss.push_back(Eigen::Matrix<int,4,1>(4,4,4,4));
  //os
  os.setRelativeIndex();
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(iss.begin(),iss.begin()+1,VTKWriter<double>::TETRA,true);
  os.appendCells(iss.begin()+1,iss.end(),VTKWriter<double>::POINT,true);
}
template <typename T>
void FEMSelfCollision<T>::writeCCVTK(VTKWriter<double>& os,const Vec3T& SV,const Mat3X4T& TET,const Mat3T& R,const Vec3T& t) const {
  writeCCVTK(os,R*SV+t,R*TET+t*Vec4T::Ones().transpose());
}
//helper
template <typename T>
void FEMSelfCollision<T>::buildExcludeSet(std::shared_ptr<FEMMesh<T>> mesh) {
  std::unordered_map<int,std::unordered_set<int>> DOFMap; //DOF id -> tet id
  for(int i=0; i<(int)_tss.size(); i++)
    for(int d=0; d<4; d++)
      for(std::pair<int,T> DOF:_tss[i]->V(d)->jacobian())
        DOFMap[DOF.first].insert(i);

  _exclude.clear();
  for(int i=0; i<(int)mesh->nrSV(); i++)
    for(std::pair<int,T> DOF:mesh->getSV(i)->jacobian())
      for(int tetId:DOFMap[DOF.first])
        _exclude[tetId].insert(i);
}
template <typename T>
void FEMSelfCollision<T>::buildDistance(std::shared_ptr<FEMMesh<T>> mesh) {
  std::unordered_map<std::array<std::shared_ptr<FEMVertex<T>>,3>,Eigen::Matrix<int,2,1>,FEMFaceHash<T>> faceMap;
  std::unordered_map<Eigen::Matrix<int,2,1>,std::pair<int,int>,EdgeHash> edgeMap;
  for(int i=0,eid=0; i<(int)_tss.size(); i++) {
    std::array<std::shared_ptr<FEMVertex<T>>,3> face;
    for(int d=0; d<4; d++) {
      face[0]=_tss[i]->V((d+1)%4);
      face[1]=_tss[i]->V((d+2)%4);
      face[2]=_tss[i]->V((d+3)%4);
      sort3(face[0],face[1],face[2]);
      if(faceMap.find(face)==faceMap.end())
        faceMap[face]=Eigen::Matrix<int,2,1>(i,-1);
      else {
        ASSERT_MSG(faceMap[face][1]==-1,"Non-manifold mesh detected!")
        faceMap[face][1]=i;
        edgeMap[Eigen::Matrix<int,2,1>(eid,eid)]=std::make_pair(faceMap[face][0],faceMap[face][1]);
        eid++;
      }
    }
  }
  //build bvh
  _bvh.assign(_tss.size(),Node<int,BBoxExact>());
  for(int i=0; i<(int)_bvh.size(); i++) {
    _bvh[i]._bb=BBoxExact();
    for(int d=0; d<4; d++)
      _bvh[i]._bb.setUnion(_tss[i]->V(d)->operator()(mesh->pos0()).template cast<BBoxExact::T>());
    _bvh[i]._nrCell=1;
    _bvh[i]._cell=i;
  }
  Node<int,BBoxExact>::buildBVHBottomUp(_bvh,edgeMap);
  //build surface
  std::unordered_map<std::shared_ptr<FEMVertex<T>>,int> vMap;
  std::vector<Eigen::Matrix<int,3,1>,Eigen::aligned_allocator<Eigen::Matrix<int,3,1>>> iss;
  std::vector<Eigen::Matrix<double,3,1>,Eigen::aligned_allocator<Eigen::Matrix<double,3,1>>> vss;
  for(std::pair<std::array<std::shared_ptr<FEMVertex<T>>,3>,Eigen::Matrix<int,2,1>> face:faceMap)
    if(face.second[1]==-1) {
      for(int d=0; d<3; d++)
        if(vMap.find(face.first[d])==vMap.end()) {
          vMap[face.first[d]]=(int)vss.size();
          vss.push_back(face.first[d]->operator()(mesh->pos0()).template cast<double>());
        }
      iss.push_back(Eigen::Matrix<int,3,1>(vMap[face.first[0]],vMap[face.first[1]],vMap[face.first[2]]));
    }
  MeshExact smesh(vss,iss);
  //calculate distance
  _depth.clear();
  for(int i=0; i<(int)_tss.size(); i++)
    for(int d=0; d<4; d++) {
      std::shared_ptr<FEMVertex<T>> V=_tss[i]->V(d);
      if(_depth.find(V)==_depth.end()) {
        Mat3T hessian;
        Eigen::Matrix<int,2,1> feat;
        Vec3T pt=V->operator()(mesh->pos0()),n,normal;
        _depth[V]=smesh.closest<T>(pt,n,normal,hessian,feat);
      }
    }
}
template class FEMSelfCollision<FLOAT>;
}

