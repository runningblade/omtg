#ifndef INTERIOR_QCP_SOLVER_H
#define INTERIOR_QCP_SOLVER_H

#include <Utils/Utils.h>
#include <Utils/Pragma.h>
#include <Utils/SparseUtils.h>

namespace PHYSICSMOTION {
template <typename T>
class QCPSolver {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  struct Problem {
    MatT _H;
    Vec _G,_scale;
    bool _QCP;
  };
  QCPSolver();
  bool callback() const;
  void callback(bool cb);
  T tolGFinal() const;
  void tolGFinal(T tol);
  T muInit() const;
  void muInit(T mu);
  T muFinal() const;
  void muFinal(T mu);
  T tolAlpha() const;
  void tolAlpha(T tol);
  int maxIter() const;
  void maxIter(int iter);
  Vec solve(const Problem& prob,const Vec* xInit,bool& succ) const;
  void debugGradient(const Problem& prob,const Vec* xInit=NULL) const;
  Vec sampleValid(bool random,const Problem& prob,const Vec* xInit=NULL,T offset=0.01f) const;
  static Problem setupQCPProb(const MatT& H,const Vec& G,const Vec& B);
  static Problem setupQPProb(const MatT& H,const Vec& G);
 protected:
  static T computeFGH(T mu,const Vec& x,Vec* g,MatT* h,const Problem& prob);
  static void limitAlpha(T& alpha,const Vec& x,const Vec& dx,const Problem& prob,T scale=0.1f);
  static void printConstraints(const Vec& x,const Problem& prob);
  static bool solveDx(const MatT& h,const Vec& g,Vec& dx);
  //param
  T _muInit,_muDec,_muFinal;
  T _alphaInc,_alphaDec,_tolAlpha;
  T _tolG,_tolGFinal;
  T _c1,_c2;    //Wolfe-Condition
  bool _callback;
  int _maxIter;
};
}

#endif
