#include "FEMPDController.h"

namespace PHYSICSMOTION {
//FEMPDController
template <typename T>
FEMPDController<T>::FEMPDController()
  :_pTarget(Vec::Zero(0)),_dTarget(Vec::Zero(0)),_kP(0),_kD(0) {}
template <typename T>
FEMPDController<T>::FEMPDController(const Vec& pTarget,T kP)
  :_pTarget(pTarget),_dTarget(Vec::Zero(0)),_kP(kP),_kD(0) {}
template <typename T>
FEMPDController<T>::FEMPDController(const Vec& pTarget,T kP,const Vec& dTarget,T kD)
  :_pTarget(pTarget),_dTarget(dTarget),_kP(kP),_kD(kD) {}
template <typename T>
const typename FEMPDController<T>::Vec& FEMPDController<T>::pTarget() const {
  return _pTarget;
}
template <typename T>
const typename FEMPDController<T>::Vec& FEMPDController<T>::dTarget() const {
  return _dTarget;
}
template <typename T>
void FEMPDController<T>::setPTarget(const Vec& pTarget) {
  ASSERT_MSG(_pTarget.size()==pTarget.size(),"pTarget size mismatch!")
  _pTarget=pTarget;
}
template <typename T>
void FEMPDController<T>::setDTarget(const Vec& dTarget) {
  ASSERT_MSG(_dTarget.size()==dTarget.size(),"dTarget size mismatch!")
  _dTarget=dTarget;
}
template <typename T>
void FEMPDController<T>::reset() {
  _pTarget.resize(0);
  _dTarget.resize(0);
  _kP=0;
  _kD=0;
}
template <typename T>
void FEMPDController<T>::resetP(const Vec& pTarget,T kP) {
  _pTarget=pTarget;
  _dTarget.resize(0);
  _kP=kP;
  _kD=0;
}
template <typename T>
void FEMPDController<T>::resetD(const Vec& dTarget,T kD) {
  _pTarget.resize(0);
  _dTarget=dTarget;
  _kP=0;
  _kD=kD;
}
template <typename T>
void FEMPDController<T>::resetPD(const Vec& pTarget,T kP,const Vec& dTarget,T kD) {
  _pTarget=pTarget;
  _dTarget=dTarget;
  _kP=kP;
  _kD=kD;
}
template <typename T>
void FEMPDController<T>::assemble(Vec* G,MatT* H,const FEMGradientInfo<T>& IN,T dt) const {
  if(_pTarget.size()>0) {
    Vec diff=IN.getU().segment(0,_pTarget.size())-_pTarget;
    if(G)
      G->segment(0,_pTarget.size())+=diff*_kP;
    if(H)
      H->block(0,0,_pTarget.size(),_pTarget.size()).diagonal().array()+=_kP*dt;
  }
  if(_dTarget.size()>0) {
    if(G)
      G->segment(0,_dTarget.size())-=_dTarget*_kD/dt;
    if(H)
      H->block(0,0,_dTarget.size(),_dTarget.size()).diagonal().array()+=_kD/dt;
  }
}
template <typename T>
void FEMPDController<T>::assemble(T* E,Vec* G,SMatT* H,const Vec& pN,const Vec& p,T dt) const {
  if(_pTarget.size()>0) {
    Vec diff=p.segment(0,_pTarget.size())-_pTarget;
    if(E)
      *E+=diff.squaredNorm()*_kP/2;
    if(G)
      G->segment(0,_pTarget.size())+=diff*_kP;
    if(H) {
      SMatT dH;
      STrips trips;
      for(int i=0; i<_pTarget.size(); i++)
        trips.push_back(STrip(i,i,_kP));
      dH.resize(H->rows(),H->cols());
      dH.setFromTriplets(trips.begin(),trips.end());
      *H+=dH;
    }
  }
  if(_dTarget.size()>0) {
    Vec diff=(p.segment(0,_dTarget.size())-pN.segment(0,_dTarget.size()))/dt-_dTarget;
    if(E)
      *E+=diff.squaredNorm()*_kD/2;
    if(G)
      G->segment(0,_dTarget.size())+=diff*(_kD/dt);
    if(H) {
      SMatT dH;
      STrips trips;
      for(int i=0; i<_dTarget.size(); i++)
        trips.push_back(STrip(i,i,_kD/(dt*dt)));
      dH.resize(H->rows(),H->cols());
      dH.setFromTriplets(trips.begin(),trips.end());
      *H+=dH;
    }
  }
}
template <typename T>
typename FEMPDController<T>::MatT FEMPDController<T>::DGDu(int rows,T dt) const {
  MatT ret;
  ret.setZero(rows,_pTarget.size()+_dTarget.size());
  if(_pTarget.size()>0)
    ret.block(0,0,_pTarget.size(),_pTarget.size()).diagonal().setConstant(-_kP);
  if(_dTarget.size()>0)
    ret.block(0,_pTarget.size(),_dTarget.size(),_dTarget.size()).diagonal().setConstant(-_kD/dt);
  return ret;
}
template <typename T>
void FEMPDController<T>::debugDGDu(int rows,T dt) {
  Vec G=Vec::Zero(rows),G2=G;
  FEMGradientInfo<T> IN(MatT::Random(rows,rows),MatT::Random(rows,rows),Vec::Random(rows),Vec::Random(rows*2));
  assemble(&G,NULL,IN,dt);

  DEFINE_NUMERIC_DELTA_T(T)
  Vec dTarget=Vec::Random(_pTarget.size()+_dTarget.size());
  _pTarget+=dTarget.segment(0,_pTarget.size())*DELTA;
  _dTarget+=dTarget.segment(_pTarget.size(),_dTarget.size())*DELTA;
  assemble(&G2,NULL,IN,dt);
  DEBUG_GRADIENT("DGDu",(DGDu(rows,dt)*dTarget).norm(),(DGDu(rows,dt)*dTarget-(G2-G)/DELTA).norm())
}
//FEMReducedPDController
template <typename T>
FEMReducedPDController<T>::FEMReducedPDController()
  :FEMPDController<T>(),_B(MatT::Zero(0,0)) {}
template <typename T>
FEMReducedPDController<T>::FEMReducedPDController(const Vec& pTarget,T kP,const MatT& B)
  :FEMPDController<T>(pTarget,kP),_B(B) {}
template <typename T>
FEMReducedPDController<T>::FEMReducedPDController(const Vec& pTarget,T kP,const Vec& dTarget,T kD,const MatT& B)
  :FEMPDController<T>(pTarget,kP,dTarget,kD),_B(B) {}
template <typename T>
void FEMReducedPDController<T>::reset() {
  FEMPDController<T>::reset();
  _B.setZero(0,0);
}
template <typename T>
void FEMReducedPDController<T>::resetPReduced(const Vec& pTarget,T kP,const MatT& B) {
  _pTarget=pTarget;
  _dTarget.setZero(0);
  _kP=kP;
  _kD=0;
  _B=B;
  ASSERT_MSGV(pTarget.size()==_B.cols(),"B size mismatch pTarget.size()=%d, B.cols()=%d!",(int)pTarget.size(),(int)B.cols())
}
template <typename T>
void FEMReducedPDController<T>::resetDReduced(const Vec& dTarget,T kD,const MatT& B) {
  _pTarget.resize(0);
  _dTarget=dTarget;
  _kP=0;
  _kD=kD;
  _B=B;
  ASSERT_MSGV(dTarget.size()==B.cols(),"B size mismatch dTarget.size()=%d, B.cols()=%d!",(int)dTarget.size(),(int)B.cols())
}
template <typename T>
void FEMReducedPDController<T>::resetPDReduced(const Vec& pTarget,T kP,const Vec& dTarget,T kD,const MatT& B) {
  _pTarget=pTarget;
  _dTarget=dTarget;
  _kP=kP;
  _kD=kD;
  _B=B;
  ASSERT_MSGV(pTarget.size()==B.cols(),"kP size mismatch pTarget.size()=%d, B.cols()=%d!",(int)pTarget.size(),(int)B.cols())
  ASSERT_MSGV(dTarget.size()==B.cols(),"kD size mismatch dTarget.size()=%d, B.cols()=%d!",(int)dTarget.size(),(int)B.cols())
}
template <typename T>
void FEMReducedPDController<T>::assemble(Vec* G,MatT* H,const FEMGradientInfo<T>& IN,T dt) const {
  if(_pTarget.size()>0) {
    Vec diff=_B.transpose()*IN.getU().segment(0,_B.rows())-_pTarget;
    if(G)
      G->segment(0,_B.rows())+=_B*diff*_kP;
    if(H)
      H->block(0,0,_B.rows(),_B.rows())+=_B*_B.transpose()*_kP*dt;
  }
  if(_dTarget.size()>0) {
    if(G)
      G->segment(0,_B.rows())-=_B*_dTarget*_kD/dt;
    if(H)
      H->block(0,0,_B.rows(),_B.rows())+=_B*_B.transpose()*_kD/dt;
  }
}
template <typename T>
void FEMReducedPDController<T>::assemble(T*,Vec*,SMatT*,const Vec&,const Vec&,T) const {
  ASSERT_MSG(false,"Reduced PDController does not support fullspace control, because it would induce a dense matrix!")
}
template <typename T>
typename FEMReducedPDController<T>::MatT FEMReducedPDController<T>::DGDu(int rows,T dt) const {
  MatT ret;
  ret.setZero(rows,_pTarget.size()+_dTarget.size());
  if(_pTarget.size()>0)
    ret.block(0,0,_B.rows(),_B.cols())=-_B*_kP;
  if(_dTarget.size()>0)
    ret.block(0,_B.cols(),_B.rows(),_B.cols())=-_B*_kD/dt;
  return ret;
}
template class FEMPDController<FLOAT>;
template class FEMReducedPDController<FLOAT>;
}
