#include "FEMGradientInfo.h"
#include "FEMReducedSystem.h"
#include "FEMMesh.h"
#include <Utils/RotationUtils.h>

namespace PHYSICSMOTION {
template <typename T>
FEMGradientInfo<T>::FEMGradientInfo() {}
template <typename T>
FEMGradientInfo<T>::FEMGradientInfo(const MatT& B,const MatT& BI,const Vec& pos0I,const Vec& x) {
  reset(mapCM(B),mapCM(BI),mapCV(pos0I),mapCV(x));
}
template <typename T>
FEMGradientInfo<T>::FEMGradientInfo(const FEMReducedSystem<T>& sys,const Vec& x) {
  reset(sys,mapCV(x));
}
template <typename T>
FEMGradientInfo<T>::FEMGradientInfo(std::shared_ptr<FEMReducedSystem<T>> sys,const Vec& x) {
  reset(sys,x);
}
template <typename T>
FEMGradientInfo<T>::FEMGradientInfo(const FEMReducedSystem<T>& sys,VecCM x) {
  reset(sys,x);
}
template <typename T>
void FEMGradientInfo<T>::reset(MatTCM B,MatTCM BI,VecCM pos0I,VecCM x) {
  _x=x;
  _nB=B.cols();
  _qInterpL=BI*getU()+pos0I;
  _DDxDtDu.setZero(0,0);    //flag that this feature is not computed
  ASSERT_MSGV(x.size()==_nB*2 || x.size()==(_nB+6)*2,"Incorrect size of x (must be %d or %d)!",_nB*2,(_nB+6)*2)
  if(x.size()==_nB*2) {
    _R.setIdentity();
    _qInterp=_qInterpL;
  } else {
    _R=expWGradV<T,Vec3T>(x.template segment<3>(_nB+3));
    _qInterp.resize(_qInterpL.size());
    OMP_PARALLEL_FOR_
    for(int i=0; i<_qInterp.size(); i+=3)
      _qInterp.template segment<3>(i)=getR()*_qInterpL.template segment<3>(i)+getT();
  }
}
template <typename T>
void FEMGradientInfo<T>::reset(const FEMReducedSystem<T>& sys,VecCM x) {
  reset(mapCM(sys.getBasis()),mapCM(sys.getBasisInterp()),mapCV(sys.getPos0Interp()),x);
}
template <typename T>
void FEMGradientInfo<T>::reset(std::shared_ptr<FEMReducedSystem<T>> sys,const Vec& x) {
  reset(*sys,mapCV(x));
}
template <typename T>
FEMGradientInfo<T> FEMGradientInfo<T>::integrate(MatTCM B,MatTCM BI,VecCM pos0L,T dt,const Vec* a,const Vec* vNew) const {
  return FEMGradientInfo<T>(B,BI,pos0L,integrate(dt,a,vNew));
}
template <typename T>
FEMGradientInfo<T> FEMGradientInfo<T>::integrate(const FEMReducedSystem<T>& sys,T dt,const Vec* a,const Vec* vNew) const {
  return FEMGradientInfo<T>(sys,integrate(dt,a,vNew));
}
template <typename T>
FEMGradientInfo<T> FEMGradientInfo<T>::integrate(std::shared_ptr<FEMReducedSystem<T>> sys,T dt,const Vec* a,const Vec* vNew) const {
  return integrate(*sys,dt,a,vNew);
}
template <typename T>
typename FEMGradientInfo<T>::VecCM FEMGradientInfo<T>::getDOF() const {
  return Eigen::Map<const Vec>(_x.data(),_x.size()/2);
}
template <typename T>
typename FEMGradientInfo<T>::VecCM FEMGradientInfo<T>::getDDOFDt() const {
  return Eigen::Map<const Vec>(_x.data()+_x.size()/2,_x.size()/2);
}
template <typename T>
typename FEMGradientInfo<T>::VecCM FEMGradientInfo<T>::getQInterp() const {
  return Eigen::Map<const Vec>(_qInterp.data(),_qInterp.size());
}
template <typename T>
typename FEMGradientInfo<T>::VecCM FEMGradientInfo<T>::getQInterpL() const {
  return Eigen::Map<const Vec>(_qInterpL.data(),_qInterpL.size());
}
template <typename T>
typename FEMGradientInfo<T>::Vec FEMGradientInfo<T>::getDQInterpDt(MatTCM BI) const {
  Vec ret=getDQInterpLDt(BI);
  OMP_PARALLEL_FOR_
  for(int i=0; i<ret.size(); i+=3)
    ret.template segment<3>(i)=getR()*(getWLocal().cross(_qInterpL.template segment<3>(i))+ret.template segment<3>(i)+getVLocal());
  return ret;
}
template <typename T>
typename FEMGradientInfo<T>::Vec FEMGradientInfo<T>::getDQInterpLDt(MatTCM BI) const {
  return BI*_x.segment(_x.size()/2,BI.cols());
}
template <typename T>
typename FEMGradientInfo<T>::Mat3XT FEMGradientInfo<T>::getQInterpJac(MatTCM BI,int off) const {
  if(_x.size()==_nB*2)
    return BI.block(off,0,3,_nB);
  else {
    Mat3XT ret;
    ret.resize(3,_nB+6);
    ret.block(0,0,3,_nB)=BI.block(off,0,3,_nB);
    ret.template block<3,3>(0,_nB).setIdentity();
    ret.template block<3,3>(0,_nB+3)=-cross<T>(_qInterpL.template segment<3>(off));
    return getR()*ret;
  }
}
template <typename T>
bool FEMGradientInfo<T>::isNoninertial() const {
  return _x.size()>_nB*2;
}
template <typename T>
const typename FEMGradientInfo<T>::Vec& FEMGradientInfo<T>::getX() const {
  return _x;
}
template <typename T>
const typename FEMGradientInfo<T>::Vec3T FEMGradientInfo<T>::getVPos(int off) const {
  return _qInterp.template segment<3>(off);
}
template <typename T>
const typename FEMGradientInfo<T>::Vec3T FEMGradientInfo<T>::getVPosLocal(int off) const {
  return _qInterpL.template segment<3>(off);
}
template <typename T>
const typename FEMGradientInfo<T>::Mat3T& FEMGradientInfo<T>::getR() const {
  return _R;
}
template <typename T>
typename FEMGradientInfo<T>::Vec3T FEMGradientInfo<T>::getT() const {
  if(_x.size()==_nB*2)
    return Vec3T::Zero();
  else return _x.template segment<3>(_nB);
}
template <typename T>
typename FEMGradientInfo<T>::Vec FEMGradientInfo<T>::getU() const {
  return Eigen::Map<const Vec>(_x.data(),_nB);
}
template <typename T>
typename FEMGradientInfo<T>::Vec FEMGradientInfo<T>::getDUDt() const {
  return Eigen::Map<const Vec>(_x.data()+_x.size()/2,_nB);
}
template <typename T>
typename FEMGradientInfo<T>::Vec3T FEMGradientInfo<T>::getVLocal() const {
  if(_x.size()==_nB*2)
    return Vec3T::Zero();
  else return _x.template segment<3>(_x.size()/2+_nB);
}
template <typename T>
typename FEMGradientInfo<T>::Vec3T FEMGradientInfo<T>::getV() {
  return getR()*getVLocal();
}
template <typename T>
typename FEMGradientInfo<T>::Vec3T FEMGradientInfo<T>::getWLocal() const {
  if(_x.size()==_nB*2)
    return Vec3T::Zero();
  else return _x.template segment<3>(_x.size()/2+_nB+3);
}
template <typename T>
typename FEMGradientInfo<T>::Vec3T FEMGradientInfo<T>::getW() const {
  return getR()*getWLocal();
}
template <typename T>
const typename FEMGradientInfo<T>::MatT& FEMGradientInfo<T>::getDDxDtDu() const {
  return _DDxDtDu;
}
template <typename T>
typename FEMGradientInfo<T>::MatT FEMGradientInfo<T>::getDvwDu() const {
  MatT ret=_DDxDtDu.block(_DDxDtDu.rows()-6,0,6,_DDxDtDu.cols());
  ret.block(0,0,3,ret.cols())=(getR()*ret.block(0,0,3,ret.cols())).eval();
  ret.block(3,0,3,ret.cols())=(getR()*ret.block(3,0,3,ret.cols())).eval();
  return ret;
}
template <typename T>
typename FEMGradientInfo<T>::MatT FEMGradientInfo<T>::getDvwDuLocal() const {
  return _DDxDtDu.block(_DDxDtDu.rows()-6,0,6,_DDxDtDu.cols());
}
template <typename T>
void FEMGradientInfo<T>::setDDxDtDu(const MatT& DDxDtDu) {
  _DDxDtDu=DDxDtDu;
}
template <typename T>
std::vector<FEMCollision<T>>& FEMGradientInfo<T>::getCollisions() {
  return _colls;
}
template <typename T>
void FEMGradientInfo<T>::debug(int N,int N2,int M) {
  MatT B=MatT::Random(N*3,M);
  MatT BI=MatT::Random(N2*3,M);
  Vec pos0I=Vec::Random(N2*3);
  {
    DEFINE_NUMERIC_DELTA_T(T)
    FEMGradientInfo<T> I(B,BI,pos0I,Vec::Random(M*2));
    Vec Q=I.getQInterp(),DQDt=I.getDQInterpDt(mapCM(BI));
    FEMGradientInfo<T> I2=I.integrate(mapCM(B),mapCM(BI),mapCV(pos0I),DELTA,NULL,NULL);
    Vec Q2=I2.getQInterp();
    DEBUG_GRADIENT("DQDt-u",DQDt.norm(),(DQDt-(Q2-Q)/DELTA).norm())
  }
  {
    DEFINE_NUMERIC_DELTA_T(T)
    FEMGradientInfo<T> I(B,BI,pos0I,Vec::Random((M+6)*2));
    Vec Q=I.getQInterp(),DQDt=I.getDQInterpDt(mapCM(BI));
    FEMGradientInfo<T> I2=I.integrate(mapCM(B),mapCM(BI),mapCV(pos0I),DELTA,NULL,NULL);
    Vec Q2=I2.getQInterp();
    DEBUG_GRADIENT("DQDt-uvw",DQDt.norm(),(DQDt-(Q2-Q)/DELTA).norm())
  }
}
//helper
template <typename T>
typename FEMGradientInfo<T>::Vec FEMGradientInfo<T>::integrate(T dt,const Vec* a,const Vec* vNew) const {
  Vec x=_x;
  if(a) {
    ASSERT_MSG(a->size()==x.size()/2,"Incorrect acceleration size!")
    x.segment(x.size()/2,x.size()/2)+=*a*dt;
  }
  if(vNew) {
    ASSERT_MSG(vNew->size()==x.size()/2,"Incorrect new velocity size!")
    x.segment(x.size()/2,x.size()/2)=*vNew;
  }
  if(x.size()==_nB*2) {
    x.segment(0,_nB)+=x.segment(_nB,_nB)*dt;
  } else {
    Mat3T R=expWGradV<T,Vec3T>(x.template segment<3>(_nB+3));
    x.segment(0,_nB)+=x.segment(x.size()/2,_nB)*dt;
    x.template segment<3>(_nB)+=R*x.template segment<3>(x.size()/2+_nB)*dt;
    Mat3T RNew=expWGradV<T,Vec3T>(R*x.template segment<3>(x.size()/2+_nB+3)*dt)*R;
    x.template segment<3>(_nB+3)=invExpW(RNew);
  }
  return x;
}
template struct FEMGradientInfo<FLOAT>;
}
