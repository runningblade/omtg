#ifndef DEFORMABLE_VISUALIZER_H
#define DEFORMABLE_VISUALIZER_H

#include "FEMReducedSystem.h"
#include "FEMGradientInfo.h"
#include "Hexahedron.h"
#include "Tetrahedron.h"
#include "FEMCollision.h"
#include <Utils/Utils.h>
#include <TinyVisualizer/Drawer.h>
#include <TinyVisualizer/MeshShape.h>
#include <TinyVisualizer/Bullet3DShape.h>
#include <TinyVisualizer/LowDimensionalMeshShape.h>

namespace PHYSICSMOTION {
template <typename T>
std::shared_ptr<DRAWER::Shape> visualizeFEMMeshSurface(std::shared_ptr<FEMMesh<T>> b,const Eigen::Matrix<GLfloat,3,1>& colorBody=Eigen::Matrix<GLfloat,3,1>(.3,.3,.3),bool wire=false) {
  using namespace DRAWER;
  std::shared_ptr<MeshShape> mesh(new MeshShape);
  for(int i=0; i<b->nrSV(); i++)
    mesh->addVertex(b->getSV(i)->operator()(b->pos0()).template cast<GLfloat>());
  if(wire) {
    std::unordered_set<Eigen::Matrix<int,2,1>,EdgeHash> vss;
    for(int i=0; i<b->nrSC(); i++)
      for(int d=0; d<3; d++) {
        Eigen::Matrix<int,2,1> id(b->getSC(i)[(d+1)%3],b->getSC(i)[(d+2)%3]);
        sort2(id[0],id[1]);
        vss.insert(id);
      }
    for(const Eigen::Matrix<int,2,1>& id:vss)
      mesh->addIndex(id);
    mesh->setMode(GL_LINES);
    mesh->setColor(GL_LINES,colorBody[0],colorBody[1],colorBody[2]);
    mesh->setLineWidth(2);
  } else {
    for(int i=0; i<b->nrSC(); i++)
      mesh->addIndex(b->getSC(i));
    mesh->setMode(GL_TRIANGLES);
    mesh->setColor(GL_TRIANGLES,colorBody[0],colorBody[1],colorBody[2]);
    mesh->computeNormals();
  }
  return mesh;
}
template <typename T>
std::shared_ptr<DRAWER::Shape> visualizeFEMLowDimensionalMeshSurface(std::shared_ptr<FEMReducedSystem<T>> sys,const Eigen::Matrix<GLfloat,3,1>& colorBody=Eigen::Matrix<GLfloat,3,1>(.3,.3,.3),bool wire=false) {
  using namespace DRAWER;
  std::shared_ptr<MeshShape> mesh=std::dynamic_pointer_cast<MeshShape>(visualizeFEMMeshSurface(sys->getBody(),colorBody,wire));
  std::shared_ptr<LowDimensionalMeshShape> LD(new LowDimensionalMeshShape(mesh));
  LD->setLowToHighDimensionalMapping(sys->getBasisInterp().block(0,0,sys->getBody()->nrSV()*3,sys->getBasisInterp().cols()).template cast<GLfloat>());
  std::shared_ptr<Bullet3DShape> trans(new Bullet3DShape);
  trans->addShape(LD);
  return trans;
}
template <typename T>
std::shared_ptr<DRAWER::Shape> visualizeFEMMeshTetrahedron(std::shared_ptr<FEMMesh<T>> b,const Eigen::Matrix<GLfloat,3,1>& colorBody=Eigen::Matrix<GLfloat,3,1>(.6,.3,.3)) {
  using namespace DRAWER;
  std::shared_ptr<MeshShape> mesh(new MeshShape);
  std::unordered_map<std::shared_ptr<FEMVertex<T>>,int> vss;
  for(int i=0; i<b->nrV(); i++) {
    vss[b->getV(i)]=i;
    mesh->addVertex(b->getV(i)->operator()(b->pos0()).template cast<GLfloat>());
  }
  std::unordered_set<Eigen::Matrix<int,2,1>,EdgeHash> iss;
  for(int i=0; i<b->nrC(); i++) {
    std::shared_ptr<Tetrahedron<T>> tet=std::dynamic_pointer_cast<Tetrahedron<T>>(b->getC(i));
    std::shared_ptr<FEMVertex<T>> V[4];
    for(int d=0; d<4; d++)
      V[d]=tet->V(d);
    for(int r=0; r<4; r++)
      for(int c=r+1; c<4; c++) {
        Eigen::Matrix<int,2,1> id(vss.find(V[r])->second,vss.find(V[c])->second);
        sort2(id[0],id[1]);
        iss.insert(id);
      }
  }
  for(const Eigen::Matrix<int,2,1>& id:iss)
    mesh->addIndex(id);
  mesh->setMode(GL_LINES);
  mesh->setColor(GL_LINES,colorBody[0],colorBody[1],colorBody[2]);
  mesh->setLineWidth(2);
  return mesh;
}
template <typename T>
std::shared_ptr<DRAWER::Shape> visualizeFEMMeshHexahedron(std::shared_ptr<FEMMesh<T>> b,const Eigen::Matrix<GLfloat,3,1>& colorBody=Eigen::Matrix<GLfloat,3,1>(.3,.3,.6)) {
  using namespace DRAWER;
  std::shared_ptr<MeshShape> mesh(new MeshShape);
  std::unordered_map<std::shared_ptr<FEMVertex<T>>,int> vss;
  for(int i=0; i<b->nrV(); i++) {
    vss[b->getV(i)]=i;
    mesh->addVertex(b->getV(i)->operator()(b->pos0()).template cast<GLfloat>());
  }
  std::unordered_set<Eigen::Matrix<int,2,1>,EdgeHash> iss;
  for(int i=0; i<b->nrC(); i++) {
    std::shared_ptr<Hexahedron<T>> tet=std::dynamic_pointer_cast<Hexahedron<T>>(b->getC(i));
    std::shared_ptr<FEMVertex<T>> V[8];
    for(int d=0; d<8; d++)
      V[d]=tet->V(d);
#define VERT(X,Y,Z) V[(X==0?0:1)+(Y==0?0:2)+(Z==0?0:4)]
    Eigen::Matrix<int,2,1> id;
    for(int x=0; x<2; x++)
      for(int y=0; y<2; y++) {
        id=Eigen::Matrix<int,2,1>(vss.find(VERT(x,y,0))->second,vss.find(VERT(x,y,1))->second);
        sort2(id[0],id[1]);
        iss.insert(id);
        id=Eigen::Matrix<int,2,1>(vss.find(VERT(x,0,y))->second,vss.find(VERT(x,1,y))->second);
        sort2(id[0],id[1]);
        iss.insert(id);
        id=Eigen::Matrix<int,2,1>(vss.find(VERT(0,x,y))->second,vss.find(VERT(1,x,y))->second);
        sort2(id[0],id[1]);
        iss.insert(id);
      }
#undef VERT
  }
  for(const Eigen::Matrix<int,2,1>& id:iss)
    mesh->addIndex(id);
  mesh->setMode(GL_LINES);
  mesh->setColor(GL_LINES,colorBody[0],colorBody[1],colorBody[2]);
  mesh->setLineWidth(2);
  return mesh;
}
template <typename T>
std::shared_ptr<DRAWER::Shape> visualizeFEMMeshCubature(std::shared_ptr<FEMReducedSystem<T>> sys,const Eigen::Matrix<GLfloat,3,1>& colorBody=Eigen::Matrix<GLfloat,3,1>(.6,.3,.3)) {
#define ID(A,B,C) mesh->addIndex(Eigen::Matrix<int,3,1>(off+(A),off+(B),off+(C)));
#define IDH(A,B,C,D) ID(A,B,C) ID(A,C,D)
  int off=0;
  using namespace DRAWER;
  std::shared_ptr<MeshShape> mesh(new MeshShape);
  FEMGradientInfo<T> I(*sys,FEMGradientInfo<T>::Vec::Zero(sys->getBasis().cols()*2));
  typename FEMGradientInfo<T>::Vec q=I.getQInterp();
  Eigen::Map<const typename FEMGradientInfo<T>::Vec> qM(q.data(),q.size());
  for(std::shared_ptr<FEMEnergy<T>> e:sys->getReducedEnergy())
    if(std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(e)) {
      std::shared_ptr<FEMStVKElasticEnergy<T>> eStVK=std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(e);
      //tet
      //1------2
      //|\    /|
      //| \  / |
      //|  \/  |
      //|  /\  |
      //| /  \ |
      //|/    \|
      //0------3
      std::shared_ptr<Tetrahedron<T>> tet=std::dynamic_pointer_cast<Tetrahedron<T>>(eStVK->getCell());
      if(tet) {
        ID(0,1,2)
        ID(0,2,3)
        ID(0,3,1)
        ID(1,3,2)
        for(int d=0; d<4; d++,off++)
          mesh->addVertex(tet->V(d)->operator()(qM).template cast<GLfloat>());
      }
      //hex
      //   6--------7
      //  /|       /|
      // / |      / |
      //4--------5  |
      //|  |     |  |
      //|  2-----|--3
      //| /      | /
      //|/       |/
      //0--------1
      std::shared_ptr<Hexahedron<T>> hex=std::dynamic_pointer_cast<Hexahedron<T>>(eStVK->getCell());
      if(hex) {
        IDH(0,2,3,1)
        IDH(4,5,7,6)
        IDH(0,4,6,2)
        IDH(1,3,7,5)
        IDH(0,1,5,4)
        IDH(2,6,7,3)
        for(int d=0; d<8; d++,off++)
          mesh->addVertex(hex->V(d)->operator()(qM).template cast<GLfloat>());
      }
    }
  mesh->setMode(GL_TRIANGLES);
  mesh->setColor(GL_TRIANGLES,colorBody[0],colorBody[1],colorBody[2]);
  mesh->computeNormals();
  return mesh;
#undef ID
}
template <typename T>
void updateSurface(std::shared_ptr<DRAWER::Shape> shape,std::shared_ptr<FEMMesh<T>> b,const typename FEMMesh<T>::Vec& pos) {
  using namespace DRAWER;
  std::shared_ptr<MeshShape> shapeM=std::dynamic_pointer_cast<MeshShape>(shape);
  ASSERT_MSG(shapeM,"MeshShape must exist!")
  std::vector<GLfloat> vertices;
  for(int i=0; i<b->nrSV(); i++) {
    Eigen::Matrix<GLfloat,3,1> v=b->getSV(i)->operator()(FEMMesh<T>::mapCV(pos)).template cast<GLfloat>();
    for(int d=0; d<3; d++)
      vertices.push_back(v[d]);
  }
  shapeM->setVertices(vertices);
  shapeM->computeNormals();
}
template <typename T>
void updateSurface(std::shared_ptr<DRAWER::Shape> shape,const FEMGradientInfo<T>& info) {
  using namespace DRAWER;
  std::shared_ptr<Bullet3DShape> trans=std::dynamic_pointer_cast<Bullet3DShape>(shape);
  ASSERT_MSG(trans && trans->numChildren()==1,"Bullet3DShape must exist and number of children must be 1!")
  trans->setLocalTranslate(info.getT().template cast<GLfloat>());
  trans->setLocalRotate(info.getR().template cast<GLfloat>());

  std::shared_ptr<LowDimensionalMeshShape> LD=std::dynamic_pointer_cast<LowDimensionalMeshShape>(trans->getChild(0));
  ASSERT_MSG(LD,"Cannot find LowDimensionalMeshShape!")
  LD->updateHighDimensionalMapping(info.getU().template cast<GLfloat>());
}
template <typename T>
void updateTetrahedron(std::shared_ptr<DRAWER::Shape> shape,std::shared_ptr<FEMMesh<T>> b,const typename FEMMesh<T>::Vec& pos) {
  using namespace DRAWER;
  std::shared_ptr<MeshShape> shapeM=std::dynamic_pointer_cast<MeshShape>(shape);
  std::vector<GLfloat> vertices;
  for(int i=0; i<b->nrV(); i++) {
    Eigen::Matrix<GLfloat,3,1> v=b->getV(i)->operator()(FEMMesh<T>::mapCV(pos)).template cast<GLfloat>();
    for(int d=0; d<3; d++)
      vertices.push_back(v[d]);
  }
  shapeM->setVertices(vertices);
}
template <typename T>
void updateHexahedron(std::shared_ptr<DRAWER::Shape> shape,std::shared_ptr<FEMMesh<T>> b,const typename FEMMesh<T>::Vec& pos) {
  updateTetrahedron(shape,b,pos);
}
template <typename T>
void updateCubature(std::shared_ptr<DRAWER::Shape> shape,const FEMGradientInfo<T>& info,const std::shared_ptr<FEMReducedSystem<T>> sys) {
  int off=0;
  using namespace DRAWER;
  std::shared_ptr<MeshShape> mesh=std::dynamic_pointer_cast<MeshShape>(shape);
  for(std::shared_ptr<FEMEnergy<T>> e:sys->getReducedEnergy())
    if(std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(e)) {
      std::shared_ptr<FEMStVKElasticEnergy<T>> eStVK=std::dynamic_pointer_cast<FEMStVKElasticEnergy<T>>(e);
      //tet
      //1------2
      //|\    /|
      //| \  / |
      //|  \/  |
      //|  /\  |
      //| /  \ |
      //|/    \|
      //0------3
      std::shared_ptr<Tetrahedron<T>> tet=std::dynamic_pointer_cast<Tetrahedron<T>>(eStVK->getCell());
      if(tet)
        for(int d=0; d<4; d++,off++) {
          int offQ=tet->V(d)->jacobian().begin()->first;
          mesh->setVertex(off,info.getQInterp().template segment<3>(offQ).template cast<GLfloat>());
        }
      //hex
      //   6--------7
      //  /|       /|
      // / |      / |
      //4--------5  |
      //|  |     |  |
      //|  2-----|--3
      //| /      | /
      //|/       |/
      //0--------1
      std::shared_ptr<Hexahedron<T>> hex=std::dynamic_pointer_cast<Hexahedron<T>>(eStVK->getCell());
      if(hex)
        for(int d=0; d<8; d++,off++) {
          int offQ=hex->V(d)->jacobian().begin()->first;
          mesh->setVertex(off,info.getQInterp().template segment<3>(offQ).template cast<GLfloat>());
        }
    }
  mesh->computeNormals();
}
template <typename T>
std::shared_ptr<DRAWER::Shape> visualizeFEMWaterLevel(T level,T sz=5,const Eigen::Matrix<GLfloat,3,1>& color=Eigen::Matrix<GLfloat,3,1>(.3,.3,.6)) {
  using namespace DRAWER;
  Eigen::Matrix<GLfloat,2,1> tc(0,0);
  std::shared_ptr<MeshShape> mesh(new MeshShape);
  mesh->addVertex(Eigen::Matrix<GLfloat,3,1>(-sz,-sz,level),&tc);
  mesh->addVertex(Eigen::Matrix<GLfloat,3,1>( sz,-sz,level),&tc);
  mesh->addVertex(Eigen::Matrix<GLfloat,3,1>( sz, sz,level),&tc);
  mesh->addVertex(Eigen::Matrix<GLfloat,3,1>(-sz, sz,level),&tc);
  mesh->addIndex(Eigen::Matrix<int,3,1>(0,1,2));
  mesh->addIndex(Eigen::Matrix<int,3,1>(0,2,3));
  mesh->setMode(GL_TRIANGLES);
  mesh->setColor(GL_TRIANGLES,color[0],color[1],color[2]);
  mesh->computeNormals();
  return mesh;
}
template <typename T>
void visualizeCollision(DRAWER::Drawer& drawer,std::shared_ptr<DRAWER::MeshShape>& cS,std::vector<FEMCollision<T>>& colls,const Eigen::Matrix<GLfloat,3,1>& color=Eigen::Matrix<GLfloat,3,1>(1,0,0)) {
  using namespace DRAWER;
  if(cS)
    drawer.removeShape(cS);
  if(colls.empty())
    return;
  //visualize
  int id=0;
  cS.reset(new MeshShape);
  for(const FEMCollision<T>& c:colls) {
    cS->addVertex(c.pos().template cast<GLfloat>());
    cS->addIndexSingle(id++);
  }
  cS->setMode(GL_POINTS);
  cS->setColor(GL_POINTS,color[0],color[1],color[2]);
  cS->setPointSize(2);
  drawer.addShape(cS);
}
}

#endif
