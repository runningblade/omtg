BASE="../../omtg-build"
import importlib,os,sys,math,random
import numpy as np
sys.path.append(BASE)
import pyPhysicsMotion as pm
from Visualizer import drawFEM

if __name__=='__main__':
	Lambda=10000
	Mu=10000
	g=9.81
    
	type=0
	if os.path.exists(BASE+"/sysRFrictional.dat"):
		sys=pm.FEMReducedSystemFLOAT()
		sys.readStr(BASE+"/sysRFrictional.dat")
	else:
		if type==0:
			body=pm.FEMMeshFLOAT(BASE+"/torus.obj_tet.mesh",True)
		elif type==1:
			body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
		else:
			body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
			bodyHOct=FEMOctreeMeshFLOAT(body,0)
			body=bodyHOct.getMesh()
    
		sys=pm.FEMReducedSystemFLOAT(body)
		sys.setEnv(pm.EnvironmentHeightFLOAT());
		class CustomEnvCallback(pm.EnvironmentCallback):
			def __init__(self):
		  		pm.EnvironmentCallback.__init__(self)
			def height(self,x,y):
				return -2+x*0.1
		sys.getEnv().createHills(-20,20,-20,20,CustomEnvCallback(),.1)
		sys.addStVKElasticEnergy(Lambda,Mu)
		sys.addGravitationalEnergy(g)
		sys.maxContact(10)
		sys.mu(0.7)
		sys.buildBasis(50)
		sys.writeStr(BASE+"/sysRFrictional.dat")
    
	frames=[pm.FEMGradientInfoFLOAT(sys,np.array([0]*(sys.getBasis().shape[1]+6)*2,dtype=np.double))]
	ctrl=pm.FEMPDControllerFLOAT()
	while(len(frames)<1000):
		print("Simulating frame: %d"%len(frames))
		ret=sys.step(frames[-1],ctrl,pm.FEMSystemFLOAT.SP)
		print("Number of collisions: %d"%ret[0].getCollisions().size())
		if len(frames)==2:
			sys.writeObj("test.obj",ret[0])
			sys.writePov("test.pov",ret[0])
		frames.append(ret[0])

    #viualize
	from pyTinyVisualizer import pyTinyVisualizer as vis
	class CustomPythonCallback(vis.PythonCallback):
		def __init__(self):
			vis.PythonCallback.__init__(self)
			self.frameId=0
		def frame(self,root):
			pm.updateSurfaceFLOAT(self.bodyS,frames[self.frameId])
			self.frameId=(self.frameId+1)%len(frames)
	drawFEM(sys,cb=CustomPythonCallback(),scaleEnvTc=np.array([.01,.01],dtype=np.single))
