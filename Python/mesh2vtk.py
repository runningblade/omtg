import argparse

def mesh2vtk(pathIn, pathOut):
    #input
    base=1
    vertices=[]
    tetrahedrons=[]
    f = open(pathIn,'r')
    for l in f.readlines():
        terms=[t for t in l.split(' ') if t!='']
        if len(terms)==4:
            vertices.append((float(terms[0]),float(terms[1]),float(terms[2])))
        if len(terms)==5:
            tetrahedrons.append((int(terms[0]),int(terms[1]),int(terms[2]),int(terms[3])))
            for d in range(4):
                base=min(base,tetrahedrons[-1][d])
    print('Found %d vertices and %d tetrahedra base=%d'%(len(vertices),len(tetrahedrons),base))
    #output
    f2 = open(pathOut,'w')
    f2.write('# vtk DataFile Version 1.0\n')
    f2.write('Unstructured Grid TetMesh\n')
    f2.write('ASCII\n')
    f2.write('\n')
    f2.write('DATASET UNSTRUCTURED_GRID\n')
    f2.write('POINTS %d double\n'%len(vertices))
    for v in vertices:
        f2.write('%f %f %f\n'%(v[0],v[1],v[2]))
    f2.write('\n')
    f2.write('CELLS %d %d\n'%(len(tetrahedrons),5*len(tetrahedrons)))
    for t in tetrahedrons:
        f2.write('4 %d %d %d %d\n'%(t[0]-base,t[1]-base,t[2]-base,t[3]-base))
    f2.write('\n')
    f2.write('CELL_TYPES %d\n'%len(tetrahedrons))
    for t in tetrahedrons:
        f2.write('10\n')
    
if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", type=str, required=True)
    parser.add_argument("--output", type=str)
    args=parser.parse_args()
    assert args.input[-5:]=='.mesh'
    if args.output is None:
        args.output=args.input[:-5]+'.vtk'
    mesh2vtk(args.input,args.output)