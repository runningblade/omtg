BASE="../../omtg-build"
import importlib,os,sys,math,random
import numpy as np
sys.path.append(BASE)
import pyPhysicsMotion as pm
from Visualizer import drawFEM

if __name__=='__main__':
	waterLevel=0
	waterDensity=2
	dragCoef=1
	Lambda=10000
	Mu=10000
	g=9.81
    
	type=0
	if os.path.exists(BASE+"/sysRBFluid.dat"):
		sys=pm.FEMReducedSystemFLOAT()
		sys.readStr(BASE+"/sysRBFluid.dat")
	else:
		if type==0:
			body=pm.FEMMeshFLOAT(BASE+"/beam.mesh",True)
		elif type==1:
			body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
		else:
			body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
			bodyHOct=FEMOctreeMeshFLOAT(body,0)
			body=bodyHOct.getMesh()
    
		sys=pm.FEMReducedSystemFLOAT(body)
		sys.addCorotatedElasticEnergy(Lambda,Mu)
		sys.addGravitationalEnergy(g)
		sys.addFluidEnergy(waterLevel,waterDensity,g,dragCoef,0)
		sys.buildBasis(20)
		sys.writeStr(BASE+"/sysRBFluid.dat")
    
	frames=[pm.FEMGradientInfoFLOAT(sys,np.array([0]*(sys.getBasis().shape[1]+6)*2,dtype=np.double))]
	ctrl=pm.FEMPDControllerFLOAT()
	while(len(frames)<400):
		print("Simulating frame: %d"%len(frames))
		if len(frames)==200:
			sys.setFluidEnergy(waterLevel+1,waterDensity,g,dragCoef,0)
		ret=sys.step(frames[-1],ctrl,pm.FEMSystemFLOAT.SP)
		frames.append(ret[0])

	#viualize
	from pyTinyVisualizer import pyTinyVisualizer as vis
	class CustomPythonCallback(vis.PythonCallback):
		def __init__(self):
			vis.PythonCallback.__init__(self)
			self.frameId=0
		def frame(self,root):
			pm.updateSurfaceFLOAT(self.bodyS,frames[self.frameId])
			self.frameId=(self.frameId+1)%len(frames)
	drawFEM(sys,waterLevel,cb=CustomPythonCallback())
