BASE="../../omtg-build"
import importlib,os,sys,math,random
import numpy as np
sys.path.append(BASE)
import pyPhysicsMotion as pm
from Visualizer import drawFEM

if __name__=='__main__':
	type=0
	if type==0:
		body=pm.FEMMeshFLOAT(BASE+"/torus.obj_tet.mesh",True)
	elif type==1:
		body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
	else:
		body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
		bodyHOct=FEMOctreeMeshFLOAT(body,0)
		body=bodyHOct.getMesh()
    
	selfCC=100000
	Lambda=1000
	Mu=1000
	g=9.81
    
	sys=pm.FEMSystemFLOAT(body)
	sys.addNonHookeanElasticEnergy(Lambda,Mu)
	sys.addGravitationalEnergy(g)
	sys.addSelfCC(selfCC)
    
	sys.setEnv(pm.EnvironmentExactFLOAT());
	env=sys.getEnvExact()
	for y in range(-10,0):
		for x in range(-4,5,2):
			xx=x+1 if y%2==0 else x
			env.addCapsule(np.array([-1.,float(xx),float(y)]),np.array([1.,float(xx),float(y)]),0.3,3)
	env.assemble()
	
	frames=[body.restPos(),body.restPos()]
	ctrl=pm.FEMPDControllerFLOAT()
	while(len(frames)<1000):
		print("Simulating frame: %d"%len(frames))
		ret=sys.step(frames[-2],frames[-1],ctrl,pm.FEMSystemFLOAT.OPT)
		frames.append(ret[0])

    #viualize
	from pyTinyVisualizer import pyTinyVisualizer as vis
	class CustomPythonCallback(vis.PythonCallback):
		def __init__(self):
			vis.PythonCallback.__init__(self)
			self.frameId=0
		def frame(self,root):
			pm.updateSurfaceFLOAT(self.bodyS,body,frames[self.frameId])
			self.frameId=(self.frameId+1)%len(frames)
	drawFEM(sys,cb=CustomPythonCallback(),scaleEnvTc=np.array([.01,.01],dtype=np.single))
