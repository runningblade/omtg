import numpy as np
from sympy import *
from sympy.printing.c import C99CodePrinter
from sympy.printing.precedence import precedence
default_printer=C99CodePrinter().doprint

def det2(m):
    return m[0,0]*m[1,1]-m[0,1]*m[1,0]
def det3(m):
    return  m[0,0]*(m[1,1]*m[2,2]-m[1,2]*m[2,1])-\
            m[0,1]*(m[1,0]*m[2,2]-m[2,0]*m[1,2])+\
            m[0,2]*(m[1,0]*m[2,1]-m[1,1]*m[2,0])
def interp1D(v0,v1,px):
    return v0*(1-px)+v1*px
def interp2D(v0,v1,v2,v3,px,py):
    return interp1D(interp1D(v0,v1,px),interp1D(v2,v3,px),py)
def interp3D(v0,v1,v2,v3,v4,v5,v6,v7,px,py,pz):
    return interp1D(interp2D(v0,v1,v2,v3,px,py),interp2D(v4,v5,v6,v7,px,py),pz)
def interp1DGrad(v0,v1,px):
    return v1-v0
def interp2DGrad(v0,v1,v2,v3,px,py):
    ret0=interp1D(interp1DGrad(v0,v1,px),
                  interp1DGrad(v2,v3,px),py)
    ret1=interp1DGrad(interp1D(v0,v1,px),
                      interp1D(v2,v3,px),py)
    return (ret0,ret1)
def interp3DGrad(v0,v1,v2,v3,v4,v5,v6,v7,px,py,pz):
    dxy0=interp2DGrad(v0,v1,v2,v3,px,py)
    dxy1=interp2DGrad(v4,v5,v6,v7,px,py)
    ret0=interp1D(dxy0[0],dxy1[0],pz)
    ret1=interp1D(dxy0[1],dxy1[1],pz)
    ret2=interp1DGrad(interp2D(v0,v1,v2,v3,px,py),
                      interp2D(v4,v5,v6,v7,px,py),pz)
    return (ret0,ret1,ret2)

#decl points
def integrateHex(expr):
    for d in range(3):
        expr=integrate(expr,(bary[d],0,1))
    return simplify(expr)
def declPt(id):
    return np.array(symbols('VSS[%d][0] VSS[%d][1] VSS[%d][2]'%(id,id,id)))
vss=[declPt(i) for i in range(8)]
bary=np.array(symbols('px py pz'))

#flatten
vssFlat=[]
for v in vss:
    vssFlat+=list(v)

#compute deformable gradient
F=interp3DGrad(vss[0],vss[1],vss[2],vss[3],
               vss[4],vss[5],vss[6],vss[7],
               bary[0],bary[1],bary[2])
F=np.array([[F[0][0],F[1][0],F[2][0]],
            [F[0][1],F[1][1],F[2][1]],
            [F[0][2],F[1][2],F[2][2]]])
DF=det3(F)
vol=integrateHex(DF)

#debug
def testCube(base,size):
    volVal=vol
    for id in range(len(vss)):
        for d in range(3):
            volVal=volVal.subs(vss[id][d],base[d]+size[d] if (id&(1<<d))!=0 else base[d])
    print("cubic(%s,%s)="%(str(base),str(size)),volVal)
testCube([1,1,1],[2,2,2])
testCube([1,1,1],[3,2,2])
testCube([1,1,1],[2,3,2])
testCube([1,1,1],[2,2,3])

#value
print("T vol=%s;"%default_printer(vol))

#gradient
gradient=[diff(vol,i) for i in vssFlat]
off=0
print("if(grad) {")
print("  grad->resize(24);")
for g in gradient:
    print("  (*grad)[%d]=%s;"%(off,default_printer(g)))
    off+=1
print("}")
    
#hessian
hessian=[[diff(g,i) for g in gradient] for i in vssFlat]
offr=0
print("if(hess) {")
print("  hess->setZero(24,24);")
for r in hessian:
    offc=0
    for c in r:
        if c==0:
            continue
        elif(offr>offc):
            print("  (*hess)(%d,%d)=(*hess)(%d,%d);"%(offr,offc,offc,offr))
        else: 
            print("  (*hess)(%d,%d)=%s;"%(offr,offc,default_printer(c)))
        offc+=1
    offr+=1
print("}")
print("return vol;")