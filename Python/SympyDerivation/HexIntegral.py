import numpy as np
from sympy import *
from sympy.printing.c import C99CodePrinter
from sympy.printing.precedence import precedence
default_printer=C99CodePrinter().doprint

def det2(m):
    return m[0,0]*m[1,1]-m[0,1]*m[1,0]
def det3(m):
    return  m[0,0]*(m[1,1]*m[2,2]-m[1,2]*m[2,1])-\
            m[0,1]*(m[1,0]*m[2,2]-m[2,0]*m[1,2])+\
            m[0,2]*(m[1,0]*m[2,1]-m[1,1]*m[2,0])
def interp1D(v0,v1,px):
    return v0*(1-px)+v1*px
def interp2D(v0,v1,v2,v3,px,py):
    return interp1D(interp1D(v0,v1,px),interp1D(v2,v3,px),py)
def interp3D(v0,v1,v2,v3,v4,v5,v6,v7,px,py,pz):
    return interp1D(interp2D(v0,v1,v2,v3,px,py),interp2D(v4,v5,v6,v7,px,py),pz)
def stencil3D(px,py,pz):
    coefs=[0 for i in range(8)]
    coefs[0]=(1.0-px)*(1.0-py)*(1.0-pz)
    coefs[1]=px*(1.0-py)*(1.0-pz)
    coefs[2]=(1.0-px)*py*(1.0-pz)
    coefs[3]=px*py*(1.0-pz)
    coefs[4]=(1.0-px)*(1.0-py)*pz
    coefs[5]=px*(1.0-py)*pz
    coefs[6]=(1.0-px)*py*pz
    coefs[7]=px*py*pz
    return coefs
def interp1DGrad(v0,v1,px):
    return v1-v0
def interp2DGrad(v0,v1,v2,v3,px,py):
    ret0=interp1D(interp1DGrad(v0,v1,px),
                  interp1DGrad(v2,v3,px),py)
    ret1=interp1DGrad(interp1D(v0,v1,px),
                      interp1D(v2,v3,px),py)
    return (ret0,ret1)
def interp3DGrad(v0,v1,v2,v3,v4,v5,v6,v7,px,py,pz):
    dxy0=interp2DGrad(v0,v1,v2,v3,px,py)
    dxy1=interp2DGrad(v4,v5,v6,v7,px,py)
    ret0=interp1D(dxy0[0],dxy1[0],pz)
    ret1=interp1D(dxy0[1],dxy1[1],pz)
    ret2=interp1DGrad(interp2D(v0,v1,v2,v3,px,py),
                      interp2D(v4,v5,v6,v7,px,py),pz)
    return (ret0,ret1,ret2)

#decl points
def integrateHex(expr):
    for d in range(3):
        expr=integrate(expr,(bary[d],0,1))
    return simplify(expr)
def declPt(id):
    return np.array(symbols('VSS[%d][0] VSS[%d][1] VSS[%d][2]'%(id,id,id)))
vss=[declPt(i) for i in range(8)]
bary=np.array(symbols('px py pz'))

#flatten
vssFlat=[]
for v in vss:
    vssFlat+=list(v)

#compute deformable gradient
D=np.array([[Symbol('_d(0,0)'),Symbol('_d(0,1)'),Symbol('_d(0,2)')],
            [Symbol('_d(1,0)'),Symbol('_d(1,1)'),Symbol('_d(1,2)')],
            [Symbol('_d(2,0)'),Symbol('_d(2,1)'),Symbol('_d(2,2)')]])
F=interp3DGrad(vss[0],vss[1],vss[2],vss[3],
               vss[4],vss[5],vss[6],vss[7],
               bary[0],bary[1],bary[2])
F=np.array([[F[0][0],F[1][0],F[2][0]],
            [F[0][1],F[1][1],F[2][1]],
            [F[0][2],F[1][2],F[2][2]]])

#integral
def integral(func):
    a,b,c=symbols('a b c')
    ret=((a+1)*Rational(1,2),(b+1)*Rational(1,2),(c+1)*Rational(1,2))
    bary=stencil3D(ret[0],ret[1],ret[2])
    
    integrand=func((a,b,c),bary)
    integrand=integrate(integrand,(a,-1,1))
    integrand=integrate(integrand,(b,-1,1))
    integrand=integrate(integrand,(c,-1,1))
    return integrand*Rational(1,8)

def massMatrix():
    sum=0
    ret=[[0 for i in range(8)] for j in range(8)]
    for r in range(8):
        for c in range(8):
            def func(bary,stencil):
                return stencil[r]*stencil[c]
            ret[r][c]=simplify(integral(func))
            print("ret(%d,%d)=%s;"%(r,c,str(default_printer(ret[r][c]))))
            sum+=ret[r][c]
    print("sum=%s"%str(sum))
    return ret

#massMatrix()

massMatrix()

def linearElastic():
    def func(b,stencil):
        for r in range(3):
            for c in range(3):
                F[r,c]=F[r,c].subs(bary[0],b[0]).subs(bary[1],b[1]).subs(bary[2],b[2])
        #f=np.matmul(F,D)
        f=F*Symbol('invSideLen')
        eps=(f+np.transpose(f))/2
        for d in range(3):
            eps[d,d]-=1
        Mu=Symbol("mu")
        Lambda=Symbol("lambda")
        E=Mu*np.trace(np.matmul(eps,eps.T))+Lambda/2*np.trace(eps)**2
        return E
    return simplify(integral(func)*Symbol("vol"))

E=linearElastic()
gradient=[diff(E,i) for i in vssFlat]
hessian=[[diff(g,i) for g in gradient] for i in vssFlat]
offr=0
for r in hessian:
    offc=0
    for c in r:
        if c==0:
            continue
        elif(offr>offc):
            print("hess(%d,%d)=hess(%d,%d);"%(offr,offc,offc,offr))
        else: 
            print("hess(%d,%d)=%s;"%(offr,offc,default_printer(c)))
        offc+=1
    offr+=1