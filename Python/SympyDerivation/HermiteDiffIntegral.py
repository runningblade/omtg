from sympy import *
from sympy.printing.c import C99CodePrinter
from sympy.printing.precedence import precedence
default_printer=C99CodePrinter().doprint

a,da,b,db,s,s0,span,target=symbols('a da b db s s0 span target')
ca=2*s**3-3*s**2+1
cdiffa=s**3-2*s**2+s
cb=-2*s**3+3*s**2
cdiffb=s**3-s**2
coef=[ca,cdiffa,cb,cdiffb]
print("D: ",[diff(c,s) for c in coef])
print("DD: ",[diff(diff(c,s),s) for c in coef])

hermite=a*ca+da*cdiffa+b*cb+db*cdiffb
print("test Hermite: ",hermite.subs(s,0),diff(hermite,s).subs(s,0),hermite.subs(s,1),diff(hermite,s).subs(s,1))

hermite=hermite.subs(s,(s-s0)/span).subs(da,da*span).subs(db,db*span)
print("test Hermite-span: ",hermite.subs(s,s0),diff(hermite,s).subs(s,s0),hermite.subs(s,s0+span),diff(hermite,s).subs(s,s0+span))

energy=integrate((diff(hermite,s)-target)**2/2,s)
energy=simplify(energy.subs(s,s0+span)-energy.subs(s,s0))

#gradient
gradient=[diff(energy,i) for i in [a,da,b,db,target]]
off=0
print("if(grad) {")
for g in gradient:
    print("  (*grad)[%d]=%s;"%(off,default_printer(g)))
    off+=1
print("}")
    
#hessian
hessian=[[diff(g,i) for g in gradient] for i in [a,da,b,db,target]]
offr=0
print("if(hess) {")
for r in hessian:
    offc=0
    for c in r:
        if(offr>offc):
            print("  (*hess)(%d,%d)=(*hess)(%d,%d);"%(offr,offc,offc,offr))
        else: print("  (*hess)(%d,%d)=%s;"%(offr,offc,default_printer(c)))
        offc+=1
    offr+=1
print("}")
print("return E;")