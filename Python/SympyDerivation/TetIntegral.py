import numpy as np
from sympy import *
from sympy.printing.c import C99CodePrinter
from sympy.printing.precedence import precedence
default_printer=C99CodePrinter().doprint

def det2(m):
    return m[0,0]*m[1,1]-m[0,1]*m[1,0]
def det3(m):
    return  m[0,0]*(m[1,1]*m[2,2]-m[1,2]*m[2,1])-\
            m[0,1]*(m[1,0]*m[2,2]-m[2,0]*m[1,2])+\
            m[0,2]*(m[1,0]*m[2,1]-m[1,1]*m[2,0])

#decl points
def integrateHex(expr):
    for d in range(3):
        expr=integrate(expr,bary[d])
        expr=expr.subs(bary[d],1)-expr.subs(bary[d],0)
    return simplify(expr)
def declPt(id):
    return np.array(symbols('VSS[%d][0] VSS[%d][1] VSS[%d][2]'%(id,id,id)))
vss=[declPt(i) for i in range(4)]

#flatten
vssFlat=[]
for v in vss:
    vssFlat+=list(v)

#compute deformable gradient
D=np.array([[Symbol('_d(0,0)'),Symbol('_d(0,1)'),Symbol('_d(0,2)')],
            [Symbol('_d(1,0)'),Symbol('_d(1,1)'),Symbol('_d(1,2)')],
            [Symbol('_d(2,0)'),Symbol('_d(2,1)'),Symbol('_d(2,2)')]])
F=np.array([[vss[1][0]-vss[0][0],vss[2][0]-vss[0][0],vss[3][0]-vss[0][0]],
            [vss[1][1]-vss[0][1],vss[2][1]-vss[0][1],vss[3][1]-vss[0][1]],
            [vss[1][2]-vss[0][2],vss[2][2]-vss[0][2],vss[3][2]-vss[0][2]]])

#integral
def integral(func):
    a,b,c=symbols('a b c')
    ret=((a+1)*Rational(1,2),(b+1)*Rational(1,2),(c+1)*Rational(1,2))
    bary=[0,0,0,0]
    bary[0]=(1-ret[0])*ret[1]
    bary[1]=ret[0]*ret[1]*ret[2]
    bary[2]=ret[0]*ret[1]*(1-ret[2])
    bary[3]=1-ret[1]
    
    integrand=func(bary)*(ret[0]*ret[1]*ret[1])
    integrand=integrate(integrand,(a,-1,1))
    integrand=integrate(integrand,(b,-1,1))
    integrand=integrate(integrand,(c,-1,1))
    return integrand*Rational(6,8)

def massMatrix():
    sum=0
    ret=[[0 for i in range(4)] for j in range(4)]
    for r in range(4):
        for c in range(4):
            def func(bary):
                return bary[r]*bary[c]
            ret[r][c]=simplify(integral(func))
            print("ret(%d,%d)=%s;"%(r,c,str(default_printer(ret[r][c]))))
            sum+=ret[r][c]
    print("sum=%s"%str(sum))
    return ret

massMatrix()

def linearElastic():
    f=np.matmul(F,D)
    eps=(f+np.transpose(f))/2
    for d in range(3):
        eps[d,d]-=1
    Mu=Symbol("mu")
    Lambda=Symbol("lambda")
    E=Mu*np.trace(np.matmul(eps,eps.T))+Lambda/2*np.trace(eps)**2
    return simplify(E*Symbol("vol"))

E=linearElastic()
gradient=[diff(E,i) for i in vssFlat]
hessian=[[diff(g,i) for g in gradient] for i in vssFlat]
offr=0
for r in hessian:
    offc=0
    for c in r:
        if c==0:
            continue
        elif(offr>offc):
            print("hess(%d,%d)=hess(%d,%d);"%(offr,offc,offc,offr))
        else: 
            print("hess(%d,%d)=%s;"%(offr,offc,default_printer(c)))
        offc+=1
    offr+=1