from mesh2vtk import *
from pathlib import Path
import os
    
if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", type=str, required=True)
    args=parser.parse_args()
    for path in Path(args.input).rglob('*.mesh'):
        input=os.path.abspath(path)
        mesh2vtk(input,input[:-5]+'.vtk')