BASE="../../omtg-build"
import importlib,os,sys,math,random
import numpy as np
sys.path.append(BASE)
import pyPhysicsMotion as pm
from Visualizer import drawFEM

if __name__=='__main__':
	waterLevel=0
	waterDensity=2
	dragCoef=100
	Lambda=10000
	Mu=10000
	g=9.81
    
	type=0
	if os.path.exists(BASE+"/sysRCFluid.dat"):
		sys=pm.FEMReducedSystemFLOAT()
		sys.readStr(BASE+"/sysRCFluid.dat")
	else:
		if type==0:
			body=pm.FEMMeshFLOAT(True,BASE+"/Cross.abq",True)
		elif type==1:
			body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
		else:
			body=pm.FEMMeshFLOAT(BASE+"/sphere.obj",.05,True)
			bodyHOct=FEMOctreeMeshFLOAT(body,0)
			body=bodyHOct.getMesh()
    
		sys=pm.FEMReducedSystemFLOAT(body)
		sys.addCorotatedElasticEnergy(Lambda,Mu)
		sys.addGravitationalEnergy(g)
		sys.addFluidEnergy(waterLevel,waterDensity,g,dragCoef,0)
		sys.buildBasis(20)
		sys.writeStr(BASE+"/sysRCFluid.dat")
    
	N=10
	ctrl=None
	sys.dt(0.01/N)
	sys.dampingCoef(1)
	sys.setFluidEnergy(waterLevel,waterDensity,g,dragCoef,0)
	frames=[pm.FEMGradientInfoFLOAT(sys,np.array([0]*(sys.getBasis().shape[1]+6)*2,dtype=np.double))]
	while(len(frames)<4000*N):
		print("Simulating frame: %d"%len(frames))
		if (len(frames)%(300*N))<160*N:
			ctrl=pm.FEMReducedPDControllerFLOAT()
			ctrl.resetDReduced(np.array([0.04],dtype=np.double),np.array([0,0.02]+[0]*(sys.getBasis().shape[1]-2),dtype=np.double))
		else:	
			ctrl=None
		ret=sys.step(frames[-1],ctrl,pm.FEMSystemFLOAT.SP,ctrl is not None)
		if ctrl is not None:
			print(ret[0].getDDxDtDu())
			print(ret[0].getDvwDu())
			print(ret[0].getDvwDuLocal())
		frames.append(ret[0])

	#viualize
	from pyTinyVisualizer import pyTinyVisualizer as vis
	class CustomPythonCallback(vis.PythonCallback):
		def __init__(self):
			vis.PythonCallback.__init__(self)
			self.frameId=0
		def frame(self,root):
			pm.updateSurfaceFLOAT(self.bodyS,frames[self.frameId])
			self.frameId=(self.frameId+N)%len(frames)
	drawFEM(sys,waterLevel,cb=CustomPythonCallback(),lightPos=[0,0,2])
