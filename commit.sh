rm *.user
find . -type d -name __pycache__ -exec rm -rdf {} \;

git add --all
git commit -m "$1"
git push -u origin $2
