import argparse
import math


def _GenerateArgsParser(control_level: str = 'Lower'):
    parser = argparse.ArgumentParser(description='Input arguments for ' + control_level + ' Control')
    parser.add_argument('--data_file', default="train_data", type=str)
    # model
    model_args = parser.add_argument_group("Model")
    model_args.add_argument('--basis_num', default=20, type=int)
    model_args.add_argument('--mesh_name', default="Cross.abq", type=str)
    model_args.add_argument('--mesh_type', default=0, type=int, choices=[0, 1, 2, 3])
    model_args.add_argument('--elastic_energy', default="Corotated", type=str, choices=["Corotated", "StVK"])
    model_args.add_argument('--lame_coefs', nargs=2, default=[1e4, 1e4], type=float)
    # world
    world_args = parser.add_argument_group("Primitive world")
    world_args.add_argument('--sim_dt', default=0.005, type=float)
    world_args.add_argument('--damp', default=1.0, type=float)
    world_args.add_argument('--max_contact', default=10, type=int)
    # env
    env_args = parser.add_argument_group("Environment")
    env_args.add_argument('--task', default='Walk', type=str, choices=["Walk", "Swim", "Climb"])
    env_args.add_argument('--ctrl_dim', default=3, type=int,
                          help="In Lower.Optimize, recommended value is 7; in Upper.Optimize, recommended value is 3.")
    env_args.add_argument('--orientation_axis', nargs=3, type=float, default=[0, 0, 1],
                          help="The reduced robot will rotate around this axis at the beginning."
                               "Note that norm(orientation_axis) cannot be zero.")
    env_args.add_argument("--env_reward_type", type=str, default="quadratic_dvr", choices=["quadratic_vr",
                                                                                           "quadratic_dvr",
                                                                                           "quadratic_tr",
                                                                                           "smooth_abs_tr"])
    env_args.add_argument('--orientation_theta', default=0, type=float)
    env_args.add_argument('--target_phi', default=0.0, type=float)
    env_args.add_argument("--level", type=float, default=-0.15)
    env_args.add_argument("--friction", type=float, default=0.7)
    env_args.add_argument("--density", type=float, default=2)
    env_args.add_argument("--drag", type=float, default=10.0)
    env_args.add_argument("--x_slope", type=float, default=1.0)
    env_args.add_argument("--y_slope", type=float, default=1.0)
    # reward's (and cost's) parameters
    reward_args = parser.add_argument_group("Reward")
    reward_args.add_argument('--t_reward_w', type=float, default=1.0)
    reward_args.add_argument('--v_reward_w', type=float, default=1.0)
    reward_args.add_argument('--r_reward_w', type=float, default=1.0)
    reward_args.add_argument('--target_v', type=float, default=1.0)
    reward_args.add_argument('--target_t', type=float, default=1.0)
    reward_args.add_argument('--u_reward_w', type=float, default=1.0)
    reward_args.add_argument('--u_reward_limiting_alpha', type=float, default=1.0)
    # control intensity
    ctrl_intensity_args = parser.add_argument_group("Control intensity")
    ctrl_intensity_args.add_argument('--p_strength', default=0.03, type=float)
    ctrl_intensity_args.add_argument('--p_stretch', default=1.5, type=float)
    # render
    render_args = parser.add_argument_group("Renderer")
    render_args.add_argument('--fps', type=int, default=25, help="fps(frames per second) for renderer")
    # signal interpreter
    signal_interpreter_args = parser.add_argument_group("Signal interpreter")
    signal_interpreter_args.add_argument('--action_pattern', type=str, choices=['SquareWave',
                                                                                'BezierCurve',
                                                                                'CatmullRomSpline',
                                                                                ],
                                         default='SquareWave')
    bc_args = parser.add_argument_group("Bezier curve arguments")
    bc_args.add_argument('--degree', type=int, choices=[2, 3], default=2)
    crs_args = parser.add_argument_group("Catmull-Rom spline arguments")
    crs_args.add_argument('--crs_alpha', type=float, choices=[0, 0.5, 1], default=0.5)
    # Lower(MPPI/siLQG)
    lower_args = parser.add_argument_group("Lower optimization")
    lower_args.add_argument('--controller', type=str, choices=["MPPI", "siLQG"], required=True)
    # ctrl_sys
    ctrl_sys_args = parser.add_argument_group("ctrl_sys")
    ctrl_sys_args.add_argument("--ctrl_sys_reward_type", default="v_stable", choices=["v_quadratic", "x_telescoping",
                                                                                      "x_linear"],
                               type=str)
    ctrl_sys_args.add_argument('--ctrl_dt', default=0.1, type=float)
    ctrl_sys_args.add_argument('--tot_time', default=10, type=float)
    ctrl_sys_args.add_argument('--horizon', default=4, type=int)
    # MPPI
    MPPI_args = parser.add_argument_group("MPPI")
    MPPI_args.add_argument('--mppi_lambda', default=1, type=float)
    MPPI_args.add_argument('--periodic_gaits', action='store_true', dest='periodic_gaits')
    MPPI_args.add_argument('--num_samples', default=16, type=int)
    MPPI_args.add_argument('--noise_sigma', default=0.25, type=float,
                           help="A value between 0.2 and 0.25 is recommended.")
    periodic_MPPI_args = parser.add_argument_group("Periodic MPPI")
    periodic_MPPI_args.add_argument('--history_noise_sigma', default=0.1, type=float,
                                    help="A value between 0.08 and 0.1 is recommended."
                                    )
    # siLQG
    siLQG_args = parser.add_argument_group("siLQG")
    siLQG_args.add_argument("--du", default=0.1, type=float)
    siLQG_args.add_argument("--step_max_tries", default=4, type=int)
    siLQG_args.add_argument("--min_step", default=0.00097656, type=float)
    siLQG_args.add_argument("--line_search_const", default=1e-2, type=float)
    siLQG_args.add_argument("--siLQG_verbosity", default=0, type=int)
    siLQG_args.add_argument("--random_init_U", action="store_true", dest="random_init_U")
    siLQG_args.add_argument("--u_cost_type", default='quadratic', type=str, choices=['quadratic','value_limiting'])
    if control_level == 'Upper':
        # Upper(Bayesian Optimization)
        upper_args = parser.add_argument_group("Upper optimization")
        # eGPR
        eGPR_args = parser.add_argument_group("extrinsic Gaussian process regression")
        eGPR_args.add_argument('--rng_dist_pre_train', default='uniform', type=str, choices=['uniform', 'normal'])
        eGPR_args.add_argument('--n_initialize', default=200, type=int, choices=range(5, 1001))
        eGPR_args.add_argument('--n_gp_base', default=None, type=int, help="The size of data in the original GP used when running OptimizeWithInitGP."
                                                                           "When n_gp_base is None, all data in the original GP is used.")
        eGPR_args.add_argument('--gp_itrs', default=100, type=int)
        eGPR_args.add_argument('--gp_kernel', default='RBF', type=str, choices=['RBF', 'Matern'])
        eGPR_args.add_argument('--gp_kernel_gamma_alpha', default=7.5, type=float)
        eGPR_args.add_argument('--gp_kernel_gamma_beta', default=0.5, type=float)
        eGPR_args.add_argument('--gp_verbosity', default=1, type=int)
        eGPR_args.add_argument('--mute_gp_noise', action='store_true', dest='mute_gp_noise')
        # General BO
        BO_args = parser.add_argument_group("Bayesian optimization on Grassmann manifold")
        BO_args.add_argument('--bo_budget', default=1000, type=float)
        BO_args.add_argument('--gp_ucb_kappa_alpha', default=0.1, type=float)
        BO_args.add_argument('--gp_ucb_kappa_beta', default=2, type=float)
        BO_args.add_argument('--bo_n_explore', default=1, type=int)
        BO_args.add_argument('--bo_n_warmup', default=5000, type=int)
        BO_args.add_argument('--acq_optimizer', default='SteepestDescent', type=str,
                             choices=['ConjugateGradient', 'SteepestDescent', 'ParticleSwarm', 'TrustRegions',
                                      'NelderMead'])
        BO_args.add_argument('--save_interval', default=10, type=int)
        BO_args.add_argument('--gp_refit_interval', default=25, type=int)
        BO_args.add_argument('--eval_step_size', default=10, type=int)
        BO_args.add_argument('--acq_func', default="GP_UCB", type=str, choices=["GP_UCB", "GP_Expected"])
        # MF-BO (currently supports trajectory length, i.e. '--tot_time' as a fidelity only.)
        MFBO_choice = parser.add_argument_group("MFBO(mutually exclusive arguments)")
        MFBO_choice_exclusive = MFBO_choice.add_mutually_exclusive_group()
        MFBO_choice_exclusive.add_argument('--boca', action='store_true', dest='boca')
        MFBO_choice_exclusive.add_argument('--mf_gp_ucb', action='store_true', dest='mf_gp_ucb')
        BOCA_args = parser.add_argument_group("BOCA")
        BOCA_args.add_argument('--nn_fidelity_kernel', action="store_true", dest="nn_fidelity_kernel")
        BOCA_args.add_argument('--grid_unit_length', type=float, default=0.01)
        BOCA_args.add_argument('--boca_gap', type=str, default="regularized_kernel", choices=["regularized_kernel",
                                                                                              "inverse_exp_map"])
        BOCA_args.add_argument('--fidelity_kernel', type=str, default='RBF', choices=['RBF', 'Matern'])
        BOCA_args.add_argument('--heuristic_window_size', type=int, default=8, choices=[4, 8, 12, 16, 20, 24, 28, 32])
        BOCA_args.add_argument('--lowest_fidelity', type=float, default=0.1)
        BOCA_args.add_argument('--init_uncertainty_bound', type=float, default=None)
        BOCA_args.add_argument('--uncertainty_bound_decrease', type=float, default=0.5)
        BOCA_args.add_argument('--uncertainty_bound_increase', type=float, default=2.0)
        BOCA_args.add_argument('--fidelity_kernel_gamma_alpha', type=float, default=2.0)
        BOCA_args.add_argument('--fidelity_kernel_gamma_beta', type=float, default=0.5)
        BOCA_args.add_argument('--resample_on_target_fidelity', action='store_true', dest='resample_on_target_fidelity')
        MF_GP_UCB_args = parser.add_argument_group("MF_GP_UCB")
        MF_GP_UCB_args.add_argument('--cost', nargs='+', type=float, default=[1.0, 2.5, 5.0])
    return parser
