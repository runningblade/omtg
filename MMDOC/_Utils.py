import os
import glob
import numpy as np
from matplotlib import pyplot as plt
import math


def moving_average(arr, ws):
    cumsum = np.cumsum(arr, axis=0)
    cumsum[ws:] = cumsum[ws:] - cumsum[:-ws]
    avg = cumsum[ws-1:] / ws
    return avg


def space_angle_to_vec(theta, phi):
    z = math.cos(theta)
    x = math.sin(theta) * math.cos(phi)
    y = math.sin(theta) * math.sin(phi)
    return np.array([x, y, z])


class NumericalWarning(UserWarning):
    pass


class NumericalError(RuntimeError):
    pass


def transpose_first_2_dims(x):
    length = len(x.shape)
    return np.transpose(x, (1, 0) + tuple(range(2, length)))


def transpose_last_2_dims(x):
    length = len(x.shape)
    return np.transpose(x, tuple(range(length - 2)) + (length - 1, length - 2))


def transpose_first_dim_to_last(x):
    length = len(x.shape)
    return np.transpose(x, tuple(range(1, length)) + (0,))


def format_time(seconds, subject: str):
    hours = int(seconds / 3600)
    seconds -= hours * 3600
    minutes = int(seconds / 60)
    seconds -= minutes * 60
    if hours:
        print(subject + " took {} hours, {} minutes, and {:.2} seconds.".format(hours, minutes, seconds))
    elif minutes:
        print(subject + " took {} minutes and {:.2} seconds.".format(minutes, seconds))
    else:
        print(subject + " took {:.2} seconds.".format(seconds))


def load_data(loader, path: str):
    paths = glob.glob(path)
    data = None
    if len(paths) == 0:
        print("Such data does not exist.")
        return data, paths
    else:
        data = []
        for path in paths:
            data.append(loader(path))
            print(f"Data {path} is loaded.")
        return data, paths


def plot_BO_res(ys,
                title,
                xlabel,
                ylabel,
                flairs=None,
                fmts=None,
                save_dir='./',
                suffix=''
                ):
    plt.clf()
    curves = []
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if fmts is None:
        fmts = [""] * len(ys)
    for y, fmt in zip(ys, fmts):
        curve, = plt.plot([x / len(y) for x in range(len(y))], y, fmt, scalex=True)
        curves.append(curve)
    if flairs is not None:
        plt.legend(curves, flairs, loc='upper left')
    plt.savefig(save_dir + 'BO_res_' + suffix + '.png')


def plot_GPR_res(actual_val, pred_dist, dir='./', suffix=''):
    pred_val = pred_dist.mean.detach().numpy()
    lower, upper = pred_dist.confidence_region()
    lower = lower.detach().numpy()
    upper = upper.detach().numpy()
    plt.clf()
    plt.plot(pred_val, color='b')
    plt.fill_between(list(range(len(lower))), lower, upper, color='c')
    plt.savefig(dir + 'pred_' + suffix + '.png')
    plt.plot(actual_val, color='r')
    plt.savefig(dir + 'pred_actual_' + suffix + '.png')
    print("actual_val={}, std_actual_val={}".format(actual_val, np.std(actual_val)))
    print("pred_val={}, std_pred_val={}".format(pred_val, np.std(pred_val)))
    avg_relative_error = 0.0
    for r, m in zip(actual_val, pred_val):
        if r != 0:
            relative_error = np.abs(r - m) / np.abs(r)
            avg_relative_error += relative_error
            print("Relative Error={}".format(relative_error))
    print("Average Relative Error={}".format(avg_relative_error / len(actual_val)))


def load_nn_sequential_from_file(file_path):
    with open(file_path) as file:
        from collections import OrderedDict
        import torch.nn as nn
        od = OrderedDict()
        for line in file:
            words = line.split()
            name = words[0]
            module_cls = words[1]
            args = [int(word) for word in words[2:]]
            od[name] = getattr(nn, module_cls)(*args)
        return nn.Sequential(od)


if __name__ == "__main__":
    model = load_nn_sequential_from_file(file_path='../../omtg-build/mmdoc_nn')
    print(model)
