import os, sys, warnings
import numpy as np

from SoftRobotEnv.ReducedSoftRobotUtils import BASE, create_body, apply_control, apply_design, simulate, is_simulation_failed, \
    apply_control_multiple, simulate_multiple, deserialize_state, controlSpaceTooLargeWarning
from abc import ABC, abstractmethod
sys.path.append(BASE)
import pyPhysicsMotion as pm


class ReducedSoftRobotBase(ABC):
    def __init__(self,
                 world,
                 agents,
                 ctrl_dim=3,
                 # orientation is an axis-theta representation of rotation the of local frame
                 orientation=(np.array([0, 0, 1]), 0),
                 # contact solver
                 contact_solver_type='SP',
                 # render related
                 x_render_res=0.1, y_render_res=0.1,
                ):
        self.world = world
        self.basis_num = self.world.getBasis().shape[-1]
        self.state_dim = 2 * (self.basis_num + 6)
        if ctrl_dim > self.basis_num:
            warnings.warn(f"Control space must be a subspace of simulation space, "
                          f"thus ctrl_dim should not be greater than {self.basis_num}."
                          f"ctrl_dim is forced to be {self.basis_num}.", controlSpaceTooLargeWarning)
            ctrl_dim = self.basis_num
        self.ctrl_dim = ctrl_dim
        self.agents = list(agents)
        self.agents_mask = None
        # contact solver
        self.contact_solver = getattr(pm.FEMSystemFLOAT, contact_solver_type)
        # terrain's name and terrain's feature
        self.terrain_name = 'Empty'
        self.terrain = None
        # render_related
        self.x_render_res = x_render_res
        self.y_render_res = y_render_res
        self.viewer = None
        self.init_orient = (orientation[0]/np.linalg.norm(orientation[0]), orientation[1])

    def __call__(self, action, state):
        if isinstance(state, pm.FEMGradientInfoFLOAT):
            return self.step(action, state)
        else:
            return self.step_multiple(action, state)

    @abstractmethod
    def reset(self, *args, **kwargs):
        pass

    @property
    def obs_dim(self):
        return self.get_obs(self.init_pos).shape[-1]

    @property
    def init_pos(self):
        vec_init_pos = np.concatenate(
            (np.zeros(self.basis_num + 3), self.init_orient[0]*self.init_orient[1], np.zeros(self.basis_num + 6))).astype(np.double)
        return deserialize_state(self.world, vec_init_pos)
    
    def step(self, action, state):
        # apply action to the bottom controller
        apply_control(action, self.agents[0])
        # advance simulator
        ret = simulate(self.world, state, self.agents[0], self.contact_solver, False)
        if is_simulation_failed(ret):
            print("Warning: Simulation Failed!")
            print("Now the action is:{}".format(action))
            print("Now the terrain is:{}".format(self.terrain))
        state = ret[0]
        return state

    def step_multiple(self, actions, states):
        assert len(actions) == len(states) and len(states) == len(self.agents)
        # apply action to the bottom controller
        apply_control_multiple(actions, self.agents, self.agents_mask)
        # native c++ multi-threading
        rets = simulate_multiple(self.world, states, self.agents, self.contact_solver, False, self.agents_mask)
        for ret, action in zip(rets, actions):
            if is_simulation_failed(ret):
                print("Warning: Simulation Failed!")
                print("Now the action is:{}".format(action))
                print("Now the terrain is:{}".format(self.terrain))
        states = [ret[0] for ret in rets]
        return states

    def set_design(self, design):
        assert len(design) == self.num_agents
        for d, agent in zip(design, self.agents):
            apply_design(d, agent)

    @property
    def num_agents(self):
        return len(self.agents)

    def copy_agents(self, n_copies):
        n_agents = len(self.agents)
        for i in range(n_copies - 1):
            for j in range(n_agents):
                self.agents.append(self.agents[j].copy())

    def truncate_agents(self, n_truncate, truncate_rear=True):
        if n_truncate >= 1:
            if truncate_rear:
                self.agents = self.agents[:-n_truncate]
            else:
                self.agents = self.agents[n_truncate:]

    def repeat_last_agents(self, reps, n_last=1):
        last_agents = self.agents[-n_last:]
        for i in range(reps - 1):
            for j in range(n_last):
                self.agents.append(last_agents[j].copy())

    def render(self, render_callback=None, init_arrow_dir=None):
        from Render.OnlineRenderer import OnlineRenderer
        if self.viewer is None:
            self.viewer = OnlineRenderer(env=self,
                                         render_callback=render_callback,
                                         scaleEnvTc=np.array([self.x_render_res, self.y_render_res], dtype=np.single))
        else:
            self.viewer.cb = render_callback
        # Terrain
        if self.terrain_name != 'Empty':
            self.viewer.switchTerrain(terrain=self.terrain_name, Env=self.world.getEnv())
        self.viewer.setBackground()
        # Arrow
        if init_arrow_dir is not None:
            self.viewer.addArrow()
            self.viewer.switchArrow(dir=init_arrow_dir)
        self.viewer.drawer.mainLoop()

    def render_bases(self, mode_num=None, duration_per_basis=400):
        # visualize every basis
        if mode_num is None:
            mode_num = self.basis_num
        assert mode_num <= self.basis_num

        frames = []
        pose = np.zeros(self.basis_num)
        for i in range(mode_num):
            pose[i] = 1.0
            cur_frame = pm.FEMGradientInfoFLOAT(self.world, np.concatenate((pose, np.zeros(self.basis_num + 12))))
            pose[i] = 0.0
            if i == 0:
                for j in range(2 * duration_per_basis):
                    frames.append(cur_frame)
            else:
                for j in range(duration_per_basis):
                    frames.append(cur_frame)

        from Render.OnlineRenderer import OnlineRenderCallBack
        render_callback = OnlineRenderCallBack(env=self, switch_arrow_step=-1, frames=frames)
        self.render(render_callback, init_arrow_dir=None)

    """
    The following methods: get_obs, reward
    are either implemented in the derived classes or dynamically bound.
    """
    def get_obs(self, state):
        pass

    def reward(self, obs):
        pass
