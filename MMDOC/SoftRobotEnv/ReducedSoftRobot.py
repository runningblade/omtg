import sys
import math
import numpy as np
from SoftRobotEnv.ReducedSoftRobotUtils import BASE, set_floor, sample_target_dir, sample_terrain, apply_control, \
    apply_control_multiple, GRAVITY, \
    apply_design, simulate, simulate_multiple, is_simulation_failed, deserialize_state, rot_tol
from SoftRobotEnv.ReducedSoftRobotBase import ReducedSoftRobotBase
from _Utils import space_angle_to_vec

sys.path.append(BASE)
import transforms3d as tr3d
import pyPhysicsMotion as pm


class ReducedSoftRobotWalk(ReducedSoftRobotBase):
    def __init__(self,
                 target_phi=0.0,
                 level=-0.15,
                 size=10.0,
                 friction=0.7,
                 **base_kwargs):
        super(ReducedSoftRobotWalk, self).__init__(**base_kwargs)
        self.terrain_name = "Plane"
        self.target_dir = space_angle_to_vec(theta=0.5 * math.pi, phi=target_phi)
        self.reset(level, size, friction)

    def reset(self,
              level,
              size,
              friction):
        set_floor(self.world, level, size, 0, 0, friction)
        self.terrain = np.array([level, size, friction])
        print(f"On plane. level={level}, size={size}, friction={friction}.")


class ReducedSoftRobotSwim(ReducedSoftRobotBase):
    def __init__(self,
                 target_phi=0.0,
                 level=0.0,
                 density=1.0,
                 drag=10.0,
                 **base_kwargs):
        super(ReducedSoftRobotSwim, self).__init__(**base_kwargs)
        self.terrain_name = "Fluid"
        self.target_dir = space_angle_to_vec(theta=0.5 * math.pi, phi=target_phi)
        self.reset(level, density, drag)

    def reset(self,
              level,
              density,
              drag):
        self.world.setFluidEnergy(level, density, GRAVITY, drag, deg=0)
        self.terrain = np.array([level, density, drag])
        print(f"In fluid. level={level}, density={density}, drag={drag}.")


class ReducedSoftRobotClimb(ReducedSoftRobotBase):
    def __init__(self,
                 target_phi=0.0,
                 level=-0.15,
                 size=10.0,
                 x_slope=1.0,
                 y_slope=1.0,
                 friction=0.7,
                 **base_kwargs):
        super(ReducedSoftRobotClimb, self).__init__(**base_kwargs)
        self.terrain_name = "Slope"
        self.target_dir = space_angle_to_vec(theta=0.5 * math.pi, phi=target_phi)
        self.reset(level, size, x_slope, y_slope, friction)

    def reset(self,
              level,
              size,
              x_slope,
              y_slope,
              friction):
        normal, rot = set_floor(self.world, level, size, x_slope, y_slope, friction, compute_rot_from_xy=True)
        self.terrain = np.array([level, size,
                                 x_slope, y_slope,
                                 friction])
        self.target_dir = tr3d.quaternions.rotate_vector(self.target_dir, rot)
        self.init_orient = tr3d.quaternions.quat2axangle(
            tr3d.quaternions.qmult(
                rot,
                tr3d.quaternions.axangle2quat(self.init_orient[0], self.init_orient[1], is_normalized=True)))
        print(f"On slope. level={level}, size={size}, x_slope={x_slope}, y_slope={y_slope}, friction={friction}.")


class ReducedSoftRobotVersatile(ReducedSoftRobotBase):
    def __init__(self,
                 seed=None,
                 # target
                 target_phi_low=0.0, target_phi_high=2 * math.pi,
                 # env(terrain) related, variable, fluid
                 fluid_level_low=-0.01, fluid_level_high=0.01,
                 fluid_density_low=1., fluid_density_high=2.,
                 drag_coef_low=8., drag_coef_high=12.,
                 # env(terrain) related, variable, floor
                 plane_level=-0.15, plane_size=10.0,
                 friction_mu_low=0.5, friction_mu_high=1.0,
                 slope_x_low=math.tan(8.0 * math.pi / 180.0), slope_x_high=math.tan(35.0 * math.pi / 180.0),
                 slope_y_low=math.tan(8.0 * math.pi / 180.0), slope_y_high=math.tan(35.0 * math.pi / 180.0),
                 **base_kwargs
                 ):
        super(ReducedSoftRobotVersatile, self).__init__(**base_kwargs)
        self.target_phi_low = target_phi_low
        self.target_phi_high = target_phi_high
        self.target_dir = None
        # parameter for sampling terrain
        # fluid
        self.fluid_level_low = fluid_level_low
        self.fluid_level_high = fluid_level_high
        self.fluid_density_low = fluid_density_low
        self.fluid_density_high = fluid_density_high
        self.drag_coef_low = drag_coef_low
        self.drag_coef_high = drag_coef_high
        # floor
        self.plane_level = plane_level
        self.plane_size = plane_size
        self.friction_mu_low = friction_mu_low
        self.friction_mu_high = friction_mu_high
        self.slope_x_low = slope_x_low
        self.slope_x_high = slope_x_high
        self.slope_y_low = slope_y_low
        self.slope_y_high = slope_y_high
        self.reset(seed)

    def reset(self, seed=None, reseed=False):
        # sample stage
        rng = np.random
        if reseed:
            rng.seed(seed)
        self.target_dir = sample_target_dir(rng, 0.5 * math.pi, 0.5 * math.pi,
                                            self.target_phi_low, self.target_phi_high)
        self.terrain_name, self.terrain, norm, rot = \
            sample_terrain(self.world, rng, ['Fluid', 'Plane', 'Slope'],
                           [self.fluid_level_low, self.fluid_density_low, self.drag_coef_low],
                           [self.fluid_level_high, self.fluid_density_high, self.drag_coef_high],
                           [self.friction_mu_low, self.slope_x_low, self.slope_y_low],
                           [self.friction_mu_high, self.slope_x_high, self.slope_y_high],
                           self.plane_level, self.plane_size)
        self.target_dir = tr3d.quaternions.rotate_vector(self.target_dir, rot)
        self.init_orient = tr3d.quaternions.quat2axangle(
            tr3d.quaternions.qmult(
                rot,
                tr3d.quaternions.axangle2quat(self.init_orient[0], self.init_orient[1], is_normalized=True)))
        # log information of current env
        if self.terrain_name == "Fluid":
            print(f"In fluid. level={self.terrain[0]}, density={self.terrain[1]}, drag={self.terrain[2]}.")
        # friction_mu is random
        elif self.terrain_name == "Plane":
            print(f"On plane. level={self.plane_level}, size={self.plane_size}, friction={self.terrain[2]}.")
            # a*x+b*y+c*z+d=0, a and b are random, c==1 and d== -self.plane_level
        else:
            print(f"On slope. level={self.plane_level}, size={self.plane_size}, "
                  f"x_slope={self.terrain[0]}, y_slope={self.terrain[1]}, friction={self.terrain[2]}.")


if __name__ == "__main__":
    from ReducedSoftRobotUtils import Agent, get_primitive_world
    from Reward import bind_quadratic_tvr_reward_package

    mesh_name_list = ["cross.abq",
                      "beam.mesh",
                      "walker_5.mesh",
                      "walker_high_10.mesh"]
    mesh_type_list = [0, 1, 1, 1]
    worlds = [get_primitive_world(mesh_name=mesh_name,
                                  basis_num=20,
                                  elastic_energy="Corotated",
                                  lame_first_coef=1e4,
                                  lame_second_coef=1e4,
                                  mesh_type=mesh_type
                                  ) for mesh_name, mesh_type in zip(mesh_name_list, mesh_type_list)]
    agents = [Agent(ctrl_reduced=False, k=[0.03])]
    envs = [ReducedSoftRobotWalk(world=worlds[0], agents=agents),
            ReducedSoftRobotSwim(world=worlds[1], agents=agents),
            ReducedSoftRobotClimb(world=worlds[2], agents=agents),
            ReducedSoftRobotVersatile(world=worlds[3], agents=agents)]
    for env in envs:
        bind_quadratic_tvr_reward_package(env,
                                          t_reward_w=1,
                                          v_reward_w=1,
                                          r_reward_w=1,
                                          target_t=15,
                                          target_v=1.5)
        # test simulating 1 frame
        action = np.zeros(env.ctrl_dim)
        state = env.init_pos
        state = env.step(action, state)
        reward = env.reward(env.get_obs(state))
        print(f"Finish simulating {env.__class__.__name__} with one agent. Reward is {reward}")
        # test simulating 2 frames simultaneously
        actions = np.array([np.zeros(env.ctrl_dim), np.zeros(env.ctrl_dim)])
        states = np.array([env.init_pos, env.init_pos])
        env.copy_agents(2)
        states = env.step_multiple(actions, states)
        reward = env.reward(env.get_obs(states))
        # print(type(states)) returned type is list no matter input states' type is list or np.ndarray
        print(f"finish simulating {env.__class__.__name__} with two agents. Reward is {reward}")
    test_render = False
    if test_render:
        for env in envs:
            env.render_bases()
