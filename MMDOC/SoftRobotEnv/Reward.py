import types
import autograd.numpy as np
import sys

sys.path.append('../../../omtg-build')
import pyPhysicsMotion as pm


def quadratic_reward(x, x_target=None, weight=None):
    if x_target is None:
        x_target = np.zeros_like(x)
    if weight is None:
        weight = np.ones_like(x)
    return -np.sum(np.square(weight * (x - x_target)), axis=-1)


def smooth_abs_reward(x, x_target=None, weight=None, alpha=1):
    """
    alpha > 0. With the smaller alpha, the reward is more close to
    an absolute value reward.
    """
    quadratic_term = -quadratic_reward(x, x_target, weight)
    reward = (alpha - np.sqrt(quadratic_term + alpha * alpha))
    return reward


def value_limiting_reward(x, x_target=None, weight=None, alpha=1):
    """
    alpha > 0, with the smaller alpha, the reward punishes more values out of range.
    """
    if x_target is None:
        x_target = np.zeros_like(x)
    if weight is None:
        weight = np.ones_like(x)
    cos_h_term = np.cosh(weight * (x - x_target) / alpha)
    reward = -np.sum((alpha * alpha * cos_h_term - 1),axis=-1)
    return reward


def get_tr(env, state):
    if isinstance(state, pm.FEMGradientInfoFLOAT):
        obs = np.concatenate(
            (
                state.getT().reshape(1, 3),
                state.getX()[env.basis_num + 3:env.basis_num + 6].reshape(1, 3),
            )
            , axis=1)
    else:
        obs = np.zeros((len(state), 6))
        for i in range(len(state)):
            obs[i] = np.concatenate(
                (
                    state[i].getT().reshape(-1, ),
                    state[i].getX()[env.basis_num + 3:env.basis_num + 6].reshape(-1, ),
                )
            )
    return obs


def split_6_dim_obs(obs):
    obs1 = obs[..., 0:3]
    obs2 = obs[..., 3:6]
    return obs1, obs2


def bind_quadratic_tr_reward_package(global_env,
                                     t_reward_w,
                                     r_reward_w,
                                     target_t):
    def reward(env, obs):
        assert obs.shape[-1] == 6
        translation, rotation = split_6_dim_obs(obs)
        t_reward = t_reward_w * quadratic_reward(translation, env.target_dir * target_t)
        r_reward = r_reward_w * quadratic_reward(rotation, env.init_orient[0] * env.init_orient[1])
        tot_reward = t_reward + r_reward
        return tot_reward

    global_env.get_obs = types.MethodType(get_tr, global_env)
    global_env.reward = types.MethodType(reward, global_env)

    return global_env


def get_vr(env, state):
    if isinstance(state, pm.FEMGradientInfoFLOAT):
        obs = np.concatenate(
            (
                state.getV().reshape(1, 3),
                state.getX()[env.basis_num + 3:env.basis_num + 6].reshape(1, 3),
            )
            , axis=1)
    else:
        obs = np.zeros((len(state), 6))
        for i in range(len(state)):
            obs[i] = np.concatenate(
                (
                    state[i].getV().reshape(-1, ),
                    state[i].getX()[env.basis_num + 3:env.basis_num + 6].reshape(-1, ),
                )
            )
    return obs


def bind_quadratic_vr_reward_package(global_env,
                                     v_reward_w,
                                     r_reward_w,
                                     target_v):
    def reward(env, obs):
        assert obs.shape[-1] == 6
        velocity, rotation = split_6_dim_obs(obs)
        v_reward = v_reward_w * quadratic_reward(velocity, env.target_dir * target_v)
        r_reward = r_reward_w * quadratic_reward(rotation, env.init_orient[0] * env.init_orient[1])
        tot_reward = v_reward + r_reward
        return tot_reward

    global_env.get_obs = types.MethodType(get_vr, global_env)
    global_env.reward = types.MethodType(reward, global_env)

    return global_env


def get_dvr(env, state):
    if isinstance(state, pm.FEMGradientInfoFLOAT):
        obs = np.concatenate(
            (state.getT().reshape(1, 3),
             state.getV().reshape(1, 3),
             state.getX()[env.basis_num + 3:env.basis_num + 6].reshape(1, 3),
             )
            , axis=1)
    else:
        obs = np.zeros((len(state), 9))
        for i in range(len(state)):
            obs[i] = np.concatenate(
                (state[i].getT().reshape(-1, ),
                 state[i].getV().reshape(-1, ),
                 state[i].getX()[env.basis_num + 3:env.basis_num + 6].reshape(-1, ),
                 )
            )
    return obs


def split_9_dim_obs(obs):
    translation = obs[..., 0:3]
    velocity = obs[..., 3:6]
    rotation = obs[..., 6:9]
    return translation, velocity, rotation


def bind_quadratic_dvr_reward_package(global_env,
                                      d_reward_w,
                                      v_reward_w,
                                      r_reward_w,
                                      target_v):
    def reward(env, obs):
        assert obs.shape[-1] == 9
        translation, velocity, rotation = split_9_dim_obs(obs)
        proj_len = np.dot(translation, env.target_dir)
        deviation = translation - np.expand_dims(proj_len, axis=-1) * (env.target_dir * np.ones_like(translation))
        d_reward = d_reward_w * quadratic_reward(deviation)
        v_reward = v_reward_w * quadratic_reward(velocity, env.target_dir * target_v)
        r_reward = r_reward_w * quadratic_reward(rotation, env.init_orient[0] * env.init_orient[1])
        tot_reward = d_reward + v_reward + r_reward
        return tot_reward

    global_env.get_obs = types.MethodType(get_dvr, global_env)
    global_env.reward = types.MethodType(reward, global_env)

    return global_env


def bind_smooth_abs_tr_reward_package(global_env,
                                      t_reward_w,
                                      r_reward_w,
                                      target_t):
    def reward(env, obs):
        assert obs.shape[-1] == 6
        t, r = split_6_dim_obs(obs)
        t_reward = t_reward_w * smooth_abs_reward(t, target_t * env.target_dir)
        r_reward = r_reward_w * smooth_abs_reward(r, env.init_orient[0] * env.init_orient[1])
        tot_reward = t_reward + r_reward
        return tot_reward

    global_env.get_obs = types.MethodType(get_tr, global_env)
    global_env.reward = types.MethodType(reward, global_env)

    return global_env


if __name__ == '__main__':
    pass
