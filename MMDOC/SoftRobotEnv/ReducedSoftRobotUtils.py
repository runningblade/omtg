import os.path
import sys, math
import numpy as np
import torch as th
import glob

BASE = "../../../omtg-build/"
MESH_DIR = "../Mesh/"
REDUCED_MODELS_DIR = "../ReducedModels/"

rot_tol = 1e-6
lame_coef_tol = 1e-2

sys.path.append(BASE)
import pyPhysicsMotion as pm
import transforms3d as tr3d


class controlSpaceTooLargeWarning(UserWarning):
    pass


class Agent:
    def __init__(self, ctrl_type='P', ctrl_reduced=False, k=None, design=None):
        self.ctrl_type = ctrl_type
        self.ctrl_reduced = 'Reduced' if ctrl_reduced else ''
        self.controller = getattr(pm, "FEM" + self.ctrl_reduced + "PDControllerFLOAT")()
        signal_num = 2 if ctrl_type == 'PD' else 1
        self.k = [0.] * signal_num if k is None else k
        self.desired_values = [0.] * signal_num
        self.design = design

    def resetP(self):
        self.controller.resetP(self.desired_values[0], self.k[0])

    def resetD(self):
        self.controller.resetD(self.desired_values[0], self.k[0])

    def resetPD(self):
        self.controller.resetPD(self.desired_values[0], self.k[0],
                                self.desired_values[1], self.k[1])

    def resetPReduced(self):
        self.controller.resetPReduced(self.desired_values[0], self.k[0], self.design)

    def resetDReduced(self):
        self.controller.resetDReduced(self.desired_values[0], self.k[0], self.design)

    def resetPDReduced(self):
        self.controller.resetPDReduced(self.desired_values[0], self.k[0], self.desired_values[1], self.k[1],
                                       self.design)

    def copy(self):
        return Agent(self.ctrl_type, True if self.ctrl_reduced == "Reduced" else False, self.k, self.design)


def deserialize_state(world, state):
    if isinstance(state, th.Tensor):
        state = state.detach().numpy()
    return pm.FEMGradientInfoFLOAT(world, state)


def serialize_state(state):
    if state is None:
        return None
    return state.getX().T[0]


def create_body(mesh_name, mesh_type=0):
    mesh_file_name = MESH_DIR + mesh_name
    if mesh_type == 0:
        body = pm.FEMMeshFLOAT(True, mesh_file_name, True)
    elif mesh_type == 1:
        body = pm.FEMMeshFLOAT(mesh_file_name, True)
    elif mesh_type == 2:
        body = pm.FEMMeshFLOAT(mesh_file_name, .05, True)
    else:
        body = pm.FEMMeshFLOAT(mesh_file_name, .05, True)
        bodyHOct = pm.FEMOctreeMeshFLOAT(body, 0)
        body = bodyHOct.getMesh()
    return body


def get_reduced_model_name_pattern(mesh_name,
                                   basis_num,
                                   elastic_energy):
    model_name_pattern = REDUCED_MODELS_DIR + mesh_name + \
                         f"_{basis_num}_" + elastic_energy + "_(*,*).rmodel"
    return model_name_pattern


def get_reduced_model_name(mesh_name,
                           basis_num,
                           elastic_energy,
                           lame_first_coef,
                           lame_second_coef):
    model_name = REDUCED_MODELS_DIR + mesh_name + \
                 f"_{basis_num}_" + elastic_energy + \
                 f"_({lame_first_coef},{lame_second_coef}).rmodel"
    return model_name


def get_primitive_world(mesh_name: str,
                        basis_num: int,
                        elastic_energy: str,
                        lame_first_coef: float,
                        lame_second_coef: float,
                        mesh_type: int = 0,
                        dt: float = 0.005,
                        damp: float = 1.0,
                        max_contact: int = 10):
    world = None
    model_name_pattern = get_reduced_model_name_pattern(mesh_name,
                                                        basis_num,
                                                        elastic_energy)
    model_names = glob.glob(model_name_pattern)
    for model_name in model_names:
        lame_coefs_start_idx = model_name.find("(")
        comma_idx = model_name.find(",")
        lame_coefs_end_idx = model_name.find(")")
        try:
            lame_first_coef_written = float(model_name[lame_coefs_start_idx + 1:comma_idx])
            lame_second_coef_written = float(model_name[comma_idx + 1:lame_coefs_end_idx])
        except ValueError:
            print(f"{model_name} is not a valid reduced model path."
                  "Content inside the parentheses should be two floats separated by a comma.")
        else:
            if math.fabs(lame_first_coef_written - lame_first_coef) < lame_coef_tol and \
                    math.fabs(lame_second_coef_written - lame_second_coef) < lame_coef_tol:
                print(f"Potential valid reduced model {model_name} will be loaded.")
                world = pm.FEMReducedSystemFLOAT()
                world.readStr(model_name)
                break
    if world is None:
        print("No valid saved reduced model is found. Build a new one through model reduction.")
        world = create_reduced_model(mesh_name,
                                     basis_num,
                                     elastic_energy,
                                     lame_first_coef,
                                     lame_second_coef,
                                     mesh_type,
                                     save=True)
    set_world_intrinsic_params(world,
                               dt,
                               damp,
                               max_contact)
    return world


GRAVITY = 9.81
NEG_INF_LEVEL = -100.0


def create_reduced_model(mesh_name: str,
                         basis_num: int,
                         elastic_energy: str,
                         lame_first_coef,
                         lame_second_coef,
                         mesh_type: int = 0,
                         save=False
                         ):
    mesh = create_body(mesh_name, mesh_type)
    world = pm.FEMReducedSystemFLOAT(mesh)
    getattr(world, "add" + elastic_energy + "ElasticEnergy")(lame_first_coef, lame_second_coef)
    world.addGravitationalEnergy(GRAVITY)
    world.addFluidEnergy(NEG_INF_LEVEL, 1.0, GRAVITY, 1.0, deg=0)
    world.buildBasis(basis_num)
    if save:
        world.writeStr(get_reduced_model_name(mesh_name,
                                              basis_num,
                                              elastic_energy,
                                              lame_first_coef,
                                              lame_second_coef))
    return world


def set_world_intrinsic_params(world,
                               dt=0.005,
                               damp=1.0,
                               max_contact=10,
                               ):
    world.dt(dt)
    world.dampingCoef(damp)
    world.maxContact(max_contact)


def set_floor(world, level, size, x_slope, y_slope, friction,
              compute_rot_from_xy=False):
    normal = np.array([-x_slope, -y_slope, 1.0])
    normal = normal / np.linalg.norm(normal)
    floor = np.append(normal, -level / normal[2])
    world.mu(friction)
    world.setEnv(pm.EnvironmentHeightFLOAT())
    world.getEnv().createFloor(np.dot(size, floor))
    if compute_rot_from_xy:
        if np.allclose(normal, np.array([0, 0, 1]), atol=rot_tol, rtol=rot_tol):
            axis = np.array([0, 0, 1])
            theta = 0.0
        else:
            axis = np.cross(np.array([0, 0, 1]), normal)
            theta = math.acos(normal[2])
        rot = tr3d.quaternions.axangle2quat(axis, theta)
        return normal, rot
    return normal


def set_terrain(world, cur_terrain_type, terrain, plane_level=-0.01, plane_size=5.0):
    """
    Only used in sample_terrain
    """
    rot = np.array([1, 0, 0, 0])
    normal = np.array([0., 0., 1.])
    if cur_terrain_type == "Fluid":
        world.setFluidEnergy(terrain[0], terrain[1], GRAVITY, terrain[2], deg=0)
    elif cur_terrain_type == "Plane":
        set_floor(world, plane_level, plane_size, terrain[0], terrain[1], terrain[2])
    else:
        assert cur_terrain_type == "Slope"
        normal, rot = set_floor(world, plane_level, plane_size, terrain[0], terrain[1], terrain[2],
                                compute_rot_from_xy=True)
    return normal, rot


def sample_terrain(world,
                   rng,
                   terrain_types,
                   fluid_low,
                   fluid_high,
                   floor_low,
                   floor_high,
                   plane_level=-0.01,
                   plane_size=5.0):
    """
    Randomly generate terrain. Used in ReducedSoftRobotVersatile.reset().
    """
    rnd_num = rng.uniform(0.0, 1.0)
    cur_terrain_type = 'Empty'
    for i in range(0, len(terrain_types)):
        if 1.0 * i / len(terrain_types) <= rnd_num < 1.0 * (i + 1) / len(terrain_types):
            cur_terrain_type = terrain_types[i]
            break

    # fluid density, fluid level and drag coefficient is random
    assert cur_terrain_type != 'Empty'
    terrain = np.zeros(3)
    if cur_terrain_type == "Fluid":
        terrain = [rng.uniform(fluid_low[i], fluid_high[i]) for i in range(0, len(fluid_low))]
    # friction_mu is random
    else:
        mu = rng.uniform(floor_low[0], floor_high[0])
        if cur_terrain_type == "Plane":
            terrain = np.array([0., 0., mu])
        # a*x+b*y+c*z+d=0, a and b are random, c==1 and d== -self.plane_level
        if cur_terrain_type == "Slope":
            slope_x = rng.uniform(floor_low[1], floor_high[1])
            slope_y = rng.uniform(floor_low[2], floor_high[2])
            terrain = np.array([slope_x, slope_y, mu])
    norm, rot = set_terrain(world, cur_terrain_type, terrain, plane_level, plane_size)
    return cur_terrain_type, terrain, norm, rot


def sample_target_dir(rng, theta_low, theta_high, phi_low, phi_high):
    theta_range = np.array([theta_low, theta_high])
    phi = rng.uniform(phi_low, phi_high)
    if math.fabs(theta_range[0] - theta_range[1]) < 1e-3:
        theta = 0.5 * (theta_range[0] + theta_range[1])
        sin_theta = math.sin(theta)
        cos_theta = math.cos(theta)
    else:
        cos_theta_range = np.cos(theta_range)
        cos_theta = rng.uniform(cos_theta_range[0], cos_theta_range[1])
        sin_theta = math.sqrt(1 - cos_theta * cos_theta)
    target_dir = np.array([sin_theta * math.cos(phi), sin_theta * math.sin(phi), cos_theta])
    return target_dir


def apply_control_multiple(actions, agents, mask=None):
    if mask is None:
        for action, agent in zip(actions, agents):
            apply_control(action, agent)
    else:
        actions_sim = np.array(actions)[mask == True]
        agents_sim = np.array(agents)[mask == True]
        for action, agent in zip(actions_sim, agents_sim):
            apply_control(action, agent)


def apply_control(action, agent):
    ctrl_dim = int(action.shape[-1] / len(agent.desired_values))
    for i in range(len(agent.desired_values)):
        agent.desired_values[i] = action[(ctrl_dim * i):(ctrl_dim * (i + 1))]
    getattr(agent, "reset" + agent.ctrl_type + agent.ctrl_reduced)()


def simulate(world, frame, agent, contact_solver, wantContact=False):
    ret = world.step(frame, agent.controller, contact_solver, wantContact)
    return ret


def simulate_multiple(world, frames, agents, contact_solver, wantContact=False, mask=None):
    if mask is None:
        ret = world.stepMultiple(frames, [agent.controller for agent in agents],
                                 contact_solver, wantContact)
    else:
        assert len(mask) == len(agents)
        frames = np.array(frames)
        agents = np.array(agents)
        mask = np.array(mask)
        agents_to_sim = agents[mask == True]
        frames_to_sim = frames[mask == True]
        ret = np.array([[frame, True] for frame in frames])
        ret_simulated = np.array(world.stepMultiple(frames_to_sim, [agent.controller for agent in agents_to_sim],
                                                    contact_solver, wantContact))
        ret[mask == True] = ret_simulated
    return ret


def apply_design(design, agent):
    assert agent.ctrl_reduced == "Reduced"
    agent.design = design


def is_simulation_failed(ret):
    sim_fail = not ret[1]
    if not sim_fail:
        sim_fail = sim_fail | \
                   ((np.abs(ret[0].getDUDt()) > 1e6).any()) | ((np.abs(ret[0].getV()) > 1e6).any()) | (
                       np.abs((ret[0].getW() > 1e6)).any()) | \
                   (np.isnan(ret[0].getDUDt()).any()) | (np.isnan(ret[0].getV()).any()) | (
                       np.isnan(ret[0].getW()).any()) | \
                   (np.isinf(ret[0].getDUDt()).any()) | (np.isinf(ret[0].getV()).any()) | (
                       np.isinf(ret[0].getW()).any())
    return sim_fail


if __name__ == "__main__":
    get_primitive_world(mesh_name="cross.abq",
                        basis_num=20,
                        elastic_energy="Corotated",
                        lame_first_coef=1e4,
                        lame_second_coef=1e4)
