import numpy as np
import math
from abc import ABC, abstractmethod

import torch as th
from scipy.special import comb


class SignalInterpreter(ABC):
    def __init__(self,
                 n_actions,
                 low_action,
                 high_action,
                 low_ctrl,
                 high_ctrl,
                 scale_in_interpreter=True):
        self.low_action = low_action
        self.high_action = high_action
        self.low_ctrl = low_ctrl
        self.high_ctrl = high_ctrl
        self.n_actions = n_actions
        self.scale_in = scale_in_interpreter

    def __call__(self, ctrl_signal):
        if isinstance(ctrl_signal, th.Tensor):
            ctrl_signal = ctrl_signal.detach().numpy()
        return self.step(ctrl_signal)

    @abstractmethod
    def step(self, ctrl_signal):
        """
        This class takes observations of the environment as inputs,
        and output a list of actions that can be directly fed to the environment.
        """
        pass

    @abstractmethod
    def copy(self):
        pass

    def set_default(self, ctrl_point_dim):
        pass

    def tile(self, base, expand_dims):
        pass

    def assign_points(self, base):
        pass

    @property
    def ctrl_points(self):
        return np.array([0])

    @ctrl_points.setter
    def ctrl_points(self, _points):
        pass

    def repeat_last(self, reps):
        """
        Designed only for compatibility with siLQG controller
        """
        pass

    def truncate(self, n_truncate, truncate_rear=True):
        pass

    def squeeze(self):
        pass


class SquareWaveGenerator(SignalInterpreter):
    def step(self, ctrl_signal):
        if self.scale_in:
            ctrl_signal = np.minimum(np.maximum(ctrl_signal, self.low_ctrl * np.ones_like(ctrl_signal)),
                                     self.high_ctrl * np.ones_like(ctrl_signal))
            action = self.low_action + (ctrl_signal - self.low_ctrl) * (self.high_action - self.low_action) / (
                    self.high_ctrl - self.low_ctrl)
        else:
            action = 1.0 * ctrl_signal
        actions = [action] * self.n_actions
        return actions

    def copy(self):
        return SquareWaveGenerator(n_actions=self.n_actions,
                                   low_action=self.low_action,
                                   high_action=self.high_action,
                                   low_ctrl=self.low_ctrl,
                                   high_ctrl=self.high_ctrl,
                                   scale_in_interpreter=self.scale_in, )




class BezierCurveGenerator(SignalInterpreter):
    def __init__(self, degree=2, initial_ctrl_point=None, scaled=False, unsqueezed=False, eps=1e-6, **base_kwargs):
        super(BezierCurveGenerator, self).__init__(**base_kwargs)
        self.degree = degree
        self.eps = eps
        if initial_ctrl_point is not None:
            if not unsqueezed:
                initial_ctrl_point = np.expand_dims(initial_ctrl_point, axis=0)
            if not scaled and self.scale_in:
                initial_ctrl_point = self.low_action + (initial_ctrl_point - self.low_ctrl) * (
                        self.high_action - self.low_action) / (
                                             self.high_ctrl - self.low_ctrl)
            self.P0 = initial_ctrl_point
        else:
            self.set_default((self.low_action.shape[-1],))

    def step(self, ctrl_signal):
        ctrl_points = np.reshape(ctrl_signal, newshape=ctrl_signal.shape[:-1] + (self.degree, -1))
        if self.scale_in:
            ctrl_signal = np.minimum(np.maximum(ctrl_signal, self.low_ctrl * np.ones_like(ctrl_signal)),
                                     self.high_ctrl * np.ones_like(ctrl_signal))
            ctrl_points = self.low_action + (ctrl_signal - self.low_ctrl) * (self.high_action - self.low_action) / (
                    self.high_ctrl - self.low_ctrl)
        ctrl_points = np.concatenate((self.P0, ctrl_points), axis=-2)
        self.P0 = ctrl_points[..., -1:, :]

        def _point_on_bezier_curve(_ctrl_points, t=0.0):
            # point(t) = (degree, k) * (1-t)^(degree - k) * t^k * _ctrl_points[k] 0 <= k <=degree, 0 <= t < 1
            # Special case: t^k is defined to be 1 when both t and k equal 0
            point = math.pow(1 - t, self.degree) * _ctrl_points[..., 0, :]
            if math.fabs(t) < self.eps:
                return point
            for k in range(self.degree):
                point += comb(self.degree, k + 1, exact=True) * math.pow(1 - t, self.degree - 1 - k) * math.pow(t,
                                                                                                                k + 1) * \
                         _ctrl_points[..., k + 1, :]
            return point

        actions = []
        for i in range(self.n_actions):
            action = _point_on_bezier_curve(ctrl_points, t=1.0 * i / self.n_actions)
            actions.append(action)
        return actions

    def copy(self):
        return BezierCurveGenerator(n_actions=self.n_actions,
                                    low_action=self.low_action,
                                    high_action=self.high_action,
                                    low_ctrl=self.low_ctrl,
                                    high_ctrl=self.high_ctrl,
                                    scale_in_interpreter=self.scale_in,
                                    initial_ctrl_point=np.copy(self.P0),
                                    scaled=True,
                                    unsqueezed=True,
                                    degree=self.degree)

    def tile(self, base, expand_dims):
        self.P0 = np.tile(base.P0, expand_dims + (1,) * len(base.P0.shape))

    def repeat_last(self, reps):
        assert len(self.P0.shape) > 2
        if reps > 1:
            repeated_P0_last = np.repeat(self.P0[-1:], reps - 1, axis=0)
            self.P0 = np.concatenate((self.P0, repeated_P0_last), axis=0)

    @property
    def ctrl_points(self):
        return np.copy(self.P0)

    def assign_points(self, base):
        self.ctrl_points = base.ctrl_points

    @ctrl_points.setter
    def ctrl_points(self, P0):
        self.P0 = P0

    def set_default(self, ctrl_point_shape):
        self.P0 = 0.5 * (self.low_action + self.high_action) * np.ones((1,) + ctrl_point_shape)

    def truncate(self, n_truncate, truncate_rear=True):
        if truncate_rear:
            self.P0 = self.P0[:-n_truncate]
        else:
            self.P0 = self.P0[n_truncate:]

    def squeeze(self):
        self.P0 = self.P0[0]


debug_catmull = False


def catmull_rom_spline_dT(P0, P1, alpha=0.5, eps=1e-6):
    dist = np.linalg.norm(P1 - P0, axis=-1, keepdims=True)
    if (dist < eps).any():
        if debug_catmull:
            print(f"Warning: Distance between 2 control points of Catmull-Rom spline is less than {eps}. "
                  f"The distance is forced to be {100 * eps}.")
            print(dist)
        dist[dist < eps] = eps
    dT = np.power(dist, alpha)
    return dT


def catmull_rom_spline_T(P, alpha=0.5, eps=1e-6):
    T = np.zeros(P.shape[:-1] + (1,))
    for i in range(3):
        T[i + 1] = catmull_rom_spline_dT(P[i], P[i + 1], alpha, eps) + T[i]
    return T


class CatmullRomSplineGenerator(SignalInterpreter):
    """
    Catmull-Rom spline is C^1 continuous by nature.
    """

    def __init__(self, alpha=0.5, initial_ctrl_points=None, scaled=False, eps=1e-6, **base_kwargs):
        super(CatmullRomSplineGenerator, self).__init__(**base_kwargs)
        self.alpha = alpha
        self.eps = eps
        if initial_ctrl_points is not None:
            if not isinstance(initial_ctrl_points, np.ndarray):
                initial_ctrl_points = np.array(initial_ctrl_points)
            if scaled or not self.scale_in:
                self.P = initial_ctrl_points
            else:
                self.P = self.low_action + (initial_ctrl_points - self.low_ctrl) * (
                        self.high_action - self.low_action) / (
                                 self.high_ctrl - self.low_ctrl)
            self.T = catmull_rom_spline_T(self.P, self.alpha, self.eps)
        else:
            self.set_default((self.low_action.shape[-1],))

    def step(self, ctrl_signal):
        self.P = np.roll(self.P, -1, axis=0)
        old_T_1 = self.T[1]
        for i in range(3):
            self.T[i + 1] -= old_T_1
        self.T = np.roll(self.T, -1, axis=0)
        if self.scale_in:
            ctrl_signal = np.minimum(np.maximum(ctrl_signal, self.low_ctrl * np.ones_like(ctrl_signal)),
                                     self.high_ctrl * np.ones_like(ctrl_signal))
            self.P[3] = self.low_action + (ctrl_signal - self.low_ctrl) * (self.high_action - self.low_action) / (
                    self.high_ctrl - self.low_ctrl)
        else:
            self.P[3] = ctrl_signal
        self.T[3] = catmull_rom_spline_dT(self.P[2], self.P[3], self.alpha, self.eps) + self.T[2]

        def _point_on_catmull_rom_spline(P, t, T=None, alpha=0.5):
            # t1 <= t < t2 where t1 = ||P1-P0||^alpha and t2 = ||P2-P1||^alpha + t1
            if T is None:
                T = catmull_rom_spline_T(P, alpha, self.eps)
            A = np.zeros(((3,) + P.shape[1:]))
            for j in range(3):
                A[j] = (T[j + 1] - t) / (T[j + 1] - T[j]) * P[j] + (t - T[j]) / (T[j + 1] - T[j]) * P[j + 1]
            B = np.zeros(((2,) + P.shape[1:]))
            for j in range(2):
                B[j] = (T[j + 2] - t) / (T[j + 2] - T[j]) * A[j] + (t - T[j]) / (T[j + 2] - T[j]) * A[j + 1]
            C = (T[2] - t) / (T[2] - T[1]) * B[0] + (t - T[1]) / (T[2] - T[1]) * B[1]
            return C

        actions = []
        ts = np.linspace(self.T[1], self.T[2], num=self.n_actions, endpoint=False)
        for i in range(self.n_actions):
            action = _point_on_catmull_rom_spline(self.P, t=ts[i], T=self.T, alpha=self.alpha)
            actions.append(action)
        return actions

    def copy(self):
        return CatmullRomSplineGenerator(n_actions=self.n_actions,
                                         low_action=self.low_action,
                                         high_action=self.high_action,
                                         low_ctrl=self.low_ctrl,
                                         high_ctrl=self.high_ctrl,
                                         scale_in_interpreter=self.scale_in,
                                         initial_ctrl_points=np.copy(self.P),
                                         scaled=True,
                                         alpha=self.alpha)

    def tile(self, base, expand_dims):
        ctrl_point_shape = base.P.shape[1:]
        T_shape = base.T.shape[1:]
        expanded_initial_ctrl_points = np.zeros((4,) + expand_dims + ctrl_point_shape)
        expanded_T = np.zeros((4,) + expand_dims + T_shape)
        for i in range(4):
            expanded_initial_ctrl_points[i] = base.P[i]
            expanded_T[i] = base.T[i]
        self.P = expanded_initial_ctrl_points
        self.T = expanded_T

    def repeat_last(self, reps):
        assert len(self.P.shape) > 2
        if reps > 1:
            repeated_P_last = np.repeat(self.P[:, -1:], reps - 1, axis=1)
            repeated_T_last = np.repeat(self.T[:, -1:], reps - 1, axis=1)
            self.P = np.concatenate((self.P, repeated_P_last), axis=1)
            self.T = np.concatenate((self.T, repeated_T_last), axis=1)

    @property
    def ctrl_points(self):
        return np.copy(self.P)

    @ctrl_points.setter
    def ctrl_points(self, P):
        self.P = P
        self.T = catmull_rom_spline_T(self.P, self.alpha, self.eps)

    def assign_points(self, base):
        self.ctrl_points = base.ctrl_points
        self.T = np.copy(base.T)

    def set_default(self, ctrl_point_shape):
        scale = 0.1 * (self.high_action - self.low_action)
        loc = 0.5 * (self.high_action + self.low_action)
        self.P = (loc+scale) * np.ones((4,) + ctrl_point_shape)
        #np.random.normal(loc=loc, scale=scale, size=(4,) + ctrl_point_shape)
        self.T = catmull_rom_spline_T(self.P, self.alpha, self.eps)

    def truncate(self, n_truncate, truncate_rear=True):
        if truncate_rear:
            self.P = self.P[:, :-n_truncate]
            self.T = self.T[:, :-n_truncate]
        else:
            self.P = self.P[:, n_truncate:]
            self.T = self.T[:, n_truncate:]

    def squeeze(self):
        self.P = self.P[:, 0]
        self.T = self.T[:, 0]


def module_test():
    # initialization
    n_actions_sw = 100
    n_actions_crs = 1 * n_actions_sw
    degree = 3
    n_actions_bc = degree * n_actions_sw

    low = -2 * np.ones(2, dtype=np.double)
    high = 2 * np.ones(2, dtype=np.double)
    initial_ctrl_points = np.array([[-0.2, -0.1],
                                    [0.1, 0.0],
                                    [0.0, 0.25],
                                    [0.2, 0.5]])
    square_wave = SquareWaveGenerator(n_actions=n_actions_sw,
                                      low_action=low,
                                      high_action=high,
                                      low_ctrl=-1.0,
                                      high_ctrl=1.0)
    bezier_curve = BezierCurveGenerator(n_actions=n_actions_bc,
                                        low_action=low,
                                        high_action=high,
                                        low_ctrl=-1.0,
                                        high_ctrl=1.0,
                                        initial_ctrl_point=initial_ctrl_points[2],
                                        degree=degree)
    expand_dims = (16,)
    _copied_bezier_curve = bezier_curve.copy()
    print(f"Before expansion, shape of BezierCurveGenerator.initial_ctrl_point is {_copied_bezier_curve.P0.shape}")
    _copied_bezier_curve.tile(bezier_curve, expand_dims)
    print(f"After expansion, shape of BezierCurveGenerator.initial_ctrl_point is {_copied_bezier_curve.P0.shape}")
    alpha = 0.5
    catmull_rom_spline = CatmullRomSplineGenerator(n_actions=n_actions_crs,
                                                   low_action=low,
                                                   high_action=high,
                                                   low_ctrl=-1.0,
                                                   high_ctrl=1.0,
                                                   initial_ctrl_points=initial_ctrl_points,
                                                   alpha=alpha)
    _copied_catmull_rom = catmull_rom_spline.copy()
    print(
        f"Before expansion, shape of CatmullRomSplineGenerator.P is {_copied_catmull_rom.P.shape}")
    _copied_catmull_rom.tile(catmull_rom_spline, expand_dims)
    print(
        f"After expansion, shape of CatmullRomSplineGenerator.P is {_copied_catmull_rom.P.shape}")
    # test
    ctrl_signals_pending = np.array([[0.3, 0.6],
                                     [-0.3, -0.4],
                                     [0.6, -0.2]])
    ctrl_signals_crs = ctrl_signals_pending[:degree]
    ctrl_signals_sw = np.concatenate((initial_ctrl_points[2:], ctrl_signals_crs[:degree - 1]), axis=0)
    ctrl_signal_bc = initial_ctrl_points[3]
    for i in range(degree - 1):
        ctrl_signal_bc = np.concatenate((ctrl_signal_bc, ctrl_signals_pending[i]))
    actions_sw = []
    for ctrl_signal in ctrl_signals_sw:
        actions_sw += square_wave(ctrl_signal)
    actions_bc = bezier_curve(ctrl_signal_bc)
    actions_crs = []
    for ctrl_signal in ctrl_signals_crs:
        actions_crs += catmull_rom_spline(ctrl_signal)
    from matplotlib import pyplot as plt
    plt.plot(np.array(actions_sw)[:-n_actions_sw + 10, 0], c="blue")
    plt.plot(np.array(actions_bc)[:, 0], c="red")
    plt.plot(np.array(actions_crs)[:, 0], c="orange")
    plt.show()


if __name__ == "__main__":
    module_test()
