import sys

sys.path.append('..')
from _GenerateArgsParser import _GenerateArgsParser
from SoftRobotEnv.ReducedSoftRobotUtils import Agent, get_primitive_world
from SoftRobotEnv.ReducedSoftRobot import ReducedSoftRobotWalk, ReducedSoftRobotSwim, ReducedSoftRobotClimb
import math
import os
import numpy as np
import torch as th
from Render.PovRayRenderer import povray_render
from LowerLevelControl.ControlSystem import v_quadratic_reward, x_telescoping_reward, x_linear_reward, save_frames, \
    ControlSystem, get_MPPI_controller, get_siLQG_controller
from LowerLevelControl.SignalInterpreter import SquareWaveGenerator, BezierCurveGenerator, CatmullRomSplineGenerator
from SoftRobotEnv.Reward import bind_quadratic_dvr_reward_package, bind_quadratic_tr_reward_package, \
    bind_quadratic_vr_reward_package, bind_smooth_abs_tr_reward_package, quadratic_reward, value_limiting_reward
import pyPhysicsMotion as pm


def get_naive_design(basis_num, ctrl_dim):
    design = np.concatenate(
        (np.identity(ctrl_dim), np.zeros((basis_num - ctrl_dim, ctrl_dim))),
        axis=0
    )
    return design


def get_control_system(args, ctrl_reduced=False, design=None):
    if not ctrl_reduced:
        agents = [Agent(ctrl_reduced=ctrl_reduced,
                        k=[args.p_strength])]
    else:
        if design is None:
            design = get_naive_design(args.basis_num, args.ctrl_dim)
        agents = [Agent(ctrl_reduced=ctrl_reduced,
                        design=design,
                        k=[args.p_strength])]
    world = get_primitive_world(mesh_name=args.mesh_name,
                                basis_num=args.basis_num,
                                elastic_energy=args.elastic_energy,
                                lame_first_coef=args.lame_coefs[0],
                                lame_second_coef=args.lame_coefs[1],
                                mesh_type=args.mesh_type,
                                dt=args.sim_dt,
                                damp=args.damp,
                                max_contact=args.max_contact
                                )
    # construct env
    max_plane_size = 100.0
    if args.task == "Walk":
        env = ReducedSoftRobotWalk(
            world=world,
            agents=agents,
            orientation=(np.array(args.orientation_axis), args.orientation_theta),
            ctrl_dim=args.ctrl_dim,
            target_phi=args.target_phi,
            level=args.level,
            size=100.0,
            friction=args.friction
        )
    elif args.task == "Swim":
        env = ReducedSoftRobotSwim(
            world=world,
            agents=agents,
            orientation=(np.array(args.orientation_axis), args.orientation_theta),
            ctrl_dim=args.ctrl_dim,
            target_phi=args.target_phi,
            level=args.level,
            density=args.density,
            drag=args.drag
        )
    else:
        env = ReducedSoftRobotClimb(
            world=world,
            agents=agents,
            orientation=(np.array(args.orientation_axis), args.orientation_theta),
            ctrl_dim=args.ctrl_dim,
            target_phi=args.target_phi,
            level=args.level,
            size=100.0,
            friction=args.friction,
            x_slope=args.x_slope,
            y_slope=args.y_slope
        )
    # bind reward to env
    if args.env_reward_type == "quadratic_dvr":
        bind_quadratic_dvr_reward_package(env,
                                          d_reward_w=args.t_reward_w,
                                          v_reward_w=args.v_reward_w,
                                          r_reward_w=args.r_reward_w,
                                          target_v=args.target_v)
    elif args.env_reward_type == "quadratic_vr":
        bind_quadratic_vr_reward_package(env,
                                         v_reward_w=args.v_reward_w,
                                         r_reward_w=args.r_reward_w,
                                         target_v=args.target_v)
    elif args.env_reward_type == "quadratic_tr":
        bind_quadratic_tr_reward_package(env,
                                         t_reward_w=args.t_reward_w,
                                         r_reward_w=args.r_reward_w,
                                         target_t=args.target_t)
    else:
        bind_smooth_abs_tr_reward_package(env,
                                          t_reward_w=args.t_reward_w,
                                          r_reward_w=args.r_reward_w,
                                          target_t=args.target_t)

    # construct signal interpreter
    p_stretch = np.array([args.p_stretch] * env.ctrl_dim)
    n_actions = int(args.ctrl_dt / args.sim_dt)
    if args.action_pattern == "SquareWave":
        signal_interpreter = SquareWaveGenerator(high_action=p_stretch,
                                                 low_action=-p_stretch,
                                                 high_ctrl=1.0,
                                                 low_ctrl=-1.0,
                                                 n_actions=n_actions)
        points_per_ctrl = 1
    elif args.action_pattern == "BezierCurve":
        signal_interpreter = BezierCurveGenerator(high_action=p_stretch,
                                                  low_action=-p_stretch,
                                                  high_ctrl=1.0,
                                                  low_ctrl=-1.0,
                                                  n_actions=n_actions,
                                                  degree=args.degree,
                                                  )
        points_per_ctrl = args.degree
    else:
        signal_interpreter = CatmullRomSplineGenerator(high_action=p_stretch,
                                                       low_action=-p_stretch,
                                                       high_ctrl=1.0,
                                                       low_ctrl=-1.0,
                                                       n_actions=n_actions,
                                                       alpha=args.crs_alpha,
                                                       )
        points_per_ctrl = 1
    # construct controller
    if args.controller == "MPPI":
        controller = get_MPPI_controller(env,
                                         signal_interpreter,
                                         points_per_ctrl=points_per_ctrl,
                                         num_agents=1,
                                         noise_sigma=args.noise_sigma,
                                         lambda_=args.mppi_lambda,
                                         num_samples=args.num_samples,
                                         horizon=args.horizon,
                                         periodic_gaits=args.periodic_gaits,
                                         history_noise_sigma=args.history_noise_sigma)
    else:
        if args.u_cost_type == "quadratic":
            u_reward = lambda u: args.u_reward_w * quadratic_reward(u)
        else:
            u_reward = lambda u: args.u_reward_w * value_limiting_reward(u, alpha=args.u_reward_limiting_alpha)
        controller = get_siLQG_controller(env,
                                          signal_interpreter,
                                          u_reward,
                                          points_per_ctrl=points_per_ctrl,
                                          num_agents=1,
                                          horizon=args.horizon,
                                          du=args.du,
                                          u_init_scale=0.5 * args.p_stretch,
                                          step_max_tries=args.step_max_tries,
                                          min_step=args.min_step,
                                          line_search_const=args.line_search_const,
                                          random_init_U=args.random_init_U,
                                          verbosity=args.siLQG_verbosity
                                          )
    # construct ctrl_sys
    if args.ctrl_sys_reward_type == "v_quadratic":
        ctrl_sys_reward = v_quadratic_reward(env.target_dir * args.target_v)
    elif args.ctrl_sys_reward_type == "x_telescoping":
        ctrl_sys_reward = x_telescoping_reward(env.target_dir, args.target_t)
    else:
        ctrl_sys_reward = x_linear_reward(env.target_dir)
    ctrl_sys = ControlSystem(env,
                             controller,
                             signal_interpreter,
                             ctrl_sys_reward,
                             state_save_interval=int(1.0 / args.sim_dt / args.fps),
                             )
    return ctrl_sys


def optimize(args, ctrl_reduced=False, design=None, seed=None,
             data_save_dir='./',
             render=True,
             camera_location=[0, 0, 5],
             camera_look_at=[0, 0, 0],
             camera_rotate=0,
             render_quality=4):
    if not os.path.exists(data_save_dir):
        os.makedirs(data_save_dir)
    np.random.seed(seed)
    th.random.manual_seed(seed)
    control_system = get_control_system(args,
                                        ctrl_reduced,
                                        design)
    n_steps = int(args.tot_time / args.ctrl_dt)
    state = control_system.env.init_pos
    tot_reward = 0.0
    for i in range(n_steps):
        state, reward = control_system.step(state)
        tot_reward += reward
        print('Optimization of the {} decision step finished. Step_reward is {}'.format(i, reward[0]))
        print('Accumulate reward is {}'.format(tot_reward))
    print('The Episode reward is {}'.format(tot_reward))
    save_frames(actions=control_system.ctrl_signal_buffer, states=control_system.state_buffer, save_dir=data_save_dir)
    """
      from ControlSystem import load_frames
    _, state = load_frames(control_system.env.world, "../../../omtg_mmdoc_data/tripod_walk_siLQG_min_step_max_0.1/")
    """
    if render:
        povray_render(control_system.env,
                      control_system.state_buffer,
                      data_save_dir,
                      ['LowControl'],
                      [[0.8, 0.8, 0.8]],
                      [[0, 0, 0]],
                      control_system.env.terrain_name,
                      level=control_system.env.terrain[0],
                      fps=args.fps,
                      camera_location=camera_location,
                      camera_look_at=camera_look_at,
                      camera_rotate=camera_rotate,
                      quality=render_quality)


def get_basis(basis_num, ctrl_dim, naive=True):
    if naive:
        return get_naive_design(basis_num, ctrl_dim)
    else:
        bases_path = "../../../omtg_mmdoc_data/walker_mppi_boca_5s/GP_train_x_4326510.npy"
        ys_path = "../../../omtg_mmdoc_data/walker_mppi_boca_5s/GP_train_targets_4326510.npy"
        ctrl_bases = np.load(bases_path)
        ys = np.load(ys_path)
        n_init_points = 5
        ys = ys[n_init_points:]
        idx = np.argmax(ys)
        basis = ctrl_bases[n_init_points:][idx]
        print(np.argmax(ys))
        print(np.max(ys))
        assert basis.shape[0] == basis_num and basis.shape[1] == ctrl_dim
        return basis


if __name__ == "__main__":
    parser = _GenerateArgsParser('Lower')
    opt_args = parser.parse_args()
    save_dir = '../../../omtg_mmdoc_data/' + opt_args.data_file + '/'
    ctrl_reduced_flag = True
    naive_basis_flag = True
    ctrl_basis = get_basis(opt_args.basis_num, opt_args.ctrl_dim, naive=naive_basis_flag)
    opt_seed = 163244  # int(time.time())
    optimize(opt_args,
             ctrl_reduced_flag,
             ctrl_basis,
             opt_seed,
             save_dir,
             render=True,
             camera_location=[5, -8.5, 4.25],
             camera_look_at=[5, 0, 0],
             camera_rotate=0,
             render_quality=4)
    """
    tripod_stand(with camera_rotate=45):
    camera_location=[5, -8, 4.25],
    camera_look_at=[5, -3.5, 1.5],
    tripod_lying:
    camera_location=[3, -5.5, 4.25],
    camera_look_at=[3, 0, 0],
    walker_far:
    camera_location=[5, -8.5, 4.25],
    camera_look_at=[5, 0, 0],
    beam:
    camera_location=[4, 0, 8],
    camera_look_at=[4, 0, 0],
    initial_test:
    camera_location=[5, 0, 2],
    camera_look_at=[0, 0, 0],
    """