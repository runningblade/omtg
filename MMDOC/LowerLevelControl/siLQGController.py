import numpy as np
from _Utils import \
    (transpose_last_2_dims,
     transpose_first_dim_to_last,
     transpose_first_2_dims,
     NumericalWarning,
     NumericalError)
from typing import Callable, Tuple, Any, Union
import warnings
import autograd
from scipy.linalg import cholesky, cho_solve, cho_factor, LinAlgError


class siLQGLineSearchWarning(NumericalWarning):
    pass


def gauss_newton_hessian(J, metric=None):
    """
    Gauss-Newton Hessian approximation.
    J: gradient of shape (m, n). m is dimension of quadratic reward, and n is dimension of domain.
    Return J^TJ as an approximation of Hessian matrix.
    """
    J_T = transpose_last_2_dims(J)
    if metric is None:
        H = J_T @ J
    else:
        H = J_T @ metric @ J
    return H


def line_search(alpha: Union[float, np.ndarray],
                update_alpha: Callable[[Union[float, np.ndarray], Any], float],
                max_tries: int,
                judge: Callable[[Union[float, np.ndarray], Any], Tuple[bool, Any]],
                warning_message="",
                warning_class=NumericalWarning,
                verbosity=1):
    additional_info = None
    for i in range(max_tries):
        flag, additional_info = judge(alpha, additional_info)
        if flag:
            return alpha, flag, additional_info
        alpha = update_alpha(alpha, additional_info)
    if verbosity:
        warnings.warn(f"Line search fails after trying {max_tries}. " + warning_message, warning_class)
    return alpha, False, additional_info


def spd_gauss_newton_hessian(J,
                             metric=None,
                             min_metric_jitter=1e-6,
                             update_coef=2,
                             max_tries=10,
                             hessian_jitter=1e-12,
                             verbosity=1):
    """
    When metric M is positive definite, J^TMJ must be positive semi-definite,
    and J^TMJ + jitter_hessian*I must be positive definite
    """
    J_T = transpose_last_2_dims(J)
    if metric is None:
        metric = np.identity(J_T.shape[-1])
    if not np.allclose(transpose_last_2_dims(metric), metric):
        raise NumericalError("Metric should be symmetric.")

    def _judge_spd(_jitter, _=None):
        _metric = metric + _jitter * np.identity(metric.shape[-1])
        try:
            cholesky(a=_metric, lower=True, overwrite_a=True)
        except LinAlgError:
            return False, _
        return True, _

    hessian_jitter = hessian_jitter * np.identity(J.shape[-1])
    H = J_T @ metric @ J + hessian_jitter
    error_message = "The metric is too singular to be positive definite."
    spd_success, _ = _judge_spd(0)
    if spd_success:
        return H
    metric_jitter, spd_success, _ = line_search(min_metric_jitter,
                                                lambda alpha, _: alpha * update_coef,
                                                max_tries,
                                                _judge_spd,
                                                warning_message=error_message,
                                                verbosity=verbosity)
    if not spd_success:
        raise NumericalError(error_message)
    H = J_T @ (metric + metric_jitter * np.identity(metric.shape[-1])) @ J + hessian_jitter
    return H


class siLQG:
    """
    This class implements a simplified iLQG controller.
    The algorithm is inspired by a version of iLQG(iterative linear-quadratic gaussian,
    https://homes.cs.washington.edu/~todorov/papers/TassaIROS12.pdf) controller.
    Instead of building a second-order model of value function w.r.t. states x and control signals u,
    we build a second-order model of cumulative reward w.r.t. control signals u only in a given horizon.
    Therefore, the Gauss-Newton step we take w.r.t. control signal u does not contain
    a feedback gain term related with states x, and unlike iLQG where the derivatives of value function
    are constructed in a backward pass, all derivatives we need in siLQG can be constructed
    together with forward simulation.
    """

    def __init__(self,
                 diff_dynamics: Callable[[np.ndarray, np.ndarray], np.ndarray],
                 line_search_dynamics: Callable[[np.ndarray, np.ndarray, np.ndarray, np.ndarray], None],
                 nu,
                 no,
                 horizon,
                 obs_cost: Callable[[np.ndarray], np.ndarray],
                 u_cost: Callable[[np.ndarray], np.ndarray],
                 grad_obs_cost: Callable[[np.ndarray], np.ndarray] = None,
                 hessian_obs_cost: Callable[[np.ndarray], np.ndarray] = None,
                 grad_u_cost: Callable[[np.ndarray], np.ndarray] = None,
                 hessian_u_cost: Callable[[np.ndarray], np.ndarray] = None,
                 states_to_obss: Callable[[np.ndarray], np.ndarray] = lambda x: x,
                 num_agents=1,
                 U_init=None,
                 u_init=None,
                 random_init_U=False,
                 du=1e-4,
                 tau_reg_update_coef=2,
                 tau_reg_max_tries=10,
                 min_tau_reg_coef=1e-10,
                 step_update_coef=0.5,
                 step_max_tries=4,
                 min_step=0.00097656,
                 line_search_const=1e-2,
                 verbosity=0,
                 ):
        """
        Note that siLQG is capable of controlling multiple agents.
        siLQG assumes there is no correlation among different dimensions of the control signal u.
        states_to_obs: here we assume obs are part of states, rather than more complex relationship
        """
        self.diff_dynamics = diff_dynamics
        self.line_search_dynamics = line_search_dynamics
        self.obs_cost = obs_cost
        self.u_cost = u_cost
        self.no = no
        self.nu = nu
        self.na = num_agents if num_agents > 1 else 1
        if grad_obs_cost is None:
            def grad_obs_cost(obs):
                """
                obs_cost is a function from (..., no) to (...,),
                thus the output of autograd.jacobian(obs_cost) is of size (..., ..., no),
                 and the second (...) dimensions are redundant.
                """
                n_redundant_dim = obs.ndim - 1
                redundant_dims = tuple(range(n_redundant_dim, 2 * n_redundant_dim))
                return autograd.jacobian(self.obs_cost)(obs).sum(axis=redundant_dims)
        self.dc_dTau = grad_obs_cost
        if hessian_obs_cost is None:
            def hessian_obs_cost(obs):
                """
                grad_obs_cost is a function from (..., no) to (..., no),
                thus the output of autograd.jacobian(grad_obs_cost) is of size (..., no, ..., no),
                anb the second (...) dimensions are redundant.
                """
                n_redundant_dim = obs.ndim - 1
                redundant_dims = tuple(range(n_redundant_dim + 1, 2 * n_redundant_dim + 1))
                return autograd.jacobian(self.dc_dTau)(obs).sum(axis=redundant_dims)
        self.d2c_dTau2 = hessian_obs_cost
        if grad_u_cost is None:
            def grad_u_cost(u):
                """
                u_cost is a function from (..., nu) to (...,),
                thus the output of autograd.jacobian(u_cost) is of size (..., ..., nu),
                and the second (...) dimensions are redundant.
                """
                n_redundant_dim = u.ndim - 1
                redundant_dims = tuple(range(n_redundant_dim, 2 * n_redundant_dim))
                return autograd.jacobian(self.u_cost)(u).sum(axis=redundant_dims)
        self.pc_pU = grad_u_cost
        if hessian_u_cost is None:
            def hessian_u_cost(u):
                """
                grad_u_cost is a function from (..., nu) to (..., nu),
                thus the output of autograd.jacobian(grad_obs_cost) is of size (..., nu, ..., nu),
                anb the second (... )dimensions are redundant.
                """
                n_redundant_dim = u.ndim - 1
                redundant_dims = tuple(range(n_redundant_dim + 1, 2 * n_redundant_dim + 1))
                return autograd.jacobian(self.pc_pU)(u).sum(axis=redundant_dims)
        self.p2c_pU2 = hessian_u_cost
        self.X_to_Tau = states_to_obss
        self.horizon = horizon
        self.width_dtau_du = 0
        self.du = du
        if u_init is None:
            u_init = np.zeros(self.nu) if num_agents == 1 else np.zeros((num_agents, self.nu))
        self.u_init = u_init
        self.random_init_U = random_init_U
        # U_init is used to reproduce a certain state of the controller
        if U_init is None:
            self.reset()
        else:
            self.U = U_init
        self.min_tau_reg_coef = min_tau_reg_coef
        self.tau_reg_update_coef = tau_reg_update_coef
        self.tau_reg_max_tries = tau_reg_max_tries
        self.step_update_coef = step_update_coef
        self.step_max_tries = step_max_tries
        self.min_step = min_step
        self.line_search_const = line_search_const
        self.verbosity = verbosity
        if self.verbosity:
            warnings.simplefilter("always", siLQGLineSearchWarning)

    def cost(self, Tau, U):
        return self.obs_cost(Tau) + self.u_cost(U)

    def __call__(self, *args, **kwargs):
        self.command(*args, **kwargs)

    @property
    def num_parallel(self):
        return self.width_dtau_du + 1

    @property
    def num_agents(self):
        return self.na

    def dtau_du(self, x, u, x_disturbed=None):
        # x is of shape (nu+1, na, nx) when na > 1
        x = np.tile(x, (self.nu + 1,) + (1,) * x.ndim)
        # x is now of shape (num_parallel', ...), num_parallel' = (t+1) * nu + 1, t from the main loop of dTau_dU()
        x = np.concatenate((x_disturbed, x), axis=0) if x_disturbed is not None else x
        u = np.tile(u, (self.num_parallel,) + (1,) * u.ndim)
        du = self.du * np.identity(self.nu)
        if self.na > 1:
            du = transpose_first_2_dims(
                np.tile(du,
                        (self.na, 1, 1))
            )
        u[-self.nu - 1:-1] = u[-self.nu - 1:-1] + du
        # In this format, x = (x_disturbed, x_to_be_diff, x_orig)
        # len(x_to_be_diff) == nu and len(x_orig) == 1
        x = self.diff_dynamics(x, u)
        x_orig = x[-1]
        x_disturbed = x[:-1]
        dx = x_disturbed - x_orig
        dtau = self.X_to_Tau(dx)
        # dtau_du is of shape (na, no, num_parallel')
        dtau_du = transpose_first_dim_to_last(dtau / self.du)
        return dtau_du, x_orig, x_disturbed

    def dTau_dU(self, x):
        """
        x is the complete state for simulation
        """
        Tau = np.zeros((self.horizon, self.no))
        x_disturbed = None
        # dTau_dU is a lower zig-zag matrix
        dTau_dU = np.zeros((self.horizon * self.no, self.horizon * self.nu))
        if self.na > 1:
            Tau = np.tile(Tau, (self.na, 1, 1))
            dTau_dU = np.tile(dTau_dU, (self.na, 1, 1))
        for t in range(self.horizon):
            # dtau_du is of shape (na, no, (t+1) * nu)
            self.width_dtau_du = self.width_dtau_du + self.nu
            dtau_du, x, x_disturbed = self.dtau_du(x, self.U[t], x_disturbed)
            Tau[..., t, :] = self.X_to_Tau(x)
            dTau_dU[..., t * self.no:(t + 1) * self.no, :self.width_dtau_du] = dtau_du
        self.width_dtau_du = 0
        return dTau_dU, Tau

    def gauss_newton_step(self, x):
        # dTau_dU is a lower zig-zag matrix of (na, horizon * no, horizon * nu) when horizon > 1
        # Tau is of shape (na, horizon, no)
        dTau_dU, Tau = self.dTau_dU(x)
        if self.na == 1:
            dc_dU = self.pc_pU(self.U).reshape((self.horizon * self.nu, 1))

            dc_dU += transpose_last_2_dims(dTau_dU) @ (self.dc_dTau(Tau).reshape((self.horizon * self.no, 1
                                                                                  )))

            d2c_dTau2 = np.zeros((self.horizon * self.no, self.horizon * self.no))
            d2c_dU2 = np.zeros((self.horizon * self.nu, self.horizon * self.nu))
            raw_d2c_dTau2 = self.d2c_dTau2(Tau)
            raw_d2c_dU2 = self.p2c_pU2(self.U)
            for t in range(self.horizon):
                d2c_dTau2[self.no * t:self.no * (t + 1), self.no * t:self.no * (t + 1)] = raw_d2c_dTau2[t, :, :]
                d2c_dU2[self.nu * t: self.nu * (t + 1), self.nu * t:self.nu * (t + 1)] = raw_d2c_dU2[t, :, :]

            d2c_dU2 += spd_gauss_newton_hessian(J=dTau_dU,
                                                metric=d2c_dTau2,
                                                min_metric_jitter=self.min_tau_reg_coef,
                                                update_coef=self.tau_reg_update_coef,
                                                max_tries=self.tau_reg_max_tries,
                                                verbosity=self.verbosity
                                                )

            # delta_U is of shape (na, horizon * nu, horizon * nu) x  (na, horizon * nu, 1)
            # = (na, horizon*nu, 1)
            delta_U = cho_solve(c_and_lower=cho_factor(d2c_dU2),
                                b=-dc_dU,
                                overwrite_b=True)
        else:
            # dc_dU is of shape  (na, horizon*nu, horizon * no) x (na, horizon * no, 1)
            # = (na, horizon  * nu, 1)
            dc_dU = self.pc_pU(transpose_first_2_dims(self.U)).reshape((self.na, self.horizon * self.nu, 1))

            dc_dU += transpose_last_2_dims(dTau_dU) @ (self.dc_dTau(Tau).reshape((self.na, self.horizon * self.no, 1
                                                                                  )))

            # d2c_dU2 is a dense spd matrix of shape
            # (na, horizon * nu, horizon * no) x (na, horizon*no, horizon*no)
            # x (na, horizon * no, horizon * nu)
            # = (na, horizon * nu, horizon * nu)
            d2c_dTau2 = np.zeros((self.na, self.horizon * self.no, self.horizon * self.no))
            d2c_dU2 = np.zeros((self.na, self.horizon * self.nu, self.horizon * self.nu))
            raw_d2c_dTau2 = self.d2c_dTau2(Tau)
            raw_d2c_dU2 = self.p2c_pU2(transpose_first_2_dims(self.U))
            for t in range(self.horizon):
                d2c_dTau2[:, self.no * t:self.no * (t + 1), self.no * t:self.no * (t + 1)] = raw_d2c_dTau2[:, t, :, :]
                d2c_dU2[:, self.nu * t: self.nu * (t + 1), self.nu * t:self.nu * (t + 1)] = raw_d2c_dU2[:, t, :, :]
            delta_U = np.zeros((self.na, self.horizon * self.nu, 1))

            for i in range(self.na):
                d2c_dU2[i] += spd_gauss_newton_hessian(J=dTau_dU[i],
                                                       metric=d2c_dTau2[i],
                                                       min_metric_jitter=self.min_tau_reg_coef,
                                                       update_coef=self.tau_reg_update_coef,
                                                       max_tries=self.tau_reg_max_tries,
                                                       verbosity=self.verbosity
                                                       )
                # delta_U is of shape (na, horizon * nu, horizon * nu) x  (na, horizon * nu, 1)
                # = (na, horizon*nu, 1)
                delta_U[i] = cho_solve(c_and_lower=cho_factor(d2c_dU2[i]),
                                       b=-dc_dU[i],
                                       overwrite_b=True)

        return delta_U, dc_dU, d2c_dU2, Tau

    def line_search_and_update(self, x, cost, delta_U, J):
        # line search
        # linear_term is of shape (na, 1, 1)
        linear_term = transpose_last_2_dims(delta_U) @ J
        _U = np.copy(self.U)
        _X = np.zeros((self.horizon,) + x.shape)

        def _is_cost_reduced(_alpha, _armijo_cond):
            for _t in range(self.horizon):
                _U[_t] = self.U[_t] + (_alpha * delta_U)[..., _t * self.nu:(_t + 1) * self.nu, 0]
            _mask = np.logical_not(_armijo_cond) if _armijo_cond is not None else None
            self.line_search_dynamics(x, _U, _X, _mask)
            _Tau = self.X_to_Tau(_X)
            _cost = self.cost(_Tau, _U).sum(axis=0)
            # expected_delta cost is of same shape as armijo_cond, which is (na, )
            expected_delta_cost = (_alpha * linear_term)[..., 0, 0]
            _armijo_cond = (_cost - cost < self.line_search_const * expected_delta_cost).reshape(self.na, )
            return _armijo_cond.all(), _armijo_cond

        def _update_alpha(_alpha, _armijo_cond):
            _alpha[_armijo_cond == False] *= self.step_update_coef
            return _alpha

        alpha, all_reduced, armijo_cond = line_search(
            alpha=np.ones((1, 1)) if self.na == 1 else np.ones((self.na, 1, 1)),
            update_alpha=_update_alpha,
            max_tries=self.step_max_tries,
            judge=_is_cost_reduced,
            warning_class=siLQGLineSearchWarning,
            warning_message=f"siLQG: The search direction of is invalid.",
            verbosity=self.verbosity)
        if not all_reduced:
            alpha[armijo_cond == False] = np.minimum(alpha[armijo_cond == False], self.min_step)
        for t in range(self.horizon):
            self.U[t] += (alpha * delta_U)[..., t * self.nu:(t + 1) * self.nu, 0]

    def command(self, x):
        """
        Input x: of shape (nx, ) when na == 1 and (na, nx) when na > 1
        Output u: of shape (nu, ) when na == 1 and (na, nu) when na > 1
        """
        self.U = np.roll(self.U, shift=-1, axis=0)
        self.U[-1] = self.u_init
        delta_U, dc_dU, d2c_dU2, Tau = self.gauss_newton_step(x)
        # Tau is of shape (na, horizon, no) while self.U is of shape (horizon, na, nu)
        if self.na > 1:
            Tau = transpose_first_2_dims(Tau)
        cost = self.cost(Tau, self.U).sum(axis=0)
        self.line_search_and_update(x, cost, delta_U, dc_dU)
        u = self.U[0]
        return u

    def reset(self):
        if self.random_init_U:
            self.U = np.random.normal(loc=self.u_init, scale=1e-3 * self.u_init,
                                      size=(self.horizon,) + self.u_init.shape)
        else:
            self.U = np.tile(self.u_init, (self.horizon,) + (1,) * self.u_init.ndim)

    def switch_num_agents(self, num_agents, truncate_rear=True):
        """
        TODO: Make the branch when orig_num_agents < num_agents robust(Currently, there's no need)
        """
        if num_agents < 1:
            num_agents = 1
        if num_agents == self.na:
            return
        orig_num_agents = self.na
        self.na = num_agents
        if orig_num_agents == 1:
            self.u_init = np.tile(self.u_init, (self.na, 1))
            self.U = transpose_first_2_dims(np.tile(self.U, (self.na, 1, 1)))
        elif self.na == 1:
            self.u_init = self.u_init[0]
            self.U = self.U[:, 0]
        elif orig_num_agents < self.na:
            reps = self.na - orig_num_agents
            u_init_added = np.repeat(self.u_init[-1:], reps, axis=0)
            self.u_init = np.concatenate((self.u_init, u_init_added), axis=0)
            U_added = np.repeat(self.U[:, -1:], reps, axis=1)
            self.U = np.concatenate((self.U, U_added), axis=1)
        elif truncate_rear:
            self.u_init = self.u_init[:num_agents]
            self.U = self.U[:, :num_agents]
        else:
            self.u_init = self.u_init[-num_agents:]
            self.U = self.U[:, -num_agents:]


def test_spd_gauss_newton_hessian(n_rand_test=10, std=0.3):
    """
    When std is relatively large (close to 1),  spd_gauss_newton_hessian are prone to failure,
    otherwise, when std is relatively small (close to 0) spd_gauss_newton_hessian are prone to success.
    """
    J = np.array([[9.2, 2.9, 6.4], [2.5, 0.7, 0], [1.4, 0, 0]])
    H, L = spd_gauss_newton_hessian(J)
    print(np.allclose(H, L @ L.T))
    M = np.zeros((3, 3))
    H, L = spd_gauss_newton_hessian(J, M)
    print(np.allclose(H, L @ L.T))
    M = np.identity(2)
    for i in range(n_rand_test):
        J = np.random.normal(size=(2, 3))
        M[0][1] = np.random.normal(scale=std)
        M[1][0] = M[0][1]
        H, L = spd_gauss_newton_hessian(J, M, max_tries=10)
        print(np.allclose(H, L @ L.T))


if __name__ == "__main__":
    test_spd_gauss_newton_hessian(std=1.0)
