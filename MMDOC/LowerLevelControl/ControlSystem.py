import numpy as np
from LowerLevelControl.MPPIController import MultiAgentMPPI, PeriodicMPPI
from LowerLevelControl.siLQGController import siLQG
from SoftRobotEnv.ReducedSoftRobotUtils import serialize_state, deserialize_state
from LowerLevelControl.SignalInterpreter import SquareWaveGenerator, BezierCurveGenerator, CatmullRomSplineGenerator
import torch as th
import pickle
from typing import Iterable, Callable, Any, Tuple
from abc import ABC, abstractmethod
from SoftRobotEnv.Reward import quadratic_reward
import pyPhysicsMotion as pm


def save_frames(actions, states, save_dir='./', suffix=''):
    # handle special case: single state or empty state
    if not isinstance(states, Iterable):
        states = [states]
    states_X = [serialize_state(state) for state in states]
    path = save_dir + 'trajectory_' + suffix + '.pickle'
    with open(path, 'wb') as f:
        pickle.dump((actions, states_X), f)


def load_frames(world, load_dir='./', suffix=''):
    path = load_dir + 'trajectory_' + suffix + '.pickle'
    with open(path, 'rb') as f:
        actions, states_X = pickle.load(f)
    states = [deserialize_state(world, state) for state in states_X]
    return actions, states


class ControlSystem:
    def __init__(self,
                 env,
                 controller,
                 signal_interpreter,
                 reward,
                 state_save_interval=1,
                 ):
        self.env = env
        self.controller = controller
        self.signal_interpreter = signal_interpreter
        self.state_buffer = []
        self.ctrl_signal_buffer = []
        self.state_save_interval = state_save_interval
        self.steps_from_saving_state = 0
        self.reward = reward
        # n_debug_vals = 5
        # self.debug_vals = np.zeros(n_debug_vals)

    def step(self, state):
        ctrl_signal = self.controller(state)
        self.ctrl_signal_buffer.append(ctrl_signal)
        actions = self.signal_interpreter(ctrl_signal)
        for action in actions:
            state = self.env(action, state)
            if self.steps_from_saving_state == 0:
                self.state_buffer.append(state)
                self.steps_from_saving_state = self.state_save_interval
            self.steps_from_saving_state -= 1
        reward_val = self.reward(state, ctrl_signal)
        return state, reward_val

    def reset(self, num_agents=1):
        self.state_buffer = []
        self.ctrl_signal_buffer = []
        self.steps_from_saving_state = 0
        self.reward.switch_num_agents(num_agents)
        self.reward.reset()


# The following "get functions" are a set of helper functions to assemble a system controller
def get_dynamics(env_step,
                 preprocess: Callable[[np.ndarray, np.ndarray], Tuple[Iterable, Iterable, Any]],
                 postprocess: Callable[[Any, Any], np.ndarray]):
    def dynamics(state, ctrl_signal):
        state, actions, info = preprocess(state, ctrl_signal)
        # advance simulate
        for action in actions:
            state = env_step(action, state)
        # serialize states
        state = postprocess(state, info)
        return state

    return dynamics


def get_parallel_dynamics_preprocess(env,
                                     parallel_signal_interpreter):
    def parallel_dynamics_preprocess(states, ctrl_signals):
        orig_states_shape = states.shape
        states = states.reshape(-1, env.state_dim)
        # interpolate control signals to actions of certain wave pattern
        actionss = parallel_signal_interpreter(ctrl_signals)
        for i in range(len(actionss)):
            actionss[i] = actionss[i].reshape(-1, env.ctrl_dim)
            # deserialize states
        states = np.array([deserialize_state(env.world, s_state) for s_state in states])
        return states, actionss, orig_states_shape

    return parallel_dynamics_preprocess


def serialize_states_with_orig_shape(states, orig_states_shape):
    # serialize states
    states = np.array([serialize_state(d_state) for d_state in states]).reshape(orig_states_shape)
    return states


def get_cost(env):
    def cost(states, ctrl_signals=None):
        states_batch_shape = states.shape[:-1]
        states = states.reshape(-1, env.state_dim)
        states = np.array([deserialize_state(env.world, s_state) for s_state in states])
        obss = env.get_obs(states).reshape(states_batch_shape + (-1,))
        return -env.reward(obss)

    return cost


def get_system_controller_type(controller_type,
                               env,
                               signal_interpreter,
                               parallel_signal_interpreter,
                               ):
    class SystemController(controller_type):
        def __call__(self, state):
            num_agents_uncopied = env.num_agents
            parallel_signal_interpreter.tile(signal_interpreter, (self.num_parallel,))
            state = serialize_state(state) if num_agents_uncopied == 1 else np.array(
                [serialize_state(d_state) for d_state in state])
            env.copy_agents(self.num_parallel)
            ctrl_signal = self.command(state)
            if isinstance(ctrl_signal, th.Tensor):
                ctrl_signal = ctrl_signal.detach().numpy()
            env.truncate_agents(env.num_agents - num_agents_uncopied)
            env.agents_mask = None
            return ctrl_signal

    return SystemController


def get_MPPI_controller(env,
                        signal_interpreter,
                        num_samples,
                        horizon,
                        noise_sigma,
                        points_per_ctrl=1,
                        num_agents=1,
                        lambda_=1.0,
                        periodic_gaits=False,
                        history_noise_sigma=0.01
                        ):
    nu = points_per_ctrl * env.ctrl_dim
    u_min = np.array([-1.0] * nu)
    u_max = np.array([1.0] * nu)
    noise_covar = th.tensor(noise_sigma * noise_sigma * np.identity(nu))
    parallel_signal_interpreter = signal_interpreter.copy()
    dynamics_preprocess = get_parallel_dynamics_preprocess(env, parallel_signal_interpreter)
    dynamics = get_dynamics(env.step_multiple,
                            preprocess=dynamics_preprocess,
                            postprocess=serialize_states_with_orig_shape)
    running_cost = get_cost(env)
    if not periodic_gaits:
        SystemController = get_system_controller_type(MultiAgentMPPI,
                                                      env,
                                                      signal_interpreter,
                                                      parallel_signal_interpreter,
                                                      )
        controller = SystemController(num_agents=num_agents,
                                      noise_abs_cost=True,
                                      dynamics=dynamics,
                                      running_cost=running_cost,
                                      nx=env.state_dim,
                                      noise_sigma=noise_covar,
                                      num_samples=num_samples,
                                      horizon=horizon,
                                      lambda_=lambda_,
                                      u_min=th.tensor(u_min, dtype=th.double, device='cpu'),
                                      u_max=th.tensor(u_max, dtype=th.double, device='cpu'))
    else:
        history_noise_covar = th.tensor(history_noise_sigma * history_noise_sigma * np.identity(nu))
        SystemController = get_system_controller_type(PeriodicMPPI,
                                                      env,
                                                      signal_interpreter,
                                                      parallel_signal_interpreter,
                                                      )
        controller = SystemController(num_agents=num_agents,
                                      noise_abs_cost=True,
                                      dynamics=dynamics,
                                      running_cost=running_cost,
                                      nx=env.state_dim,
                                      warm_start_noise_sigma=noise_covar,
                                      history_noise_sigma=history_noise_covar,
                                      num_samples=num_samples,
                                      horizon=horizon,
                                      lambda_=lambda_,
                                      u_min=th.tensor(u_min, dtype=th.double, device='cpu'),
                                      u_max=th.tensor(u_max, dtype=th.double, device='cpu'),
                                      action_cost_scale=0.1)
    return controller


def get_siLQG_controller(env,
                         signal_interpreter,
                         u_reward,
                         horizon,
                         num_agents=1,
                         points_per_ctrl=1,
                         u_init_scale=0.2,
                         du=1e-1,
                         step_max_tries=4,
                         min_step=0.00097656,
                         line_search_const=1e-2,
                         random_init_U=False,
                         verbosity=0
                         ):
    nu = points_per_ctrl * env.ctrl_dim
    signal_interpreter.scale_in = False
    parallel_signal_interpreter = signal_interpreter.copy()
    SystemController = get_system_controller_type(siLQG,
                                                  env,
                                                  signal_interpreter,
                                                  parallel_signal_interpreter)
    parallel_dynamics_preprocess = get_parallel_dynamics_preprocess(env,
                                                                    parallel_signal_interpreter)

    def diff_dynamics_preprocess(states, ctrl_signals):
        parallel_signal_interpreter.repeat_last(nu + 1)
        siLQG_num_agents = ctrl_signals.shape[1] if ctrl_signals.ndim > 2 else 1
        env.repeat_last_agents(nu + 1, siLQG_num_agents)
        return parallel_dynamics_preprocess(states, ctrl_signals)

    diff_dynamics = get_dynamics(env.step_multiple,
                                 preprocess=diff_dynamics_preprocess,
                                 postprocess=serialize_states_with_orig_shape
                                 )
    line_search_signal_interpreter = signal_interpreter.copy()
    line_search_dynamics_preprocess = get_parallel_dynamics_preprocess(env,
                                                                       line_search_signal_interpreter)

    one_step_line_search_dynamics = get_dynamics(env.step_multiple,
                                                 preprocess=line_search_dynamics_preprocess,
                                                 postprocess=serialize_states_with_orig_shape)

    def line_search_dynamics(x, U, res, mask):
        siLQG_num_agents = U.shape[1] if U.ndim > 2 else 1
        line_search_signal_interpreter.assign_points(signal_interpreter)
        env.truncate_agents(env.num_agents - siLQG_num_agents)
        env.agents_mask = mask
        if siLQG_num_agents == 1 or mask is None:
            for i in range(len(U)):
                x = one_step_line_search_dynamics(x, U[i])
                res[i] = x
        else:
            for i in range(len(U)):
                x = one_step_line_search_dynamics(x, U[i])
                res[i, mask == True] = x[mask == True]

    def states_to_obss(X):
        """
        X is of shape (..., nx)
        """
        orig_X_shape = X.shape
        X = X.reshape(-1, env.state_dim)
        Tau = np.zeros((X.shape[0], env.obs_dim))
        for i in range(Tau.shape[0]):
            deserialized_x = deserialize_state(env.world, X[i])
            Tau[i] = env.get_obs(deserialized_x)
        Tau = Tau.reshape(orig_X_shape[:-1] + (env.obs_dim,))
        return Tau

    u_init = u_init_scale * np.ones(nu) if num_agents == 1 else u_init_scale * np.ones((num_agents, nu))

    controller = SystemController(diff_dynamics=diff_dynamics,
                                  line_search_dynamics=line_search_dynamics,
                                  states_to_obss=states_to_obss,
                                  num_agents=num_agents,
                                  obs_cost=lambda obs: -env.reward(obs),
                                  u_cost=lambda u: -u_reward(u),
                                  nu=nu,
                                  u_init=u_init,
                                  no=env.obs_dim,
                                  horizon=horizon,
                                  du=du,
                                  step_max_tries=step_max_tries,
                                  min_step=min_step,
                                  line_search_const=line_search_const,
                                  random_init_U=random_init_U,
                                  verbosity=verbosity
                                  )
    return controller


class ctrl_sys_reward(ABC):
    @abstractmethod
    def __call__(self, s, u):
        pass

    def reset(self):
        pass

    def switch_num_agents(self, num_agents, truncate_rear=True):
        pass


class v_quadratic_reward(ctrl_sys_reward):
    def __init__(self, target):
        self.target = target

    def __call__(self, s, u):
        if isinstance(s, pm.FEMGradientInfoFLOAT):
            v = s.getV().reshape(1, 3)
        else:
            v = np.zeros((len(s), 3))
            for i in range(len(s)):
                v[i] = s[i].getV().reshape(-1, )
        reward = quadratic_reward(x=v, x_target=self.target)
        return reward


class x_telescoping_reward(ctrl_sys_reward):
    def __init__(self, target_dir, target_scale, ):
        self.target = target_scale
        self.target_dir = target_dir
        self.x_prev = np.zeros((1, 1))

    def __call__(self, s, u):
        if isinstance(s, pm.FEMGradientInfoFLOAT):
            x = s.getT().reshape(1, 3)
        else:
            x = np.zeros((len(s), 3))
            for i in range(len(s)):
                x[i] = s[i].getT().reshape(-1, )
        x = np.expand_dims(np.dot(x, self.target_dir), axis=-1)
        reward = quadratic_reward(x, self.target) - quadratic_reward(self.x_prev, self.target)
        self.x_prev = x
        return reward

    def switch_num_agents(self, num_agents, truncate_rear=True):
        cur_n = self.x_prev.shape[0]
        if cur_n == num_agents:
            return
        elif cur_n < num_agents:
            x_added = np.zeros((num_agents - cur_n, 1))
            self.x_prev = np.concatenate((self.x_prev, x_added), axis=0)
        elif truncate_rear:
            self.x_prev = self.x_prev[:num_agents]
        else:
            self.x_prev = self.x_prev[-num_agents:]

    def reset(self):
        self.x_prev = np.zeros_like(self.x_prev)


class x_linear_reward(ctrl_sys_reward):
    def __init__(self, target_dir):
        self.x_prev = np.zeros(1)
        self.target_dir = target_dir

    def __call__(self, s, u):
        if isinstance(s, pm.FEMGradientInfoFLOAT):
            x = s.getT().reshape(1, 3)
        else:
            x = np.zeros((len(s), 3))
            for i in range(len(s)):
                x[i] = s[i].getT().reshape(-1, )
        x = np.dot(x, self.target_dir)
        reward = x - self.x_prev
        self.x_prev = x
        return reward

    def switch_num_agents(self, num_agents, truncate_rear=True):
        cur_n = self.x_prev.shape[0]
        if cur_n == num_agents:
            return
        elif cur_n < num_agents:
            x_added = np.zeros(num_agents - cur_n)
            self.x_prev = np.concatenate((self.x_prev, x_added), axis=0)
        elif truncate_rear:
            self.x_prev = self.x_prev[:num_agents]
        else:
            self.x_prev = self.x_prev[-num_agents:]

    def reset(self):
        self.x_prev = np.zeros_like(self.x_prev)
