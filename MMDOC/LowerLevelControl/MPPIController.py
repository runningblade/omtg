import os
import sys
import time

import numpy as np
import torch

sys.path.append('..')
from pytorch_mppi import mppi
import math
import pickle
from torch.distributions.multivariate_normal import MultivariateNormal as MultivariateNormal


def _standardize(cost):
    cost_mean = torch.mean(cost, dim=0)
    cost_std = torch.std(cost, dim=0, unbiased=True)
    if (cost_std > 1e-7).all():
        cost = (cost - cost_mean) / cost_std
    else:
        print("MPPI Warning: std of cost is too small.")
    return cost


def _ensure_non_zero(cost, factor):
    beta = torch.min(cost, dim=0).values
    return torch.exp(-factor * (cost - beta))


class MultiAgentMPPI(mppi.MPPI):
    """
    This class extended pytorch-mppi: https://github.com/UM-ARM-Lab/pytorch_mppi
    so that 'parallel dynamics' can be applied.
    Suppose a dynamics F handles NA agents parallely, and MPPI generate K samples per agent.
    Then action's shape is (K, NA, nu) instead of (K, nu), and state's shape is (K, NA, nk) instead of (K, nk)
    The limitation of this extended mppi is that it can't handle stochastic dynamics.
    """

    def __init__(self, num_agents=1, action_cost_scale=1.0, **base_kwargs):
        super(MultiAgentMPPI, self).__init__(rollout_samples=1, rollout_var_cost=0, rollout_var_discount=0.95,
                                             **base_kwargs)
        self.NA = num_agents if num_agents > 1 else 1
        self.action_cost_scale = action_cost_scale
        if self.NA > 1:
            self.u_init = self.u_init.repeat(self.NA, 1)
            self.noise_mu = self.noise_mu.repeat(self.NA, 1)
            self.noise_dist = MultivariateNormal(self.noise_mu, self.noise_sigma)
            self.U = self.noise_dist.sample((self.T,))

    def reset(self):
        self.U = self.noise_dist.sample((self.T,))

    def _dynamics(self, state, u, t):
        state = self.F(state, u, t) if self.step_dependency else self.F(state, u)
        if not torch.is_tensor(state):
            state = torch.tensor(state)
        return state

    def _running_cost(self, state, u):
        cost = self.running_cost(state, u)
        if not torch.is_tensor(cost):
            cost = torch.tensor(cost)
        return cost

    def command(self, state):
        # shift command 1 time step
        self.U = torch.roll(self.U, -1, dims=0)
        self.U[-1] = self.u_init

        if not torch.is_tensor(state):
            state = torch.tensor(state)
        self.state = state.to(dtype=self.dtype, device=self.d)

        # compute weights
        cost_total = self._compute_total_cost_batch()  # shape = (num_samples,) or (num_samples, num_agents)
        self.cost_total_non_zero = _ensure_non_zero(cost_total, 1 / self.lambda_)
        eta = torch.sum(self.cost_total_non_zero, dim=0)
        self.omega = (1.0 / eta) * self.cost_total_non_zero  # shape = (num_samples,) or (num_samples, num_agents)
        # action = (weights * noise).sum()
        # noise[:,t] is of shape (num_samples, nu) or (num_samples, num_agents, nu)
        for t in range(self.T):
            self.U[t] += torch.sum(torch.unsqueeze(self.omega, -1) * self.noise[:, t], dim=0)
        action = self.U[:self.u_per_command]
        if self.u_per_command == 1:
            action = action[0]
        return action

    def _sample_noise(self):
        self.noise = self.noise_dist.sample((self.K, self.T))

    def _compute_total_cost_batch(self):
        self._sample_noise()
        self.perturbed_action = self.U + self.noise
        if self.sample_null_action:
            self.perturbed_action[self.K - 1] = 0
        self.perturbed_action = self._bound_action(self.perturbed_action)
        self.noise = self.perturbed_action - self.U
        if self.noise_abs_cost:
            action_cost = self.lambda_ * torch.abs(self.noise) @ self.noise_sigma_inv
        else:
            action_cost = self.lambda_ * self.noise @ self.noise_sigma_inv  # Like original paper
        self.cost_total, self.states, self.actions = self._compute_rollout_costs(self.perturbed_action)
        self.actions /= self.u_scale
        perturbation_cost = torch.sum(self.U * action_cost, dim=(1, -1))
        self.cost_total = (_standardize(self.cost_total) + self.action_cost_scale * _standardize(
            perturbation_cost)) / np.sqrt(1 + self.action_cost_scale ** 2)
        return self.cost_total

    def _compute_rollout_costs(self, perturbed_actions):
        if self.NA == 1:
            K, T, nu = perturbed_actions.shape
            assert nu == self.nu
            cost_total = torch.zeros(K, device=self.d, dtype=self.dtype)
        else:
            K, T, NA, nu = perturbed_actions.shape
            assert nu == self.nu and NA == self.NA
            cost_total = torch.zeros((K, NA), device=self.d, dtype=self.dtype)
        if self.NA == 1:
            state = self.state if self.state.shape == (K, self.nx) else self.state.repeat(K, 1)
        else:
            state = self.state if self.state.shape == (K, self.NA, self.nx) else self.state.repeat(K, 1, 1)

        states = []
        actions = []
        for t in range(T):
            u = self.u_scale * perturbed_actions[:, t]
            state = self._dynamics(state, u, t)
            step_cost = self._running_cost(state, u)
            cost_total += step_cost
            # Save total states/actions
            states.append(state)
            actions.append(u)
        actions = torch.stack(actions, dim=1)
        states = torch.stack(states, dim=1)

        # action perturbation cost
        if self.terminal_state_cost:
            cost_total += self.terminal_state_cost(states, actions)
        return cost_total, states, actions

    def _slice_control(self, t):
        return slice(t, t + 1)

    def get_rollouts(self, state, num_rollouts=1):
        assert num_rollouts == 1
        state = state.view(self.nx, ) if self.NA == 1 else state.view(self.NA, self.nx)
        T = self.U.shape[0]
        states_shape = (T + 1, self.nx) if self.NA == 1 else (T + 1, self.NA, self.nx)
        states = torch.zeros(states_shape, dtype=self.U.dtype, device=self.U.device)
        states[0] = state
        for t in range(T):
            states[t + 1] = self._dynamics(states[t], self.u_scale * self.U[t], t)
        return states[1:]

    def switch_num_agents(self, num_agents, truncate_rear=True):
        """
        TODO: Make the branch when orig_num_agents < num_agents robust(Currently, there's no need)
        """
        if num_agents < 1:
            num_agents = 1
        if num_agents == self.NA:
            return
        orig_num_agents = self.NA
        self.NA = num_agents
        if orig_num_agents == 1:
            self.u_init = self.u_init.repeat(num_agents, 1)
            self.noise_mu = self.noise_mu.repeat(num_agents, 1)
            self.U = torch.cat(
                (torch.unsqueeze(self.U, dim=1), self.u_init[0].repeat(self.T, num_agents - orig_num_agents, 1)), dim=1)
        elif num_agents == 1:
            self.u_init = self.u_init[0]
            self.noise_mu = self.noise_mu[0]
            self.U = self.U[:, 0]
        elif orig_num_agents < num_agents:
            self.u_init = torch.cat((self.u_init, self.u_init[0].repeat(num_agents - orig_num_agents, 1)),dim=0)
            self.noise_mu = torch.cat((self.noise_mu, self.noise_mu[0].repeat(num_agents - orig_num_agents, 1)),dim=0)
            self.U = torch.cat((self.U, self.u_init[0].repeat(self.T, num_agents - orig_num_agents, 1)), dim=1)
        elif truncate_rear:
            self.u_init = self.u_init[:num_agents]
            self.noise_mu = self.noise_mu[:num_agents]
            self.U = self.U[:, :num_agents]
        else:
            self.u_init = self.u_init[-num_agents:]
            self.noise_mu = self.noise_mu[-num_agents:]
            self.U = self.U[:, -num_agents:]
        self.noise_dist = MultivariateNormal(self.noise_mu, self.noise_sigma)

    @property
    def num_parallel(self):
        return self.K

    @property
    def num_agents(self):
        return self.NA


class PeriodicMPPI(MultiAgentMPPI):
    """
    PeriodicMPPI samples a horizon window in 2 ways: "warm start" and "history",
    as stated in RoboGrammar(https://people.csail.mit.edu/jiex/papers/robogrammar/index.html).
    It outperforms simple MPPI algorithm which samples in "warm_start" way only, based on the observation that life-like gaits
    with high performance are usually periodic gaits.
    We refer to the corresponding part of source code of RoboGrammar
    (https://github.com/allanzhao/RoboGrammar/blob/main/core/src/optim.cpp, line 21- line 35) while implementing this class.
    """

    def __init__(self,
                 warm_start_noise_sigma,
                 history_noise_sigma,
                 **kwargs):
        super(PeriodicMPPI, self).__init__(noise_sigma=warm_start_noise_sigma,
                                           noise_mu=None,
                                           **kwargs)
        self.warm_start_noise_sigma = warm_start_noise_sigma
        self.inv_warm_start_noise_sigma = torch.inverse(warm_start_noise_sigma)
        self.history_noise_sigma = history_noise_sigma
        self.inv_history_noise_sigma = torch.inverse(history_noise_sigma)
        self.U_history = torch.zeros_like(self.U)
        self.sample_idx = 0

    def reset(self):
        self._warm_start()
        super(PeriodicMPPI, self).reset()
        self.U_history = torch.zeros_like(self.U)
        self.sample_idx = 0

    def command(self, state):
        action = super(PeriodicMPPI, self).command(state)
        self.U_history = torch.roll(self.U_history, -1, dims=0)
        self.U_history[-1] = action
        self.sample_idx += 1
        return action

    def _warm_start(self):
        self.noise_sigma = self.warm_start_noise_sigma
        self.inv_noise_sigma = self.inv_warm_start_noise_sigma
        self.noise_mu = torch.zeros_like(self.u_init, dtype=self.dtype)
        self.noise_dist = MultivariateNormal(self.noise_mu, self.noise_sigma)

    def _history(self):
        self.noise_sigma = self.history_noise_sigma
        self.inv_noise_sigma = self.inv_history_noise_sigma
        look_back = (int(self.sample_idx / 2)) % (int(self.T / 2)) + int(self.T / 2) + 1
        U_history_used = torch.cat((self.U_history[:look_back], self.U_history[:self.T - look_back]), dim=0)
        alpha = torch.tensor([1.0 * (t + 1) / self.T for t in range(self.T)], dtype=self.dtype).reshape(
            (self.T,) + (1,) * (self.U.dim() - 1))
        noise_mu_batch_T = alpha * (U_history_used - self.U)
        self.noise_dist = MultivariateNormal(noise_mu_batch_T, self.noise_sigma)

    def _sample_noise(self):
        if self.sample_idx >= self.T and self.sample_idx % 2 == 0:
            self._history()
            self.noise = self.noise_dist.sample((self.K,))
        else:
            self._warm_start()
            self.noise = self.noise_dist.sample((self.K, self.T))

    def switch_num_agents(self, num_agents, truncate_rear=True):
        if num_agents < 1:
            num_agents = 1
        if num_agents == self.NA:
            return
        orig_num_agents = self.NA
        self.NA = num_agents
        if orig_num_agents == 1:
            self.u_init = self.u_init.repeat(self.NA, 1)
            self.U = torch.cat(
                (torch.unsqueeze(self.U, dim=1), self.u_init[0].repeat(self.T, self.NA - orig_num_agents, 1)), dim=1)
            self.U_history = torch.cat(
                (torch.unsqueeze(self.U_history, dim=1), self.u_init[0].repeat(self.T, self.NA - orig_num_agents, 1)),
                dim=1)
        elif self.NA == 1:
            self.u_init = self.u_init[0]
            self.U = self.U[:, 0]
            self.U_history = self.U_history[:, 0]
        elif orig_num_agents < self.NA:
            self.u_init = torch.cat((self.u_init, self.u_init[0].repeat(self.NA - orig_num_agents, 1)),dim=0)
            self.U = torch.cat((self.U, self.u_init[0].repeat(self.T, self.NA - orig_num_agents, 1)), dim=1)
            self.U_history = torch.cat((self.U_history, self.u_init[0].repeat(self.T, self.NA - orig_num_agents, 1)),
                                       dim=1)
        elif truncate_rear:
            self.u_init = self.u_init[:self.NA]
            self.U = self.U[:, :self.NA]
            self.U_history = self.U_history[:, :self.NA]
        else:
            self.u_init = self.u_init[-self.NA:]
            self.U = self.U[:, -self.NA:]
            self.U_history = self.U_history[:, -self.NA:]


if __name__ == '__main__':
    renderer_frame_dt = 1.0 / 30
    '''
    from LowerLevelControl.OfflineRenderCallBack import OfflineRenderCallBack
    renderer = OfflineRenderCallBack(env=env, switch_arrow_step=len(frames), frames=frames,
                                     playback_speed=renderer_frame_dt/args.decision_cycle)
    env.render(renderer, init_arrow_dir=env.target_dir)
    '''
