rm nohup.out
nohup python3 -u ../UpperLevelControl/Optimize.py \
      --data_file beam_walk_boca_mppi_2 \
      --mesh_name beam.mesh --mesh_type 1 --basis_num 20 \
      --task Walk --ctrl_dim 3 --level -0.15 --tot_time 5.0 \
      --env_reward_type quadratic_tr --target_t 15 --r_reward_w 50.0 \
      --ctrl_sys_reward_type x_linear \
      --controller MPPI --periodic_gaits --mppi_lambda 0.01 \
      --p_stretch 3 \
      --n_initialize 5 --bo_budge 200 --gp_refit_interval 50 \
      --gp_ucb_kappa_alpha 0.02 \
      --boca \
      --lowest_fidelity 0.2 \
      --uncertainty_bound_decrease 0.45 --uncertainty_bound_increase 1.25 \
      --resample_on_target_fidelity &