rm nohup.out
nohup python3 -u ../LowerLevelControl/Optimize.py --controller siLQG --basis_num 20 \
        --mesh_name beam.mesh --mesh_type 1 --data_file Debug \
        --terrain Plane \
        --u_reg_coef 0.625 --tot_time 5 --ctrl_sys_reward_type x_linear \
        --p_stretch 0.2 &