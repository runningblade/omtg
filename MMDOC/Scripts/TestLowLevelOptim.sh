#!/bin/bash
mesh_name="tripod_10.mesh"
mesh_type=1
basis_num=20
lame_coefs="1e5 1e5"
orientation_axis="-1 1 0"
orientation_theta=2.186276035465284
task="Walk"
level=-0.45
target_phi=0.785
ctrl_dim=3
env_reward_type="quadratic_tr"
target_t=15
r_reward_w=20.0
tot_time=10
ctrl_sys_reward_type="x_linear"
controller=siLQG
line_search_const=0.2
u_reward_ws=(0.01 0.25 0.03 0.05 0.06 0.075 0.08 0.1)
p_stretch=5
for u_reward_w in "${u_reward_ws[@]}"
do
  python3 ../LowerLevelControl/Optimize.py --data_file "tripod_walk_siLQG_"$u_reward_w --u_reward_w $u_reward_w \
  --mesh_name $mesh_name  --mesh_type $mesh_type --basis_num $basis_num --lame_coefs $lame_coefs \
  --orientation_axis $orientation_axis --orientation_theta $orientation_theta \
  --task $task --level $level --target_phi $target_phi \
  --ctrl_dim $ctrl_dim --tot_time $tot_time \
  --env_reward_type $env_reward_type --target_t $target_t --r_reward_w $r_reward_w \
  --ctrl_sys_reward_type $ctrl_sys_reward_type \
   --controller $controller --line_search_const $line_search_const \
   --p_stretch $p_stretch
done

:'
action_pattern=SquareWave
u_reg_coefs=(0.625 0.875)
for i in "${u_reg_coefs[@]}"
do
  python3 ../LowerLevelControl/Optimize.py --action_pattern $action_pattern --reward_w $reward_w \
  --ctrl_dim $ctrl_dim --horizon $horizon --tot_time $tot_time --du $du --controller $controller \
  --u_reg_coef $i --data_file "test_siLQG_"$i"_S"
done
'