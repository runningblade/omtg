rm nohup.out
nohup python3 -u ../UpperLevelControl/Optimize.py \
      --data_file tripod_bo_stable_mppi \
      --mesh_name tripod_10.mesh --mesh_type 1 --basis_num 20 --lame_coef 1e6 1e6 \
      --orientation_axis -1 1 0 --orientation_theta 2.186276035465284 \
      --task Walk \
      --ctrl_dim 3 --level -0.45 --tot_time 5.0 \
      --env_reward_type quadratic_tr --target_t 15 --r_reward_w 20 \
      --ctrl_sys_reward_type x_linear \
      --controller MPPI --periodic_gaits --mppi_lambda 0.01 --num_samples 32 --horizon 8 \
      --p_stretch 7 \
      --n_initialize 5 --bo_budge 300  --gp_refit_interval 45 \
      --gp_ucb_kappa_alpha 0.02 &