# kappa_alpha = 0.1 + BO + x_linear
python3 -u ../UpperLevelControl/Optimize.py --data_file bo_45 --controller OLLQ --basis_num 20 --u_reg_coef 0.125 \
      --tot_time 5 --ctrl_sys_reward_type x_linear --target_phi 0.7853981633974483 0.7853981633974483 \
      --n_initialize 5 --n_bo 200 --mute_gp_noise --gp_refit_interval 50 --gp_ucb_kappa_alpha 0.1

python3 -u ../UpperLevelControl/Optimize.py --data_file bo_90 --controller OLLQ --basis_num 20 --u_reg_coef 0.125 \
      --tot_time 5 --ctrl_sys_reward_type x_linear --target_phi 1.5707963267948966 1.5707963267948966 \
      --n_initialize 5 --n_bo 200 --mute_gp_noise --gp_refit_interval 50 --gp_ucb_kappa_alpha 0.1

python3 -u ../UpperLevelControl/Optimize.py --data_file bo_135 --controller OLLQ --basis_num 20 --u_reg_coef 0.125 \
      --tot_time 5 --ctrl_sys_reward_type x_linear --target_phi 2.356194490192345 2.356194490192345 \
      --n_initialize 5 --n_bo 200 --mute_gp_noise --gp_refit_interval 50 --gp_ucb_kappa_alpha 0.1

python3 -u ../UpperLevelControl/Optimize.py --data_file bo_180 --controller OLLQ --basis_num 20 --u_reg_coef 0.125 \
      --tot_time 5 --ctrl_sys_reward_type x_linear --target_phi 3.141592653589793 3.141592653589793 \
      --n_initialize 5 --n_bo 200 --mute_gp_noise --gp_refit_interval 50 --gp_ucb_kappa_alpha 0.1

python3 -u ../UpperLevelControl/Optimize.py --data_file bo_225 --controller OLLQ --basis_num 20 --u_reg_coef 0.125 \
      --tot_time 5 --ctrl_sys_reward_type x_linear --target_phi -2.356194490192345 -2.356194490192345 \
      --n_initialize 5 --n_bo 200 --mute_gp_noise --gp_refit_interval 50 --gp_ucb_kappa_alpha 0.1

python3 -u ../UpperLevelControl/Optimize.py --data_file bo_270 --controller OLLQ --basis_num 20 --u_reg_coef 0.125 \
      --tot_time 5 --ctrl_sys_reward_type x_linear --target_phi -1.5707963267948966 -1.5707963267948966 \
      --n_initialize 5 --n_bo 200 --mute_gp_noise --gp_refit_interval 50 --gp_ucb_kappa_alpha 0.1

python3 -u ../UpperLevelControl/Optimize.py --data_file bo_315 --controller OLLQ --basis_num 20 --u_reg_coef 0.125 \
      --tot_time 5 --ctrl_sys_reward_type x_linear --target_phi  -0.7853981633974483 -0.7853981633974483 \
      --n_initialize 5 --n_bo 200 --mute_gp_noise --gp_refit_interval 50 --gp_ucb_kappa_alpha 0.1
