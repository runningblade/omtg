rm nohup.out
nohup python3 -u ../UpperLevelControl/Optimize.py \
      --data_file walker_mppi_boca_5s \
      --mesh_name walker_high_wide_8.mesh --lame_coefs 1e5 1e5 --mesh_type 1 --basis_num 20 \
      --task Walk --ctrl_dim 3 --level -0.55 --tot_time 5.0 \
      --env_reward_type quadratic_tr --target_t 15 --r_reward_w 10.0 \
       --ctrl_sys_reward_type x_linear \
      --controller MPPI --periodic_gaits --mppi_lambda 0.01 --horizon 8 --num_samples 32 \
      --p_stretch 6 \
      --n_initialize 5 --bo_budge 300 --gp_refit_interval 50 \
      --gp_ucb_kappa_alpha 0.05 \
      --boca \
      --lowest_fidelity 0.2 \
      --uncertainty_bound_decrease 0.6 --uncertainty_bound_increase 1.25 \
      --resample_on_target_fidelity &