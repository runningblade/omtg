# BO + cross swimming
rm nohup.out
nohup python3 -u ../UpperLevelControl/Optimize.py --controller siLQG --basis_num 20 --u_reg_coef 0.125 \
      --terrain Fluid \
      --tot_time 5 --ctrl_sys_reward_type x_linear \
      --n_initialize 5 --bo_budge 300 --mute_gp_noise --gp_refit_interval 50 &
