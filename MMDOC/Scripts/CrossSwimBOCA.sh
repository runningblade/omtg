# BOCA + cross swimming
rm nohup.out
nohup python3 -u ../UpperLevelControl/Optimize.py --controller siLQG --basis_num 20 --u_reg_coef 0.125 \
      --tot_time 5 --ctrl_sys_reward_type x_linear \
      --terrain Fluid \
      --n_initialize 5 --bo_budget 300 --mute_gp_noise --gp_refit_interval 50 \
      --boca --lowest_fidelity 0.2 --uncertainty_bound_decrease 0.45 --uncertainty_bound_increase 1.25 --resample_on_target_fidelity &