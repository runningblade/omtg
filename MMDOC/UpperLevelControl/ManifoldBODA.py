from ManifoldBO import ManBOBase, _GP_Expected, _GP_Sigma_Post, GP_Expected
from ExGPVectorizedWrapper import ExGPVecWrap
import torch as th
from pymanopt.optimizers.optimizer import Optimizer as pymanopt_optimizer
from typing import Callable
import pymanopt
import numpy as np
import math
import time
from _Utils import format_time


def MF_GP_UCB(gp_regression: ExGPVecWrap,
              x,
              t,
              kappa,
              gaps):
    f_predictions = gp_regression.evaluate(x, False)
    ucbs = th.zeros((gp_regression.size, 1))
    for i in range(gp_regression.size):
        ucbs[i] = f_predictions[i].mean + th.sqrt(kappa(t) * f_predictions[i].variance) + gaps[i]
    return th.min(ucbs, dim=0)


class BODAInitError(RuntimeError):
    pass


class ManBODA(ManBOBase):
    """
    This class implements MF-GP-UCB algorithm.
    (https://people.eecs.berkeley.edu/~kandasamy/pubs/kandasamyJAIR19mfgpucb.pdf) on Riemann manifolds.
    Note that this algorithm can only handle discrete fidelities, and no information is shared among fidelities.
    We rename this algorithm as BODA(Discrete Approximations)
    in contrast to the BOCA(Continuous Approximations) algorithm.
    In our implementation, fidelity is an integer index of GP regression models.
    """

    def __init__(self,
                 cost: np.ndarray,
                 gp_ucb_kappa: Callable[[int], float],
                 uncertainty_bounds: np.ndarray,
                 diff_gap,
                 do_heuristics=True,
                 gaps_judge_coef=0.9,
                 gaps_update_coef=2,
                 bounds_update_coef=2,
                 **base_class_kwargs):
        """
        gp_regression: is an ExGPVecWrap of size M. ExGP in this wrapper should be sorted in ascending order w.r.t.
        fidelity, i.e. in ascending order w.r.t. cost.
        cost: An array of length M sorted in ascending order.
        uncertainty_bounds: An array of length M-1.
        gaps_judge_coef: in range of (0 ,1], used for heuristics on gaps. With smaller value,
        less additional evaluations of black box function will be taken; while with greater value,
        MF_GP_UCB will be more accurate, thus performance of this algorithm will have more of theoretical guarantee.
        gaps_update_coef: greater than 1. With greater value, MF_GP_UCB may be a too loose upper bound;
        while with smaller value, MF_GP_UCB may not be accurate enough (not an upper bound).
        bounds_update_coef: greater than 1. The greater the value be, the less evaluations will be taken on
        the low fidelity approximations.
        """
        super(ManBODA, self).__init__(**base_class_kwargs)
        if len(cost) != self.gp_regression.size or len(uncertainty_bounds) < self.target_fidelity:
            raise BODAInitError
        self.cost = cost
        self.gp_ucb_kappa = gp_ucb_kappa
        self.uncertainty_bounds = uncertainty_bounds
        self.gaps = diff_gap * (self.target_fidelity - np.array(range(self.gp_regression.size)))
        self.do_heuristics = do_heuristics
        self.gaps_judge_coef = gaps_judge_coef
        self.gaps_update_coef = gaps_update_coef
        self.bounds_update_coef = bounds_update_coef
        self.cur_highest_fidelity = 0
        self.num_stuck_evals = 0

    def update_gp_regression(self,
                             n_explore,
                             n_warmup,
                             acq_optimizer: pymanopt_optimizer
                             ):
        x_new, acq_max = self.argmax_acq(n_explore, n_warmup, acq_optimizer)
        fidelity_new = self.select_fidelity(x_new)
        y_new = self.black_box_function(x_new, fidelity_new)
        self.gp_regression.add_train_data(x_new, y_new, idx=fidelity_new)
        self.log(x_new[0], y_new[0], acq_max, fidelity_new[0])
        if self.do_heuristics:
            self.heuristics_on_gaps(x_new, y_new, fidelity_new)
            self.heuristics_on_bounds(fidelity_new)

    def log(self, x_new, y_new, acq_max, fidelity_new):
        print("A new acq_max = {} is found.".format(acq_max))
        print("A new y = {} is found when fidelity = {}.".format(y_new, fidelity_new))
        if fidelity_new == self.target_fidelity and y_new > self.y_max:
            self.x_max = x_new
            self.y_max = y_new
            print("A new y_max = {} is found on the target fidelity in current iteration of BO.".format(
                self.y_max))

    def select_fidelity(self, x):
        selected_fidelity = None
        # filter fidelity
        select_fidelity_start_time = time.time()
        for fidelity in range(self.target_fidelity):
            if math.sqrt(self.gp_ucb_kappa(self.optim_itr)) * _GP_Sigma_Post(self.gp_regression[fidelity], x) \
                    >= self.uncertainty_bounds[fidelity]:
                selected_fidelity = fidelity
        if selected_fidelity is None:
            selected_fidelity = self.target_fidelity
        select_fidelity_end_time = time.time()
        format_time(select_fidelity_end_time - select_fidelity_start_time, "Fidelity selection")
        return selected_fidelity

    def heuristics_on_gaps(self, x, y, fidelity):
        if fidelity == 0:
            return
        diff_gap = self.gaps[fidelity - 1] - self.gaps[fidelity]
        expected_diff_gap = np.fabs(y - _GP_Expected(self.gp_regression[fidelity - 1], x))
        if diff_gap < self.gaps_judge_coef * expected_diff_gap:
            if self.verbosity:
                print(f"Diff_gap={diff_gap} is too small."
                      f"Approximately, diff_gap should be no smaller than {self.gaps_judge_coef * expected_diff_gap}")
            print(f"We will take an additional evaluation on fidelity {fidelity - 1} "
                  "due to potential violation of the gap condition.")
            y_fidelity_below = self.black_box_function(x, fidelity - 1)
            real_diff_gap = np.fabs(y - y_fidelity_below)
            if diff_gap < self.gaps_judge_coef * real_diff_gap:
                if self.verbosity:
                    print(f"Gap condition is violated. We update diff_gap to {self.gaps_update_coef * real_diff_gap}")
                diff_gap = self.gaps_update_coef * real_diff_gap
                self.gaps = diff_gap * (self.target_fidelity - np.array(range(self.gp_regression)))

    def heuristics_on_bounds(self, fidelity):
        if fidelity != self.target_fidelity and fidelity <= self.cur_highest_fidelity:
            self.num_stuck_evals += 1
            if self.num_stuck_evals > self.cost[fidelity + 1] / self.cost[fidelity]:
                uncertainty_bound = self.bounds_update_coef * self.uncertainty_bounds[fidelity]
                self.uncertainty_bounds = np.maximum(self.uncertainty_bounds, uncertainty_bound)
                self.num_stuck_evals = 0
        else:
            self.cur_highest_fidelity = fidelity
            self.num_stuck_evals = 0

    def config_acq_func(self,
                        backend: Callable,
                        eval_mode=False):
        if eval_mode:
            self.acq_func = GP_Expected

            @backend(self.gp_regression.manifold(0))
            def opposite_acq_func(x):
                return -1.0 * self.acq_func(self.gp_regression[self.target_fidelity], x)[0]
        else:
            self.acq_func = lambda gp_regression, x, t: \
                MF_GP_UCB(gp_regression, x, t, self.gp_ucb_kappa, self.gaps)

            @backend(self.gp_regression.manifold(0))
            def opposite_acq_func(x):
                return -1.0 * self.acq_func(self.gp_regression, x, self.optim_itr)[0]

        self.acq_optim_problem = pymanopt.Problem(self.gp_regression.manifold, opposite_acq_func)

    def black_box_eval(self, x):
        return self.black_box_function(x, np.array([self.target_fidelity] * len(x)))

    def reset(self, gp_regression):
        super(ManBODA, self).reset(gp_regression)
        self.num_stuck_evals = 0

    @property
    def target_fidelity(self):
        return self.gp_regression.size - 1


if __name__ == "__main__":
    pass
