import shutil
import math
import os.path
import sys

sys.path.append('..')
import gpytorch as gp
import numpy as np
import torch as th
import pymanopt.optimizers
from _Manifolds import Grassmann
from ManifoldBO import ManBO, GP_UCB, GP_Expected
from ExtrinsicGP import GPModel, ExGP
from ContinuousFidelityExGP import ContFdExGP
from ExtrinsicGP import OnlineStandardization
from BlackBoxes import TrajectoryGenerator, MultiAgentTrajectoryGenerator, generate_trajectory_for_BO, \
    generate_trajectory_for_BOCA
from _GenerateArgsParser import _GenerateArgsParser
from ManifoldBOCA import ManBOCA
from ManifoldBODA import ManBODA
from _Backends import pytorch_backend
from LowerLevelControl.Optimize import get_control_system as define_inner_problem
from UpperLevelControl.Evaluate import evaluate_BO
from UpperLevelControl.NeuralKernel import NeuralKernel
from _Utils import load_nn_sequential_from_file


def get_GPR(manifold, args):
    # Construct extrinsic Gaussian process regression
    lengthscale_prior = gp.priors.GammaPrior(
        args.gp_kernel_gamma_alpha,
        args.gp_kernel_gamma_beta
    )
    if not args.boca and not args.mf_gp_ucb:
        gp_model = GPModel(mean=gp.means.ConstantMean(),
                           kernel=getattr(gp.kernels, args.gp_kernel + 'Kernel')(lengthscale_prior=lengthscale_prior,
                                                                                 eps=0.05, ),
                           likelihood=gp.likelihoods.GaussianLikelihood(),
                           ).double()
        gp_model.scale_kernel({'outputscale_prior': gp.priors.GammaPrior(2.0, 2.0)},
                              {'outputscale': 1.0})
        GPR = ExGP(gp_model, manifold, verbosity=args.gp_verbosity)
    elif args.boca:
        gp_model = GPModel(mean=gp.means.ConstantMean(),
                           kernel=getattr(gp.kernels, args.gp_kernel + 'Kernel')(lengthscale_prior=lengthscale_prior,
                                                                                 eps=0.05,
                                                                                 active_dims=tuple(
                                                                                     range(manifold._n * manifold._n))),
                           likelihood=gp.likelihoods.GaussianLikelihood(),
                           ).double()
        if args.nn_fidelity_kernel:
            fidelity_kernel = NeuralKernel(base_kernel=getattr(gp.kernels, args.fidelity_kernel + 'Kernel')(
                lengthscale_prior=gp.priors.GammaPrior(args.fidelity_kernel_gamma_alpha,
                                                       args.fidelity_kernel_gamma_beta),
                eps=0.05),
                feature_extractor=load_nn_sequential_from_file("../../../omtg-build/mmdoc_nn"),
                active_dims=manifold._n * manifold._n).double()
        else:
            fidelity_kernel = getattr(gp.kernels, args.fidelity_kernel + 'Kernel')(
                lengthscale_prior=gp.priors.GammaPrior(args.fidelity_kernel_gamma_alpha,
                                                       args.fidelity_kernel_gamma_beta),
                eps=0.05,
                active_dims=manifold._n * manifold._n).double()
        gp_model.combine_with_kernel(
            operation='product',
            another_kernel=fidelity_kernel
        )
        gp_model.scale_kernel({'outputscale_prior': gp.priors.GammaPrior(2.0, 2.0)},
                              {'outputscale': 1.0})
        GPR = ContFdExGP(gp_model, manifold, verbosity=args.gp_verbosity)
    else:
        assert args.mf_gp_ucb
        """
         gp_models = [GPModel(mean=gp.means.ConstantMean(),
                             kernel=getattr(gp.kernels, args.gp_kernel)(lengthscale_prior=gp.priors.GammaPrior(
                                 args.gp_kernel_gamma_alpha,
                                 args.gp_kernel_gamma_beta
                             )),
                             likelihood=gp.likelihoods.GaussianLikelihood(),
                             ).double() for i in range(len(args.cost))]
        for gp_model in gp_models:
            gp_model.scale_kernel({'outputscale_prior': gp.priors.GammaPrior(40.0, 1.0)})
        GPR = [ExGP(gp_model, manifold, 1) for gp_model in gp_models]
        """
        raise NotImplementedError()
    if args.mute_gp_noise:
        print("Mute randomness in GP model due to no randomness in the black box function.")
        GPR.gp_model.mute_likelihood()
    return GPR


def define_outer_problem(trajectory_generator, manifold, args):
    from UpperLevelControl.ManifoldBO import get_gp_ucb_kappa_test
    kappa = get_gp_ucb_kappa_test(manifold,
                                  args.gp_ucb_kappa_alpha,
                                  args.gp_ucb_kappa_beta)
    GPR = get_GPR(manifold, args)
    if not args.boca and not args.mf_gp_ucb:
        if args.acq_func == "GP_UCB":
            def acq_func(model, x, t):
                return GP_UCB(model, x, t, kappa=kappa)

        else:
            def acq_func(model, x, t):
                return GP_Expected(model, x)

        BO = ManBO(gp_regression=GPR,
                   acq_func=acq_func,
                   black_box_function=generate_trajectory_for_BO(trajectory_generator),
                   verbosity=0)
    elif args.boca:
        if args.boca_gap == "regularized_kernel":
            fidelity_kernel = GPR.regularized_kernel['fidelity']

            def gap(fidelity):
                target_fidelity = np.array([[1.0]])
                correlation_to_target_fidelity = fidelity_kernel(fidelity, target_fidelity)[:, 0]
                return np.sqrt(1.0 - correlation_to_target_fidelity ** 2)
        else:
            def gap(fidelity):
                target_fidelity = np.array([[1.0]])
                correlation_to_target_fidelity = np.exp(-1.0 * np.square(fidelity - target_fidelity).sum(axis=-1))[0]
                return np.sqrt(1.0 - correlation_to_target_fidelity ** 2)

        BO = ManBOCA(gp_regression=GPR,
                     black_box_function=generate_trajectory_for_BOCA(trajectory_generator),
                     cost=lambda fd: fd[..., 0],
                     gap=gap,
                     gp_ucb_kappa=kappa,
                     uncertainty_bound=args.init_uncertainty_bound,
                     heuristic_window_size=args.heuristic_window_size,
                     uncertainty_bound_decrease=args.uncertainty_bound_decrease,
                     uncertainty_bound_increase=args.uncertainty_bound_increase,
                     lowest_fidelity=args.lowest_fidelity * np.ones((1, 1)),
                     grid_unit_length=max(args.grid_unit_length, args.ctrl_dt / args.tot_time),
                     fidelity_dim=1,
                     verbosity=0,
                     )
    else:
        assert args.mf_gp_ucb
        """
                 steps_array = (1.0 * np.array(args.cost) / args.decision_cycle).astype(dtype=int)
                BO = ManBODA(gp_regression=GPR,
                                    black_box_function=lambda x, fidelity: getApproximateReward_steps_discrete(
                                        x,
                                        fidelity,
                                        steps_array=steps_array,
                                        target_steps=target_steps,
                                        ctrl_sys=ctrl_sys,
                                    ),
                                    gp_ucb_kappa=kappa,
                                    cost=args.cost,
                                    uncertainty_bound=None,
                                    gap=None,
                                    verbosity=0)
        """
        BO = None
        raise NotImplementedError()
    return BO


def initialize_BO(BO, gp_optim_type, gp_optim_info, args):
    from LowerLevelControl.Optimize import get_naive_design
    print("================Sample on manifold and train hyperparameters of GP model===============")
    if not args.boca and not args.mf_gp_ucb:
        train_inputs = BO.gp_regression.manifold.random_point(size=args.n_initialize-1,
                                                              distribution=args.rng_dist_pre_train)
        benchmark_design = np.expand_dims(get_naive_design(args.basis_num, args.ctrl_dim), axis=0)
        train_inputs = np.concatenate((train_inputs, benchmark_design), axis=0)
        train_targets = BO.black_box_function(train_inputs)
        BO.gp_regression.set_train_data(train_inputs, train_targets)
        BO.gp_regression(args.gp_itrs, gp_optim_type, gp_optim_info)
    elif args.boca:
        train_x = BO.gp_regression.manifold.random_point(size=args.n_initialize - 1,
                                                         distribution=args.rng_dist_pre_train)
        benchmark_design = np.expand_dims(get_naive_design(args.basis_num, args.ctrl_dim), axis=0)
        train_x = np.concatenate((train_x, benchmark_design), axis=0)
        grid_unit_length = max(args.grid_unit_length, args.ctrl_dt / args.tot_time)
        train_fidelity = np.sort(
            grid_unit_length * np.random.randint(low=int(args.lowest_fidelity / grid_unit_length),
                                                 high=int(1.0 / grid_unit_length) + 1,
                                                 size=(args.n_initialize - 1, 1)), axis=0)
        target_fidelity = np.ones((1, 1))
        train_fidelity = np.concatenate((train_fidelity, target_fidelity), axis=0)
        train_inputs = {"x": train_x, "fidelity": train_fidelity}
        train_targets = BO.black_box_function(train_x, train_fidelity)
        BO.gp_regression.set_train_data(train_inputs, train_targets)
        BO.gp_regression(args.gp_itrs, gp_optim_type, gp_optim_info)
    else:
        assert args.mf_gp_ucb
        """
        train_x = BO.gp_regression[0].manifold.random_point(size=args.n_initialize,
                                                            distribution=args.rng_dist_pre_train)
        train_fidelity = np.sort(np.random.randint(low=0, high=len(args.cost), size=args.n_initialize))
        train_targets = BO.black_box_function(train_x, train_fidelity)
        targets_standardizer = OnlineStandardization(mean=np.mean(train_targets),
                                                     var_sum=np.var(train_targets, ddof=args.n_initialize - 1),
                                                     size=args.n_initialize)
        for idx in range(len(args.cost)):
            BO.gp_regression[idx].set_train_data(train_inputs=train_x,
                                                 train_targets=train_targets[np.where(train_fidelity == idx)],
                                                 targets_standardizer=targets_standardizer)
            BO.gp_regression[idx](args.gp_itrs,
                                  solver_type='LBFGS',
                                  info_solver={'lr': 5 * 1e-3, 'history_size': 200}
                                  )
        """
        raise NotImplementedError()


def resample_on_target_fidelity(BO, bb, save_dir, suffix):
    from ManifoldBOCA import fidelity_tol
    is_low_fd = ((BO.gp_regression.fidelity + fidelity_tol) < BO.target_fidelity)[..., 0]
    resampled_x = BO.gp_regression.x[is_low_fd]
    resampled_fidelity = np.ones((len(resampled_x), 1))
    print("================Resampling of Multi-fidelity BO starts.===============")
    resampled_targets = bb(resampled_x, resampled_fidelity)
    complete_targets = np.copy(BO.gp_regression.y)
    complete_targets[is_low_fd] = resampled_targets
    np.save(save_dir + "BOCA_resampled_targets_" + suffix, complete_targets)


if __name__ == '__main__':
    '''
        Find optimal design matrix B using Bayesian Optimization
    '''
    seed_ = 4326510  # int(time.time())
    np.random.seed(seed_)
    th.random.manual_seed(seed_)
    parser = _GenerateArgsParser('Upper')
    args_ = parser.parse_args()
    train_data_save_dir = '../../../omtg_mmdoc_data/' + args_.data_file + '/'
    if os.path.exists(train_data_save_dir):
        shutil.rmtree(train_data_save_dir)
    os.makedirs(train_data_save_dir)
    eval_data_save_dir = '../../../omtg_mmdoc_data/' + args_.data_file + '_eval/'
    if os.path.exists(eval_data_save_dir):
        shutil.rmtree(eval_data_save_dir)
    os.makedirs(eval_data_save_dir)
    ctrl_sys_ = define_inner_problem(args_, ctrl_reduced=True)
    manifold_ = Grassmann(n=args_.basis_num, p=args_.ctrl_dim)
    trajectory_generator = TrajectoryGenerator(ctrl_sys_,
                                               target_steps=int(args_.tot_time / args_.ctrl_dt),
                                               scale_reward=True if args_.ctrl_sys_reward_type == "x_linear" else False,
                                               save_dir=train_data_save_dir)
    multi_agent_trajectory_generator = MultiAgentTrajectoryGenerator(ctrl_sys_,
                                                                     target_steps=int(args_.tot_time / args_.ctrl_dt),
                                                                     scale_reward=True if args_.ctrl_sys_reward_type == "x_linear" else False,
                                                                     save_dir=train_data_save_dir,
                                                                     verbosity=1)
    BO_ = define_outer_problem(multi_agent_trajectory_generator, manifold_, args_)
    gp_optim_type = "LBFGS"
    gp_optim_info = {'lr': 5 * 1e-3, 'history_size': 200}
    initialize_BO(BO_, gp_optim_type, gp_optim_info, args_)
    BO_.gp_regression.save_model(train_data_save_dir, str(seed_))
    acq_optimizer = getattr(pymanopt.optimizers, args_.acq_optimizer)(verbosity=0)
    BO_.black_box_function = generate_trajectory_for_BO(trajectory_generator) if not args_.boca and not args_.mf_gp_ucb \
        else generate_trajectory_for_BOCA(trajectory_generator)
    print("================Optimization of BO starts.===============")
    BO_(budget=args_.bo_budget,
        n_warmup=args_.bo_n_warmup,
        n_explore=args_.bo_n_explore,
        acq_optimizer=acq_optimizer,
        backend=pytorch_backend,
        gp_refit_interval=args_.gp_refit_interval,
        gp_itrs=args_.gp_itrs,
        gp_optim_type=gp_optim_type,
        gp_optim_info=gp_optim_info,
        save_interval=args_.save_interval,
        suffix=str(seed_),
        dir=train_data_save_dir)
    multi_agent_trajectory_generator.reset(save_dir=eval_data_save_dir)
    evaluate_BO(BO_,
                seed=16345607,
                step_size=args_.eval_step_size,
                n_warmup=args_.bo_n_warmup,
                n_explore=args_.bo_n_explore,
                acq_optimizer=acq_optimizer,
                black_box_function=generate_trajectory_for_BO(
                    multi_agent_trajectory_generator) if not args_.boca and not args_.mf_gp_ucb else generate_trajectory_for_BOCA(
                    multi_agent_trajectory_generator),
                save_dir=eval_data_save_dir)
    if args_.boca and args_.resample_on_target_fidelity:
        resample_train_data_dir = '../../../omtg_mmdoc_data/' + args_.data_file + '_resample/'
        if os.path.exists(resample_train_data_dir):
            shutil.rmtree(resample_train_data_dir)
        os.makedirs(resample_train_data_dir)
        multi_agent_trajectory_generator.reset(save_dir=resample_train_data_dir)
        resample_on_target_fidelity(BO_,
                                    generate_trajectory_for_BOCA(multi_agent_trajectory_generator),
                                    resample_train_data_dir,
                                    str(seed_))
