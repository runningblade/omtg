import sys
import torch

sys.path.append('..')
from UpperLevelControl.ExtrinsicGP import ExGP, GPModel
from _Utils import plot_GPR_res, format_time
from UpperLevelControl._Manifolds import Grassmann
from UpperLevelControl.BlackBoxes import geodesicDistanceAvg
import torch as th
import numpy as np
import gpytorch as gp
import os


class ContFdExGP(ExGP):

    def embed_to_euclidean(self, inputs) -> th.Tensor:
        x = inputs['x']
        fidelity = inputs['fidelity']
        if isinstance(x, np.ndarray):
            x = th.from_numpy(x)
        if isinstance(fidelity, np.ndarray):
            fidelity = th.from_numpy(fidelity)
        vec_xxT = super(ContFdExGP, self).embed_to_euclidean(x)
        # handle special case when vec_xxT.shape=(n, vec_xxT_dim) and fidelity.shape=(1,fidelity_dim)
        if vec_xxT.shape[0] > 1 and fidelity.shape[0] == 1:
            fidelity = torch.repeat_interleave(fidelity, repeats=vec_xxT.shape[0], dim=0)
        return th.cat((vec_xxT, fidelity), dim=1)

    def cache_train_data(self, new_train_inputs, new_train_targets):
        self.train_inputs['x'] = np.concatenate((self.train_inputs['x'], new_train_inputs['x']))
        self.train_inputs['fidelity'] = np.concatenate((self.train_inputs['fidelity'], new_train_inputs['fidelity']))
        self.train_targets = np.concatenate((self.train_targets, new_train_targets))

    def save_data(self, dir: str = './', suffix: str = ''):
        np.save(dir + 'GP_train_x_' + suffix, self.train_inputs['x'])
        np.save(dir + 'GP_train_fidelity_' + suffix, self.train_inputs['fidelity'])
        np.save(dir + 'GP_train_targets_' + suffix, self.train_targets)

    def get_truncated_model(self, length, targets_standardizer=None):
        truncated_gp_model = self.gp_model.get_truncated_model(length)
        truncated_model = ContFdExGP(truncated_gp_model, self.manifold, self.verbosity)
        truncated_model.train_inputs = {'x': self.train_inputs['x'][:length],
                                        'fidelity': self.train_inputs['fidelity'][:length]}
        truncated_model.train_targets = self.train_targets[:length]
        """
        truncated_model.targets_standardizer = OnlineStandardization(
            np.mean(truncated_model.train_targets),
            np.var(truncated_model.train_targets, ddof=len(truncated_model.train_targets)-1),
            length
        ) if targets_standardizer is None else targets_standardizer
        """
        return truncated_model

    @property
    def regularized_kernel(self):
        """
        kernel.forward(x1,x2) only do computations between legal inputs.
        kernel.__call__(x1,x2) == preprocess(x1,x2) and kernel.forward(x1,x2)
        """
        if isinstance(self.gp_model.kernel, gp.kernels.ScaleKernel):
            compound_kernel = self.gp_model.kernel.base_kernel
        else:
            compound_kernel = self.gp_model.kernel
        return {'x': lambda x1, x2: compound_kernel.kernels[0].forward(th.from_numpy(x1),
                                                                       th.from_numpy(x2)).detach().numpy(),
                'fidelity': lambda f1, f2: compound_kernel.kernels[1].forward(th.from_numpy(f1),
                                                                              th.from_numpy(f2)).detach().numpy()
                }

    @property
    def fidelity(self):
        return self.train_inputs['fidelity']

    @property
    def x(self):
        return self.train_inputs['x']


def get_gp_model_test(x_dim, noise_active=True):
    gp_model = GPModel(mean=gp.means.ConstantMean(),
                       kernel=gp.kernels.RBFKernel(lengthscale_prior=gp.priors.GammaPrior(5.0, 0.5),
                                                   active_dims=tuple(range(x_dim))),
                       likelihood=gp.likelihoods.GaussianLikelihood()
                       ).double()
    if not noise_active:
        gp_model.mute_likelihood()
    gp_model.initialize(**{'kernel.lengthscale': 0.5})
    fidelity_kernel = gp.kernels.RBFKernel(lengthscale_prior=gp.priors.GammaPrior(2.0, 0.5),
                                           active_dims=x_dim)
    gp_model.combine_with_kernel(operation='product', another_kernel=fidelity_kernel)
    gp_model.scale_kernel(info_scale_kernel={'outputscale_prior': gp.priors.GammaPrior(40.0, 1.0)},
                          info_scale_kernel_init={'outputscale': 22.0})
    return gp_model


def get_black_box_test(n_fixed_points, manifold, obs_noise):
    from BlackBoxes import geodesicDistanceAvg
    fixed_points = manifold.random_point(size=n_fixed_points)
    def black_box(x, fidelity):
        return geodesicDistanceAvg(x, fidelity, fixed_points, obs_noise)

    return black_box

def set_GP_randomly(gp_regression,
                    black_box_function,
                    data_size,
                    grid_unit_len,
                    lowest_fidelity,
                    distribution="uniform",
                    fit_gp=True,
                    itrs=100,
                    optim_type="LBFGS",
                    optim_info: dict = {}):
    train_x = gp_regression.manifold.random_point(size=data_size, distribution=distribution)
    train_fidelity = np.sort(
        grid_unit_len * np.random.randint(low=int(lowest_fidelity/grid_unit_len), high=int(1.0 / grid_unit_len), size=(data_size, 1)),
        axis=0)
    train_inputs = {'x': train_x, 'fidelity': train_fidelity}
    train_targets = black_box_function(train_x, train_fidelity)
    gp_regression.set_train_data(train_inputs=train_inputs, train_targets=train_targets)
    if fit_gp:
        gp_regression(itrs, optim_type, optim_info)


def module_test(data_path,
                n_train_points,
                n_test_points,
                gp_itrs=100,
                seed=0):
    np.random.seed(seed)
    th.random.manual_seed(seed)
    Gn = 56
    Gp = 7
    manifold = Grassmann(n=Gn, p=Gp)
    obs_noise = 1e-5
    if obs_noise <= 1e-4:
        noise_activate = False
    else:
        noise_activate = True
    gp_model = get_gp_model_test(Gn*Gn, noise_activate)
    GPR = ContFdExGP(gp_model,
                     manifold,
                     1)
    n_fixed_points = 10
    black_box = get_black_box_test(n_fixed_points, manifold, obs_noise)
    print('First train {} random samples on different fidelities.'.format(n_train_points))
    set_GP_randomly(GPR, black_box, n_train_points, 0.01, 0.01,
                    itrs=gp_itrs, optim_type="LBFGS",
                    optim_info={'lr': 5 * 1e-3,
                                'history_size': 200}
                    )
    print('Then test on {} random samples on the target fidelity.'.format(n_test_points))
    test_x = manifold.random_point(size=n_test_points)
    test_fidelity = np.array([1.0] * n_test_points)
    test_inputs = {'x': test_x, 'fidelity': test_fidelity}
    test_targets = black_box(test_x, test_fidelity)
    fpred, ypred = GPR.evaluate(test_inputs)
    test_targets = GPR.targets_standardizer(test_targets)
    # plot
    plot_GPR_res(test_targets, fpred, data_path, 'multi_fidelity_1')


if __name__ == '__main__':
    '''
           module test for ContinuousFidelityExGP.py.
           black box function is y(x) = avgGeodesicDistance(x, fidelity * n points of n fixed points).
    '''
    data_save_path = '../../../local_test_data/'
    if not os.path.exists(data_save_path):
        os.makedirs(data_save_path)
    module_test(data_save_path,
                2000,
                50,
                seed=0)
