import numpy as np
from pymanopt.manifolds import Grassmann as OrigGrassmann
from pymanopt.tools.multi import multiqr
from abc import ABC, abstractmethod
from pymanopt.manifolds.manifold import Manifold
import torch as th

'''
These classes are written to strengthen the ability to sample points on manifolds
so that points can be sampled from any distributions.
In pymanopt.manifolds.manifold, points can only be sampled from a normal distribution. 
'''


class Grassmann(OrigGrassmann):
    def __init__(self, n, p, *, k=1, center=None):
        super(Grassmann, self).__init__(n, p, k=k)
        if center is None:
            self._center = np.concatenate((np.identity(p), np.zeros((n - p, p))), axis=0)
            if k > 1:
                self._center = np.tile(self._center, (k, 1, 1))
        else:
            if k == 1:
                assert center.shape == (n, p)
            else:
                assert center.shape == (k, n, p)
            self._center = center

    def random_point(self, size=1, distribution='uniform'):
        if distribution == 'uniform':
            q, r = multiqr(np.random.uniform(low=-1.0, high=1.0, size=(size * self._k, self._n, self._p)))
        else:
            assert distribution == 'normal'
            _loc = np.repeat(np.expand_dims(self._center, axis=0), repeats=size, axis=0) if self._k == 1 else np.repeat(
                self._center, repeats=size, axis=0)
            q, r = multiqr(np.random.normal(loc=_loc, scale=0.1))
        if self._k > 1:
            q = q.reshape(size, self._k, self._n, self._p)
        return q

    @classmethod
    def eq_embed_to_euclidean(cls, x):
        """
               Use an equivariant embedding J(X)=XXT to embed G(m,n) in R^(mn)
        """
        if isinstance(x, np.ndarray):
            return x @ np.transpose(x, tuple(range(x.ndim - 2)) + (x.ndim - 1, x.ndim - 2))
        else:
            assert isinstance(x, th.Tensor)
            return x @ th.transpose(x, dim0=-1, dim1=-2)

    @classmethod
    def dist(cls, a, b):
        if isinstance(a, np.ndarray):
            aTb = np.transpose(a, tuple(range(a.ndim - 2)) + (a.ndim - 1, a.ndim - 2)) @ b
            sigma = np.linalg.svd(aTb,
                                  compute_uv=False)
            sigma[sigma > 1] = 1
            theta = np.arccos(sigma)
            return np.linalg.norm(theta, axis=-1)
        else:
            assert isinstance(a, th.Tensor)
            aTb = th.transpose(a, dim0=-1, dim1=-2) @ b
            sigma = th.linalg.svdvals(aTb)
            sigma[sigma > 1] = 1
            theta = th.arccos(sigma)
            return th.norm(theta, dim=-1)



if __name__ == '__main__':
    Gn = 56
    Gp = 7
    data_size = (1,)
    m1 = Grassmann(n=Gn, p=Gp)
    m2 = Grassmann(n=Gn, p=Gp, k=5)
    rp1 = m1.random_point(size=100, distribution='uniform')
    rp2 = m1.random_point(size=100, distribution='normal')
    print(rp1.shape, rp2.shape)
    rp3 = m2.random_point(size=100, distribution='normal')
    print(rp3.shape)
    print(m1.eq_embed_to_euclidean(rp1).shape)
    print(m1.eq_embed_to_euclidean(rp2).shape)
