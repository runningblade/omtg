import gpytorch as gp
import torch as th
import torch.nn


class NeuralKernel(gp.kernels.Kernel):
    def __init__(self, base_kernel, feature_extractor, active_dims=None, **base_kwargs):
        super(NeuralKernel, self).__init__(active_dims=active_dims, **base_kwargs)
        self.base_kernel = base_kernel
        self.feature_extractor = feature_extractor

    def forward(self, x1, x2, diag=False, last_dim_is_batch=False, **params):
        proj_x1 = self.feature_extractor(x1)
        proj_x2 = self.feature_extractor(x2)
        return self.base_kernel(proj_x1, proj_x2, diag, last_dim_is_batch, **params)

