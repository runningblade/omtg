import time, sys

sys.path.append('..')
from typing import Callable
import gpytorch as gp
import pymanopt
from UpperLevelControl._Manifolds import Grassmann
from pymanopt.optimizers.optimizer import Optimizer as pymanopt_optimizer
import torch as th
import numpy as np
import math
from UpperLevelControl.ExtrinsicGP import ExGP, GPModel
from UpperLevelControl._Backends import pytorch_backend
from _Utils import format_time
from BlackBoxes import geodesticDistanceDifference, geodesicDistanceAvg
import os
from abc import abstractmethod, ABC


def GP_Expected(gp_regression, x):
    f_prediction = gp_regression.evaluate(x, False)
    return f_prediction.mean


'''
Recommended kappa value is 2.576 in 
https://github.com/fmfn/BayesianOptimization/blob/91441fe4002fb6ebdb4aa5e33826230d8df560d0/bayes_opt/bayesian_optimization.py#L236
line 236.
'''


def _GP_Expected(gp_regression, x):
    f_prediction = gp_regression.evaluate(x, False)
    return f_prediction.mean.detach().numpy()


def _GP_Sigma_Post(gp_regression, x):
    f_prediction = gp_regression.evaluate(x, False)
    return th.sqrt(f_prediction.variance).detach().numpy()


def GP_UCB(gp_regression, x, t, kappa=lambda t: 2.576):
    f_prediction = gp_regression.evaluate(x, False)
    return f_prediction.mean + th.sqrt(kappa(t) * f_prediction.variance)


class ManBOBase(ABC):
    """
    This abstract class branches to (Multi-fidelity) Bayesian optimization algorithms
    on manifolds.
    """

    def __init__(self,
                 gp_regression,
                 black_box_function=None,
                 verbosity=2):
        self.gp_regression = gp_regression
        self.black_box_function = black_box_function
        self.acq_func = None
        self.acq_optim_problem = None
        self.verbosity = verbosity
        self.consumed_budget = 0
        self.cur_itr = 0
        self.x_max = None
        self.y_max = - math.inf
        self.elapsed_time = [0.0]

    def __call__(self, *args, **kwargs):
        return self.optimize(*args, **kwargs)

    def optimize(self,
                 budget: float,
                 n_explore,
                 n_warmup,
                 acq_optimizer: pymanopt_optimizer,
                 backend: Callable,
                 gp_refit_interval: int = 0,
                 gp_itrs: int = 100,
                 gp_optim_type: str = 'LBFGS',
                 gp_optim_info: dict = {},
                 save_interval: int = 0,
                 dir: str = './',
                 suffix: str = 'train',
                 ):
        self.config_acq_func(backend)
        total_budget = self.consumed_budget + budget
        while self.consumed_budget < total_budget:
            self.cur_itr += 1
            print("================Start {}-th iteration of {}================".format(self.cur_itr,
                                                                                       self.__class__.__name__))
            itr_start_time = time.time()
            self.update_gp_regression(n_explore, n_warmup, acq_optimizer)
            itr_end_time = time.time()
            duration = itr_end_time - itr_start_time
            format_time(duration, "The current iteration of {}".format(self.__class__.__name__))
            self.elapsed_time.append(self.elapsed_time[-1]+duration)
            print(f"Budget consumed is {self.consumed_budget}")
            # refit gp_regression
            if gp_refit_interval > 0 and self.cur_itr % gp_refit_interval == 0:
                self.gp_regression(gp_itrs, gp_optim_type, gp_optim_info)
                self.save(dir, suffix, save_model=True)
            # save data
            if save_interval > 0 and self.cur_itr % save_interval == 0:
                self.save(dir, suffix, save_data=True)
        self.acq_optim_problem = None
        self.save(dir, suffix, save_data=True)

    def evaluate(self,
                 step_size,
                 n_explore,
                 n_warmup,
                 acq_optimizer: pymanopt_optimizer,
                 backend: Callable,
                 base_size=0
                 ):
        eval_size = self.gp_regression.data_size - base_size
        assert 1 <= step_size <= eval_size
        x_eval = []
        orig_gp_regression = self.gp_regression
        orig_acq_func = self.acq_func
        self.config_acq_func(backend, eval_mode=True)
        for i in range(math.ceil(eval_size / step_size)):
            print('================Start finding the x_max of {}-th GP.================'.format(i + 1))
            itr_start_time = time.time()
            length = min((i + 1) * step_size, eval_size) + base_size
            self.gp_regression = orig_gp_regression.get_truncated_model(length)
            x_new, _ = self.argmax_acq(n_explore, n_warmup, acq_optimizer)
            x_eval.append(x_new[0])
            itr_end_time = time.time()
            format_time(itr_end_time - itr_start_time, "The current iteration for evaluating BO")
        self.acq_func = orig_acq_func
        self.acq_optim_problem = None
        x_eval = np.array(x_eval)
        print('================Start computing y_max.================')
        y_eval = self.black_box_eval(x_eval)
        return x_eval, y_eval

    def argmax_acq(self,
                   n_explore,
                   n_warmup,
                   optimizer: pymanopt_optimizer,
                   ):

        # warm up with random points on the manifold
        cost_min = math.inf
        res = None
        argmax_acq_start_time = time.time()
        if n_warmup > 1:
            x_warmup = self.gp_regression.manifold.random_point(size=n_warmup, distribution='uniform')
            y_warmup = self.acq_optim_problem.cost(x_warmup)
            cost_min = y_warmup.min()
            idx = y_warmup.argmin()
            res = x_warmup[idx:idx + 1]
            if self.verbosity:
                print('A new acq_max = {} is found in the warmup phase.\n'.format(-1.0 * cost_min))
        # explore the manifold more thoroughly
        for i in range(n_explore):
            if self.verbosity:
                print('The {}-th exploration begins on the manifold.'.format(i + 1))
            x0 = res if res is not None else self.gp_regression.manifold.random_point(size=1, distribution='uniform')
            with th.autograd.set_detect_anomaly(True):
                solution = optimizer.run(self.acq_optim_problem, initial_point=x0)
            if solution.cost < cost_min:
                res = solution.point
                cost_min = solution.cost
                if self.verbosity:
                    print('A new acq_max = {} is found in the {}-th exploration.\n'.format(-1.0 * cost_min, i + 1))
        argmax_acq_end_time = time.time()
        format_time(argmax_acq_end_time - argmax_acq_start_time, "Optimization of acquisition function")
        return res, -1.0 * cost_min

    def save(self, dir='./', suffix='', save_data=False, save_model=False):
        if save_data:
            self.gp_regression.save_data(dir, suffix)
        if save_model:
            self.gp_regression.save_model(dir, suffix)
        np.save(dir+"BO_elapsed_time_"+suffix, self.elapsed_time)

    def reset(self, gp_regression):
        self.gp_regression = gp_regression
        self.x_max = None
        self.y_max = - math.inf
        self.cur_itr = 0
        self.consumed_budget = 0
        self.elapsed_time = [0.0]

    @abstractmethod
    def update_gp_regression(self,
                             n_explore,
                             n_warmup,
                             acq_optimizer: pymanopt_optimizer
                             ):
        """
        arg_max_acq() should be called inside this method
        """
        pass

    @abstractmethod
    def config_acq_func(self,
                        backend: Callable,
                        eval_mode=False):
        pass

    @abstractmethod
    def black_box_eval(self, x):
        pass

    @property
    def res(self):
        return self.x_max, self.y_max


class ManBO(ManBOBase):
    """
    The matrices representing the point on Grassmann Manifold are vectorized.
    All vectors here are of numpy.ndarray.
    The conversion between numpy and tensor is done implicitly with the help of backend decoration.
    """

    def __init__(self,
                 acq_func,
                 **base_class_kwargs):
        super(ManBO, self).__init__(**base_class_kwargs)
        self.acq_func = acq_func

    def update_gp_regression(self,
                             n_explore,
                             n_warmup,
                             acq_optimizer: pymanopt_optimizer
                             ):
        x_new, acq_max = self.argmax_acq(n_explore, n_warmup, acq_optimizer)
        y_new = self.black_box_function(x_new)
        self.gp_regression.add_train_data(x_new, y_new)
        self.consumed_budget += 1
        self.log(x_new, y_new, acq_max)

    def log(self, x_new, y_new, acq_max):
        print("A new acq_max = {} is found.".format(acq_max))
        print("A new y = {} is found.".format(y_new))
        if y_new > self.y_max:
            self.x_max = x_new
            self.y_max = y_new
            print("A new y_max = {} is found in the current iteration of BO.".format(self.y_max))

    def config_acq_func(self,
                        backend: Callable,
                        eval_mode=False):
        if eval_mode:
            self.acq_func = GP_Expected

            @backend(self.gp_regression.manifold)
            def opposite_acq_func(x):
                return -1.0 * self.acq_func(self.gp_regression, x)[0]
        else:
            @backend(self.gp_regression.manifold)
            def opposite_acq_func(x):
                return -1.0 * self.acq_func(self.gp_regression, x, self.cur_itr)[0]

        self.acq_optim_problem = pymanopt.Problem(manifold=self.gp_regression.manifold, cost=opposite_acq_func)

    def black_box_eval(self, x):
        return self.black_box_function(x)


def get_gp_ucb_kappa_test(manifold, alpha, beta):
    def kappa(t):
        """
        This form of kappa(t) follows the suggestion from the paper
        High Dimensional Bayesian Optimisation and Bandits via Additive Models
        (https://proceedings.mlr.press/v37/kandasamy15.html)
        Alpha should be in range [0.01, 0.1].
        """
        dim = manifold.dim
        diameter = 0.5 * math.pi * math.sqrt(manifold._p)
        return alpha * dim * math.log2(beta * diameter * t + 1)

    return kappa


def module_test(data_dir,
                n_pre_train=5,
                gp_itrs=100,
                bo_budget=100,
                bo_n_explore=1,
                bo_n_warmup=5000,
                gp_refit_interval=25,
                seed=0,
                ):
    '''
        module test for bayesian optimization, based on the fact that
        ExtrinsicGP.py is free of bugs.
        black box function is y(x) = geodesicDistance(x,a) - geodesicDistance(x,b)
    '''
    np.random.seed(seed)
    th.random.manual_seed(seed)
    Gn = 20
    Gp = 4
    manifold = Grassmann(n=Gn, p=Gp)
    # pre_train gp_model
    obs_noise = 1e-5
    if obs_noise <= 1e-4:
        noise_activate = False
    else:
        noise_activate = True
    from ExtrinsicGP import get_gp_model_test, get_black_box_test
    gp_model = get_gp_model_test(noise_activate)
    gp_regression = ExGP(gp_model,
                         manifold,
                         verbosity=0)
    black_box = get_black_box_test(n_pre_train, n_fixed_points=100, manifold=manifold, obs_noise=obs_noise)

    def black_box_on_Rn(x):
        x = x.reshape((Gn, Gp))
        q, r = np.linalg.qr(x)
        return -1.0 * black_box(np.expand_dims(q, axis=0))

    from scipy.optimize import basinhopping
    res = basinhopping(func=black_box_on_Rn, x0=np.zeros((Gn, Gp)), niter=1)
    print(f"ymax found by basinhopping={res.fun}")

    BO = ManBO(gp_regression=gp_regression,
               acq_func=lambda model, x, t: GP_UCB(model, x, t, get_gp_ucb_kappa_test(manifold)),
               black_box_function=black_box,
               verbosity=0)
    from UpperLevelControl.ExtrinsicGP import set_GP_randomly
    gp_optim_info = {'lr': 5 * 1e-3, 'history_size': 200}
    gp_optim_type = "LBFGS"
    set_GP_randomly(BO.gp_regression, BO.black_box_function, n_pre_train,
                    itrs=gp_itrs, optim_type=gp_optim_type, optim_info=gp_optim_info)
    BO(bo_budget,
       bo_n_explore,
       bo_n_warmup,
       gp_itrs=gp_itrs,
       gp_optim_type=gp_optim_type,
       gp_optim_info=gp_optim_info,
       acq_optimizer=pymanopt.optimizers.SteepestDescent(verbosity=0),
       backend=pytorch_backend,
       gp_refit_interval=gp_refit_interval,
       save_interval=5,
       dir=data_dir,
       suffix='test')
    x_eval, y_eval = BO.evaluate(
        step_size=5,
        base_size=n_pre_train,
        n_explore=bo_n_explore,
        n_warmup=bo_n_warmup,
        acq_optimizer=pymanopt.optimizers.SteepestDescent(verbosity=0),
        backend=pytorch_backend)
    print(x_eval.shape)
    print(y_eval.shape)
    np.save(data_dir + "x_eval", x_eval)
    np.save(data_dir + "y_eval", y_eval)


if __name__ == '__main__':
    data_save_dir = '../../../bo_test_toy_function_init_25_update_25_alpha_0/'
    if not os.path.exists(data_save_dir):
        os.makedirs(data_save_dir)
    module_test(data_save_dir, bo_budget=300, n_pre_train=25)
    Y_eval = np.load(data_save_dir + "y_eval.npy")
    Y_train = np.load(data_save_dir + "GP_train_targets_test.npy")
    import matplotlib.pyplot as plt

    plt.plot(Y_train[25:])
    # plt.plot(y_eval)
    # plt.plot(y_train[-bo_itrs:])
    plt.show()
