import os.path

import gpytorch as gp
import torch.nn
from gpytorch.models import ExactGP
from pymanopt.manifolds.manifold import Manifold
import numpy as np
import torch as th
# from typing import Literal
from UpperLevelControl._Manifolds import Grassmann
from UpperLevelControl.BlackBoxes import geodesticDistanceDifference
import time
from _Utils import format_time, plot_GPR_res


def furthest_sample(S, remaining_set, measure):
    """
    Furthest sampling can be used in sampling points on manifold before training GP.
    However, we found furthest sampling does not effectively improve performance in our problem.
    """
    sampled_set = np.zeros_like(remaining_set[:S])
    idx = np.random.random_integers(0, len(remaining_set) - 1)
    sampled_set[0] = remaining_set[idx]
    dist_to_sampled_set = measure(remaining_set, sampled_set[0])
    for i in range(S - 1):
        idx = dist_to_sampled_set.argmax()
        sampled_set[i + 1] = remaining_set[idx]
        dist_to_sampled_set = np.minimum(measure(remaining_set, sampled_set[i + 1]), dist_to_sampled_set)
    return sampled_set


class GPModel(ExactGP):
    def __init__(self,
                 mean: gp.means.Mean,
                 kernel: gp.kernels.Kernel,
                 likelihood: gp.likelihoods.Likelihood,
                 train_x=None,
                 train_y=None,
                 ):
        super(GPModel, self).__init__(train_x, train_y, likelihood)
        self.mean = mean
        self.kernel = kernel

    def forward(self, x):
        mean = self.mean(x)
        cov = self.kernel(x)
        return gp.distributions.MultivariateNormal(mean, cov)

    def mute_likelihood(self):
        self.likelihood.noise = 1e-4
        self.likelihood.requires_grad_(False)

    def get_truncated_model(self, length):
        truncated_x = self.train_inputs[0][:length]
        truncated_y = self.train_targets[:length]
        truncated_model = GPModel(self.mean, self.kernel, self.likelihood, train_x=truncated_x, train_y=truncated_y)
        return truncated_model

    def scale_kernel(self, info_scale_kernel: dict = {}, info_scale_kernel_init: dict = {}):
        self.kernel = gp.kernels.ScaleKernel(self.kernel, **info_scale_kernel)
        self.kernel.initialize(**info_scale_kernel_init)

    def combine_with_kernel(self,
                            operation, #: Literal['product', 'add']
                            another_kernel
                            ):
        if operation == 'product':
            self.kernel = self.kernel * another_kernel
        else:
            assert operation == 'add'
            self.kernel = self.kernel + another_kernel


def pytorch_train(itrs,
                  loss,
                  solver: th.optim.Optimizer,
                  callback,
                  verbosity=0):
    def closure():
        solver.zero_grad()
        loss_val = loss()
        loss_val.backward()
        return loss_val

    for i in range(itrs):
        solver.step(closure)
        if verbosity:
            print("The {}-th iteration of pytorch_train finished.".format(i))
        callback()


class OnlineStandardization:
    def __init__(self, mean, var_sum, size):
        self.mean = mean
        self.var_sum = var_sum
        self.size = size

    def update(self, new_entries):
        for new_entry in new_entries:
            self.size += 1
            delta = new_entry - self.mean
            self.mean += delta / self.size
            delta_ = new_entry - self.mean
            self.var_sum += delta * delta_

    @property
    def std(self):
        return np.sqrt(self.var_sum / (self.size - 1))

    def __call__(self, data):
        return (data - self.mean) / self.std


class ExGP:
    """
    1. Extrinsic Gaussian Process Regression introduced in https://par.nsf.gov/servlets/purl/10089755 is implemented.
       The points on manifold are first embedded in R^N equivariantly, then, GPR can be done
       with the kernels provided by GPytorch.
    2. train_x in this class saves original points on manifold.
       All points on manifold are not embedded in R^N until they have to.
    3. All values are initially numpy arrays and not converted to tensor until they have to.
    4. As for our problem, the black box function to be regressed is a function from Grassmann Manifold to R.
    """

    def __init__(self,
                 gp_model,
                 manifold,
                 verbosity=1):
        self.gp_model = gp_model
        self.manifold = manifold
        self.verbosity = verbosity
        self.fitted = False
        self.mode = 'idle'
        self.train_inputs = None
        self.train_targets = None
        self.targets_standardizer = None

    def __call__(self, *args, **kwargs):
        return self.fit(*args, **kwargs)

    def embed_to_euclidean(self, inputs) -> th.Tensor:
        if isinstance(inputs, np.ndarray):
            inputs = th.from_numpy(inputs)
        mat_res = self.manifold.eq_embed_to_euclidean(inputs)
        vec_res = mat_res.reshape(*mat_res.shape[:-2], mat_res.shape[-2] * mat_res.shape[-1])
        return vec_res

    def train_mode(self):
        self.gp_model.train()
        self.gp_model.likelihood.train()
        self.mode = 'train'

    def eval_mode(self):
        self.gp_model.eval()
        self.gp_model.likelihood.eval()
        self.mode = 'eval'

    def set_train_data(self, train_inputs, train_targets, targets_standardizer=None):
        self.train_inputs = train_inputs
        train_inputs_Euclidean = self.embed_to_euclidean(self.train_inputs)
        self.train_targets = train_targets
        if targets_standardizer is not None:
            self.targets_standardizer = targets_standardizer
        else:
            self.targets_standardizer = OnlineStandardization(
                mean=np.mean(train_targets, axis=-1),
                var_sum=np.var(train_targets, ddof=len(train_targets) - 1, axis=-1),
                size=len(train_targets)
            )
        train_targets_standardized = self.targets_standardizer(self.train_targets)
        self.gp_model.set_train_data(train_inputs_Euclidean, th.from_numpy(train_targets_standardized), strict=False)

    def cache_train_data(self, new_train_inputs, new_train_targets):
        self.train_inputs = np.concatenate((self.train_inputs, new_train_inputs))
        self.train_targets = np.concatenate((self.train_targets, new_train_targets))

    def add_train_data(self, new_train_inputs, new_train_targets):
        self.cache_train_data(new_train_inputs, new_train_targets)
        new_train_inputs_Euclidean = self.embed_to_euclidean(new_train_inputs)
        # type(self.gp_model.train_inputs) is tuple
        train_inputs_Euclidean = self.gp_model.train_inputs[0]
        train_inputs_Euclidean = th.cat((train_inputs_Euclidean, new_train_inputs_Euclidean))
        self.targets_standardizer.update(new_train_targets)
        train_targets_standardized = self.targets_standardizer(self.train_targets)
        self.gp_model.set_train_data(train_inputs_Euclidean, th.from_numpy(train_targets_standardized), strict=False)

    def get_truncated_model(self, length, targets_standardizer=None):
        """
        This method is used only in BO.evaluate(), so targets_standardizer of
        truncated_model can be ignored, and there's no need to restore
        targets in truncated_gp_model with old mean and old var.
        """
        truncated_gp_model = self.gp_model.get_truncated_model(length)
        truncated_model = ExGP(truncated_gp_model, self.manifold, self.verbosity)
        truncated_model.train_inputs = self.train_inputs[:length]
        truncated_model.train_targets = self.train_targets[:length]
        # truncated_model.targets_standardizer = self.targets_standardizer
        """
        truncated_model.targets_standardizer = OnlineStandardization(
            np.mean(truncated_model.train_targets),
            np.var(truncated_model.train_targets, ddof=len(truncated_model.train_targets)-1),
            length
        ) if targets_standardizer is None else targets_standardizer
        """
        return truncated_model

    def fit(self, itrs, solver_type: str, info_solver: dict):
        if self.mode != 'train':
            self.train_mode()
        mll = gp.mlls.ExactMarginalLogLikelihood(model=self.gp_model, likelihood=self.gp_model.likelihood)
        solver = getattr(th.optim, solver_type)(self.gp_model.parameters(), **info_solver)
        # type(self.gp_model.train_inputs) is tuple
        train_inputs = self.gp_model.train_inputs[0]
        train_targets = self.gp_model.train_targets

        def loss():
            train_targets_hat = self.gp_model(train_inputs)
            return -1.0 * mll(train_targets_hat, train_targets)

        def callback():
            if self.verbosity:
                self.log()

        print("================Fit GP's Hyper-parameters===============")
        fit_start_time = time.time()
        pytorch_train(itrs,
                      loss,
                      solver,
                      callback=callback,
                      verbosity=self.verbosity)
        fit_end_time = time.time()
        format_time(fit_end_time - fit_start_time, f"Fitting hyper-parameters of GP {itrs} iterations")
        self.fitted = True

    def evaluate(self, eval_inputs, noise_included=True):
        if self.mode != 'eval':
            self.eval_mode()
        eval_inputs_Euclidean = self.embed_to_euclidean(eval_inputs)
        f_pred = self.gp_model(eval_inputs_Euclidean)
        if noise_included:
            y_pred = self.gp_model.likelihood(f_pred)
            return f_pred, y_pred
        else:
            return f_pred

    def log(self):
        for param_name, param, constraint in self.gp_model.named_parameters_and_constraints():
            actual_value = constraint.transform(
                param).detach().numpy() if constraint is not None else param.detach().numpy()
            str1 = f'Parameter name: {param_name:42}actual value = {actual_value}'
            str2 = f'grad = {param.grad.detach().numpy()}' if param.grad is not None else f'grad = None'
            print(str1 + '\t\t' + str2)
        print('\n')

    def save_data(self, dir: str = './', suffix: str = ''):
        np.save(dir + 'GP_train_x_' + suffix, self.train_inputs)
        np.save(dir + 'GP_train_targets_' + suffix, self.train_targets)

    def save_model(self, dir: str = './', suffix: str = ''):
        th.save(self.gp_model.state_dict(), dir + 'GP_model_state_' + suffix)

    @property
    def data_size(self):
        return self.train_targets.shape[0]

    @property
    def x(self):
        return self.train_inputs

    @property
    def y(self):
        return self.train_targets

    @property
    def regularized_kernel(self):
        """
        kernel.forward(x1,x2) only do computations between legal inputs.
        kernel.__call__(x1,x2) == preprocess(x1,x2) and kernel.forward(x1,x2)
        """
        if isinstance(self.gp_model.kernel, gp.kernels.ScaleKernel):
            kernel = self.gp_model.kernel.base_kernel
        else:
            kernel = self.gp_model.kernel
        return lambda x1, x2: kernel.forward(th.from_numpy(x1), th.from_numpy(x2)).detach().numpy()

    @property
    def kernel_scale(self):
        ret = self.get_gp_parameter("kernel.outputscale") if isinstance(self.kernel, gp.kernels.ScaleKernel) else 1.0
        return ret

    @property
    def kernel(self):
        return self.gp_model.kernel

    def reset(self):
        """
        To reset self.gp_model,
        we need to call self.set_train_data() in the outer level.
        """
        self.fitted = False
        self.mode = 'idle'
        self.train_inputs = None
        self.train_targets = None
        self.targets_standardizer = None

    def get_gp_parameter(self, target: str):
        module_path, _, param_name = target.rpartition('.')
        mod = self.gp_model.get_submodule(module_path)
        if not hasattr(mod, param_name):
            raise AttributeError(mod._get_name() + " has no attribute `"
                                 + param_name + "`")
        return getattr(mod, param_name).detach().numpy()


def get_gp_model_test(noise_activate=True):
    gp_model = GPModel(mean=gp.means.ConstantMean(),
                       kernel=gp.kernels.RBFKernel(lengthscale_prior=gp.priors.GammaPrior(2.0, 0.4)),
                       likelihood=gp.likelihoods.GaussianLikelihood(),
                       ).double()
    if not noise_activate:
        gp_model.mute_likelihood()
    gp_model.initialize(**{'kernel.lengthscale': 0.69})
    gp_model.scale_kernel(info_scale_kernel={'outputscale_prior': gp.priors.GammaPrior(2.0, 2.0)},
                          info_scale_kernel_init={'outputscale': 1.0})
    return gp_model


def get_black_box_test(n_train_points, n_fixed_points, manifold, obs_noise):
    from BlackBoxes import geodesicDistanceAvg
    fixed_points = manifold.random_point(size=n_fixed_points)

    def black_box(x):
        return geodesicDistanceAvg(x,
                                   sample_rate=np.ones((n_train_points, 1)),
                                   fixed_points=fixed_points,
                                   noise=obs_noise)

    return black_box


def set_GP_randomly(gp_regression,
                    black_box_function,
                    data_size,
                    distribution="uniform",
                    fit_gp=True,
                    itrs=100,
                    optim_type="LBFGS",
                    optim_info: dict = {}
                    ):
    train_x = gp_regression.manifold.random_point(size=data_size, distribution=distribution)
    train_y = black_box_function(train_x)
    gp_regression.set_train_data(train_x, train_y)
    if fit_gp:
        gp_regression(itrs, optim_type, optim_info)


def module_test(data_path,
                n_train_points,
                n_test_points,
                gp_itrs=100,
                seed=0):
    """
            module test for ExtrinsicGP.py.
            black box function is y(x) = geodesicDistance(x,a) - geodesicDistance(x,b)
    """
    np.random.seed(seed)
    th.random.manual_seed(seed)
    Gn = 56
    Gp = 7
    manifold = Grassmann(n=Gn, p=Gp)
    obs_noise = 1e-5
    if obs_noise <= 1e-4:
        noise_activate = False
    else:
        noise_activate = True
    gp_model = get_gp_model_test(noise_activate)
    gp_regression = ExGP(gp_model,
                         manifold,
                         1)
    black_box = get_black_box_test(n_train_points, n_fixed_points=100, manifold=manifold, obs_noise=obs_noise)
    print('First train hyperparameters on {} random samples.'.format(n_train_points))
    set_GP_randomly(gp_regression, black_box, n_train_points,
                    itrs=gp_itrs, optim_type="LBFGS",
                    optim_info={'lr': 5 * 1e-3,
                                'history_size': 200})
    print('Then test on {} random samples.'.format(n_test_points))
    test_x = manifold.random_point(size=n_test_points, distribution='uniform')
    test_y = black_box(test_x)
    test_y = gp_regression.targets_standardizer(test_y)
    fpred, ypred = gp_regression.evaluate(test_x)
    # plot
    plot_GPR_res(test_y, fpred, data_path, '1')


if __name__ == '__main__':
    data_save_path = '../../../local_test_data/'
    if not os.path.exists(data_save_path):
        os.makedirs(data_save_path)
    module_test(data_save_path,
                2000,
                50,
                seed=0)
