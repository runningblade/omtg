import sys, os
import numpy as np
import math

sys.path.append('..')
from SoftRobotEnv.ReducedSoftRobotUtils import BASE

sys.path.append(BASE)
import pyPhysicsMotion as pm
from SoftRobotEnv.ReducedSoftRobot import ReducedSoftRobotVersatile
from LowerLevelControl.ControlSystem import ControlSystem, save_frames
from typing import Iterable, Union
from UpperLevelControl._Manifolds import Grassmann
import time
from _Utils import format_time
from LowerLevelControl.SignalInterpreter import BezierCurveGenerator

def geodesticDistanceDifference(x, a, b, noise=0.0, manifold=Grassmann):
    """
    toy function to test gaussian process and bayesian optimization.
    The optimal x should be the middle point of a and b on Manifold
    in terms of geodestic distance.
    """
    d_x_a = manifold.dist(x, a)
    d_x_b = manifold.dist(x, b)
    difference = (d_x_a - d_x_b)
    if noise > 1e-4:
        difference += np.random.normal(0, noise, difference.shape)
    return -1.0 * np.abs(difference)


def geodesicDistanceAvg(points, sample_rate, fixed_points, noise=0.0, manifold=Grassmann, high_fidelity=False):
    """
    toy function to test multi-fidelity gaussian process and multi-fidelity bayesian optimization.
    The optimal x should be the geometric mean of fixed points on Manifold.
    """
    n_fixed = len(fixed_points)
    n_inputs = len(points)
    sample_size = (sample_rate * n_fixed).astype(int)
    sample_reps = (1.0/sample_rate).astype(int)[:,0]
    d_avg = np.zeros(n_inputs)
    if high_fidelity:
        for i in range(n_inputs):
            d_avg_i = 0
            tot = sample_reps[i] * sample_size[i]
            for j in range(sample_reps[i]):
                idxs = np.random.choice(a=n_fixed, size=sample_size[i], replace=False)
                d_avg_i += np.sum(manifold.dist(points[i], fixed_points[idxs]), axis=-1) / tot
            d_avg[i] = d_avg_i
    else:
        for i in range(n_inputs):
            idxs = np.random.choice(a=n_fixed, size=sample_size[i], replace=False)
            d_avg[i] = np.sum(manifold.dist(points[i], fixed_points[idxs]), axis=-1) / sample_size[i]
    if noise > 1e-4:
        d_avg += np.random.normal(0, noise, d_avg.shape)
    return -1.0 * d_avg


class TrajectoryGenerator:
    def __init__(self,
                 ctrl_sys,
                 target_steps,
                 scale_reward=False,
                 save_dir='./',
                 verbosity=0):
        self.ctrl_sys = ctrl_sys
        self.target_steps = target_steps
        self.save_dir = save_dir
        if not os.path.exists(save_dir):
            os.mkdir(save_dir)
        self.cnt = 0
        self.verbosity = verbosity
        self.scale = scale_reward

    def __call__(self, *args, **kwargs):
        return self.step(*args, **kwargs)

    def preprocess(self, num_agents):
        """
        In base class TrajectoryGenerator, num_agents == 1.
        However, in derived class MultiAgentTrajectoryGenerator, this method is reused, and num_agents > 1
        """
        self.ctrl_sys.reset(num_agents)
        self.ctrl_sys.controller.switch_num_agents(num_agents)
        self.ctrl_sys.controller.reset()
        if num_agents <= self.ctrl_sys.env.num_agents:
            self.ctrl_sys.env.truncate_agents(self.ctrl_sys.env.num_agents - num_agents)
        else:
            self.ctrl_sys.env.repeat_last_agents(num_agents - self.ctrl_sys.env.num_agents + 1)
        signal_interpreter_ctrl_point_shape = (self.ctrl_sys.env.ctrl_dim,) if num_agents == 1 \
            else (num_agents, self.ctrl_sys.env.ctrl_dim)
        self.ctrl_sys.signal_interpreter.set_default(signal_interpreter_ctrl_point_shape)

    def step(self, design, trj_rate):
        """
                black_box_function here is a function from R^(n x m) to R.
                Given a design matrix D, a maximum reward under this D should be computed using MPPI controller.
                Make full use of multi-step interface to sample the points for pre-training of GPR.
            """
        num_agents = design.shape[0]
        self.preprocess(num_agents)
        self.ctrl_sys.env.set_design(design)
        reward = self.generate_trajectory(trj_rate)
        self.save_trajectory()
        self.cnt += 1
        if self.scale:
            reward /= trj_rate
        return reward

    def reset(self,
              save_dir=None,
              verbosity=None,
              scale_reward=None):
        self.cnt = 0
        if save_dir is not None:
            self.save_dir = save_dir
        if verbosity is not None:
            self.verbosity = verbosity
        if scale_reward is not None:
            self.scale = scale_reward

    def generate_trajectory(self, trj_rate):
        state = self.ctrl_sys.env.init_pos
        steps = round(trj_rate * self.target_steps)
        tot_reward = np.array([0.0])
        eval_start_time = time.time()
        for t in range(steps):
            state, reward = self.ctrl_sys.step(state)
            tot_reward += reward
            if self.verbosity:
                print(
                    f"Optimization of the {t} decision step finished. Step reward is {reward}, accumulated reward is {tot_reward}")
        eval_end_time = time.time()
        format_time(eval_end_time - eval_start_time, f"Evaluation of a trajectory of length {steps}")
        return tot_reward

    def save_trajectory(self):
        save_frames(actions=self.ctrl_sys.ctrl_signal_buffer,
                    states=self.ctrl_sys.state_buffer,
                    save_dir=self.save_dir,
                    suffix=str(self.cnt))


class MultiAgentTrajectoryGenerator(TrajectoryGenerator):
    def __init__(self, *args, **kwargs):
        super(MultiAgentTrajectoryGenerator, self).__init__(*args, **kwargs)
        self.stop_points = [0]
        self.num_agents_stopped = [0]

    def step(self, design, trj_rate):
        """
               Important: we suppose trj_rate are sorted in ascending order.
        """
        assert (len(trj_rate) > 1) and (trj_rate == np.sort(trj_rate)).all()
        return super(MultiAgentTrajectoryGenerator, self).step(design, trj_rate)

    def mark_stop_points(self, steps):
        self.stop_points = [0]
        self.num_agents_stopped = [0]
        current_stop_point = steps[0]
        num_agent_to_stop = 0
        for step in steps:
            # a new stop point appears
            if step > current_stop_point:
                # append the older stop point, and the corresponding number of agents stopped
                self.stop_points.append(current_stop_point)
                self.num_agents_stopped.append(num_agent_to_stop)
                # start counting the number of agents stopped at the new stop point
                current_stop_point = step
                num_agent_to_stop = 0
            # the number of agents stopped at the current stop point increases by 1
            num_agent_to_stop += 1
        # append the last stop point, and the corresponding number of agents stopped
        self.stop_points.append(current_stop_point)
        self.num_agents_stopped.append(num_agent_to_stop)

    def generate_trajectory(self, trj_rate):
        # mark stop points before simulation
        steps = np.round((np.array(trj_rate) * self.target_steps)).astype(int)
        self.mark_stop_points(steps)
        # initialize data for simulation
        state = [self.ctrl_sys.env.init_pos] * len(trj_rate)
        tot_reward = np.zeros(len(trj_rate))
        num_stop_points = len(self.stop_points)
        # simulation begins
        eval_start_time = time.time()
        for i in range(num_stop_points - 1):
            # stop self.num_agents_stopped[i] agents at time self.stop_points[i]
            state = self.stop_agents(state, i)
            # forward simulation
            steps_forward = self.stop_points[i + 1] - self.stop_points[i]
            for t in range(steps_forward):
                state, reward = self.ctrl_sys.step(state)
                tot_reward[-self.ctrl_sys.controller.num_agents:] = tot_reward[
                                                                    -self.ctrl_sys.controller.num_agents:] + reward
                if self.verbosity:
                    print(f"Optimization of the {self.stop_points[i] + t} decision step finished. "
                          f"Step reward is {reward}, accumulated reward is {tot_reward}")
            if self.verbosity:
                print(
                    f"Optimization of {self.num_agents_stopped[i + 1]} trajectories of "
                    f"length {self.stop_points[i + 1]} finished.")
        eval_end_time = time.time()
        format_time(eval_end_time - eval_start_time, f"Evaluation of {len(trj_rate)} trajectory of length {steps}")
        return tot_reward

    def stop_agents(self, state, idx_stop_points):
        # truncate state
        state = state[self.num_agents_stopped[idx_stop_points]:]
        # truncate agents
        self.ctrl_sys.env.truncate_agents(self.num_agents_stopped[idx_stop_points], truncate_rear=False)
        # set controller and control signal interpreter to adapt the new number of agents
        self.ctrl_sys.signal_interpreter.truncate(self.num_agents_stopped[idx_stop_points], truncate_rear=False)
        n_remain_agents = len(state)
        self.ctrl_sys.controller.switch_num_agents(n_remain_agents, truncate_rear=False)
        self.ctrl_sys.reward.switch_num_agents(n_remain_agents, truncate_rear=False)
        # handle the special case when number of agents reduces to 1
        if n_remain_agents == 1:
            state = state[0]
            self.ctrl_sys.signal_interpreter.squeeze()
        return state

    def save_trajectory(self):
        """
        save states agent by agent
        """
        # compute total number of agents
        num_agents = 0
        num_stop_points = len(self.stop_points)
        for i in range(num_stop_points):
            num_agents += self.num_agents_stopped[i]
        # save states and ctrl signals agent by agent
        for i in range(num_agents):
            state_buffer = []
            # the first idx is cur_frame_step
            # 1 frame_step = self.ctrl_sys.state_save_interval * sim_step
            # 1 ctrl_step = self.ctrl_sys.signal_interpreter.n_actions * sim_step
            cur_sim_step = 0
            cur_frame_step = 0
            # the second idx is j
            cur_stop_point = 0
            j = i
            # for each agent, save states frame_step by frame_step
            while cur_frame_step < len(self.ctrl_sys.state_buffer):
                # when there are agents stopped, we decrease j
                while cur_sim_step >= self.stop_points[cur_stop_point] * self.ctrl_sys.signal_interpreter.n_actions:
                    j = j - self.num_agents_stopped[cur_stop_point]
                    cur_stop_point = cur_stop_point + 1
                # j < 0 means the simulation of the i-th agent is done when cur_saved_step
                if j < 0:
                    break
                if isinstance(self.ctrl_sys.state_buffer[cur_frame_step], pm.FEMGradientInfoFLOAT):
                    state_buffer.append(self.ctrl_sys.state_buffer[cur_frame_step])
                else:
                    state_buffer.append(self.ctrl_sys.state_buffer[cur_frame_step][j])
                # 1 frame_step = self.ctrl_sys.state_save_interval * sim_step
                cur_frame_step += 1
                cur_sim_step += self.ctrl_sys.state_save_interval
            ctrl_buffer = []
            # the first idx is cur_ctrl_step and the second idx is j
            cur_stop_point = 0
            j = i
            # for each agent, save ctrl signals ctrl_step by ctrl_step
            for cur_ctrl_step in range(len(self.ctrl_sys.ctrl_signal_buffer)):
                if cur_ctrl_step == self.stop_points[cur_stop_point]:
                    j = j - self.num_agents_stopped[cur_stop_point]
                    cur_stop_point = cur_stop_point + 1
                if j < 0:
                    break
                if isinstance(self.ctrl_sys.state_buffer[cur_ctrl_step], pm.FEMGradientInfoFLOAT):
                    ctrl_buffer.append(self.ctrl_sys.ctrl_signal_buffer[cur_ctrl_step])
                else:
                    ctrl_buffer.append(self.ctrl_sys.ctrl_signal_buffer[cur_ctrl_step][j])
            save_frames(states=state_buffer,
                        actions=ctrl_buffer,
                        save_dir=self.save_dir,
                        suffix=str(self.cnt)+"_"+str(i))


def generate_trajectory_for_BO(trajectory_generator):
    if isinstance(trajectory_generator, MultiAgentTrajectoryGenerator):
        def _black_box(x):
            return trajectory_generator(design=x, trj_rate=np.array([1.0] * x.shape[0]))
    else:
        def _black_box(x):
            return trajectory_generator(design=x, trj_rate=1.0)

    return _black_box


def generate_trajectory_for_BOCA(trajectory_generator):
    """
    fidelity is of shape (N, 1)
    """
    if isinstance(trajectory_generator, MultiAgentTrajectoryGenerator):
        def _black_box(x, fidelity):
            return trajectory_generator(design=x, trj_rate=fidelity.T[0])

    else:
        def _black_box(x, fidelity):
            return trajectory_generator(design=x, trj_rate=fidelity[0][0])

    return _black_box


def test_module(data_save_dir, test_fidelity=False):
    from _GenerateArgsParser import _GenerateArgsParser

    parser = _GenerateArgsParser("Lower")
    args = parser.parse_args()
    from LowerLevelControl.Optimize import get_naive_design
    benchmark_design = get_naive_design(args.basis_num, args.ctrl_dim)
    from LowerLevelControl.Optimize import get_control_system
    ctrl_sys = get_control_system(args, ctrl_reduced=True, design=benchmark_design)
    manifold = Grassmann(args.basis_num, args.ctrl_dim)
    target_steps = int(args.tot_time / args.ctrl_dt)
    trajectory_generator = TrajectoryGenerator(ctrl_sys,
                                               target_steps,
                                               save_dir=data_save_dir,
                                               verbosity=1)
    multi_agent_trajectory_generator = MultiAgentTrajectoryGenerator(ctrl_sys,
                                                                     target_steps,
                                                                     save_dir=data_save_dir,
                                                                     verbosity=1)
    bb_BO = generate_trajectory_for_BO(trajectory_generator)
    bb_BO_multi_agent = generate_trajectory_for_BO(multi_agent_trajectory_generator)
    bb_BOCA = generate_trajectory_for_BOCA(trajectory_generator)
    bb_BOCA_multi_agent = generate_trajectory_for_BOCA(multi_agent_trajectory_generator)
    n_agents = 5
    random_designs = manifold.random_point(size=n_agents - 1)
    benchmark_design = np.expand_dims(benchmark_design, axis=0)
    designs = np.concatenate((random_designs, benchmark_design), axis=0)
    fidelity = np.array([[0.5]])
    fidelity = np.array([[0.1], [0.2], [0.5], [0.5], [0.7]])
    print("All black boxes are initialized.")
    print(f"reward_multi_agent_bb={bb_BO_multi_agent(designs)}.\n")
    print(f"reward_single_agent_bb={bb_BO(benchmark_design)}.\n")
    if test_fidelity:
        print(f"reward_single_agent_fidelity_bb={bb_BOCA(benchmark_design, fidelity)}.\n"
              f"reward_multi_agent_fidelity_bb={bb_BOCA_multi_agent(designs, fidelity)}.\n")


if __name__ == "__main__":
    data_dir = '../../../local_test_data/'
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    test_module(data_dir)
