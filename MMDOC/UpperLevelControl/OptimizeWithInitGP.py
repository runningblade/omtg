import os
import shutil
import sys

import numpy as np
import pymanopt.optimizers
import torch as th

sys.path.append('..')
from _GenerateArgsParser import _GenerateArgsParser
from UpperLevelControl.Optimize import define_outer_problem
from LowerLevelControl.Optimize import get_control_system as define_inner_problem
from UpperLevelControl._Manifolds import Grassmann
from UpperLevelControl._Backends import pytorch_backend
from UpperLevelControl.BlackBoxes import TrajectoryGenerator, MultiAgentTrajectoryGenerator, \
    generate_trajectory_for_BO, generate_trajectory_for_BOCA
from UpperLevelControl.Evaluate import evaluate_BO

if __name__ == '__main__':
    '''
        Find optimal design matrix B using Bayesian Optimization
    '''
    seed = 4326510  # int(time.time())
    np.random.seed(seed)
    th.random.manual_seed(seed)
    load_data_dir = '../../../omtg_mmdoc_data/beam_walk_boca_mppi_2/'
    save_data_dir = '../../../omtg_mmdoc_data/mmdoc_train_data_cont/'
    if os.path.exists(save_data_dir):
        shutil.rmtree(save_data_dir)
    os.makedirs(save_data_dir)
    parser = _GenerateArgsParser('Upper')
    args = parser.parse_args()
    ctrl_sys = define_inner_problem(args, ctrl_reduced=True)
    manifold = Grassmann(n=args.basis_num, p=args.ctrl_dim)
    trajectory_generator = TrajectoryGenerator(ctrl_sys,
                                               scale_reward=True if args.ctrl_sys_reward_type == "x_linear" else False,
                                               target_steps=int(args.tot_time / args.ctrl_dt),
                                               save_dir=save_data_dir)
    BO = define_outer_problem(trajectory_generator, manifold, args)
    loaded_x = np.load(load_data_dir + 'GP_train_x_' + str(seed)+".npy")
    loaded_targets = np.load(load_data_dir + 'GP_train_targets_' + str(seed)+".npy")
    loaded_model_state_dict = th.load(load_data_dir + 'GP_model_state_' + str(seed))
    BO.gp_regression.gp_model.load_state_dict(loaded_model_state_dict, strict=True)
    if args.n_gp_base is not None:
        loaded_x = loaded_x[:args.n_gp_base]
        loaded_targets = loaded_targets[:args.n_gp_base]
    if args.boca:
        loaded_fidelity = np.load(load_data_dir + "GP_train_fidelity_" + str(seed) + ".npy")
        if args.n_gp_base is not None:
            loaded_fidelity = loaded_fidelity[:args.n_gp_base]
        loaded_inputs = {"x": loaded_x, "fidelity": loaded_fidelity}
    else:
        loaded_inputs = loaded_x
    BO.gp_regression.set_train_data(loaded_inputs, loaded_targets)
    BO.cur_itr = len(loaded_targets) - args.n_initialize
    acq_optimizer = getattr(pymanopt.optimizers, args.acq_optimizer)(verbosity=0)
    print("================Optimization of BO starts.===============")
    BO(budget=args.bo_budget,
       n_warmup=args.bo_n_warmup,
       n_explore=args.bo_n_explore,
       acq_optimizer=acq_optimizer,
       backend=pytorch_backend,
       gp_refit_interval=args.gp_refit_interval,
       gp_itrs=args.gp_itrs,
       gp_optim_type='LBFGS',
       gp_optim_info={'lr': 5 * 1e-3, 'history_size': 200},
       save_interval=args.save_interval,
       suffix=str(seed),
       dir=save_data_dir)
    '''
       Evaluate performance of Bayesian Optimization
    '''
    eval_data_dir = "../../../omtg_mmdoc_data/mmdoc_eval_data_cont/"
    if os.path.exists(eval_data_dir):
        shutil.rmtree(eval_data_dir)
    os.makedirs(eval_data_dir)
    multi_agent_trajectory_generator = MultiAgentTrajectoryGenerator(ctrl_sys,
                                                                     target_steps=int(args.tot_time / args.ctrl_dt),
                                                                     save_dir=eval_data_dir,
                                                                     scale_reward=True if args.ctrl_sys_reward_type == "x_linear" else False,
                                                                     verbosity=1)
    evaluate_BO(BO,
                seed=16345607,
                step_size=args.eval_step_size,
                n_warmup=args.bo_n_warmup,
                n_explore=args.bo_n_explore,
                acq_optimizer=acq_optimizer,
                black_box_function=generate_trajectory_for_BO(
                    multi_agent_trajectory_generator) if not args.boca and not args.mf_gp_ucb else generate_trajectory_for_BOCA(
                    multi_agent_trajectory_generator),
                save_dir=eval_data_dir)
    if args.boca and args.resample_on_target_fidelity:
        resample_train_data_dir = '../../../omtg_mmdoc_data/mmdoc_resample_data_cont/'
        if os.path.exists(resample_train_data_dir):
            shutil.rmtree(resample_train_data_dir)
        os.makedirs(resample_train_data_dir)
        multi_agent_trajectory_generator.reset(save_dir=resample_train_data_dir)
        from UpperLevelControl.Optimize import resample_on_target_fidelity
        resample_on_target_fidelity(BO,
                                    generate_trajectory_for_BOCA(multi_agent_trajectory_generator),
                                    resample_train_data_dir,
                                    str(seed))
