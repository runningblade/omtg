import sys

sys.path.append('..')
from UpperLevelControl.BlackBoxes import MultiAgentTrajectoryGenerator
from UpperLevelControl._Backends import pytorch_backend
import numpy as np
import torch as th
from _Utils import plot_BO_res, load_data
from LowerLevelControl.ControlSystem import load_frames
from Render.PovRayRenderer import povray_render
from _GenerateArgsParser import _GenerateArgsParser
from LowerLevelControl.Optimize import get_control_system
import math


def evaluate_BO(BO,
                seed,
                step_size,
                n_warmup,
                n_explore,
                acq_optimizer,
                backend=pytorch_backend,
                black_box_function=None,
                save_dir='./'):
    if black_box_function is not None:
        BO.black_box_function = black_box_function
    np.random.seed(seed)
    th.random.manual_seed(seed)
    print("================Evaluation of BO starts.===============")
    x_eval, y_eval = BO.evaluate(step_size=step_size,
                                 n_warmup=n_warmup,
                                 n_explore=n_explore,
                                 acq_optimizer=acq_optimizer,
                                 backend=backend)
    np.save(save_dir + 'BO_eval_x_' + str(seed), x_eval)
    np.save(save_dir + 'BO_eval_targets_' + str(seed), y_eval)


def benchmark_states(idx, _data_dir):
    _ctrl_sys = get_control_system(args, ctrl_reduced=True, design=None)
    _, _states = load_frames(_ctrl_sys.env.world,
                             _data_dir,
                             "0_" + str(idx))
    return _ctrl_sys, _states


def best_performance_states(_x,
                            _y,
                            _data_dir,
                            _fidelity=None):
    max_y = -math.inf
    max_idx = None
    if _fidelity is not None:
        for i in range(len(_y)):
            if _fidelity[i] == 1 and _y[i] > max_y:
                max_y = _y[i]
                max_idx = i
    else:
        max_idx = np.argmax(_y)
    assert max_idx is not None
    print(_y[max_idx])
    _ctrl_sys = get_control_system(args, ctrl_reduced=True, design=_x[max_idx])
    _, _states = load_frames(world=_ctrl_sys.env.world,
                             load_dir=_data_dir,
                             suffix=str(max_idx))
    return _ctrl_sys, _states


if __name__ == "__main__":
    version = ""
    parent_dir = "../../../omtg_mmdoc_data/"
    data_name = "tripod_walk_bo_siLQG_225" + version
    data_dir = parent_dir + data_name + "/"
    render_quality = 4
    mode = "render"
    terrain = "Plane"
    n_init_points = 5
    fidelities, f_paths = load_data(np.load, data_dir + "*fidelity*")
    # ys, y_paths = load_data(np.load, data_dir + "test_data*/" + "*y_buffer*")
    ys, y_paths = load_data(np.load, data_dir + "*targets*")
    xs, x_paths = load_data(np.load, data_dir + "*GP_train_x*")

    for i in range(len(ys)):
        if xs is not None:
            xs[i] = xs[i][n_init_points:]
        ys[i] = ys[i][n_init_points:]
        max_idx = np.argmax(ys[i])
        max_y = np.max(ys[i])
        print(max_y)
        print(max_idx)
        if fidelities is not None:
            print(fidelities[i][max_idx])
    if fidelities is not None:
        for i in range(len(fidelities)):
            fidelities[i] = fidelities[i][n_init_points:]
    boca_complete = True
    if mode == "plot":
        if fidelities is not None and not boca_complete:
            for i in range(len(fidelities)):
                ys[i] = ys[i][(fidelities[i] == 1)[..., 0]]
                print(len(ys[i]), len(fidelities[i]))
                print(np.max(ys[i]))
        if ys is not None:
            y_flairs = ["bo"]
            y_name_suffix = "bo_data_1"
            plot_BO_res(ys=ys,
                        title=version + "_" + y_name_suffix,
                        xlabel="budget",
                        ylabel="reward",
                        flairs=y_flairs,
                        save_dir=parent_dir,
                        suffix=y_name_suffix)
        if fidelities is not None:
            f_flairs = [path[path.find("data_"):path.find("data_") + len("data_x")] for path in f_paths]
            f_name_suffix = "fidelity_2"
            plot_BO_res(ys=fidelities,
                        title=version + "_" + f_name_suffix,
                        xlabel="#iterations",
                        ylabel="fidelity",
                        flairs=f_flairs,
                        # fmts=['.'],
                        save_dir=parent_dir,
                        suffix=f_name_suffix)
    elif mode == "render":
        parser = _GenerateArgsParser("Lower")
        args = parser.parse_args()
        y = ys[0]
        x = xs[0]
        choices = ["best", "benchmark"]
        choice = choices[1]
        if choice == "best":
            if fidelities is not None:
                fidelity = fidelities[0]
            else:
                fidelity = None
            ctrl_sys, states = best_performance_states(x,
                                                       y,
                                                       data_dir,
                                                       fidelity)
        elif choice == "benchmark":
            ctrl_sys, states = benchmark_states(4,
                                                data_dir)
        povray_render(ctrl_sys.env,
                      states,
                      parent_dir + data_name + "_" + choice + "/",
                      ['LowControl'],
                      [[0.8, 0.8, 0.8]],
                      [[0, 0, 0]],
                      terrain,  # args.terrain[0],
                      camera_location=[4, 0, 8],
                      camera_look_at=[4, 0, 0],
                      quality=render_quality,
                      camera_rotate=0,
                      fps=args.fps,
                      level=args.level)
