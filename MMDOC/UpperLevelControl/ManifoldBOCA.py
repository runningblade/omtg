import math
import time
from abc import abstractmethod, ABC
from typing import Callable
import os
from BlackBoxes import geodesicDistanceAvg
import numpy as np
import pymanopt
import torch as th
import gpytorch as gp
from pymanopt.optimizers.optimizer import Optimizer as pymanopt_optimizer
from ManifoldBO import ManBOBase
from ManifoldBO import GP_UCB, _GP_Expected, GP_Expected, _GP_Sigma_Post
from _Utils import format_time, NumericalWarning
from _Manifolds import Grassmann
from _Backends import pytorch_backend
from ExtrinsicGP import GPModel
from ContinuousFidelityExGP import ContFdExGP
import warnings

fidelity_tol = 1e-6


class BOCANumericalWarning(NumericalWarning):
    pass


uncertainty_bound_max = 32
uncertainty_bound_min = 0.0625
uncertainty_bound_init_pow = -3


class ManBOCA(ManBOBase):
    """
    This class implements BOCA algorithm (https://arxiv.org/pdf/1703.06240.pdf) on Riemann manifolds.
    Note that this algorithm can handle both discrete fidelities and continuous fidelities,
    and information is shared among fidelities.
    Moreover, this algorithm also needs heuristics to trade off between
    exploration in lower fidelities and exploitation in the target fidelity.
    """

    def __init__(self,
                 cost,
                 gp_ucb_kappa: Callable[[int], float],
                 gap,
                 uncertainty_bound: float = None,
                 grid_unit_length=0.1,
                 fidelity_dim=1,
                 lowest_fidelity=None,
                 do_heuristics=True,
                 heuristic_window_size=20,
                 uncertainty_bound_decrease=0.5,
                 uncertainty_bound_increase=2.0,
                 **base_class_kwargs):
        super(ManBOCA, self).__init__(**base_class_kwargs)
        self.cost = cost
        self.gp_ucb_kappa = gp_ucb_kappa
        self.gap = gap
        if uncertainty_bound is not None:
            if uncertainty_bound > uncertainty_bound_max:
                warnings.warn(f"Uncertainty bound should be not greater than {uncertainty_bound_max},"
                              f"and is forced to be {uncertainty_bound_max} due to the violation.")
                uncertainty_bound = uncertainty_bound_max
            elif uncertainty_bound < uncertainty_bound_min:
                warnings.warn(f"Uncertainty bound should be no smaller than {uncertainty_bound_min},"
                              f"and is forced to be {uncertainty_bound_min} due to the violation.")
                uncertainty_bound = uncertainty_bound_min
        self.uncertainty_bound = uncertainty_bound
        self.num_stuck_evals = 0
        self.do_heuristics = do_heuristics
        if fidelity_dim > 3:
            warnings.warn("Fidelity should be low-dimensional. "
                          "We recommend the dimension of fidelity is no greater than 3."
                          "Otherwise, performance is not guaranteed", BOCANumericalWarning)
        if uncertainty_bound_increase < 1.0 + 1e-6:
            warnings.warn("Uncertainty_bound_increase should be greater than 1, and "
                          "is forced to be 2 due to the violation.", BOCANumericalWarning)
            uncertainty_bound_increase = 2.0
        if uncertainty_bound_decrease > 1.0 - 1e-6:
            warnings.warn("Uncertainty_bound_decrease should be smaller than 1, and "
                          "is forced to be 0.5 due to the violation.", BOCANumericalWarning)
            uncertainty_bound_decrease = 0.5
        self.uncertainty_bound_dec = uncertainty_bound_decrease
        self.uncertainty_bound_inc = uncertainty_bound_increase
        self.heuristic_window_size = heuristic_window_size
        self.target_fidelity = np.ones((1, fidelity_dim))
        self.grid_unit_length = np.array(grid_unit_length)
        if self.grid_unit_length.ndim == 0:
            self.grid_unit_length = self.grid_unit_length * self.target_fidelity
        elif self.grid_unit_length.ndim == 1:
            self.grid_unit_length = np.expand_dims(self.grid_unit_length, axis=0)
        if lowest_fidelity is None:
            self.lowest_fidelity = self.grid_unit_length.copy()
        else:
            self.lowest_fidelity = lowest_fidelity
        self.max_cost = self.cost(self.target_fidelity)
        # Note that 0 <= fidelity <= 1
        self.cost_ratio_exp = 1.0 / (2 + fidelity_dim + self.gp_regression.manifold.dim)

    def update_gp_regression(self,
                             n_explore,
                             n_warmup,
                             acq_optimizer: pymanopt_optimizer
                             ):
        x_new, acq_max = self.argmax_acq(n_explore, n_warmup, acq_optimizer)
        fidelity_new = self.select_fidelity(x_new)
        y_new = self.black_box_function(x_new, fidelity_new)
        self.gp_regression.add_train_data({'x': x_new, 'fidelity': fidelity_new}, y_new)
        self.consumed_budget += self.cost(fidelity_new)[0]
        self.log(x_new, y_new, acq_max, fidelity_new)
        if self.do_heuristics:
            self.heuristics()

    def log(self, x_new, y_new, acq_max, fidelity_new):
        print("A new acq_max = {} is found.".format(acq_max))
        print("A new y = {} is found when fidelity = {}.".format(y_new, fidelity_new))
        if fidelity_new == self.target_fidelity and y_new > self.y_max:
            self.x_max = x_new
            self.y_max = y_new
            print("A new y_max = {} is found on the target fidelity in current iteration of BO.".format(
                self.y_max))

    def select_fidelity(self, x):
        # build a grid
        all_fidelity_axes = []
        fidelity_dim = self.target_fidelity.shape[1]
        for i in range(fidelity_dim):
            all_fidelity_axes.append(np.arange(start=self.lowest_fidelity[0][i],
                                               stop=self.target_fidelity[0][i],
                                               step=self.grid_unit_length[0][i]))
        all_fidelity = np.stack(np.meshgrid(*all_fidelity_axes, indexing="ij"), axis=-1).reshape((-1, fidelity_dim))

        # filter fidelity
        max_gap = self.gap(self.lowest_fidelity)
        uncertainty_scale = np.sqrt(self.gp_regression.kernel_scale)
        if self.uncertainty_bound is None:
            lowest_fd_uncertainty = _GP_Sigma_Post(self.gp_regression,
                                                   {'x': x, 'fidelity': self.lowest_fidelity}) / uncertainty_scale
            lowest_fd_uncertainty *= np.power(self.max_cost / self.cost(self.lowest_fidelity), self.cost_ratio_exp) \
                                     / max_gap
            alpha = math.pow(self.uncertainty_bound_inc, uncertainty_bound_init_pow)
            self.uncertainty_bound = alpha * lowest_fd_uncertainty
            if self.verbosity:
                print(f"uncertainty_bound is initialized to {alpha} times the uncertainty of the lowest fidelity,"
                      f"thus sufficient samples can be acquired on the low fidelity")

        def _filter(_fd_arr):
            mask = np.zeros(len(_fd_arr), dtype=bool)
            for _i in range(len(_fd_arr)):
                cost = self.cost(_fd_arr[_i:_i + 1])
                real_gap = self.gap(_fd_arr[_i:_i + 1])
                relative_gap = real_gap / max_gap
                uncertainty = _GP_Sigma_Post(self.gp_regression,
                                             {'x': x, 'fidelity': _fd_arr[_i:_i + 1]}) / uncertainty_scale
                # print(f"UC={uncertainty}")
                uncertainty = uncertainty * np.power(self.max_cost / cost, self.cost_ratio_exp) / real_gap
                # print(f"UC={uncertainty}")
                # print(f"gap={real_gap}, r_gap={relative_gap}")
                if uncertainty > self.uncertainty_bound and relative_gap * math.sqrt(
                        self.gp_ucb_kappa(self.cur_itr)) > 0.9 * self.gp_ucb_kappa(1):
                    mask[_i] = True
            return _fd_arr[mask]

        # filter fidelity and find the minimizer of cost in the filtered fidelity 
        select_fidelity_start_time = time.time()
        filtered_fidelity = _filter(all_fidelity)
        if len(filtered_fidelity) == 0:
            fidelity = self.target_fidelity.copy()
        else:
            filtered_cost = self.cost(filtered_fidelity)
            selected_idx = np.argmin(filtered_cost)
            fidelity = filtered_fidelity[selected_idx:selected_idx + 1]
        select_fidelity_end_time = time.time()
        format_time(select_fidelity_end_time - select_fidelity_start_time, "Fidelity selection")
        return fidelity

    def heuristics(self):
        cur_fidelity = self.gp_regression.fidelity[-1]
        if cur_fidelity + fidelity_tol < self.target_fidelity:
            self.num_stuck_evals += 1
        if self.cur_itr % self.heuristic_window_size == 0:
            if self.num_stuck_evals > 0.75 * self.heuristic_window_size:
                self.uncertainty_bound *= self.uncertainty_bound_inc
                if self.verbosity:
                    print(
                        f"Uncertainty bound is increased by {self.uncertainty_bound_inc} due to too sparse "
                        f"evaluations on the target fidelity.")
            elif self.num_stuck_evals < 0.25 * self.heuristic_window_size:
                self.uncertainty_bound *= self.uncertainty_bound_dec
                if self.verbosity:
                    print(f"Uncertainty bound is decreased by {self.uncertainty_bound_dec} due to too dense "
                          f"evaluations on the target fidelity.")
            if self.uncertainty_bound > uncertainty_bound_max:
                self.uncertainty_bound = uncertainty_bound_max
                if self.verbosity:
                    print("Uncertainty bound is too large, and clipped to 16.")
            elif self.uncertainty_bound < uncertainty_bound_min:
                self.uncertainty_bound = uncertainty_bound_min
                if self.verbosity:
                    print("Uncertainty bound is too small, and clipped to 0.0625.")
            self.num_stuck_evals = 0

    def config_acq_func(self,
                        backend: Callable,
                        eval_mode=False):
        if eval_mode:
            self.acq_func = lambda gp_regression, x: GP_Expected(gp_regression,
                                                                 {'x': x, 'fidelity': self.target_fidelity})

            @backend(self.gp_regression.manifold)
            def opposite_acq_func(x):
                return -1.0 * self.acq_func(self.gp_regression, x)[0]
        else:
            self.acq_func = lambda gp_regression, x, t: \
                GP_UCB(gp_regression, {'x': x, 'fidelity': self.target_fidelity}, t,
                       self.gp_ucb_kappa)

            @backend(self.gp_regression.manifold)
            def opposite_acq_func(x):
                return -1.0 * self.acq_func(self.gp_regression, x, self.cur_itr)[0]

        self.acq_optim_problem = pymanopt.Problem(self.gp_regression.manifold, opposite_acq_func)

    def black_box_eval(self, x):
        return self.black_box_function(x, np.repeat(self.target_fidelity, repeats=x.shape[0], axis=0))

    def reset(self, gp_regression, uncertainty_bound=None):
        super(ManBOCA, self).reset(gp_regression)
        self.num_stuck_evals = 0
        if uncertainty_bound is not None:
            if uncertainty_bound > uncertainty_bound_max:
                warnings.warn(f"Uncertainty bound should be not greater than {uncertainty_bound_max},"
                              f"and is forced to be {uncertainty_bound_max} due to the violation.")
                uncertainty_bound = uncertainty_bound_max
            elif uncertainty_bound < uncertainty_bound_min:
                warnings.warn(f"Uncertainty bound should be no smaller than {uncertainty_bound_min},"
                              f"and is forced to be {uncertainty_bound_min} due to the violation.")
                uncertainty_bound = uncertainty_bound_min
        self.uncertainty_bound = uncertainty_bound


def module_test(data_dir,
                n_pre_train=5,
                gp_itrs=100,
                bo_budget=100,
                bo_n_explore=1,
                bo_n_warmup=5000,
                gp_refit_interval=25,
                seed=0,
                do_heuristic=True,
                heuristic_window_size=20,
                uncertainty_bound_decrease=0.9,
                lowest_fidelity_div_grid_unit=1,
                init_uncertainty_bound=0.125,
                bo_verbosity=1):
    np.random.seed(seed)
    th.random.manual_seed(seed)
    Gn = 20
    Gp = 4
    manifold = Grassmann(n=Gn, p=Gp)
    # pre_train gp_model
    obs_noise = 1e-5
    if obs_noise <= 1e-4:
        noise_activate = False
    else:
        noise_activate = True
    from ContinuousFidelityExGP import get_gp_model_test, get_black_box_test
    gp_model = get_gp_model_test(Gn * Gn, noise_activate)
    GPR = ContFdExGP(gp_model,
                     manifold,
                     verbosity=0)
    n_fixed_points = 100
    black_box = get_black_box_test(n_fixed_points, manifold, obs_noise)

    def black_box_on_Rn(x):
        x = x.reshape((Gn, Gp))
        q, r = np.linalg.qr(x)
        return -1.0 * black_box(np.expand_dims(q, axis=0), np.ones((1, 1)))

    from scipy.optimize import basinhopping
    res = basinhopping(func=black_box_on_Rn, x0=np.zeros((Gn, Gp)), niter=1)
    print(f"ymax found by basinhopping={res.fun}")

    from ManifoldBO import get_gp_ucb_kappa_test
    grid_unit_length = 0.01
    fidelity_kernel = GPR.regularized_kernel['fidelity']

    def gap(fidelity):
        target_fidelity = np.array([[1.0]])
        correlation_to_target_fidelity = fidelity_kernel(fidelity, target_fidelity)[:, 0]
        return np.sqrt(1.0 - correlation_to_target_fidelity ** 2)

    BO = ManBOCA(gp_regression=GPR,
                 black_box_function=black_box,
                 cost=lambda fd: fd[..., 0],
                 gap=gap,
                 gp_ucb_kappa=get_gp_ucb_kappa_test(manifold),
                 uncertainty_bound=init_uncertainty_bound,
                 grid_unit_length=grid_unit_length,
                 uncertainty_bound_decrease=uncertainty_bound_decrease,
                 heuristic_window_size=heuristic_window_size,
                 do_heuristic=do_heuristic,
                 lowest_fidelity=np.array([[lowest_fidelity_div_grid_unit * grid_unit_length]]),
                 verbosity=bo_verbosity
                 )
    from UpperLevelControl.ContinuousFidelityExGP import set_GP_randomly
    gp_optim_type = "LBFGS"
    gp_optim_info = {'lr': 5 * 1e-3,
                     'history_size': 200}
    set_GP_randomly(BO.gp_regression, BO.black_box_function, n_pre_train,
                    grid_unit_length, grid_unit_length * lowest_fidelity_div_grid_unit,
                    itrs=gp_itrs, optim_info=gp_optim_info, optim_type=gp_optim_type)
    BO(bo_budget,
       bo_n_explore,
       bo_n_warmup,
       acq_optimizer=pymanopt.optimizers.SteepestDescent(verbosity=0),
       backend=pytorch_backend,
       gp_refit_interval=gp_refit_interval,
       gp_itrs=gp_itrs,
       gp_optim_type=gp_optim_type,
       gp_optim_info=gp_optim_info,
       save_interval=5,
       dir=data_dir,
       suffix='test',
       )

    x_eval, y_eval = BO.evaluate(step_size=5,
                                 base_size=n_pre_train,
                                 n_explore=bo_n_explore,
                                 n_warmup=bo_n_warmup,
                                 acq_optimizer=pymanopt.optimizers.SteepestDescent(verbosity=0),
                                 backend=pytorch_backend)
    print(x_eval.shape)
    print(y_eval.shape)
    np.save(data_dir + "x_eval", x_eval)
    np.save(data_dir + "y_eval", y_eval)


if __name__ == '__main__':
    '''
       module test for multi-bayesian bayesian optimization, based on the fact that
       ContinuousFidelityExGP.py is free of bugs.
       black box function is y(x) = avgGeodesicDistance(x, fidelity * n points of n fixed points).
    '''
    data_save_dir = '../../../boca_test_toy_function_uncertainty_0.5_sw/'
    if not os.path.exists(data_save_dir):
        os.makedirs(data_save_dir)
    module_test(data_save_dir, bo_budget=200, n_pre_train=25,
                do_heuristic=True,
                heuristic_window_size=8,
                init_uncertainty_bound=0.5,
                uncertainty_bound_decrease=0.5,
                lowest_fidelity_div_grid_unit=50,
                bo_verbosity=1)
    y_eval = np.load(data_save_dir + "y_eval.npy")
    y_train = np.load(data_save_dir + "GP_train_targets_test.npy")
    import matplotlib.pyplot as plt

    plt.yticks(np.arange(-2.4, -2.2, step=0.005))
    plt.plot(y_train[25:])
    plt.savefig(data_save_dir + "train.png")
    plt.clf()
    plt.plot(y_eval)
    plt.savefig(data_save_dir + "eval.png")
    plt.show()
