import time
from UpperLevelControl.ExtrinsicGP import pytorch_train
import gpytorch as gp
import torch as th
import os
from _Utils import format_time
import numpy as np


class ExGPVecWrap:
    def __init__(self,
                 ex_gp_arr):
        self.ex_gp_arr = ex_gp_arr
        self.verbosity = 0
        for ex_gp in ex_gp_arr:
            self.verbosity = self.verbosity or ex_gp.verbosity

    def __call__(self, *args, **kwargs):
        return self.fit(*args, **kwargs)

    def __getitem__(self, idx):
        return self.ex_gp_arr[idx]

    def train_mode(self):
        for ex_gp in self.ex_gp_arr:
            ex_gp.train_mode()

    def eval_mode(self):
        for ex_gp in self.ex_gp_arr:
            ex_gp.eval_mode()

    def set_train_data(self, train_inputss, train_targetss, idxs, targets_standardizers=None):
        assert len(train_inputss) == len(train_targetss) == len(idxs)
        if targets_standardizers is None:
            targets_standardizers = [None]*len(idxs)
        else:
            assert len(targets_standardizers) == len(idxs)
        for idx, train_inputs, train_targets, targets_standardizer \
                in zip(idxs, train_inputss, train_targetss, targets_standardizers):
            self.ex_gp_arr[idx].set_train_data(train_inputs, train_targets, targets_standardizer)

    def add_train_data(self, new_train_inputs, new_train_targets, idx):
        self.ex_gp_arr[idx].add_train_data(new_train_inputs, new_train_targets)

    def fit(self,
            itrs,
            solver_type: str,
            info_solver: dict):
        mlls = []
        params_list = []
        for ex_gp in self.ex_gp_arr:
            # activate train mode
            if ex_gp.mode != 'train':
                ex_gp.train_mode()
            # construct mlls and params_list
            mlls.append(gp.mlls.ExactMarginalLogLikelihood(model=ex_gp.gp_model,
                                                           likelihood=ex_gp.gp_model.likelihood))
            params_list.append({'params': ex_gp.gp_model.parameters()})
        solver = getattr(th.optim, solver_type)(params_list, **info_solver)

        def loss():
            val = 0
            for i in range(self.size):
                # type(gp_model.train_inputs) is tuple
                train_inputs = self.ex_gp_arr[i].gp_model.train_inputs[0]
                train_targets_hat = self.ex_gp_arr[i].gp_model(train_inputs)
                train_targets = self.ex_gp_arr[i].gp_model.train_targets
                val += -1.0 * mlls[i](train_targets_hat, train_targets)
            return val

        def callback():
            if self.verbosity:
                self.log()

        print("================Fit GP's Hyper-parameters===============")
        fit_start_time = time.time()
        pytorch_train(itrs,
                      loss,
                      solver,
                      callback=callback,
                      verbosity=self.verbosity)
        fit_end_time = time.time()
        format_time(fit_end_time - fit_start_time, f"Fitting hyper-parameters of GP {itrs} iterations")
        for ex_gp in self.ex_gp_arr:
            ex_gp.fitted = True

    def log(self):
        for i in range(self.size):
            print("For the {}-th GP model of the vectorized ExGP:".format(i))
            self.ex_gp_arr[i].log()

    def evaluate(self, eval_inputs, noise_included=True):
        if not noise_included:
            f_preds = []
            for i in range(self.size):
                f_preds.append(self.ex_gp_arr[i].evaluate(eval_inputs, noise_included))
            return f_preds
        else:
            f_preds = []
            y_preds = []
            for i in range(self.size):
                f_pred, y_pred = self.ex_gp_arr.evaluate(eval_inputs, noise_included)
                f_preds.append(f_pred)
                y_preds.append(y_pred)
            return f_preds, y_preds

    def save_data(self, dir='./', suffix=''):
        subdir = dir + 'vectorized_GP_data_' + suffix + '/'
        if not os.path.exists(subdir):
            os.mkdir(subdir)
        for i in range(self.size):
            self.ex_gp_arr[i].save_data(subdir, str(i))

    def save_model(self, dir='./', suffix=''):
        models_dict = {}
        for i in range(self.size):
            models_dict[i] = self.ex_gp_arr[i].gp_model.state_dict()
        th.save(models_dict, dir + 'vectorized_GP_model_state_' + suffix)

    def reset(self):
        for ex_gp in self.ex_gp_arr:
            ex_gp.reset()

    def get_gp_parameter(self, target: str, idx):
        return self.ex_gp_arr[idx].get_gp_parameter(target)

    def get_truncated_model(self, length, target_standardizer=None, idx=0):
        return self.ex_gp_arr[idx].get_truncated_model(length, target_standardizer)

    @property
    def size(self):
        return len(self.ex_gp_arr)

    @property
    def data_size(self):
        data_sizes = np.zeros(self.size)
        for i in range(self.size):
            data_sizes[i] = self.ex_gp_arr[i].data_size
        return data_sizes

    @property
    def kernel(self):
        kernels = []
        for ex_gp in self.ex_gp_arr:
            kernels.append(ex_gp.kernel)
        return kernels

    @property
    def kernel_scale(self):
        rets = np.zeros(self.size)
        for i in range(self.size):
            rets[i] = self.ex_gp_arr[i].kernel_scale
        return rets

    @property
    def regularized_kernel(self):
        reg_kernels = []
        for ex_gp in self.ex_gp_arr:
            reg_kernels.append(ex_gp.regularized_kernel)
        return reg_kernels

    def manifold(self, idx):
        return self.ex_gp_arr[idx].manifold
