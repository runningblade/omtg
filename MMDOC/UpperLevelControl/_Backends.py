from pymanopt.autodiff.backends._pytorch import PyTorchBackend as OrigPyTorchBackend
from pymanopt.autodiff.backends._backend import Backend
from pymanopt.autodiff import backend_decorator_factory
import functools


class PyTorchBackend(OrigPyTorchBackend):
    """
    This class is developed because the backends given in pymanopt 2.0.0 is not reliable.
    Specifically, in the file pymanopt.autodiff.backends._pytorch, line 47 should be
    "return function(*map(self._from_numpy, args)).detach().numpy()" instead of
    "return function(*map(self._from_numpy, args)).numpy()"
    I inherit the erroneous class and rewrite the corresponding method
    in order to circumvent modifying the pymanopt library directly
    """

    @Backend._assert_backend_available
    def prepare_function(self, function):
        @functools.wraps(function)
        def wrapper(*args):
            return function(*map(self._from_numpy, args)).detach().numpy()
        return wrapper


pytorch_backend = backend_decorator_factory(PyTorchBackend)
