import math
import sys
sys.path.append('..')
import numpy as np
import vapory as vp
import os
# from typing import Literal
from SoftRobotEnv.ReducedSoftRobot import ReducedSoftRobotVersatile, ReducedSoftRobotWalk
from LowerLevelControl.ControlSystem import load_frames
from SoftRobotEnv.ReducedSoftRobotUtils import get_primitive_world


def add_macro_pov_entity(name: str, *args, **kwargs):
    include_stm = "#include \"" + "../Render/" + name + ".pov\""
    entity = vp.Union(include_stm, name, "(", *args, *kwargs.values(), ")")
    return entity


def states_to_pov(world,
                  states,
                  save_path,
                  ):
    for i in range(len(states)):
        world.writePov(save_path + str(i) + '.pov', states[i])


def povray_render(env,
                  states,
                  save_dir,
                  mesh_name_prefixes: list,
                  mesh_colors: list,
                  mesh_translates: list,
                  terrain,  #: Literal["Fluid", "Plane"],
                  camera_location,
                  camera_look_at,
                  camera_rotate,
                  level=0.0,
                  fps=30,
                  playback_speed=1,
                  plane_rotate=0,
                  render_fluid=False,
                  quality=11):
    assert len(mesh_name_prefixes) == len(mesh_colors) and len(mesh_colors) == len(mesh_translates)
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    os.system('rm -rf ' + save_dir + '*.pov '+save_dir+'*.png')
    if not isinstance(states[0], list):
        statess = [states]
    else:
        statess = states
    tot_time = len(statess[0])
    num_agents = len(statess)
    for states, mesh_name_prefix in zip(statess, mesh_name_prefixes):
        states_to_pov(env.world, states, save_dir+mesh_name_prefix)

    # add camera, lights, and background
    simple_objects_args = [
        ('Camera', ['location', camera_location,
                    'look_at', camera_look_at,
                    'rotate', [0, 0, camera_rotate]]
         ),
        ('LightSource',
         [[1, 1, 1.5],
          'color', [1, 1, 1],
          'area_light', [1, 0, 0], [0, 1, 0], 5, 5,
          'circular'
          ]),
        ('LightSource',
         [[-1, -2, 1.5],
          'color', [1, 1, 1],  # [255 / 255., 229 / 255., 204 / 255.],
          'area_light', [1, 0, 0], [0, 1, 0], 2, 2,
          'circular'
          ]),
        ('Background', ['color', [1.0, 1.0, 1.0]]),
    ]
    simple_objects = []
    for simple_object_args in simple_objects_args:
        simple_objects.append(getattr(vp, simple_object_args[0])(*simple_object_args[1]))
    mesh_textures = [vp.Texture(vp.Pigment("color", "rgb", mesh_color)) for mesh_color in mesh_colors]
    time = 0
    while time < tot_time:
        # add mesh
        mesh_povs = []
        for i in range(num_agents):
            # add mesh
            mesh_name = mesh_name_prefixes[i] + str(time)
           # mesh_name = "LowControl249"
            # add arrow
            mesh_center = statess[i][time].getT().reshape(-1, )
            arrow_pov = add_macro_pov_entity("Arrow",
                                             arrow_start=mesh_center,
                                             arrow_end=mesh_center + 1.2 * env.target_dir,
                                             head_length=0.2,
                                             head_radius=0.05,
                                             shaft_radius=0.02)
            mesh_pov = vp.Union("#include \"" + save_dir + mesh_name + ".pov\"", arrow_pov, mesh_textures[i],
                                "translate", mesh_translates[i])
            mesh_povs.append(mesh_pov)
        # add terrain
        if terrain == 'Fluid' and render_fluid:
            terrain_pov = add_macro_pov_entity(terrain,
                                               fluid_level=level,
                                               wind_direction=0.0,
                                               wind_speed=5.0,
                                               wave_clock=time * 0.05,
                                               wave_scale=25,
                                               wave_z_scale=0.15,
                                               wave_width=1.5,
                                               checker_color1=[0, 0, 0],
                                               checker_color2=[0.8 * 153 / 255., 0.8 * 204 / 255.,
                                                               0.8 * 255 / 255.],
                                               checker_scale=0.5,
                                               )
        elif terrain == "Fluid":
            terrain_pov = vp.Plane('z',
                                   level-1.0,
                                   vp.Texture(
                                       vp.Pigment('checker',
                                                  'color', [0.8, 0.8, 0.8 * 204 / 255.],
                                                  'color', [1.0 * 0 / 255., 0.8 * 204 / 255., 1.0 * 204 / 255.],
                                                  'scale', 0.5,
                                                  'rotate', [0, 0, plane_rotate]),
                                       vp.Finish('ambient', 0.1, 'diffuse', 0.8)
                                   )
                                   )
        else:
            terrain_pov = vp.Plane('z',
                                   level,
                                   vp.Texture(
                                       vp.Pigment('checker',
                                                  'color', [0.8, 0.8, 0.8 * 204 / 255.],
                                                  'color', [1.0 * 0 / 255., 0.8 * 204 / 255., 1.0 * 204 / 255.],
                                                  'scale', 0.5,
                                                  'rotate', [0, 0, plane_rotate]),
                                       vp.Finish('ambient', 0.1, 'diffuse', 0.8)
                                   )
                                   )
        objects = simple_objects + [terrain_pov] + mesh_povs
        scene = vp.Scene(objects[0], objects[1:])
        with open(save_dir+'__##temp##__.pov', 'w') as f:
            f.write(scene.__str__())
        scene.render(save_dir + 'temp_pic' + str(int(time / playback_speed)) + '.png', 720, 960, quality=quality)
        print('The {}-th frame is rendered'.format(time))
        time += playback_speed
    os.system(
        'ffmpeg -y -r ' + str(
            fps) + ' -i ' + save_dir + 'temp_pic%d.png -c:v libx264 -pix_fmt yuv420p ' + save_dir + 'animation.mov')


if __name__ == '__main__':
    data_dir = '../../../omtg-MMDOC-data/test_walker_high_wide_vr_MPPI/'
    world = get_primitive_world(mesh_name="walker_high_wide_10.mesh",
                                basis_num=20,
                                elastic_energy="Corotated",
                                lame_first_coef=1e4,
                                lame_second_coef=1e4,
                                mesh_type=1,
                               )
    env_ = ReducedSoftRobotWalk(
            world=world,
            agents=[None],
            ctrl_dim=3,
            target_phi=0,
            level=-0.55,
            size=100.0,
        )
    _, states = load_frames(env_.world, data_dir)
    terrain_ = 'Plane'
    rot = 45
    mesh_name_prefixes_ = ['basis_simple', 'basis_best', 'basis_init']
    mesh_colors_ = [[0.8, 0.8, 0.8], [255 / 255., 153 / 255., 51 / 255.], [255 / 255.0, 102 / 255.0, 102 / 255.0]]
    mesh_translates_ = [[3.5 * math.sin(rot * math.pi / 180), -3.5 * math.cos(rot * math.pi / 180), 0],
                        [1.5 * math.sin(rot * math.pi / 180), -1.5 * math.cos(rot * math.pi / 180), 0],
                        [0, 0, 0],
                        [-1.5 * math.sin(rot * math.pi / 180), 1.5 * math.cos(rot * math.pi / 180), 0]]
    povray_render(env_,
                  states,
                  data_dir,
                  ['LowControl'],
                  [[0.8, 0.8, 0.8]],
                  [[0, 0, 0]],
                  terrain_,
                  global_rotate=0,
                  level=env_.terrain[0],
                  fps=25,
                  quality=4)
    """
    povray_render(env_,
                  frames_X_,
                  data_dir,
                  mesh_name_prefixes_,
                  mesh_colors_,
                  mesh_translates_,
                  terrain_,
                  global_rotate=rot)
    """
