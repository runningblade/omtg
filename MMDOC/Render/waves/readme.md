- waves/gamma.inc and waves/waves.inc are copied directly from
  [The waves include file](https://www.imagico.de/pov/water/water_inc.php)
  used in the [imagico.de tutorial on realistic water with POV-ray](https://www.imagico.de/pov/water/index.php)
- The waves function is called in the following way:
  ```povray
  function {
    Waves(Wind_Direction, Wind_Speed, Time, Scale)
  }
  ```
  where the parameter "time" controls the "phase" of waves. 
- For more information of the waves function, please refer to 
the links given above.