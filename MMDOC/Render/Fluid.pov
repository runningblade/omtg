# macro Fluid(fluid_level,
              wind_direction,
              wind_speed,
              wave_clock,
              wave_scale,
              wave_z_scale,
              wave_width,
              checker_color1,
              checker_color2,
              checker_scale)
    #include "functions.inc"
    #include "../Render/waves/waves.inc"
    plane {
    z
    fluid_level-1.0
    texture {
        pigment {
            checker
            color checker_color1
            color checker_color2
            scale checker_scale
        }
         finish {
          ambient 0.1
          diffuse 0.6
    }
    }
    /*photons {
    target
    reflection off
    refraction on
    }*/
    }
    plane {
    z
    fluid_level
    material {
      texture {
        pigment {
          color rgbf <1, 1, 1, 1>
        }
        finish {
          ambient 0.0
          diffuse 0.0

          reflection {
            0.0, 1.0
            fresnel on
          }

          specular 0.4
          roughness 0.003
        }
        normal {
          function {
            z - Waves(wind_direction, wind_speed, wave_clock, wave_scale)*wave_z_scale
          }
          scale wave_width
        }
      }
      interior {
        ior 1.3
      }
    }
    /*photons {
    target
    reflection off
    refraction on
    }*/
    }
#end