# macro Arrow(arrow_start,
              arrow_end,
              head_length,
              head_radius,
              shaft_radius,
              )
 #local arrow_vector = arrow_end - arrow_start;
 #local shaft_end = arrow_end - vnormalize (arrow_vector) * head_length;
  cylinder {arrow_start, shaft_end, shaft_radius}
  cone {shaft_end, head_radius, arrow_end, 0}
#end