light_source {
<1,1,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
5
5
circular 
}
light_source {
<-1,-2,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
2
2
circular 
}
background {
color
<1.0,1.0,1.0> 
}
plane {
z
( -0.15 )
texture {
pigment {
checker
color
<0.8,0.8,0.64>
color
<0.0,0.64,0.8>
scale
0.5
rotate
<0,0,0> 
}
finish {
ambient
0.1
diffuse
0.8 
} 
} 
}
union {
#include "../../../omtg_mmdoc_data/beam_walk_bo_best/LowControl249.pov"
union {
#include "../Render/Arrow.pov"
Arrow
(
<7.188530992394162,-0.029787649323890622,-0.040705137058874165>
<8.388530992394161,-0.029787649323890622,-0.04070513705887409>
0.2
0.05
0.02
) 
}
texture {
pigment {
color
rgb
<0.8,0.8,0.8> 
} 
}
translate
<0,0,0> 
}
camera {
location
<4,0,8>
look_at
<4,0,0>
rotate
<0,0,0> 
}
global_settings{

}