light_source {
<1,1,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
5
5
circular 
}
light_source {
<-1,-2,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
2
2
circular 
}
background {
color
<1.0,1.0,1.0> 
}
plane {
z
( -0.15 )
texture {
pigment {
checker
color
<0.8,0.8,0.64>
color
<0.0,0.64,0.8>
scale
0.5
rotate
<0,0,0> 
}
finish {
ambient
0.1
diffuse
0.8 
} 
} 
}
union {
#include "../../../omtg_mmdoc_data/beam_walk_boca_best_2/LowControl249.pov"
union {
#include "../Render/Arrow.pov"
Arrow
(
<6.744390394180828,0.33657340983627904,-0.016288572317325264>
<7.944390394180828,0.33657340983627904,-0.01628857231732519>
0.2
0.05
0.02
) 
}
texture {
pigment {
color
rgb
<0.8,0.8,0.8> 
} 
}
translate
<0,0,0> 
}
camera {
location
<4,0,8>
look_at
<4,0,0>
rotate
<0,0,0> 
}
global_settings{

}