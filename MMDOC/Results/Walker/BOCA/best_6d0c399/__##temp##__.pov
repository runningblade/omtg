light_source {
<1,1,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
5
5
circular 
}
light_source {
<-1,-2,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
2
2
circular 
}
background {
color
<1.0,1.0,1.0> 
}
plane {
z
( -0.55 )
texture {
pigment {
checker
color
<0.8,0.8,0.64>
color
<0.0,0.64,0.8>
scale
0.5
rotate
<0,0,0> 
}
finish {
ambient
0.1
diffuse
0.8 
} 
} 
}
union {
#include "../../../omtg_mmdoc_data/walker_boca_best/LowControl249.pov"
union {
#include "../Render/Arrow.pov"
Arrow
(
<7.311078421068863,0.4133796621278732,0.002394970059447291>
<8.511078421068863,0.4133796621278732,0.0023949700594473643>
0.2
0.05
0.02
) 
}
texture {
pigment {
color
rgb
<0.8,0.8,0.8> 
} 
}
translate
<0,0,0> 
}
camera {
location
<5,-8.5,4.25>
look_at
<5,0,0>
rotate
<0,0,0> 
}
global_settings{

}