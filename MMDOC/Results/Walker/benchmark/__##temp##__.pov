light_source {
<1,1,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
5
5
circular 
}
light_source {
<-1,-2,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
2
2
circular 
}
background {
color
<1.0,1.0,1.0> 
}
plane {
z
( -0.55 )
texture {
pigment {
checker
color
<0.8,0.8,0.64>
color
<0.0,0.64,0.8>
scale
0.5
rotate
<0,0,0> 
}
finish {
ambient
0.1
diffuse
0.8 
} 
} 
}
union {
#include "../../../omtg_mmdoc_data/walker_bo_benchmark/LowControl249.pov"
union {
#include "../Render/Arrow.pov"
Arrow
(
<5.2360474428867105,0.10993231636850843,-0.021406485158512685>
<6.436047442886711,0.10993231636850843,-0.021406485158512612>
0.2
0.05
0.02
) 
}
texture {
pigment {
color
rgb
<0.8,0.8,0.8> 
} 
}
translate
<0,0,0> 
}
camera {
location
<5,-8.5,4.25>
look_at
<5,0,0>
rotate
<0,0,0> 
}
global_settings{

}