light_source {
<1,1,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
5
5
circular 
}
light_source {
<-1,-2,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
2
2
circular 
}
background {
color
<1.0,1.0,1.0> 
}
plane {
z
( -0.15 )
texture {
pigment {
checker
color
<0.8,0.8,0.64>
color
<0.0,0.64,0.8>
scale
0.5
rotate
<0,0,0> 
}
finish {
ambient
0.1
diffuse
0.8 
} 
} 
}
union {
#include "../../../boca_cross_max/LowControl249.pov"
union {
#include "../Render/Arrow.pov"
Arrow
(
<7.396077257073209,-0.17484560473738867,-0.01681659977408459>
<8.596077257073208,-0.17484560473738867,-0.016816599774084517>
0.2
0.05
0.02
) 
}
texture {
pigment {
color
rgb
<0.8,0.8,0.8> 
} 
}
translate
<0,0,0> 
}
camera {
location
<4,0,8>
look_at
<4,0,0>
rotate
<0,0,0> 
}
global_settings{

}