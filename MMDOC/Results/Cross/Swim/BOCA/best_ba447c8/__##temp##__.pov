light_source {
<1,1,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
5
5
circular 
}
light_source {
<-1,-2,1.5>
color
<1,1,1>
area_light
<1,0,0>
<0,1,0>
2
2
circular 
}
background {
color
<1.0,1.0,1.0> 
}
union {
#include "../Render/Fluid.pov"
Fluid
(
0.0
0.0
5
12.450000000000001
25
0.15
1.5
<0,0,0>
<0.48000000000000004,0.64,0.8>
0.5
) 
}
union {
#include "../../../result_buffer/cross_swim_boca_best/LowControl249.pov"
union {
#include "../Render/Arrow.pov"
Arrow
(
<10.972211836187693,-0.29200211355342315,0.062092093665792616>
<12.172211836187692,-0.29200211355342315,0.06209209366579269>
0.2
0.05
0.02
) 
}
texture {
pigment {
color
rgb
<0.8,0.8,0.8> 
} 
}
translate
<0,0,0> 
}
camera {
location
<4.5,0,9>
look_at
<4.5,0,0>
rotate
<0,0,0> 
}
global_settings{

}