#include <Deformable/FEMMesh.h>
#include <Deformable/FEMOctreeMesh.h>
#include <Deformable/FEMGradientInfo.h>
#include <Deformable/FEMReducedSystem.h>
#include <Deformable/DeformableVisualizer.h>
#include <TinyVisualizer/FirstPersonCameraManipulator.h>
#include <TinyVisualizer/CaptureGIFPlugin.h>
#include <TinyVisualizer/Bullet3DShape.h>
#include <TinyVisualizer/Camera3D.h>

using namespace PHYSICSMOTION;
using namespace DRAWER;
typedef FLOAT T;

#define TYPE 0
int main(int argc,char** argv) {
  mpfr_float::default_precision(1000);
  std::shared_ptr<FEMReducedSystem<T>> sys;
  if(exists("sysRBFluid.dat")) {
    sys.reset(new FEMReducedSystem<T>);
    sys->SerializableBase::readStr("sysRBFluid.dat");
  } else {
    std::shared_ptr<FEMMesh<T>> body;
    if(TYPE==0) {
      body.reset(new FEMMesh<T>("beam.mesh",true));
    } else if(TYPE==1) {
      body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
    } else if(TYPE==2) {
      body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
      FEMOctreeMesh<T> bodyHOct(*body,0);
      body=bodyHOct.getMesh();
    } else return 0;

    sys.reset(new FEMReducedSystem<T>(body));
    sys->addCorotatedElasticEnergy(10000,10000);
    sys->addGravitationalEnergy(9.81f);
    sys->addFluidEnergy(0,2,9.81f,1,0);
    sys->buildBasis(20);
    sys->SerializableBase::writeStr("sysRBFluid.dat");
  }

  //sys->addSelfCC(10000);
  std::vector<FEMGradientInfo<T>> vss;
  typename FEMGradientInfo<T>::Vec x;
  x.setZero((sys->getBasis().cols()+6)*2);
  x.template segment<3>((sys->getBasis().cols()+6)+sys->getBasis().cols())-=FEMSystem<T>::Vec3T::UnitX()*8;
  vss.push_back(FEMGradientInfo<T>(*sys,x));
  std::shared_ptr<FEMPDController<T>> ctrl(new FEMPDController<T>());
  while(vss.size()<400) {
    bool succ;
    if(vss.size()==200)
      sys->setFluidEnergy(1,2,9.81f,1,0);
    std::cout << "Solving frame " << vss.size()-1 << std::endl;
    FEMGradientInfo<T> ret=sys->step(vss[(int)vss.size()-1],ctrl,&succ,FEMSystem<T>::SP);
    if(!succ) {
      std::cout << "Failed!" << std::endl;
      break;
    }
    vss.push_back(ret);
  }

  //drawer
  int frmId=0;
  Drawer drawer(argc,argv);
  drawer.addPlugin(std::shared_ptr<Plugin>(new CaptureGIFPlugin(GLFW_KEY_1,"record.gif",drawer.FPS())));
  drawer.addShape(visualizeFEMWaterLevel(0,5));
  std::shared_ptr<Shape> s=visualizeFEMLowDimensionalMeshSurface(sys,Eigen::Matrix<GLfloat,3,1>(.8,.8,.8));
  //std::shared_ptr<Shape> s=visualizeFEMMeshCubature(sys,Eigen::Matrix<GLfloat,3,1>(.8,.8,.8));
  drawer.addShape(s);
  drawer.setFrameFunc([&](std::shared_ptr<SceneNode>&) {
    updateSurface(s,vss[frmId]);
    //updateCubature(s,vss[frmId],sys);
    frmId=(frmId+1)%(int)vss.size();
  });

#define USE_LIGHT
#ifdef USE_LIGHT
  drawer.addLightSystem();
  drawer.getLight()->lightSz(10);
  drawer.getLight()->addLight(Eigen::Matrix<GLfloat,3,1>(0,0,1),
                              Eigen::Matrix<GLfloat,3,1>(.3,.3,.3),
                              Eigen::Matrix<GLfloat,3,1>(.8,.8,.8),
                              Eigen::Matrix<GLfloat,3,1>(.5,.5,.5));
#endif
  drawer.addCamera3D(90,Eigen::Matrix<GLfloat,3,1>(0,0,1));
  drawer.getCamera3D()->setManipulator(std::shared_ptr<CameraManipulator>(new FirstPersonCameraManipulator(drawer.getCamera3D())));
  drawer.mainLoop();
  return 0;
}
