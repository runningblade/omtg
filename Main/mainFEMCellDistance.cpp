#include <Deformable/FEMMesh.h>
#include <Deformable/FEMOctreeMesh.h>
#include <Deformable/DeformableVisualizer.h>
#include <TinyVisualizer/Bullet3DShape.h>

using namespace PHYSICSMOTION;
using namespace DRAWER;
typedef FLOAT T;

#define TYPE 2
void tetrahedronDistance(int argc,char** argv) {
  Tetrahedron<T>::Vec vss;
  std::shared_ptr<Tetrahedron<T>> tet=Tetrahedron<T>::createRegular(vss);
  std::cout << "Mass=" << tet->massMatrix()/tet->volume0() << " sum=" << (tet->massMatrix()/tet->volume0()).sum() << std::endl;

  Drawer drawer(argc,argv);
  std::shared_ptr<MeshShape> cell(new MeshShape);
  cell->addVertex(tet->V(0)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  cell->addVertex(tet->V(1)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  cell->addVertex(tet->V(2)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  cell->addVertex(tet->V(3)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  cell->addIndex(Eigen::Matrix<int,3,1>(0,1,3));
  cell->addIndex(Eigen::Matrix<int,3,1>(0,3,2));
  cell->addIndex(Eigen::Matrix<int,3,1>(0,2,1));
  cell->addIndex(Eigen::Matrix<int,3,1>(1,2,3));
  cell->setMode(GL_TRIANGLES);
  cell->computeNormals();
  drawer.addShape(cell);

  int off=0;
  std::shared_ptr<MeshShape> lines(new MeshShape);
  for(GLfloat x=-1; x<=2; x+=0.3)
    for(GLfloat y=-1; y<=2; y+=0.3)
      for(GLfloat z=-1; z<=2; z+=0.3) {
        T sqrDistance;
        Tetrahedron<T>::Vec3T pt,cp,b;
        pt=Tetrahedron<T>::Vec3T(x,y,z);
        lines->addVertex(Eigen::Matrix<GLfloat,3,1>(x,y,z));
        tet->calcPointDist(FEMMesh<T>::mapCV(vss),pt,sqrDistance,cp,b);
        lines->addVertex(Eigen::Matrix<T,3,1>(cp[0],cp[1],cp[2]).template cast<GLfloat>());
        lines->addIndex(Eigen::Matrix<int,2,1>(off+0,off+1));
        off+=2;
      }
  lines->setLineWidth(2);
  lines->setMode(GL_LINES);
  lines->setColor(GL_LINES,0,0,0);
  drawer.addShape(lines);
  drawer.mainLoop();
}
void hexahedronDistance(int argc,char** argv) {
  T sideLen=0.7f;
  Hexahedron<T>::Vec vss;
  std::shared_ptr<Hexahedron<T>> hex=Hexahedron<T>::createRegular(sideLen,vss);
  std::cout << "Mass=" << hex->massMatrix()/hex->volume0() << " sum=" << (hex->massMatrix()/hex->volume0()).sum() << std::endl;

  Drawer drawer(argc,argv);
  std::shared_ptr<MeshShape> cell(new MeshShape);
  cell->addVertex(hex->V(0)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  cell->addVertex(hex->V(1)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  cell->addVertex(hex->V(2)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  cell->addVertex(hex->V(3)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  cell->addVertex(hex->V(4)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  cell->addVertex(hex->V(5)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  cell->addVertex(hex->V(6)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  cell->addVertex(hex->V(7)->operator()(FEMMesh<T>::mapCV(vss)).template cast<GLfloat>());
  //X
  cell->addIndex(Eigen::Matrix<int,3,1>(0,4,6));
  cell->addIndex(Eigen::Matrix<int,3,1>(0,6,2));
  cell->addIndex(Eigen::Matrix<int,3,1>(1,3,7));
  cell->addIndex(Eigen::Matrix<int,3,1>(1,7,5));
  //Y
  cell->addIndex(Eigen::Matrix<int,3,1>(0,1,5));
  cell->addIndex(Eigen::Matrix<int,3,1>(0,5,4));
  cell->addIndex(Eigen::Matrix<int,3,1>(2,6,7));
  cell->addIndex(Eigen::Matrix<int,3,1>(2,7,3));
  //Z
  cell->addIndex(Eigen::Matrix<int,3,1>(0,2,3));
  cell->addIndex(Eigen::Matrix<int,3,1>(0,3,1));
  cell->addIndex(Eigen::Matrix<int,3,1>(4,5,7));
  cell->addIndex(Eigen::Matrix<int,3,1>(4,7,6));
  cell->setMode(GL_TRIANGLES);
  cell->computeNormals();
  drawer.addShape(cell);

  int off=0;
  std::shared_ptr<MeshShape> lines(new MeshShape);
  for(GLfloat x=-1; x<=2; x+=0.3)
    for(GLfloat y=-1; y<=2; y+=0.3)
      for(GLfloat z=-1; z<=2; z+=0.3) {
        T sqrDistance;
        Hexahedron<T>::Vec3T pt,cp,b;
        pt=Hexahedron<T>::Vec3T(x,y,z);
        lines->addVertex(Eigen::Matrix<GLfloat,3,1>(x,y,z));
        hex->calcPointDist(FEMMesh<T>::mapCV(vss),pt,sqrDistance,cp,b);
        lines->addVertex(Eigen::Matrix<T,3,1>(cp[0],cp[1],cp[2]).template cast<GLfloat>());
        lines->addIndex(Eigen::Matrix<int,2,1>(off+0,off+1));
        off+=2;
      }
  lines->setLineWidth(2);
  lines->setMode(GL_LINES);
  lines->setColor(GL_LINES,0,0,0);
  drawer.addShape(lines);
  drawer.mainLoop();
}
int main(int argc,char** argv) {
  //tetrahedronDistance(argc,argv);
  hexahedronDistance(argc,argv);
  return 0;
}
