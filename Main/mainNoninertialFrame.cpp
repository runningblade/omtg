#include <Deformable/FEMNoninertialFrame.h>
#include <Utils/ParallelVector.h>

using namespace PHYSICSMOTION;
typedef FLOAT T;
DECL_MAT_VEC_MAP_TYPES_T
DECL_MAP_FUNCS
typedef Eigen::Triplet<T,int> STrip;
typedef ParallelVector<STrip> STrips;
typedef Eigen::SparseMatrix<T,0,int> SMatT;

#define N 27
#define NB 10
int main(int argc,char** argv) {
  mpfr_float::default_precision(1000);
  MatT m=MatT::Random(N,N);
  MatT B=MatT::Random(N,NB);
  Vec pos0=Vec::Random(N);
  Vec f=Vec::Random(N);
  SMatT mass=(m*m.transpose()).sparseView();
  FEMNoninertialFrame<T>(B,pos0,mass).debug();
  FEMQuadraticFST<T>(B,pos0,mass).debug();
  FEMLinearFST<T>(B,pos0,f).debug();
  return 0;
}
