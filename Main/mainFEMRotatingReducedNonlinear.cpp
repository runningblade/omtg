#include <Deformable/FEMMesh.h>
#include <Deformable/FEMGradientInfo.h>
#include <Deformable/FEMReducedSystem.h>
#include <Deformable/FEMOctreeMesh.h>
#include <Deformable/DeformableVisualizer.h>
#include <TinyVisualizer/FirstPersonCameraManipulator.h>
#include <TinyVisualizer/CaptureGIFPlugin.h>
#include <TinyVisualizer/Bullet3DShape.h>
#include <TinyVisualizer/Camera3D.h>

using namespace PHYSICSMOTION;
using namespace DRAWER;
typedef FLOAT T;

#define TYPE 0
int main(int argc,char** argv) {
  mpfr_float::default_precision(1000);
  std::shared_ptr<FEMReducedSystem<T>> sys;
  if(exists("sysRRotatingNonlinear.dat")) {
    sys.reset(new FEMReducedSystem<T>);
    sys->SerializableBase::readStr("sysRRotatingNonlinear.dat");
  } else {
    std::shared_ptr<FEMMesh<T>> body;
    if(TYPE==0) {
      //body.reset(new FEMMesh<T>("torus.obj_tet.mesh",true));
      body.reset(new FEMMesh<T>(true,"Cross.abq",true));
    } else if(TYPE==1) {
      body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
    } else if(TYPE==2) {
      body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
      FEMOctreeMesh<T> bodyHOct(*body,0);
      body=bodyHOct.getMesh();
    } else return 0;

    sys.reset(new FEMReducedSystem<T>(body));
    sys->addCorotatedElasticEnergy(1000,1000);
    sys->buildBasis(50);
    sys->SerializableBase::writeStr("sysRRotatingNonlinear.dat");
  }

  sys->dampingCoef(5);
  std::vector<FEMGradientInfo<T>> vss;
  typename FEMGradientInfo<T>::Vec x;
  x.setZero((sys->getBasis().cols()+6)*2);
  x[x.size()-2]=10;
  vss.push_back(FEMGradientInfo<T>(*sys,x));
  while(vss.size()<1000) {
    bool succ;
    std::cout << "Solving frame " << vss.size()-1 << std::endl;
    FEMGradientInfo<T> ret=sys->step(vss[(int)vss.size()-1],NULL,&succ,FEMSystem<T>::SP);
    if(!succ) {
      std::cout << "Failed!" << std::endl;
      break;
    }
    vss.push_back(ret);
  }

  //drawer
  int frmId=0;
  Drawer drawer(argc,argv);
  drawer.addPlugin(std::shared_ptr<Plugin>(new CaptureGIFPlugin(GLFW_KEY_1,"record.gif",drawer.FPS())));
  std::shared_ptr<Shape> s=visualizeFEMLowDimensionalMeshSurface(sys,Eigen::Matrix<GLfloat,3,1>(.8,.8,.8));
  drawer.addShape(s);
  drawer.setFrameFunc([&](std::shared_ptr<SceneNode>&) {
    updateSurface(s,vss[frmId]);
    frmId=(frmId+1)%(int)vss.size();
  });

#define USE_LIGHT
#ifdef USE_LIGHT
  drawer.addLightSystem();
  drawer.getLight()->lightSz(10);
  drawer.getLight()->addLight(Eigen::Matrix<GLfloat,3,1>(0,0,1.5),
                              Eigen::Matrix<GLfloat,3,1>(.3,.3,.3),
                              Eigen::Matrix<GLfloat,3,1>(.8,.8,.8),
                              Eigen::Matrix<GLfloat,3,1>(.5,.5,.5));
#endif
  drawer.addCamera3D(90,Eigen::Matrix<GLfloat,3,1>(0,0,1));
  drawer.getCamera3D()->setManipulator(std::shared_ptr<CameraManipulator>(new FirstPersonCameraManipulator(drawer.getCamera3D())));
  drawer.mainLoop();
  return 0;
}
