#include <Deformable/FEMMesh.h>
#include <Deformable/FEMSystem.h>
#include <Deformable/FEMOctreeMesh.h>
#include <Deformable/DeformableVisualizer.h>
#include <Environment/EnvironmentVisualizer.h>
#include <TinyVisualizer/FirstPersonCameraManipulator.h>
#include <TinyVisualizer/CaptureGIFPlugin.h>
#include <TinyVisualizer/Bullet3DShape.h>
#include <TinyVisualizer/MakeTexture.h>
#include <TinyVisualizer/MeshShape.h>
#include <TinyVisualizer/Camera3D.h>

using namespace PHYSICSMOTION;
using namespace DRAWER;
typedef FLOAT T;

#define TYPE 0
int main(int argc,char** argv) {
  std::shared_ptr<FEMMesh<T>> body;
  mpfr_float::default_precision(1000);
  if(TYPE==0) {
    body.reset(new FEMMesh<T>("10.mesh",true));
    body->writeObj("10.obj",NULL,NULL);
  } else if(TYPE==1) {
    body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
  } else if(TYPE==2) {
    body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
    FEMOctreeMesh<T> bodyHOct(*body,0);
    body=bodyHOct.getMesh();
  } else return 0;

  FEMSystem<T> sys(body);
  sys.addNonHookeanElasticEnergy(1000,1000);
  sys.addGravitationalEnergy(9.81f);
  sys.addSelfCC(1e5);

  //create env
  {
    sys.setEnv(std::shared_ptr<EnvironmentExact<T>>(new EnvironmentExact<T>));
    for(int y=-1; y>=-10; y-=1)
      for(int x=-4; x<=4; x+=2) {
        double xx;
        if(y%2==0)
          xx=x+1;
        else xx=x;
        sys.getEnvExact()->addCapsule(Eigen::Matrix<double,3,1>(-1,xx,y),Eigen::Matrix<double,3,1>(1,xx,y),0.3,3);
      }
    sys.getEnvExact()->assemble();
  }

  FEMSystem<T>::Vec p;
  std::vector<FEMSystem<T>::Vec,Eigen::aligned_allocator<FEMSystem<T>::Vec>> vss;
  vss.push_back(body->pos0());
  vss.push_back(body->pos0());
  while(vss.size()<1000) {
    std::cout << "Solving frame " << vss.size()-1 << std::endl;
    bool succ=sys.step(vss[(int)vss.size()-2],vss[(int)vss.size()-1],p,NULL,FEMSystem<T>::OPT);
    if(!succ) {
      std::cout << "Failed!" << std::endl;
      break;
    }
    vss.push_back(p);
  }

  //drawer
  int frmId=0;
  Drawer drawer(argc,argv);
  drawer.addPlugin(std::shared_ptr<Plugin>(new CaptureGIFPlugin(GLFW_KEY_1,"record.gif",drawer.FPS())));
  std::shared_ptr<Shape> s=visualizeFEMMeshSurface(body,Eigen::Matrix<GLfloat,3,1>(.8,.8,.8));
  drawer.addShape(s);
  std::shared_ptr<Shape> envS=visualizeEnvironment(sys.getEnv(),Eigen::Matrix<GLfloat,2,1>(0.01f,0.01f));
  envS->setColor(GL_TRIANGLES,.5,.5,.5);
  drawer.addShape(envS);
  drawer.setFrameFunc([&](std::shared_ptr<SceneNode>&) {
    updateSurface(s,body,vss[frmId]);
    frmId=(frmId+1)%(int)vss.size();
  });

#define USE_LIGHT
#ifdef USE_LIGHT
  drawer.addLightSystem();
  drawer.getLight()->lightSz(10);
  drawer.getLight()->addLight(Eigen::Matrix<GLfloat,3,1>(0,0,1.5),
                              Eigen::Matrix<GLfloat,3,1>(.3,.3,.3),
                              Eigen::Matrix<GLfloat,3,1>(.8,.8,.8),
                              Eigen::Matrix<GLfloat,3,1>(.5,.5,.5));
#endif
  drawer.addCamera3D(90,Eigen::Matrix<GLfloat,3,1>(0,0,1));
  drawer.getCamera3D()->setManipulator(std::shared_ptr<CameraManipulator>(new FirstPersonCameraManipulator(drawer.getCamera3D())));
  drawer.mainLoop();
  return 0;
}
