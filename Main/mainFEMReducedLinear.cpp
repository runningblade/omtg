#include <Deformable/FEMReducedSystem.h>
#include <Deformable/FEMGradientInfo.h>
#include <Deformable/FEMOctreeMesh.h>
#include <TinyVisualizer/Drawer.h>
#include <Utils/Utils.h>

using namespace PHYSICSMOTION;
using namespace DRAWER;
typedef FLOAT T;

#define TYPE 1
int main(int argc,char** argv) {
  mpfr_float::default_precision(2000);
  FEMReducedSystem<T> sys;
  if(exists("sysRL.dat")) {
    sys.SerializableBase::readStr("sysRL.dat");
  } else {
    std::shared_ptr<FEMMesh<T>> body;
    if(TYPE==0)
      body.reset(new FEMMesh<T>("torus.obj_tet.mesh",true));
    else if(TYPE==1)
      body.reset(new FEMMesh<T>("sphere.obj",.2f,true));
    else if(TYPE==2) {
      body.reset(new FEMMesh<T>("sphere.obj",.2f,true));
      FEMOctreeMesh<T> bodyHOct(*body,0);
      body=bodyHOct.getMesh();
    } else return 0;

#ifdef IS_FIXED
    body->fixDOF([&](const FEMMesh<T>::Vec3T& pos)->bool {
      return pos[0]<0;
    });
#endif
    sys=FEMReducedSystem<T>(body);
    sys.addPointConstraint(body->pos0(),FEMSystem<T>::Vec3T::Zero(),1000);
    sys.addLinearElasticEnergy(10000,10000);
    sys.addGravitationalEnergy(9.81f);
    //sys.debugDStiffness(10,0);
    sys.buildBasis(10);
    sys.SerializableBase::writeStr("sysRL.dat");
  }
  sys.debugAssemble();
  return 0;
}
