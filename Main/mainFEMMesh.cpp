#include <Deformable/FEMMesh.h>
#include <Deformable/FEMOctreeMesh.h>
#include <Deformable/DeformableVisualizer.h>
#include <TinyVisualizer/Bullet3DShape.h>

using namespace PHYSICSMOTION;
using namespace DRAWER;
typedef FLOAT T;

#define TYPE 0
int main(int argc,char** argv) {
  GLfloat dx;
  std::shared_ptr<FEMMesh<T>> body;
  if(TYPE==0) {
    dx=10;
    body.reset(new FEMMesh<T>("torus.obj_tet.mesh",true));
  } else if(TYPE==1) {
    dx=2;
    body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
  } else if(TYPE==2) {
    dx=2;
    body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
    FEMOctreeMesh<T> bodyHOct(*body,0);
    body=bodyHOct.getMesh();
  } else return 0;

  Drawer drawer(argc,argv);
  {
    std::shared_ptr<Shape> s;
    if(TYPE==0)
      s=visualizeFEMMeshTetrahedron(body);
    else s=visualizeFEMMeshHexahedron(body);
    std::shared_ptr<Bullet3DShape> sTrans(new Bullet3DShape);
    sTrans->addShape(s);
    drawer.addShape(sTrans);
  }
  {
    std::shared_ptr<Shape> s=visualizeFEMMeshSurface(body);
    std::shared_ptr<Bullet3DShape> sTrans(new Bullet3DShape);
    sTrans->setLocalTranslate(Eigen::Matrix<GLfloat,3,1>(dx,0,0));
    sTrans->addShape(s);
    drawer.addShape(sTrans);
  }
  drawer.mainLoop();
  return 0;
}
