#include <Deformable/FEMMesh.h>
#include <Deformable/FEMGradientInfo.h>
#include <Deformable/FEMReducedSystem.h>
#include <Deformable/FEMOctreeMesh.h>
#include <Deformable/DeformableVisualizer.h>
#include <TinyVisualizer/FirstPersonCameraManipulator.h>
#include <TinyVisualizer/CaptureGIFPlugin.h>
#include <TinyVisualizer/Bullet3DShape.h>
#include <TinyVisualizer/Camera3D.h>

using namespace PHYSICSMOTION;
using namespace DRAWER;
typedef FLOAT T;

#define TYPE 0
int main(int argc,char** argv) {
  mpfr_float::default_precision(1000);
  std::shared_ptr<FEMMesh<T>> body;
  if(TYPE==0) {
    body.reset(new FEMMesh<T>(true,"Cross.abq",true));
  } else if(TYPE==1) {
    body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
  } else if(TYPE==2) {
    body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
    FEMOctreeMesh<T> bodyHOct(*body,0);
    body=bodyHOct.getMesh();
  } else return 0;
  body->fixDOF([&](const FEMMesh<T>::Vec3T& pos) {
    return pos[0]<-0.4f;
  });

  FEMSystem<T> sys(body);
  //sys.addLinearElasticEnergy(10000,10000);
  //sys.addStVKElasticEnergy(10000,10000);
  sys.addCorotatedElasticEnergy(10000,10000);
  //sys.addNonHookeanElasticEnergy(10000,10000);
  sys.addGravitationalEnergy(9.81f);

  FEMSystem<T>::Vec p;
  std::vector<FEMSystem<T>::Vec,Eigen::aligned_allocator<FEMSystem<T>::Vec>> vss;
  vss.push_back(body->pos0());
  vss.push_back(body->pos0());
  while(vss.size()<1000) {
    std::cout << "Solving frame " << vss.size()-1 << std::endl;
    bool succ=sys.step(vss[(int)vss.size()-2],vss[(int)vss.size()-1],p,NULL,FEMSystem<T>::LCP);
    if(!succ) {
      std::cout << "Failed!" << std::endl;
      break;
    }
    vss.push_back(p);
  }

  //drawer
  int frmId=0;
  Drawer drawer(argc,argv);
  drawer.addPlugin(std::shared_ptr<Plugin>(new CaptureGIFPlugin(GLFW_KEY_1,"record.gif",drawer.FPS())));
  std::shared_ptr<Shape> s=visualizeFEMMeshSurface(body,Eigen::Matrix<GLfloat,3,1>(.8,.8,.8));
  drawer.addShape(s);
  drawer.setFrameFunc([&](std::shared_ptr<SceneNode>&) {
    updateSurface(s,body,vss[frmId]);
    frmId=(frmId+1)%(int)vss.size();
  });

#define USE_LIGHT
#ifdef USE_LIGHT
  drawer.addLightSystem();
  drawer.getLight()->lightSz(10);
  drawer.getLight()->addLight(Eigen::Matrix<GLfloat,3,1>(0,0,1.5),
                              Eigen::Matrix<GLfloat,3,1>(.3,.3,.3),
                              Eigen::Matrix<GLfloat,3,1>(.8,.8,.8),
                              Eigen::Matrix<GLfloat,3,1>(.5,.5,.5));
#endif
  drawer.addCamera3D(90,Eigen::Matrix<GLfloat,3,1>(0,0,1));
  drawer.getCamera3D()->setManipulator(std::shared_ptr<CameraManipulator>(new FirstPersonCameraManipulator(drawer.getCamera3D())));
  drawer.mainLoop();
  return 0;
}
