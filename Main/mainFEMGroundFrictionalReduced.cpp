#include <Deformable/FEMMesh.h>
#include <Deformable/FEMOctreeMesh.h>
#include <Deformable/FEMGradientInfo.h>
#include <Deformable/FEMReducedSystem.h>
#include <Deformable/DeformableVisualizer.h>
#include <Environment/EnvironmentVisualizer.h>
#include <TinyVisualizer/FirstPersonCameraManipulator.h>
#include <TinyVisualizer/CaptureGIFPlugin.h>
#include <TinyVisualizer/Bullet3DShape.h>
#include <TinyVisualizer/MakeTexture.h>
#include <TinyVisualizer/Camera3D.h>

using namespace PHYSICSMOTION;
using namespace DRAWER;
typedef FLOAT T;

#define TYPE 0
int main(int argc,char** argv) {
  mpfr_float::default_precision(1000);
  std::shared_ptr<FEMReducedSystem<T>> sys;
  if(exists("sysRFrictional.dat")) {
    sys.reset(new FEMReducedSystem<T>);
    sys->SerializableBase::readStr("sysRFrictional.dat");
  } else {
    std::shared_ptr<FEMMesh<T>> body;
    if(TYPE==0) {
      body.reset(new FEMMesh<T>("beam.mesh",true));
    } else if(TYPE==1) {
      body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
    } else if(TYPE==2) {
      body.reset(new FEMMesh<T>("sphere.obj",.1f,true));
      FEMOctreeMesh<T> bodyHOct(*body,0);
      body=bodyHOct.getMesh();
    } else return 0;

    sys.reset(new FEMReducedSystem<T>(body));
    sys->setEnv(std::shared_ptr<EnvironmentHeight<T>>(new EnvironmentHeight<T>));
    sys->getEnv()->createHills(-20,20,-20,20,[&](double x,double)->double {
      return -2+x*0.1f;
    },.1);
    sys->addCorotatedElasticEnergy(10000,10000);
    sys->addGravitationalEnergy(9.81f);
    sys->maxContact(10);
    sys->mu(0.7f);
    sys->buildBasis(50);
    sys->SerializableBase::writeStr("sysRFrictional.dat");
  }

  recreate("frames");
  sys->dampingCoef(5);
  std::vector<FEMGradientInfo<T>> vss;
  typename FEMGradientInfo<T>::Vec x;
  x.setZero((sys->getBasis().cols()+6)*2);
  x[x.size()/2-3]=M_PI/4;
  vss.push_back(FEMGradientInfo<T>(*sys,x));
  while(vss.size()<1000) {
    bool succ;
    std::cout << "Solving frame " << vss.size()-1 << std::endl;
    FEMGradientInfo<T> ret=sys->step(vss[(int)vss.size()-1],NULL,&succ,FEMSystem<T>::SP);
    sys->writeObj("frames/frm"+std::to_string(vss.size())+".obj",ret);
    sys->writePov("frames/frm"+std::to_string(vss.size())+".pov",ret);
    if(!succ) {
      std::cout << "Failed!" << std::endl;
      break;
    } else {
      std::shared_ptr<FEMReducedPDController<T>> ctrl(new FEMReducedPDController<T>);
      ctrl->resetPDReduced(FEMSystem<T>::Vec::Random(5),1.0,FEMSystem<T>::Vec::Random(5),1.0,FEMSystem<T>::MatT::Random(10,5));
      sys->debugDDxDtDu(vss[(int)vss.size()-1],ctrl,FEMSystem<T>::LCP);
    }
    vss.push_back(ret);
  }

  //drawer
  int frmId=0;
  Drawer drawer(argc,argv);
  drawer.addPlugin(std::shared_ptr<Plugin>(new CaptureGIFPlugin(GLFW_KEY_1,"record.gif",drawer.FPS())));
  std::shared_ptr<Shape> s=visualizeFEMLowDimensionalMeshSurface(sys,Eigen::Matrix<GLfloat,3,1>(.8,.8,.8));
  drawer.addShape(s);
  std::shared_ptr<Shape> envS=visualizeEnvironment(sys->getEnv(),Eigen::Matrix<GLfloat,2,1>(0.01f,0.01f));
  envS->setColorAmbient(GL_TRIANGLES,1,1,1);
  envS->setTexture(drawGrid(10,0.01f,0.03f,Eigen::Matrix<GLfloat,3,1>(1,1,1),Eigen::Matrix<GLfloat,3,1>(135/255.,54/255.,0/255.)));
  drawer.addShape(envS);
  drawer.setFrameFunc([&](std::shared_ptr<SceneNode>&) {
    updateSurface(s,vss[frmId]);
    frmId=(frmId+1)%(int)vss.size();
  });

#define USE_LIGHT
#ifdef USE_LIGHT
  drawer.addLightSystem();
  drawer.getLight()->lightSz(10);
  drawer.getLight()->addLight(Eigen::Matrix<GLfloat,3,1>(0,0,0.5),
                              Eigen::Matrix<GLfloat,3,1>(.3,.3,.3),
                              Eigen::Matrix<GLfloat,3,1>(.8,.8,.8),
                              Eigen::Matrix<GLfloat,3,1>(.5,.5,.5));
#endif
  drawer.addCamera3D(90,Eigen::Matrix<GLfloat,3,1>(0,0,1));
  drawer.getCamera3D()->setManipulator(std::shared_ptr<CameraManipulator>(new FirstPersonCameraManipulator(drawer.getCamera3D())));
  drawer.mainLoop();
  return 0;
}
